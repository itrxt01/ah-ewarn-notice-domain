package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidCategoryException extends Exception {
	private static final long serialVersionUID = 7084390195468263127L;

	public InvalidCategoryException(String msg) {
		super(msg);
	}
}
