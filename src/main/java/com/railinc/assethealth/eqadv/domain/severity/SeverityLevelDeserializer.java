package com.railinc.assethealth.eqadv.domain.severity;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class SeverityLevelDeserializer extends StdDeserializer<SeverityLevel> {
	private static final long serialVersionUID = 5690703706738320767L;

	public SeverityLevelDeserializer() {
		this(null);
	}

	public SeverityLevelDeserializer(Class<SeverityLevel> vc) {
		super(vc);
	}

	@Override
	public SeverityLevel deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(jp, new TypeReference<SeverityLevel>() {});
	}
}
