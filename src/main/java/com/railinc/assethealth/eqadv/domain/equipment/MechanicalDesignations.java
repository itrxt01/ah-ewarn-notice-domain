package com.railinc.assethealth.eqadv.domain.equipment;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.notice.NoticeItemSet;

@JsonDeserialize(using=MechanicalDesignationsDeserializer.class)
public class MechanicalDesignations implements NoticeItemSet<MechanicalDesignation>, Serializable {
	private static final long serialVersionUID = 1281959581930824623L;
	
	private Set<MechanicalDesignation>	equipmentDesignations = new TreeSet<>();
	
	public MechanicalDesignations(){		
	}
	
	public MechanicalDesignations(MechanicalDesignations designations){
		this.equipmentDesignations.addAll(designations.equipmentDesignations);
	}
	
	public MechanicalDesignations(Collection <MechanicalDesignation> designations) {
		this.equipmentDesignations.addAll(designations);
	}

	@JsonProperty("mechanicalDesignations")
	public Set<MechanicalDesignation> getMechanicalDesignations(){
		return this.equipmentDesignations;
	}

	public boolean hasMechanicalDesignations() {
		return CollectionUtils.isNotEmpty(equipmentDesignations);
	}
	
	// If there are no mechanical designations specified, the default
	// is that the notice applies to all types.
	public boolean containsMechanicalDesignation(MechanicalDesignation designation) {
		if(CollectionUtils.isNotEmpty(equipmentDesignations)) {
			return equipmentDesignations.contains(designation);
		}
		return true;
	}
	
	public boolean containsMechanicalDesignation(String designation) throws InvalidMechanicalDesignationException {
		MechanicalDesignation temp = new MechanicalDesignation(designation);
		return this.containsMechanicalDesignation(temp);
	}	

	public void addMechanicalDesignations(Collection<MechanicalDesignation> designations) {
		this.equipmentDesignations.addAll(designations);
	}
	
	public void addMechanicalDesignation(MechanicalDesignation desgnation) {
		equipmentDesignations.add(desgnation);
	}
	
	public void addMechanicalDesignation(String designation) throws InvalidMechanicalDesignationException {
		MechanicalDesignation mech = new MechanicalDesignation(designation);
		this.equipmentDesignations.add(mech);
	}
	
	public String getListAsDelimitedString() {
		return getDelimitedString(equipmentDesignations, ", ", false);
	}
}
