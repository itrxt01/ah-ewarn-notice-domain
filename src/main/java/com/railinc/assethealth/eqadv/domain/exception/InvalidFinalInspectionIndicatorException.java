package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidFinalInspectionIndicatorException extends EWDeserializationException {		// NOSONAR - Approve 6 parents for this exception.
	private static final long serialVersionUID = -2162714967650372997L;

	public InvalidFinalInspectionIndicatorException(String msg) {
		super(msg);
	}

}
