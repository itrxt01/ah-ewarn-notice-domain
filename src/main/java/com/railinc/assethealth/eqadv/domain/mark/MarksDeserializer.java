package com.railinc.assethealth.eqadv.domain.mark;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class MarksDeserializer extends StdDeserializer<Marks> { 
	private static final long serialVersionUID = 5505830105556603883L;

	public MarksDeserializer() { 
		this(null); 
	} 

	public MarksDeserializer(Class<Marks> vc) { 
		super(vc); 
	}

	@Override
	public Marks deserialize(JsonParser jp, DeserializationContext ctxt) 
			throws IOException{
		JsonNode node = jp.getCodec().readTree(jp);
		
		String marksInput;
		if(node.isObject()) {
			marksInput = node.get("marksString").asText();
		} else {
			marksInput =  node.asText();	
		}

		String[] marksArray = marksInput.split("\\s*(\\s|,)\\s*");
		List<Mark> marksList = new ArrayList<>();

		for (String currentMark : marksArray) {
			marksList.add(MarkFactory.newMark(currentMark.trim()));
		}
		return new Marks(marksList);
	}
}
