package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidPeriodicTimeUnitException extends EWDeserializationException {			// NOSONAR - Approve 6 parents for this exception.
	private static final long serialVersionUID = -4538247517463879408L;

	public InvalidPeriodicTimeUnitException(String msg){
		super(msg);
	}
}
