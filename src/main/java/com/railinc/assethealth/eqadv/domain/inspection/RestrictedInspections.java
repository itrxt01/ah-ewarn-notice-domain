package com.railinc.assethealth.eqadv.domain.inspection;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.railinc.assethealth.eqadv.domain.EWDomainWithID;
import com.railinc.assethealth.eqadv.domain.mark.Mark;
import com.railinc.assethealth.eqadv.domain.mark.Marks;

/*
 * This represents a collection of inspections that may ONLY be reported
 * by specific Marks.
 */
public class RestrictedInspections extends EWDomainWithID implements Serializable {

	private static final long serialVersionUID = -3473256733016620903L;

	private final Map<String, Marks> inspectionCodesMap = new HashMap<>();
	private final List<RestrictedInspection> inspections = new ArrayList<>();
	
	public RestrictedInspections(List<RestrictedInspection> restrictions) {
		inspections.addAll(restrictions);
		updateMap(inspections);
	}

	private void updateMap(List<RestrictedInspection> restrictions) {
		for(RestrictedInspection restriction : restrictions) {
			if(!inspectionCodesMap.containsKey(restriction.getInspectionCode().getCode())) {
				inspectionCodesMap.put(restriction.getInspectionCode().getCode(), new Marks());
			}
			inspectionCodesMap.get(restriction.getInspectionCode().getCode()).addMark(restriction.getMark());
		}
	}

	public List<RestrictedInspection> getRestrictedInspections(){
		return Collections.unmodifiableList(inspections);
	}
	
	public Map<String, List<InspectionCode>> getRestrictedInspectionCodes(){
		
		Map<String, List<InspectionCode>> restrictedCodesMap = new HashMap<>();
		inspections
				//.stream()
				.forEach(i -> {
					if(!restrictedCodesMap.containsKey(i.getMarkString())) {
						restrictedCodesMap.put(i.getMarkString(), new ArrayList<>());
					} 
					restrictedCodesMap.get(i.getMarkString()).add(i.getInspectionCode());
				});
		
		return restrictedCodesMap;
	}
	
	public boolean isPermittedToReportInspection(InspectionCode inspection, Mark mark) {
		Marks marks = inspectionCodesMap.get(inspection.getCode());

		// Is there a list of marks to which reporting is restricted? If not assume permitted. 
		return (marks == null || (marks.isMarkListed(mark)));
	}	

	public boolean isRestricted(InspectionCode inspection) {
		return null != this.inspections.stream()
				.filter(r -> r.getCode().equals(inspection.getCode()))
				.findFirst()
				.orElse(null);
	}	

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false);
    }


	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false);
	}	

}

