package com.railinc.assethealth.eqadv.domain.mark;

/**
 * Marks must be no longer than 4 characters in length and match the pattern:
 * 
 * AAAA - road mark
 * 
 * This is just an initial validation.  Final validation is against the list of marks retrieved from Umler.
 * 
 * @author ron taylor
 *
 */
public class RoadMark extends Mark {
	private static final long serialVersionUID = 351087469485661728L;
	static final String ROAD_MARK_REGEX_PATTERN = "^([a-z, A-Z]{2,4})$";
	private static final String ROAD_MARK_ERROR_MSG_FORMAT = "The value[%s] is not a valid road mark!";
	private static final boolean IS_ROAD_MARK_EQUIPMENT_OWNER = true;
	
	RoadMark(String mark) {
		super(mark);
	}
	
	RoadMark(String mark, Mark parent) {
		super(mark, parent);
	}
	
	RoadMark(Mark mark) {
		super(mark);
	}
	
	@Override
	protected String getRegex() {
		return ROAD_MARK_REGEX_PATTERN;
	}

	@Override
	protected String getErrorMessageFormat() {
		return ROAD_MARK_ERROR_MSG_FORMAT;
	}

	@Override
	protected boolean getIsEquipmentOwnerMark() {
		return IS_ROAD_MARK_EQUIPMENT_OWNER;
	}
}
