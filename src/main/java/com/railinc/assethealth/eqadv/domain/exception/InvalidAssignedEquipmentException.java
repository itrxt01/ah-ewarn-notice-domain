package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidAssignedEquipmentException extends Exception {
	private static final long serialVersionUID = 6557000052132314483L;

	public InvalidAssignedEquipmentException(String msg) {
		super(msg);
	}

}
