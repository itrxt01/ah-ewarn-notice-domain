package com.railinc.assethealth.eqadv.domain.pdf;

import org.apache.commons.lang3.StringUtils;

import com.railinc.assethealth.eqadv.domain.exception.UnsupportedAttachmentTypeException;

// Have found that different mime types occur for the same file from various browsers.  Put the primary type to use on download first in the array.
public enum ATTACHMENT_TYPE {
	PDF("portable document format", "pdf", new String[] {"application/pdf"})
	, JPG("JPEG Image", "jpg", new String[] {"image/jpeg", "image/pjpeg", "image/jpg"})
	, PNG("PNG Image", "png", new String[] {"image/png", "image/x-png"})
	, TXT("text", "txt", new String[] {"text/plain"})
	, DOC("MS Office Word Document", "doc", new String[] {"application/msword"})
	, DOCX("OpenXML Word Document", "docx", new String[] {"application/vnd.openxmlformats-officedocument.wordprocessingml.document"})
	, PPT("MS Office Powerpoint", "ppt", new String[] {"application/vnd.ms-powerpoint"})
	, PPTX("OpenXML Presentation", "pptx", new String[] {"application/vnd.openxmlformats-officedocument.presentationml.presentation"})
	, XLS("MS Office Excel", "xls", new String[] {"application/vnd.ms-excel"})
	, XLSX("OpenXML Spreadsheet", "xlsx", new String[] {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
	, BMP("Bitmap", "bmp", new String[] {"image/bmp"})
	, CSV("Comma separated values", "csv", new String[] {"text/csv, application/csv, application/excel, application/vnd.ms-excel"})
	, UNKNOWN("Unsupported attachment type", "", new String[] {});
	
	public final String[] mimeTypes;  // NOSONAR - immutable enum
	public final String extension;	  // NOSONAR - immutable enum
	public final String type;		  // NOSONAR - immutable enum
	
	ATTACHMENT_TYPE(String t, String ext, String[] mimeTypes){
		this.type = t;
		this.extension = ext;
		this.mimeTypes = mimeTypes;
	}

	public static ATTACHMENT_TYPE forExtension(String s){
		if(StringUtils.isBlank(s)){
			return UNKNOWN;
		}
		for(ATTACHMENT_TYPE t : ATTACHMENT_TYPE.values()){
			if(t.extension.equalsIgnoreCase(s)){
				return t;
			}
		}
		return UNKNOWN;
	}
	
	public static ATTACHMENT_TYPE forMimeType(String s){
		if(StringUtils.isBlank(s)){
			return UNKNOWN;
		}
		for(ATTACHMENT_TYPE t : ATTACHMENT_TYPE.values()){
			for(String mimeType : t.mimeTypes){
				if(mimeType.equalsIgnoreCase(s)){
					return t;
				}
			}
		}
		return UNKNOWN;
	}

	public String getMimeType() throws UnsupportedAttachmentTypeException{
		if(UNKNOWN == this) {
			throw new UnsupportedAttachmentTypeException("");
		}
		// first mime type is always the primary type
		return this.mimeTypes[0];
	}
	
	
	public String[] getMimeTypes() throws UnsupportedAttachmentTypeException{
		if(UNKNOWN == this) {
			throw new UnsupportedAttachmentTypeException("");
		}
		return this.mimeTypes;
	}
	
}
