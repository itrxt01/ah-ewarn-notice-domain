package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidContentTypeException extends Exception {
	private static final long serialVersionUID = -5697816744074262099L;

	public InvalidContentTypeException(String message) {
        super(message);
    }

}
