package com.railinc.assethealth.eqadv.domain.inspection;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.railinc.assethealth.eqadv.domain.EWCode;

/**
 * These represent the inspection codes.
 * @author ron taylor
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonPropertyOrder({ "code", "description", "isFinal", "isActive", "isPeriodic" })
public final class InspectionCode extends EWCode implements Comparable<InspectionCode>, Serializable {
	private static final long serialVersionUID = -4139543801711990034L;
	
    private static final Set<String> BACKOUT_INSPECTION_CODES = new HashSet<>(Arrays.asList("MF", "MP"));
    private static final Set<String> ESCALATION_INSPECTION_CODES = new HashSet<>(Arrays.asList("MY"));
    private static final Set<String> EQUIPMENT_INCORRECTLY_ASSIGNED_INSPECTION_CODES = new HashSet<>(Arrays.asList("MN"));
	
	private final INSPECTION_TYPE type;
	private final boolean isActive;
	private final boolean isPeriodic;

	public InspectionCode(INSPECTION_TYPE type, String inspectionCode, String desc, boolean isActive, boolean isPeriodic) {
		super(inspectionCode, desc);
		this.type = type;
		this.isActive = isActive;
		this.isPeriodic = isPeriodic;
	}

	public InspectionCode(INSPECTION_TYPE type, String code) {
		super(code);
		this.type = type;
		this.code = code;
		this.isPeriodic = false;
		this.isActive = true;
	}

	public boolean isFinal() {
		return type.isFinal;
	}
	
	public boolean isPeriodic() {
		return this.isPeriodic;
	}
	
	public boolean isBackout() {
		return BACKOUT_INSPECTION_CODES.contains(code);
	}

	public boolean isEquipmentIncorrectlyAssigned() {
		return EQUIPMENT_INCORRECTLY_ASSIGNED_INSPECTION_CODES.contains(code);
	}
	
	public boolean isEscalationActivityCode() {
		return ESCALATION_INSPECTION_CODES.contains(code);
	}

	public INSPECTION_TYPE getInspectionType() {
		return type;
	}

	public boolean isActive() {
		return isActive;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, "desc");
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this) return true;  
		if(obj == null) return false; 	
		if(obj.getClass() == this.getClass()){ 
			final InspectionCode that = (InspectionCode)obj;
			return new EqualsBuilder()
					.append(this.code, that.code)
					.append(this.isActive, that.isActive)
					.append(this.isPeriodic, that.isPeriodic)
					.isEquals();
		} 
		return false;
	}
	
	public int compareTo(InspectionCode o) {
		if (o == null)
			return -1;

		return new CompareToBuilder().append(this.code, o.code).toComparison();
	}
}
