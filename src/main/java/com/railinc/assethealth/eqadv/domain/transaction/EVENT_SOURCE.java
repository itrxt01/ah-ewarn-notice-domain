package com.railinc.assethealth.eqadv.domain.transaction;

public enum EVENT_SOURCE {
    UI,
    WEBSERVICE,
    BATCH,
    MESSAGE
}
