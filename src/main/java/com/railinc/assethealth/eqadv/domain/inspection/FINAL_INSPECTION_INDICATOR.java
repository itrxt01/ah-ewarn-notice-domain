package com.railinc.assethealth.eqadv.domain.inspection;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.railinc.assethealth.eqadv.domain.exception.InvalidFinalInspectionIndicatorException;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonIgnoreProperties({ "isRetrieveFromDatabase" })
@JsonRootName("FinalInspectionIndicator")
@JsonDeserialize(using=FinalInspectionIndicatorDeserializer.class)
public enum FINAL_INSPECTION_INDICATOR {
	OPEN("O", false, "Open", "Any mark with access can report activity."),
	SPECIFIED("S", true, "Specified", "Only specified list of marks can report activity."),
	INTERNAL("I", true, "Internal System", "Only specified list of internal RailInc systems can report activity.");

	public final String code;
	public final String value;
	public final boolean isRetrieveFromDatabase;	
	public final String description;

	private static final int EXACT_INDICATOR_LENGTH = 1;
	private static final String MSG_FORMAT = "The value [%s] is not a valid Final Inspection Indicator!";

	FINAL_INSPECTION_INDICATOR(String ind, boolean getFromDB, String desc, String tooltip){
		this.code = ind;
		this.isRetrieveFromDatabase = getFromDB;
		this.value = desc;
		this.description = tooltip;
	}

	public static FINAL_INSPECTION_INDICATOR forIndicator(String ind) throws InvalidFinalInspectionIndicatorException {
		if(StringUtils.isBlank(ind)) {
			throw new InvalidFinalInspectionIndicatorException(String.format(MSG_FORMAT, ind));
		}

		String trimmed = ind.trim().toUpperCase();
		if(EXACT_INDICATOR_LENGTH != trimmed.length()) {
			throw new InvalidFinalInspectionIndicatorException(String.format(MSG_FORMAT, ind));
		}
		
		for(FINAL_INSPECTION_INDICATOR indicator : FINAL_INSPECTION_INDICATOR.values()) {
			if (StringUtils.equals(indicator.code, trimmed)) {
				return indicator;
			}
		}

		throw new InvalidFinalInspectionIndicatorException(String.format(MSG_FORMAT, ind));
	}	
}
