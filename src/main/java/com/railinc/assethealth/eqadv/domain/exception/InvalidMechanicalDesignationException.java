package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidMechanicalDesignationException extends EWDeserializationException {		// NOSONAR - Approve 6 parents for this exception.
	private static final long serialVersionUID = -2378276296183626988L;

	public InvalidMechanicalDesignationException(String msg) {
		super(msg);
	}

}
