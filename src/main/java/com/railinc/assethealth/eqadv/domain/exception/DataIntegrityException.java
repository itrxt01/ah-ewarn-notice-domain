package com.railinc.assethealth.eqadv.domain.exception;

public class DataIntegrityException extends RuntimeException {
	private static final long serialVersionUID = 2957321585400187192L;

	public DataIntegrityException(String msg) {
		super(msg);
	}

	public DataIntegrityException(Exception e, String msg) {
		super(msg, e);
	}
}
