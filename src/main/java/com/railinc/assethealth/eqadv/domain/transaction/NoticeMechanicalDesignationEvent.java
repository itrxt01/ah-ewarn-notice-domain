package com.railinc.assethealth.eqadv.domain.transaction;

public class NoticeMechanicalDesignationEvent extends NoticeEvent {

    private static final long serialVersionUID = 8330734931544120122L;

    private String mechanicalDesignation;

    public String getMechanicalDesignation() {
        return mechanicalDesignation;
    }

    public void setMechanicalDesignation(String mechanicalDesignation) {
        this.mechanicalDesignation = mechanicalDesignation;
    }
}
