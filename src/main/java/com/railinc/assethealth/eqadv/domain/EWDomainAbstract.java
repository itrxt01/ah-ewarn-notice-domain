package com.railinc.assethealth.eqadv.domain;


import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author ron taylor
 *
 */
public abstract class EWDomainAbstract implements Serializable {
	private static final long serialVersionUID = -5335600884373049696L;
	
	@JsonInclude(Include.NON_NULL)
	protected String 	createUsername;
	
	@JsonInclude(Include.NON_NULL)
	protected String	createCompany;

	@JsonIgnore
	protected Date 		createdTimestamp;
	
	@JsonInclude(Include.NON_NULL)
	protected String 	modifyUsername;

	@JsonInclude(Include.NON_NULL)
	protected String 	modifyCompany;

	@JsonIgnore
	protected Date		modifiedTimestamp;
	
	protected EWDomainAbstract() {}
	
	protected EWDomainAbstract(EWDomainAbstract obj){
		if(null != obj) {
			this.createUsername = obj.createUsername;
			this.createCompany = obj.createCompany;
			this.createdTimestamp = obj.createdTimestamp;
			this.modifyUsername = obj.modifyUsername;
			this.modifyCompany = obj.modifyCompany;
			this.modifiedTimestamp = obj.modifiedTimestamp;			
		}
	}
	
	protected static String leftJustifyString(String s, int width) {
		return StringUtils.rightPad(s, width, " ");
	}

	protected static String rightJustifyString(String s, int width) {
		return StringUtils.leftPad(s, width, " ");
	}

	protected static String rightJustifyNumber(long number, int width) {
		return StringUtils.leftPad(numberAsString(number), width, " ");
	}
	
	protected static String numberAsString(long number) {
		String format = "%d";
		return String.format(format, number);
	}
	
	protected static String padNumberWithLeadingZeros(long number, int len) {
		String format = "%0" + len + "d";
		return String.format(format, number);
	}
	
	protected static boolean isValidDateRange(Date date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
		return null != date && !date.after(new Date()) && date.after(sdf.parse("2000-01-01"));
	}
	
	public String getCreateUsername() {
		return createUsername;
	}

	public void setCreateUsername(String createUsername) {
		this.createUsername = createUsername;
	}

	public String getCreateCompany() {
		return createCompany;
	}

	public void setCreateCompany(String createCompany) {
		this.createCompany = createCompany;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getModifyUsername() {
		return modifyUsername;
	}

	public void setModifyUsername(String modifyUsername) {
		this.modifyUsername = modifyUsername;
	}

	public String getModifyCompany() {
		return modifyCompany;
	}

	public void setModifyCompany(String modifyCompany) {
		this.modifyCompany = modifyCompany;
	}

	public Date getModifiedTimestamp() {
		return modifiedTimestamp;
	}

	public void setModifiedTimestamp(Date modifiedTimestamp) {
		this.modifiedTimestamp = modifiedTimestamp;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
	
    public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false, EWDomainAbstract.class);
    }

    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass());
    }
}
