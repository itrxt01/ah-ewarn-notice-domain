package com.railinc.assethealth.eqadv.domain;


import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author ron taylor
 *
 */
public abstract class EWCode extends EWDomainAbstract implements Serializable {
	private static final long serialVersionUID = -8909740217719332734L;

	@JsonInclude(Include.ALWAYS)
	protected String	code;
	@JsonInclude(Include.NON_NULL)
	protected String 	description = null;
	
	protected EWCode(String code) {
		this.code = code;
	}

	protected EWCode(String code, String desc) {
		this.code = code;
		this.description = desc;
	}
	
	protected EWCode(EWCode obj){
		super(obj);
		this.code = obj.code;
		this.description = obj.description;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE, false);
	}	
	
	@Override
    public int hashCode() {
          return HashCodeBuilder.reflectionHashCode(this);
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false, super.getClass());
    }
}
