package com.railinc.assethealth.eqadv.domain.transaction;

import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;

public abstract class NoticeEvent extends EWDomainAbstract {
 	private static final long serialVersionUID = 8546305750402626587L;
	private Long eventId;
    private Long noticeId;

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }
}
