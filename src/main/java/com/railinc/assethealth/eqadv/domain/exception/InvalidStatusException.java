package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidStatusException extends EWDeserializationException {			// NOSONAR - Approve 6 parents for this exception.
	private static final long serialVersionUID = 5617688572601758612L;

	public InvalidStatusException(String msg) {
		super(msg);
	}
}
