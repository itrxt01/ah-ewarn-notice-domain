package com.railinc.assethealth.eqadv.domain;


import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author ron taylor
 *
 */
public abstract class EWDomainWithID extends EWDomainAbstract implements Serializable {
	private static final long serialVersionUID = -7429488475235435343L;
	
	protected Long id = null;
	
	protected EWDomainWithID() {}
	
	protected EWDomainWithID(EWDomainWithID obj){
		super(obj);
		if(null != obj) {
			this.id = obj.id;			
		}
	}
	
	public void setTrackChanges(boolean track) {
		this.setTrackChanges(track);
	}
	
	@JsonInclude(Include.NON_NULL)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE, false);
	}	
	
	@Override
    public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false, EWDomainAbstract.class);
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false, EWDomainAbstract.class);
    }
}
