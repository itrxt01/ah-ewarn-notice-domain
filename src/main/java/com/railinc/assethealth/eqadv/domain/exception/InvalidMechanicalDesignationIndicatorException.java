package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidMechanicalDesignationIndicatorException extends EWDeserializationException {		// NOSONAR - Approve 6 parents for this exception.
	private static final long serialVersionUID = -3376280359831395386L;

	public InvalidMechanicalDesignationIndicatorException(String msg) {
		super(msg);
	}

}
