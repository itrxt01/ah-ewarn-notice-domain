package com.railinc.assethealth.eqadv.domain.transaction;

public class NoticeAssignmentReporterEvent extends NoticeEvent {

    private static final long serialVersionUID = -7191429775443496738L;

    private String mark;

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
