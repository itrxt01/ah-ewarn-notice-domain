package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidEquipmentStatusException extends Exception {
	private static final long serialVersionUID = 7426599156166258252L;

	public InvalidEquipmentStatusException(String msg) {
		super(msg);
	}
}
