package com.railinc.assethealth.eqadv.domain.inspection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class NoticeInspectionCodesDeserializer extends StdDeserializer<NoticeInspectionCodes> { 
	private static final long serialVersionUID = -2424359032005804195L;

	public NoticeInspectionCodesDeserializer() { 
		this(null); 
	} 

	public NoticeInspectionCodesDeserializer(Class<NoticeInspectionCodes> vc) { 
		super(vc); 
	}

	@Override
	public NoticeInspectionCodes deserialize(JsonParser jp, DeserializationContext ctxt) 
			throws IOException{
		JsonNode node = jp.getCodec().readTree(jp);
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>();
		
		for(JsonNode n : node) {
			if(StringUtils.isNotBlank(n.asText().trim().toUpperCase())) {
				NoticeInspectionCode nic = new NoticeInspectionCode();
				InspectionCode inspectionCode = new InspectionCode(INSPECTION_TYPE.FINAL, n.asText().trim().toUpperCase());
				nic.setInspectionCode(inspectionCode);
				inspectionCodes.add( nic );
			}
		}
		return new NoticeInspectionCodes(inspectionCodes);
	}
}
