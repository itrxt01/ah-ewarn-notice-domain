package com.railinc.assethealth.eqadv.domain.exception;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.railinc.assethealth.eqadv.domain.errors.ErrorDetails;
import com.railinc.assethealth.eqadv.domain.errors.ErrorMessage;
import com.railinc.assethealth.eqadv.domain.errors.SubError;

public class EWDeserializationException extends JsonProcessingException {

	private static final long serialVersionUID = -9067281632782142340L;
	private final transient List<SubError> errors = new ArrayList<>();
	
	public EWDeserializationException(String msg) {
		super(msg);
	}

	public EWDeserializationException(Exception e, String msg) {
		super(msg, e);
	}

	public EWDeserializationException(ErrorDetails errorDetails) {
    	super(errorDetails.getMessage());
    	for(SubError error : errorDetails.getSubErrors()) {
    		errors.add(new ErrorMessage(error.toString()));
    	}
    }
    
    public void addSubError(String msg) {
    	errors.add(new ErrorMessage(msg));
    }

    public List<SubError> getErrors() {
        return errors;
    }
}
