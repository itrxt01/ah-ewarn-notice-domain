package com.railinc.assethealth.eqadv.domain.errors;

public class ErrorMessage implements SubError {

    private String message;

    public ErrorMessage(String message) {
        this.message = message;
    }

    public ErrorMessage() {}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
