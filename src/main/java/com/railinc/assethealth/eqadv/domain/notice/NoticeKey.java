package com.railinc.assethealth.eqadv.domain.notice;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.exception.InvalidStatusException;
import com.railinc.assethealth.eqadv.domain.status.NOTICE_STATUS;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;

@JsonPropertyOrder({"id", "category", "number", "supplementNumber", "status"})
public class NoticeKey {
	
	@JsonInclude(Include.ALWAYS)
	private Long id; 
	
	private Category category;	
	
	@JsonInclude(Include.ALWAYS)
	private Integer number;	
	
	@JsonInclude(Include.ALWAYS)
	private Integer supplementNumber;
	
	@JsonInclude(Include.ALWAYS)
	private NOTICE_STATUS status;
	
	public NoticeKey() {}
	
	public NoticeKey(Long id, Integer number, Integer supplement, NOTICE_STATUS status) {
		this.id = id;
		this.number = number;
		this.supplementNumber = supplement;
		this.status = status;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonIgnore
	public Category getCategory() {
		return category;
	}

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("category")
	public String getCategoryCode() {
		return Optional.ofNullable(category).map(Category::getCode).orElse(null);
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Integer getSupplementNumber() {
		return supplementNumber;
	}
	public void setSupplementNumber(Integer supplementNumber) {
		this.supplementNumber = supplementNumber;
	}
	
	@JsonProperty("status")
	public String getStatus() {
		return status.statusName;
	}

	@JsonProperty("status")
	public void setStatus(String status) throws InvalidStatusException {
		this.status = NOTICE_STATUS.forCode(status);
	}
	
	@JsonIgnore
	public String getJson() throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(this);	
	}
	
}
