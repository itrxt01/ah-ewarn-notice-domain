package com.railinc.assethealth.eqadv.domain.inspection;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.railinc.assethealth.eqadv.domain.exception.EWDeserializationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidFinalInspectionIndicatorException;

public class FinalInspectionIndicatorDeserializer extends StdDeserializer<FINAL_INSPECTION_INDICATOR>{
	private static final long serialVersionUID = -4143827544596690998L;

	public FinalInspectionIndicatorDeserializer() { 
		this(null); 
	} 

	public FinalInspectionIndicatorDeserializer(Class<FINAL_INSPECTION_INDICATOR> indicator) { 
		super(indicator); 
	}

	@Override
	public FINAL_INSPECTION_INDICATOR deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException {
		JsonNode node = jp.getCodec().readTree(jp);
		String indicator = null;
		if(node.isObject()) {
			indicator = node.get("code").asText();
		} else {
			indicator =  node.asText();	
		}

		try {
			return FINAL_INSPECTION_INDICATOR.forIndicator(indicator);
		} catch (InvalidFinalInspectionIndicatorException e) {
			throw new EWDeserializationException(e, String.format("Problem creating new FINAL_INSPECTION_INDICATOR from '%s'", indicator));
		}
	}
}
