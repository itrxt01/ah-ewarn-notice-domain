package com.railinc.assethealth.eqadv.domain.pdf;

public class PDFValidator {
	
	private static final byte[] PDF_HEADER_13 = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x33};  // "%PDF-1.3"
	private static final byte[] PDF_TERMINATOR = {0x25, 0x25, 0x45, 0x4f, 0x46};    // "%%EOF" 
	private static final byte[] PDF_HEADER_14 = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x34};  // "%PDF-1.4"
	private static final byte[] PDF_HEADER_15 = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x35};  // "%PDF-1.5"
	private static final byte[] PDF_HEADER_16 = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x36};  // "%PDF-1.6"
	private static final byte[] PDF_HEADER_17 = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x37};  // "%PDF-1.7"
	private static final byte[] PDF_HEADER_20 = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x32, 0x2E, 0x30};  // "%PDF-2.0"
	
	private enum PDF_VERSION {
		PDF_1_3(PDF_HEADER_13, PDF_TERMINATOR, "1.3")
		, PDF_1_4(PDF_HEADER_14, PDF_TERMINATOR, "1.4")
		, PDF_1_5(PDF_HEADER_15, PDF_TERMINATOR, "1.5")
		, PDF_1_6(PDF_HEADER_16, PDF_TERMINATOR, "1.6")
		, PDF_1_7(PDF_HEADER_17, PDF_TERMINATOR, "1.7")
		, PDF_2_0(PDF_HEADER_20, PDF_TERMINATOR, "2.0")
		, UNKNOWN(null, null, null);
		
		protected final byte[] header;
		protected final byte[] terminator;
		protected final String version;
		
		PDF_VERSION(byte[] header, byte[] terminator, String version){
			this.header = header;
			this.terminator = terminator;
			this.version = version;
		}
		
		public static PDF_VERSION getVersion(byte[] pdf) {
			PDF_VERSION pdfVersion = UNKNOWN;
			for (PDF_VERSION v : PDF_VERSION.values()) {
				pdfVersion = v;
				if (PDF_VERSION.UNKNOWN != pdfVersion) {
					boolean same = compareBytes(pdf, v.header, v.header.length);
					if (same) {
						break;
					}
				}
			}
			return pdfVersion;
		}
	    
	    private static boolean compareBytes(byte[] input, byte[] compareTo, int length){
	    	boolean same = true;
    		for(int i = 0; i < length; i++){
    			if(input[i] != compareTo[i]){
    				same = false;
    				break;	    				
    			}
    		}
    		return same;
	    }
	}
	

    PDFValidator() {
        throw new UnsupportedOperationException("PDFValidator should not be instantiated");
    }
    
    /**
     * Get the PDF version based upon the contents.
     * @param data
     * @return the pdf version
     */
    public static String getPDFVersion(byte[] data) {
    	return PDF_VERSION.getVersion(data).version;
    }
    

    /**
     * Test if the data in the given byte array represents a PDF file.
     * @param data
     * @return true if the pdf is supported
     */
    public static boolean isValidPDF(byte[] data) {
    	boolean valid = false;
    	for(PDF_VERSION version : PDF_VERSION.values()){
    		if(version != PDF_VERSION.UNKNOWN
    				&& PDF_VERSION.getVersion(data) != PDF_VERSION.UNKNOWN
    				&& isTerminatorValid(data, version.terminator)){
    			valid = true;
    			break;
    		}
    	}
    	return valid;
    }
    
	private static boolean isTerminatorValid(byte[] data, byte[] terminator){
		for(int i = terminator.length - 1, j = getLength(data); i != 0 && j != 0; i--, j--){
			if(data[j] != terminator[i])
				return false;
		}
		return true;
	}	
	
	// reduce the length by any control characters or a space at the end of file
	private static int getLength(byte[] data){
		byte controlCharacterMask = 0x20;  // Space and below
		int index = data.length - 1;

		while( data[index] <= controlCharacterMask && index > 0) index--;
		return index;
	}    
}
