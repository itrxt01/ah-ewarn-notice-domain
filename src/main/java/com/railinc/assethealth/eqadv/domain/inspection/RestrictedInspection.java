package com.railinc.assethealth.eqadv.domain.inspection;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;
import com.railinc.assethealth.eqadv.domain.mark.Mark;

/**
 * This represents an inspection restricted to being reported by a specific Mark.
 */
public class RestrictedInspection extends EWDomainAbstract {

	private static final long serialVersionUID = 7144641104699913246L;

	private InspectionCode inspectionCode;
	private Mark mark;

    public RestrictedInspection(){ /* Zero arg c'tor for deserialzation */}

    public RestrictedInspection(InspectionCode code, Mark mark){
    	this.mark = mark;
    	this.inspectionCode = code;
    }
    
    @JsonIgnore
    public InspectionCode getInspectionCode() {
        return inspectionCode;
    }

    public void setInspectionCode(InspectionCode inspectionCode) {
        this.inspectionCode = inspectionCode;
    }
    
    @JsonProperty("inspectionCode")
    public String getCode() {
    	return inspectionCode.getCode();
    }
    
    public void setMark(Mark mark) {
    	this.mark = mark;
    }
    
    @JsonIgnore
    public Mark getMark() {
    	return this.mark;
    }

    @JsonProperty("mark")
    public String getMarkString() {
    	return mark.getValueAsString();
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false);
    }

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false);
	}	

}
