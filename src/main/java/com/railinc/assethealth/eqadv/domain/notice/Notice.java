/**
 * Copyright (c) Railinc - 2018. All rights reserved.
 */
package com.railinc.assethealth.eqadv.domain.notice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.railinc.assethealth.eqadv.domain.Constants;
import com.railinc.assethealth.eqadv.domain.EWDomainWithID;
import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.equipment.Assignment;
import com.railinc.assethealth.eqadv.domain.equipment.EquipmentSet;
import com.railinc.assethealth.eqadv.domain.equipment.MECHANICAL_DESIGNATION_INDICATOR;
import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignation;
import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignations;
import com.railinc.assethealth.eqadv.domain.exception.InspectionNotPeriodicException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidAssignmentReporterIndicatorException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidFinalInspectionIndicatorException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidInspectionCodeException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidSeverityCodeException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidStatusException;
import com.railinc.assethealth.eqadv.domain.inspection.FINAL_INSPECTION_INDICATOR;
import com.railinc.assethealth.eqadv.domain.inspection.INSPECTION_TYPE;
import com.railinc.assethealth.eqadv.domain.inspection.InspectionCode;
import com.railinc.assethealth.eqadv.domain.inspection.NoticeInspectionCode;
import com.railinc.assethealth.eqadv.domain.inspection.NoticeInspectionCodes;
import com.railinc.assethealth.eqadv.domain.mark.ASSIGNMENT_REPORTER_INDICATOR;
import com.railinc.assethealth.eqadv.domain.mark.Marks;
import com.railinc.assethealth.eqadv.domain.pdf.NoticeContent;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevels;
import com.railinc.assethealth.eqadv.domain.status.NOTICE_STATUS;


/**
 * @author ron taylor
 */
@JsonPropertyOrder({
	"id"
    , "status"
	, "category"
	, "number"
	, "supplementNumber"
	, "effectiveDate"
	, "fileNumber"
	, "noticeName"
	, "nextPublishedName"
	, "title"
    , "componentRegistryNotice"
    , "assignmentReporterInd"
    , "assignmentReporterMarks"
    , "equipmentPriorityScoreFactors"
    , "finalInspectionInd"
    , "finalInspectionMarks"
    , "mechanicalDesignationInd"
    , "mechanicalDesingations"
    , "severityLevels"
    , "lifeTimeNumberEquipmentAssigned"
    , "equipmentNumberCurrentlyAssigned"
    , "inspectionCodeMC"
    , "inspectionInterval"
    , "frequencyUnits"
    , "finalInspectionCodes"
    , "pdfFilename"
})
public class Notice extends EWDomainWithID implements Serializable {

	private static final long serialVersionUID = -143582039273067948L;
	
	@JsonInclude(Include.ALWAYS)
	private Category category;		
	
	@JsonInclude(Include.NON_NULL)
	private Integer number = null;	
	
	@JsonInclude(Include.ALWAYS)
	private NOTICE_STATUS status = NOTICE_STATUS.DRAFT;
	
	@JsonIgnore
	private Integer nextNoticeNumber;
	
	@JsonInclude(Include.ALWAYS)
	@JsonFormat(locale = Constants.LOCALE, shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT, timezone = Constants.TIME_ZONE)
	private Date effectiveDate;	
	
	@JsonInclude(Include.ALWAYS)
	private String fileNumber;			
	
	@JsonInclude(Include.ALWAYS)
	private String title;			
	
	@JsonInclude(Include.ALWAYS)
	private Integer supplementNumber;

	@JsonInclude(Include.ALWAYS)
	private NoticeInspectionCodes inspectionCodes;	
	
	private ASSIGNMENT_REPORTER_INDICATOR assignmentReporterInd;
	
	@JsonInclude(Include.NON_EMPTY)
	private Marks assignmentReporterMarks;
	private FINAL_INSPECTION_INDICATOR finalInspectionInd;
	
	@JsonInclude(Include.NON_EMPTY)
	private Marks finalInspectionMarks;
	
	@JsonInclude(Include.NON_NULL)
	private MECHANICAL_DESIGNATION_INDICATOR mechanicalDesignationInd = MECHANICAL_DESIGNATION_INDICATOR.OPEN;
	
	@JsonInclude(Include.NON_EMPTY)
	private MechanicalDesignations mechanicalDesignations;

	@JsonInclude(Include.ALWAYS)
	@JsonUnwrapped
	private SeverityLevels severityLevels;
	
	@JsonInclude(Include.ALWAYS)
	private boolean isComponentRegistryNotice;
	
	@JsonInclude(Include.ALWAYS)
	private boolean isInspectionCodeMC;

	private int inspectionInterval;
	private String frequencyUnits;
	
	@JsonInclude
	private long lifeTimeNumberEquipmentAssigned;

	@JsonInclude
	private long equipmentNumberCurrentlyAssigned;

	@JsonIgnore
	private transient NoticeContent content;
	
	@JsonInclude(Include.NON_EMPTY)
	private final EquipmentSet<Assignment> assignedEquipment = new EquipmentSet<>();
	
	@JsonInclude(Include.NON_NULL)
	private NoticeEqmtScoreFactors equipmentPriorityScoreFactors;

	public Notice() {
		super();
	}

	// Category
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	// Number
	@JsonProperty("number")
	public Integer getNumber() {
		return number;
	}

	@JsonIgnore
	public void setNumber(int number) {
		this.number = Integer.valueOf(number);
	}

	@JsonProperty("number")
	public void setNumber(Integer number) {
		this.number = number;
	}

	@JsonIgnore
	public String getNumberAsString() {
		if(this.isDraft() && (number == null || number == 0)) {
			return Constants.DRAFT;
		} else {
			return padNumberWithLeadingZeros(number, 4);
		}
	}	
	
	@JsonInclude(Include.NON_NULL)
	public Integer getNextNoticeNumber() {
		return nextNoticeNumber;
	}
	
	public void setNextNoticeNumber(Integer number) {
		nextNoticeNumber = number;
	}

	// Effective Date
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	// File number
	public String getFileNumber() {
		return fileNumber;
	}

	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}

	// Title
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	// Supplement number
	public Integer getSupplementNumber() {
		return supplementNumber;
	}

	public void setSupplementNumber(Integer supplementNumber) {
		this.supplementNumber = supplementNumber;
	}

	private String getSupplementAsString() {
		return padNumberWithLeadingZeros(this.supplementNumber, 2);
	}

	// Notice content
	@JsonIgnore
	public NoticeContent getContent() {
		return content;
	}

	public void setContent(NoticeContent content) {
		this.content = content;
	}
	
	@JsonInclude(Include.ALWAYS)
	@JsonProperty("pdfFilename")
	public String getContentFilename() {
		return this.content.getFileName();
	}

	// Total number assigned
	public long getLifeTimeNumberEquipmentAssigned() {
		return lifeTimeNumberEquipmentAssigned;
	}

	public void setLifetimeNumberEquipmentAssigned(long lifeTimeNumberEquipmentAssigned) {
		this.lifeTimeNumberEquipmentAssigned = lifeTimeNumberEquipmentAssigned;
	}

	// Equipment currently assigned
	public long getEquipmentNumberCurrentlyAssigned() {
		return equipmentNumberCurrentlyAssigned;
	}

	public void setEquipmentNumberCurrentlyAssigned(long equipmentNumberCurrentlyAssigned) {
		// Transient value, don't track changes
		this.equipmentNumberCurrentlyAssigned = equipmentNumberCurrentlyAssigned;
	}

	// Component registry
	public void setComponentRegistryNotice(boolean isComponentRegistryNotice) {
		this.isComponentRegistryNotice = isComponentRegistryNotice;
	}

	public boolean isComponentRegistryNotice() {
		return this.isComponentRegistryNotice;
	}

	@JsonProperty("inspectionCodeMC")
	public boolean isInspectionCodeMC() {
		return isInspectionCodeMCEnabled()  || null != this.getMCInspectionCode();
	}

	@JsonProperty("inspectionCodeMC")
	public void setInspectionCodeMC(boolean isInspectionCodeMC) {
		if(!isInspectionCodeMC && null != this.getMCInspectionCode()) {
			this.removeMCInspectionCode();
		}
		this.isInspectionCodeMC = isInspectionCodeMC;
	}
	
	@JsonIgnore
	public Integer getMCInspectionInterval() {
		if(isInspectionCodeMC() && null != this.getMCInspectionCode()) {
			return this.getMCInspectionCode().getFrequency();
		}
		return this.inspectionInterval;
	}

	@JsonProperty("inspectionInterval")
	public void setInspectionInterval(int inspectionInterval) {
		this.inspectionInterval = inspectionInterval;
	}
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("inspectionInterval")
	public int getInspectionInterval() {
		return getMCInspectionInterval() == null ? 0 : getMCInspectionInterval().intValue();
	}

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("frequencyUnits")
	public String getMCFrequencyUnits(){
		if(null != this.getMCInspectionCode()) {
			return this.getMCInspectionCode().getFrequencyUnits();
		}
		return null;
	}

	@JsonProperty("frequencyUnits")
	public void setMCFrequencyUnits(String frequencyUnits) {
		this.frequencyUnits = frequencyUnits;
	}
	
	@JsonIgnore
	public String getFrequencyUnits() {
		return (null != frequencyUnits ? frequencyUnits : getMCFrequencyUnits());
	}

	private boolean isInspectionCodeMCEnabled() {
		return this.isInspectionCodeMC || null != this.getMCInspectionCode();
	}
	
	@JsonIgnore
	public NoticeInspectionCode getMCInspectionCode() {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MC");
		if(null != this.inspectionCodes) {
			return this.inspectionCodes.getInspectionCodes()
					.stream()
					.filter(c -> c.getInspectionCode().getCode().equals(code.getCode()))
					.findFirst()
					.orElse(null);
		} else {
			return null;
		}				
	}

	public void removeMCInspectionCode() {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MC");
		this.inspectionCodes.removeInspection(code);
	}

	@JsonIgnore
	public boolean isInspectionReportingPermitted(SeverityCode severity, InspectionCode inspection) {
		return this.inspectionCodes.isInspectionPermitted(inspection,  severity);
	}

	@JsonIgnore
	public int getInspectionFrequency(SeverityCode severity, InspectionCode inspection) 
			throws InvalidSeverityCodeException, InvalidInspectionCodeException, InspectionNotPeriodicException {
		return this.inspectionCodes.getInspectionFrequency(inspection, severity);
	}

	// Draft notice
	@JsonIgnore
	public boolean isDraft() {
		return NOTICE_STATUS.DRAFT == this.status;
	}

	// Published
	@JsonIgnore
	public boolean hasPreviouslyBeenPublished() {
		// Number assigned less than the max published number, so, has been published before
		return null != this.number && this.number <= Constants.PUBLISHED_NOTICE_NUMBER_MAX;
	}

	@JsonIgnore
	public boolean hasNeverBeenPublished() {
		return (null == this.number && NOTICE_STATUS.DRAFT == this.status)							// Never been persisted since no notice number assigned
				|| (null != this.number  && this.number >= Constants.DRAFT_NOTICE_NUMBER_START);	// persisted since draft number assigned
	}
	
	// Persisted
	@JsonIgnore
	public boolean hasBeenPersisted() {
		return null != this.number && this.number >= Constants.PUBLISHED_NOTICE_NUMBER_START;
	}

	@JsonIgnore
	public void setStatus(NOTICE_STATUS status) {
		this.status = status;
	}
	@JsonProperty("status")
	public void setStatus(String status) throws InvalidStatusException {
		this.status = NOTICE_STATUS.forCode(status);
	}
	@JsonIgnore
	public NOTICE_STATUS getStatusIndicator() {
		return this.status;
	}
	@JsonProperty("status")
	public String getStatus() {
		return this.status.statusName;
	}

	// Assignment Report Marks
	@JsonIgnore
	public Marks getAssignmentReporterMarks() {
		return assignmentReporterMarks;
	}

	@JsonProperty("assignmentReporterMarks")
	@JsonInclude(Include.NON_NULL)
	public String getAssignmentReporters() {
		if(assignmentReporterInd.isRetrieveFromDatabase) {
			return assignmentReporterMarks.getMarksString();			
		}
		return null;
	}

	@JsonProperty("assignmentReporterMarks")
	public void setAssignmentReporterMarks(Marks assignmentReporterMarks) {
		this.assignmentReporterMarks = assignmentReporterMarks;
	}
	
	@JsonInclude(Include.ALWAYS)	
	@JsonProperty("assignmentReporterInd")
	public ASSIGNMENT_REPORTER_INDICATOR getAssignmentReporterInd() {
		return assignmentReporterInd;
	}

	@JsonProperty("assignmentReporterInd")
	public void setAssignmentReporterInd(ASSIGNMENT_REPORTER_INDICATOR assignmentReporterInd) {
		this.assignmentReporterInd = assignmentReporterInd;
	}

	@JsonIgnore
	public void setAssignmentReporterInd(String ind) throws InvalidAssignmentReporterIndicatorException {
		this.assignmentReporterInd = ASSIGNMENT_REPORTER_INDICATOR.forIndicator(ind);
	}

	// Final inspection Marks
	public Marks getFinalInspectionMarks() {
		return finalInspectionMarks;
	}

	// Final inspection Marks
	@JsonProperty("finalInspectionMarks")
	@JsonInclude(Include.NON_NULL)
	public String getFinalInspectionReporters() {
		if(finalInspectionInd.isRetrieveFromDatabase) {
			return finalInspectionMarks.getMarksString();
		}
		return null;
	}

	@JsonProperty("finalInspectionMarks")
	public void setFinalInspectionMarks(Marks finalInspectionMarks) {
		this.finalInspectionMarks = finalInspectionMarks;
	}

	@JsonInclude(Include.ALWAYS)	
	@JsonProperty("finalInspectionInd")
	public FINAL_INSPECTION_INDICATOR getFinalInspectionInd() {
		return finalInspectionInd;
	}

	@JsonProperty("finalInspectionInd")
	public void setFinalInspectionInd(FINAL_INSPECTION_INDICATOR finalInspectionInd) {
		this.finalInspectionInd = finalInspectionInd;
	}

	@JsonIgnore
	public void setFinalInspectionInd(String finalInspectionInd) throws InvalidFinalInspectionIndicatorException {
		this.finalInspectionInd = FINAL_INSPECTION_INDICATOR.forIndicator(finalInspectionInd);
	}

	public MECHANICAL_DESIGNATION_INDICATOR getMechanicalDesignationInd() {
		return mechanicalDesignationInd;
	}

	public void setMechanicalDesignationInd(MECHANICAL_DESIGNATION_INDICATOR mechanicalDesignationInd) {
		this.mechanicalDesignationInd = mechanicalDesignationInd;
	}

	// Mechanical designations
	@JsonIgnore
	public MechanicalDesignations getMechanicalDesignations() {
		return mechanicalDesignations;
	}

	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty("mechanicalDesignations")
	public String getMechanicalDesignationsString() {
		if(this.mechanicalDesignationInd.isRetrieveFromDatabase) {
			return mechanicalDesignations.getListAsDelimitedString();			
		}
		return null;
	}

	@JsonIgnore
	public Set<MechanicalDesignation> getMechanicalDesignationSet() {
		return mechanicalDesignations.getMechanicalDesignations();
	}

	@JsonProperty("mechanicalDesignations")
	public void setMechanicalDesignations(MechanicalDesignations mechanicalDesignations) {
		if(null != mechanicalDesignations && CollectionUtils.isNotEmpty(mechanicalDesignations.getMechanicalDesignations())){
			this.setMechanicalDesignationInd(MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED);
			this.mechanicalDesignations = mechanicalDesignations;
		} else {
			this.setMechanicalDesignationInd(MECHANICAL_DESIGNATION_INDICATOR.OPEN);			
		}
	}

	@JsonIgnore 
	public boolean isMechanicalDesignationAllowed(String designation) throws InvalidMechanicalDesignationException {
		if(StringUtils.isBlank(designation)) {
			return false;
		}
		MechanicalDesignation mech = new MechanicalDesignation(designation);
		return mechanicalDesignations.containsMechanicalDesignation(mech);
	}

	public void addMechanicalDesignations(Collection<MechanicalDesignation> designations) {
		if(null == this.mechanicalDesignations) {
			this.mechanicalDesignations = new MechanicalDesignations();
		}
		this.mechanicalDesignations.addMechanicalDesignations(designations);
	}

	public void addMechanicalDesignation(String mechanicalDesignation) throws InvalidMechanicalDesignationException {
		MechanicalDesignation mech = new MechanicalDesignation(mechanicalDesignation);
		this.mechanicalDesignations.addMechanicalDesignation(mech);
	}
	
	// Final activity codes permitted.
	public Set<NoticeInspectionCode> getFinalInspectionCodes(SeverityCode severity) {
		return this.inspectionCodes.getPermittedFinalInspectionCodesForSeverity(severity);
	}

	@JsonProperty("finalInspectionCodes")
	public void setFinalInspectionCodes(NoticeInspectionCodes inspectionCodes) {
		this.inspectionCodes = inspectionCodes;
	}
	
	@JsonIgnore
	public boolean isFinalInspectionCode(String code) {
		return this.inspectionCodes.isFinalInspection(code);
	}

	// Final activity codes permitted.
	@JsonIgnore
	public List<NoticeInspectionCode> getNoticeFinalInspectionCodes() {
		if(null == this.inspectionCodes || CollectionUtils.isEmpty(this.inspectionCodes.getInspectionCodes())){
			return new ArrayList<>();
		}
		return this.inspectionCodes.getInspectionCodes()
				.stream()
				.filter(NoticeInspectionCode::isFinal)
				.collect(Collectors.toSet())
				.stream()
				.collect(Collectors.toList());
	}

	@JsonIgnore
	public List<InspectionCode> getFinalInspectionCodes() {
		if(null == this.inspectionCodes || CollectionUtils.isEmpty(this.inspectionCodes.getInspectionCodes())){
			return new ArrayList<>();
		}
		return this.inspectionCodes.getInspectionCodes()
				.stream()
				.map(NoticeInspectionCode::getInspectionCode)
				.collect(Collectors.toSet())
				.stream()
				.filter(InspectionCode::isFinal)
				.collect(Collectors.toList());
	}

	@JsonIgnore
	public List<NoticeInspectionCode> getNoticeInspectionCodes() {
		if(null == this.inspectionCodes || CollectionUtils.isEmpty(this.inspectionCodes.getInspectionCodes())){
			return new ArrayList<>();
		}
		return this.inspectionCodes.getInspectionCodes();
	}
		
	@JsonInclude(Include.ALWAYS)
	@JsonProperty("finalInspectionCodes")
	public String getFinalInspectionCodesString() {
		return this.getNoticeFinalInspectionCodes()
				.stream()
				.map(NoticeInspectionCode::getCode)
				.collect(Collectors.toSet())
				.stream()
				.collect(Collectors.joining(", "));		
	}
	
	// Equipment Assigned to the notice.
	@JsonIgnore
	public EquipmentSet<Assignment> getAssignedEquipment() {
		return this.assignedEquipment;
	}

	@JsonIgnore
	public List<Assignment> getAssignedEquipmentList() {
		return this.assignedEquipment.getEquipment()
		.stream()
		.sorted()
		.collect(Collectors.toList());
	}

	@JsonIgnore
	public List<String> getAssignedEquipmentIdList() {
		return getAssignedEquipmentList().stream().map(Assignment::getEquipmentId).collect(Collectors.toList());
	}
	public int setAssignedEquipment(Collection<Assignment> equipment) {
		return assignedEquipment.replaceAll(equipment);
	}

	public void addAssignedEquipment(Assignment equipment) {
		assignedEquipment.add(equipment);
		lifeTimeNumberEquipmentAssigned += 1;
	}

	public int addAssignedEquipment(Collection<Assignment> equipment){
		assignedEquipment.addAll(equipment);
		lifeTimeNumberEquipmentAssigned += equipment.size();
		return equipment.size();
	}

	public void setSeverityLevels(SeverityLevels severityLevels) {		
		this.severityLevels = severityLevels;
	}
	
	public SeverityLevels getSeverityLevels() {
		return this.severityLevels;
	}
	
	@JsonIgnore
	public int getNumberEquipmentCurrentlyAssigned() {
		return this.assignedEquipment.size();
	}
	
	public boolean hasSeverityLevel(String code) {
		return this.severityLevels.hasSeverity(code);
	}
	
	@JsonIgnore
	public long getNumberEquipmentAtSeverity(SeverityCode severity) {
		if(null != this.assignedEquipment.getEquipment()) {
			return this.assignedEquipment.getEquipment()
					.stream()
					.filter(e -> e.getSeverity().getSeverityCode().equals(severity))
					.count();
		} 
		return 0;		
	}

	@JsonIgnore
	public Set<Assignment> getEquipmentAtSeverity(SeverityCode severity) {
		if(null != this.assignedEquipment.getEquipment()) {
			return this.assignedEquipment.getEquipment()
					.stream()
					.filter(e -> e.getSeverity().getSeverityCode().equals(severity))
					.collect(Collectors.toSet());
		} 
		return new TreeSet<>();
	}

	// Returns a formatted notice name with supplement suffix.
	@JsonProperty("noticeName")
	public String getNoticeName() {
		StringBuilder result = new StringBuilder(category.getCode().trim());
		String noticeNumberString = this.getNumberAsString().trim();
		if(this.hasNeverBeenPublished()) {
			result.append("-" + Constants.DRAFT);				
		} else if( StringUtils.isNotBlank( noticeNumberString )){
			result.append("-" + noticeNumberString);

			if (null != supplementNumber && supplementNumber > 0 ) {
				result.append(" Supplement " + this.getSupplementAsString() );
			}				
			if(this.isDraft()) {
				result.append(" " + Constants.DRAFT);
			}
		}
		return result.toString();
	}

	// Returns a formatted notice that will be assigned to this notice if it is
	// published now (before any other notice is publised.
	public String getNextPublishedName() {
		StringBuilder result = new StringBuilder(category.getCode().trim());
		String noticeNumberString = this.getNumberAsString().trim();
		if(this.hasNeverBeenPublished()) {
			result.append("-" + padNumberWithLeadingZeros(nextNoticeNumber, 4));				
		} else if(this.isDraft()){
			result.append("-" + noticeNumberString);

			if (null != supplementNumber && supplementNumber > 0 ) {
				result.append(" " + Constants.SUPPLEMENT + " " + this.getSupplementAsString() );
			}				
		} else {
			result.append("-" + padNumberWithLeadingZeros(number, 4));				
			result.append(" " + Constants.SUPPLEMENT + " " + padNumberWithLeadingZeros(this.supplementNumber + 1L, 2) );
		}
		return result.toString();
	}
	
	public NoticeEqmtScoreFactors getEquipmentPriorityScoreFactors() {
		return equipmentPriorityScoreFactors;
	}

	public void setEquipmentPriorityScoreFactors(NoticeEqmtScoreFactors equipmentPriorityScoreFactors) {
		this.equipmentPriorityScoreFactors = equipmentPriorityScoreFactors;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, "assignedEquipment");
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false);
	}
}

