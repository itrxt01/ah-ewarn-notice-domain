package com.railinc.assethealth.eqadv.domain.transaction;

public class NoticeFinalInspectionReporterEvent extends NoticeEvent {

    private static final long serialVersionUID = 372575882413122497L;

    private String Mark;

    public String getMark() {
        return Mark;
    }

    public void setMark(String mark) {
        Mark = mark;
    }
}
