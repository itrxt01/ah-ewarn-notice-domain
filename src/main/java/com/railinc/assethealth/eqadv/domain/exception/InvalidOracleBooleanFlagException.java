package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidOracleBooleanFlagException extends EWDeserializationException {			// NOSONAR - Approve 6 parents for this exception.
	private static final long serialVersionUID = 8506535707547490617L;

	public InvalidOracleBooleanFlagException(String msg) {
		super(msg);
	}
}
