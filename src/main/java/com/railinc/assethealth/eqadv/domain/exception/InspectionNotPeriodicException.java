package com.railinc.assethealth.eqadv.domain.exception;

public class InspectionNotPeriodicException extends Exception {
	private static final long serialVersionUID = -6357113741547193573L;

	public InspectionNotPeriodicException(String msg){
		super(msg);
	}
}
