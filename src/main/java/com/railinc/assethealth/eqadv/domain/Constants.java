package com.railinc.assethealth.eqadv.domain;

/**
 * <p>
 * Class holding constant values.  Since the util project is upstream to 
 * every project with a dependency on the core-java-util jar, 
 * this class should be used to store constants common to all dependent projects.
 * Other, more domain specific constants may be found elsewhere, so don't treat this
 * class as the end-all be-all class for constant values.
 * </p>
 * 
 * <p>
 * It would be best to extend this class with a 'final' modifier and have constants
 * segregated by functional area and domain.  Example:  RestConstants and AppConstants.
 * </p>
 *
 * @author $Author: Ron.Taylor 
 *
 */
public class Constants {   
   /*
    * Privatize the constructor to keep anyone from instantiating this class.  Elements 
    * should be statically referenced within this class.  
    */
   private Constants() {}
   
	public static final int DRAFT_NOTICE_NUMBER_START = 500_000;
	public static final int PUBLISHED_NOTICE_NUMBER_MAX = 9999;
	public static final int PUBLISHED_NOTICE_NUMBER_START = 1;
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String TIME_ZONE = "America/New_York";
	public static final String LOCALE = "us";
	
	public static final String DRAFT = "DRAFT";
	public static final String SUPPLEMENT = "Supplement";
	public static final String ESCALATION_INSPECTION_CODE = "MY";
	public static final String NOTICE_NUMBER_FORMAT = "%04d";
	public static final String SUPPLEMENT_NUMBER_FORMAT = "%02d";
	public static final String NOTICE_NAME_FORMAT = "%s-%04d";
}
