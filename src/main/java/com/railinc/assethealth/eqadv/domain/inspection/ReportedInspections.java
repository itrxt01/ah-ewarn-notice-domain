package com.railinc.assethealth.eqadv.domain.inspection;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReportedInspections  implements Serializable {
	private static final long serialVersionUID = 4205186410960110389L;
	private Map<String, Integer> inspections = new HashMap<>();
	
	public void addInspectionQty(InspectionCode code, int qty) {
		inspections.put(code.getCode(), Integer.valueOf(qty));
	}
	
	@JsonProperty("reportedInspections")
	public Map<String, Integer> getReportedInspections(){
		return Collections.unmodifiableMap(inspections);
	}
}

