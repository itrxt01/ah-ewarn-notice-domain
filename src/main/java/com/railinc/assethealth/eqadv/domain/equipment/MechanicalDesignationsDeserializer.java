package com.railinc.assethealth.eqadv.domain.equipment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;

public class MechanicalDesignationsDeserializer extends StdDeserializer<MechanicalDesignations> {
	private static final long serialVersionUID = 5821532797817162752L;
	private static final Logger LOG = LoggerFactory.getLogger(MechanicalDesignationsDeserializer.class);

	public MechanicalDesignationsDeserializer() {
		this(null);
	}

	public MechanicalDesignationsDeserializer(Class<MechanicalDesignations> vc) {
		super(vc);
	}

	@Override
	public MechanicalDesignations deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
		List<MechanicalDesignation> mechDesignsList = new ArrayList<>();
		JsonNode node = jp.getCodec().readTree(jp);
		
		if(node.isNull()) {
			return new MechanicalDesignations(mechDesignsList);
		}
		String mechDesigsInput = node.asText();

		String[] mechDesignsArray = mechDesigsInput.split("\\s*(\\s|,)\\s*");
		

		for (String currentMechDesign : mechDesignsArray) {
			try {
				mechDesignsList.add(new MechanicalDesignation(currentMechDesign.trim()));
			} catch (InvalidMechanicalDesignationException e) {
				LOG.error("Problem creating new MechanicalDesignation from '" + currentMechDesign + "'.", e);
			}
		}

		return new MechanicalDesignations(mechDesignsList);
	}
}
