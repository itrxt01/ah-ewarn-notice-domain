package com.railinc.assethealth.eqadv.domain.notice;

public class InspectionNotice {

    private Long noticeId;
    private Integer noticeNumber;
    private String categoryCode;
    private String isFinal;
    private String noticeActivityCode;
    private String inspectionCode;
    private String inspectionReporter;

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }

    public Integer getNoticeNumber() {
        return noticeNumber;
    }

    public void setNoticeNumber(Integer noticeNumber) {
        this.noticeNumber = noticeNumber;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getIsFinal() {
        return isFinal;
    }

    public void setIsFinal(String isFinal) {
        this.isFinal = isFinal;
    }

    public String getNoticeActivityCode() {
        return noticeActivityCode;
    }

    public void setNoticeActivityCode(String noticeActivityCode) {
        this.noticeActivityCode = noticeActivityCode;
    }

    public String getInspectionCode() {
        return inspectionCode;
    }

    public void setInspectionCode(String inspectionCode) {
        this.inspectionCode = inspectionCode;
    }

    public String getInspectionReporter() {
        return inspectionReporter;
    }

    public void setInspectionReporter(String inspectionReporter) {
        this.inspectionReporter = inspectionReporter;
    }
}
