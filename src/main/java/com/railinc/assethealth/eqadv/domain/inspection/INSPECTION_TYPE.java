package com.railinc.assethealth.eqadv.domain.inspection;

import org.apache.commons.lang3.StringUtils;

import com.railinc.assethealth.eqadv.domain.exception.InvalidInspectionCodeType;

public enum INSPECTION_TYPE {
	PRELIMINARY("PI", false)
	, FINAL("FI", true);
	
	public String code;		// NOSONAR - immutable enum
	public boolean isFinal;  	// NOSONAR - immutable enum 
	
	private INSPECTION_TYPE(String code, boolean isFinal) {
		this.code = code;
		this.isFinal = isFinal;
	}
	
	public static INSPECTION_TYPE forFlag(boolean flag) throws InvalidInspectionCodeType {
		for(INSPECTION_TYPE type : INSPECTION_TYPE.values()) {
			if(type.isFinal == flag) {
				return type;
			}
		}
		throw new InvalidInspectionCodeType(String.format("The value [%s] is not a valid inspection code flag.", flag));
	}
	
	public static INSPECTION_TYPE forCode(String code) throws InvalidInspectionCodeType {
		if(StringUtils.isBlank(code)) {
			throw new InvalidInspectionCodeType("The value for inspection code is blank.");			
		}
		for(INSPECTION_TYPE type : INSPECTION_TYPE.values()) {
			if(type.code.equals(code.toUpperCase().trim())) {
				return type;
			}
		}
		throw new InvalidInspectionCodeType(String.format("The value [%s] is not a valid inspection code type.", code));
	}
}
