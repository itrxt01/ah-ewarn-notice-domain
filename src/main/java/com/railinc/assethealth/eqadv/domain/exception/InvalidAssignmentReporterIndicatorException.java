package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidAssignmentReporterIndicatorException extends EWDeserializationException {	// NOSONAR - Approve 6 parents for this exception.  
	private static final long serialVersionUID = 2654034818635504968L;

	public InvalidAssignmentReporterIndicatorException(String msg) {
		super(msg);
	}

}
