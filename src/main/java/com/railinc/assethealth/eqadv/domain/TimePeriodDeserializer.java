package com.railinc.assethealth.eqadv.domain;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class TimePeriodDeserializer extends StdDeserializer<TimePeriod> { 
	private static final long serialVersionUID = 9034066911590388364L;

	public TimePeriodDeserializer() { 
		this(null); 
	} 

	public TimePeriodDeserializer(Class<TimePeriod> vc) { 
		super(vc); 
	}

	@Override
	public TimePeriod deserialize(JsonParser jp, DeserializationContext ctxt) 
			throws IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(jp, new TypeReference<TimePeriod>() {});
	}
}
