package com.railinc.assethealth.eqadv.domain.equipment;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.railinc.assethealth.eqadv.domain.Constants;

@JsonPropertyOrder({ "equipmentId", "componentInfo", "noticeNumber", "noticeId", "noticeStatus", "markOwner", "umlerOwner"
		, "lessee", "mechanicalDesignation", "equipmentStatus", "severityLevel", "assignmentDate", "escalationDate", "EIN" })
public class EquipmentView {
	private String equipmentId;
	private String componentInfo;
	private long noticeId;
	private int noticeNumber;
	private String noticeStatus;
	private String markOwner;
	private String umlerOwner;
	private String lessee;
	private String mechanicalDesignation;
	private String equipmentStatus;
	private String severityLevel;
	private Date assignmentDate;
	private Date escalationDate;
	private String ein;
	
	public EquipmentView() {/* zero argument constructor */}
	
	public EquipmentView(Assignment equipment) {
		this.equipmentId = equipment.getEquipmentId();
		this.noticeId = equipment.getNoticeId();
		this.assignmentDate = equipment.assignedDate;
		this.ein = equipment.getEinAsString();
		this.severityLevel = equipment.getSeverity().getSeverityLevelCode();		
	}
	
	public String getEquipmentId() {
		return equipmentId;
	}
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}
	public String getComponentInfo() {
		return componentInfo;
	}
	public void setComponentInfo(String componentInfo) {
		this.componentInfo = componentInfo;
	}
	@JsonProperty("noticeId")
	public long getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}
	public int getNoticeNumber() {
		return noticeNumber;
	}
	public void setNoticeNumber(int noticeNumber) {
		this.noticeNumber = noticeNumber;
	}
	public String getNoticeStatus() {
		return this.noticeStatus;
	}
	public void setNoticeStatus(String status) {
		this.noticeStatus = status;
	}
	public String getMarkOwner() {
		return markOwner;
	}
	public void setMarkOwner(String markOwner) {
		this.markOwner = markOwner;
	}
	public String getUmlerOwner() {
		return umlerOwner;
	}
	public void setUmlerOwner(String umlerOwner) {
		this.umlerOwner = umlerOwner;
	}
	public String getLessee() {
		return lessee;
	}
	public void setLessee(String lessee) {
		this.lessee = lessee;
	}
	public String getMechanicalDesignation() {
		return mechanicalDesignation;
	}
	public void setMechanicalDesignation(String mechanicalDesignation) {
		this.mechanicalDesignation = mechanicalDesignation;
	}
	public String getEquipmentStatus() {
		return equipmentStatus;
	}
	public void setEquipmentStatus(String equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}
	public Date getAssignmentDate() {
		return assignmentDate;
	}
	public void setSeverityLevel(String sevCode) {
		this.severityLevel = sevCode;
	}
	public String getSeverityLevel() {
		return this.severityLevel;
	}
	@JsonFormat(locale = Constants.LOCALE, shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT, timezone = Constants.TIME_ZONE)
	public void setAssignmentDate(Date assignmentDate) {
		this.assignmentDate = assignmentDate;
	}
	public Date getEscalationDate() {
		return escalationDate;
	}

	@JsonFormat(locale = Constants.LOCALE, shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT, timezone = Constants.TIME_ZONE)
	public void setEscalationDate(Date escalationDate) {
		this.escalationDate = escalationDate;
	}

	public String getEin() {
		return ein;
	}
	public void setEin(String ein) {
		this.ein = ein;
	}
}
