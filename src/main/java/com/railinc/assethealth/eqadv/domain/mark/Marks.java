package com.railinc.assethealth.eqadv.domain.mark;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.railinc.assethealth.eqadv.domain.notice.NoticeItemSet;

@JsonDeserialize(using=MarksDeserializer.class)
public class Marks implements NoticeItemSet<Mark>, Serializable {
	private static final long serialVersionUID = 5661214343511323601L;

	private static final String LIST_DELIMITER = ", ";
	
	@JsonIgnore
	private Set<Mark> set = new TreeSet<>(); 
	
	public Marks() {}
	
	public Marks(Collection<Mark> marks) {
		for(Mark mark : marks) {
			this.set.add(MarkFactory.newMark(mark));			
		}
	}
	
	public Marks(Marks marks) {
		this.set.addAll(marks.getMarksSet());
	}
	
	@JsonIgnore
	public Collection<Mark> getMarksSet(){
		return set;
	}

	@JsonIgnore
	public List<Mark> getMarksList(){
		return new ArrayList<>(set);
	}

	public String getMarksString() {
		return this.getDelimitedString(set, LIST_DELIMITER, false);
	}
	
	public boolean isMarkListed(Mark mark) {
		return null != mark && this.hasMarks() && this.set.contains(mark);
	}
	
	public void addMark(Mark mark) {
		this.set.add(mark);
	}
	
	public boolean hasMarks() {
		return CollectionUtils.isNotEmpty(this.set);
	}

	public String getListAsDelimitedString(boolean padded) {
		return this.getDelimitedString(set, LIST_DELIMITER, padded);
	}

	public String getListAsDelimitedString(String delimiter) {
		return this.getDelimitedString(set, delimiter, false);
	}

	public int size() {
		return this.set.size();
	}
}
