package com.railinc.assethealth.eqadv.domain.errors;

public interface SubError {
	public String getMessage();
}
