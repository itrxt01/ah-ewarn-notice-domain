package com.railinc.assethealth.eqadv.domain.transaction;

public class NoticeSeverityEvent extends NoticeEvent {

    private static final long serialVersionUID = -8939266135554169847L;

    private String severityCode;
    private Integer escalationPeriod;
    private String escalationUnit;
    private String escalationSevCode;
    private String escalationType;
    private Integer escalationQty;

    public String getSeverityCode() {
        return severityCode;
    }

    public void setSeverityCode(String severityCode) {
        this.severityCode = severityCode;
    }

    public Integer getEscalationPeriod() {
        return escalationPeriod;
    }

    public void setEscalationPeriod(Integer escalationPeriod) {
        this.escalationPeriod = escalationPeriod;
    }

    public String getEscalationUnit() {
        return escalationUnit;
    }

    public void setEscalationUnit(String escalationUnit) {
        this.escalationUnit = escalationUnit;
    }

    public String getEscalationSevCode() {
        return escalationSevCode;
    }

    public void setEscalationSevCode(String escalationSevCode) {
        this.escalationSevCode = escalationSevCode;
    }

    public String getEscalationType() {
        return escalationType;
    }

    public void setEscalationType(String escalationType) {
        this.escalationType = escalationType;
    }

    public Integer getEscalationQty() {
        return escalationQty;
    }

    public void setEscalationQty(Integer escalationQty) {
        this.escalationQty = escalationQty;
    }
}
