package com.railinc.assethealth.eqadv.domain.inspection;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.railinc.assethealth.eqadv.domain.EWDomainWithID;
import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.exception.InspectionNotPeriodicException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidInspectionCodeException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidSeverityCodeException;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;

/*
 * This represents a collection of the permitted final inspection codes that may be reported
 * for any given severity on a notice.
 * @author ron taylor
 */
@JsonDeserialize(using=NoticeInspectionCodesDeserializer.class)
public class NoticeInspectionCodes extends EWDomainWithID implements Serializable {
	private static final long serialVersionUID = -5303806913774535889L;
	private Map<SeverityCode, List<NoticeInspectionCode>> inspectionCodesMap = new HashMap<>();
	private List<NoticeInspectionCode> inspectionCodes = new ArrayList<>();
	
	public NoticeInspectionCodes(List<NoticeInspectionCode> codes) {
		this.inspectionCodes.addAll(codes);
		this.updateMap();
	}
	
	public NoticeInspectionCodes(NoticeInspectionCodes finalInspectionCodes) {
		this.inspectionCodes.addAll(finalInspectionCodes.inspectionCodes);
		this.updateMap();
	}
	
	private void updateMap() {
		for(NoticeInspectionCode inspectionCode : inspectionCodes) {
			inspectionCodesMap.putIfAbsent(inspectionCode.getSeverityCode(), new ArrayList<>());
			inspectionCodesMap.get(inspectionCode.getSeverityCode()).add(inspectionCode);
		}
	}

	@JsonProperty("finalInspectionCodes")
	public List<NoticeInspectionCode> getInspectionCodes(){
		return Collections.unmodifiableList(inspectionCodes);
	}
	
	public void removeInspection(InspectionCode code) {
		this.inspectionCodes.removeIf(c -> c.getCode().equals(code.getCode()));	
		this.inspectionCodesMap.clear();
		this.updateMap();		
	}	
	
	
	
	private NoticeInspectionCode getInspectionCodeForSeverity(InspectionCode inspection, SeverityCode severity) 
			throws InvalidSeverityCodeException, InvalidInspectionCodeException {
		if(!inspectionCodesMap.containsKey(severity)) {
			throw new InvalidSeverityCodeException("The notice does not support the severity code[" + severity.getSeverityCode() + "]");
		}
		
		List<NoticeInspectionCode> codes = inspectionCodesMap.get(severity);		
		return codes.stream()
			.collect(Collectors.toList())
			.stream()
			.filter(i -> i.getInspectionCode().equals(inspection))
			.findFirst()
			.orElseThrow(()->new InvalidInspectionCodeException("The notice does not support the inspection code[" + inspection.getCode() + "]"));
	}
	
	public List<NoticeInspectionCode> getInspectionCodesForSeverity(SeverityCode severity) {
		return inspectionCodesMap.get(severity);
	}
	
	public boolean isInspectionPermitted(InspectionCode inspection, SeverityCode severity) {
		if(!inspectionCodesMap.containsKey(severity)) {
			return false;
		}
		return inspectionCodesMap.get(severity).stream().anyMatch(c -> c.getInspectionCode().equals(inspection));
	}
	
	public boolean isFinalInspectionForSeverity(InspectionCode inspection, SeverityCode severity) {
		if(!inspectionCodesMap.containsKey(severity)) {
			return false;
		}
		return inspectionCodesMap.get(severity).stream().anyMatch(c -> c.getInspectionCode().equals(inspection) && c.isFinal());
	}
	
	public boolean isFinalInspection(String code) {
		NoticeInspectionCode inspectionCode = this.inspectionCodes.stream().filter(c -> c.isFinal() && c.getCode().contentEquals(code)).findAny().orElse(null);
		return null != inspectionCode;
	}
	
	public int getInspectionFrequency(InspectionCode inspection, SeverityCode severity) 
			throws InvalidSeverityCodeException, InvalidInspectionCodeException, InspectionNotPeriodicException {
		NoticeInspectionCode temp = this.getInspectionCodeForSeverity(inspection, severity);
		if( temp.isPeriodic()  ) {
			return temp.getFrequency();
		}
		throw new InspectionNotPeriodicException("The activity[" + inspection.getCode() + "] is not a periodic activity!");
	}

	public String getInspectionFrequencyUnits(InspectionCode inspection, SeverityCode severity) 
			throws InvalidSeverityCodeException, InvalidInspectionCodeException, InspectionNotPeriodicException {
		NoticeInspectionCode temp = this.getInspectionCodeForSeverity(inspection, severity);
		if( temp.isPeriodic()  ) {
			return temp.getFrequencyUnits();
		}
		throw new InspectionNotPeriodicException("The activity[" + inspection.getCode() + "] is not a periodic activity!");
	}

	public Set<NoticeInspectionCode> getPermittedFinalInspectionCodesForSeverity(SeverityCode severity) {
		List<NoticeInspectionCode> serverityInspectionCodes = inspectionCodesMap.get(severity);
		if(CollectionUtils.isEmpty(serverityInspectionCodes)) {
			return new TreeSet<>();
		}
		return serverityInspectionCodes
				.stream()
				.filter(c -> c.getInspectionCode().isActive() && c.isFinal())
				.collect(Collectors.toSet());
	}
	
	public void setFrequencyForInspection(InspectionCode inspection, SeverityCode severity, int frequency, PERIODIC_TIME_UNIT unit) 
			throws InvalidSeverityCodeException, InvalidInspectionCodeException {
		NoticeInspectionCode temp = this.getInspectionCodeForSeverity(inspection, severity);
		temp.setFrequency(new TimePeriod(frequency, unit));
	}

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false);
    }

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false);
	}
}

