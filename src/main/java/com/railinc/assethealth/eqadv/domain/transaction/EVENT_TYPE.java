package com.railinc.assethealth.eqadv.domain.transaction;

public enum EVENT_TYPE {
    ASSIGNMENT,
    INSPECTION,
    PUBLISH_NOTICE
}
