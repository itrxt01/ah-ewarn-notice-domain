package com.railinc.assethealth.eqadv.domain.equipment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;

import com.railinc.assethealth.eqadv.domain.notice.NoticeItemSet;

public class EquipmentSet<T extends Equipment> implements NoticeItemSet<T>, Serializable {
	private static final long serialVersionUID = -5509904471671607443L;
	private Set<T> equipment = new TreeSet<>();

	public EquipmentSet() {}

	public EquipmentSet(Collection<T> equipment) {
		this.equipment.addAll(equipment);
	}

	public EquipmentSet(EquipmentSet<T> equipmentSet) {
		if (null == equipmentSet) {
			this.equipment = SetUtils.emptySet();
		} else { 
			this.equipment.addAll(equipmentSet.equipment);
		}
	}

	public boolean contains(T equipment) {
		return this.equipment.contains(equipment);
	}
	
	public T getEquipment(String mark, long equipmentId) {
		for(T eq : equipment) {
			if(eq.stencilMark.getValueAsString().equals(mark) && eq.number == equipmentId) {
				return eq;
			}
		}
		return null;
	}

	public void add(T equipment) {
		this.equipment.add(equipment);
	}

	public int size() {
		return this.equipment.size();
	}

	public List<T> getEquipment() {
		return new ArrayList<>(equipment);
	}
	
	public boolean isEmpty() {
		return CollectionUtils.isEmpty(equipment);
	}

	public int addAll(Collection<T> equipment) {
		if (CollectionUtils.isEmpty(equipment)) {
			return 0;
		}

		// Adding to a set, so, must preserve the set size
		// to determine how much the set changed.
		int size = this.size();
		this.equipment.addAll(equipment);
		return this.size() - size;
	}

	public int replaceAll(Collection<T> equipment) {
		this.equipment.clear();
		return this.addAll(equipment);		
	}
}
