package com.railinc.assethealth.eqadv.domain.escalation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonRootName("escalationType")
@JsonPropertyOrder({ "type", "description" })
public class EscalationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6920398095587527463L;
	private String type;
	private String description;

	public EscalationType() {
	}

	public EscalationType(String type, String description) {
		this.type = type;
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false, EscalationType.class, "type");
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass(), "type");
	}

}
