package com.railinc.assethealth.eqadv.domain.pdf;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.railinc.assethealth.eqadv.domain.EWNoticeAttribute;
import com.railinc.assethealth.eqadv.domain.notice.Notice;

public class NoticeFile extends EWNoticeAttribute {
	private static final long serialVersionUID = 6349273109293887533L;
	
	@JsonIgnore
	private Notice notice;
	private String fileName;
	private Integer fileSize;
	private String fileExtension;
	private byte[] fileContent;
	private String md5;
	
	public NoticeFile() {
		
	}

	public NoticeFile(NoticeFile file) {
		super(file);
		this.notice = file.notice;
		this.fileName = file.fileName;
		this.fileSize = file.fileSize;
		this.fileExtension = file.fileExtension;
		this.fileContent = file.fileContent;
		this.md5 = file.md5;
	}
	
	public Notice getNotice() {
		return notice;
	}

	public void setNotice(Notice notice) {
		super.setNoticeId(notice.getId());
		super.setCreateCompany(notice.getCreateCompany());
		super.setCreateUsername(notice.getCreateUsername());
		super.setModifyCompany(notice.getModifyCompany());
		super.setModifyUsername(notice.getModifyUsername());		
		this.notice = notice;
	}
	
	public String getNoticeName() {
		return notice.getNoticeName();
	}

	public String getNextNoticeName() {
		return notice.getNextPublishedName();
	}

	public String getStatus() {
		return notice.getStatus();
	}
	
	public Integer getNoticeNumber() {
		return notice.getNumber();
	}	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getFileSize() {
		return fileSize;
	}

	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}
	
	public ATTACHMENT_TYPE getFileType() {
		return ATTACHMENT_TYPE.forExtension(fileExtension);
	}
	
	public boolean isSameMD5Checksum(NoticeFile file) {
		if(null == file) {
			return false;
		}
		if(null == md5 && null == file.getMd5()) {
			return true;
		}
		if(null == md5 || null == file.getMd5()) {
			return false;
		}
		return md5.equals(file.getMd5());
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public boolean isValid() {
		// Performs a basic validation of the file.		
		return isValidFile() && isSupportedFileType();		
	}

	private boolean isValidFile() {
		return StringUtils.isNotBlank(fileName)
				&& StringUtils.isNotBlank(fileExtension)
				&& this.fileSize > 0
				&& null != this.fileContent 
				&& this.fileContent.length > 0
				&& this.fileSize == this.fileContent.length
				&& this.isSupportedFileType();
	}
	
	public boolean isEquivalent(NoticeFile file) {
		return this.isValid()
			&& null != file
			&& file.isValid()
			&& this.getFileName().equals(file.getFileName())
			&& this.isSameMD5Checksum(file);
	}
	
	
	public boolean isSupportedFileType() {
		return ATTACHMENT_TYPE.UNKNOWN != getFileType();
	}
}
