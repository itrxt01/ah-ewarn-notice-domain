package com.railinc.assethealth.eqadv.domain.equipment;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.railinc.assethealth.eqadv.domain.exception.EWDeserializationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidOracleBooleanFlagException;

public class MechanicalDesignationIndicatorDeserializer extends StdDeserializer<MECHANICAL_DESIGNATION_INDICATOR> {
	private static final long serialVersionUID = 1L;

	public MechanicalDesignationIndicatorDeserializer() {
		this(null);
	}

	public MechanicalDesignationIndicatorDeserializer(Class<MECHANICAL_DESIGNATION_INDICATOR> indicator) {
		super(indicator);
	}

	@Override
	public MECHANICAL_DESIGNATION_INDICATOR deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException {
		JsonNode node = jp.getCodec().readTree(jp);
		if(node.isObject()) {
			String code = node.get("code").asText();
			try {
				return MECHANICAL_DESIGNATION_INDICATOR.forCode(code);
			} catch (InvalidOracleBooleanFlagException e) {
				throw new EWDeserializationException(e, String.format("Error deserializing Mechanical Designation Indicator for code: '%s'", code));
			}
			
		} else {
			String flag =  node.asText();	
			try {
				return MECHANICAL_DESIGNATION_INDICATOR.forFlag(flag);
			} catch (InvalidOracleBooleanFlagException e) {
				throw new EWDeserializationException(e, String.format("Error deserializing Oracle Boolean flag for value: '%s'", flag));
			}
		}
	}
}
