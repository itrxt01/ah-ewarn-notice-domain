package com.railinc.assethealth.eqadv.domain.notice;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;
import com.railinc.assethealth.eqadv.domain.EWNoticeAttribute;

/**
 * This class represents the equipment score factors that may be used for
 * calculating an Equipment Priority Score. These are optional and are not
 * required for every notice.
 * 
 * @author ron
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NoticeEqmtScoreFactors extends EWNoticeAttribute {
	private static final long serialVersionUID = -8197280063678926724L;

	Integer hazmatCarried;
	Integer nonHazmatCarried;
	Integer lastMovementLTE90Days;
	Integer lastMovementLTE180Days;
	Integer lastMovementGT180Days;
	String carType;
	Integer carTypeEquals;
	Integer initialMaxPercentByRoad;
	Integer initialEscalationDelay;

	public Integer getHazmatCarried() {
		return hazmatCarried;
	}

	public void setHazmatCarried(Integer hazmatCarried) {
		this.hazmatCarried = hazmatCarried;
	}

	public Integer getNonHazmatCarried() {
		return nonHazmatCarried;
	}

	public void setNonHazmatCarried(Integer nonHazmatCarried) {
		this.nonHazmatCarried = nonHazmatCarried;
	}

	public Integer getLastMovementLTE90Days() {
		return lastMovementLTE90Days;
	}

	public void setLastMovementLTE90Days(Integer lastMovementLTE90Days) {
		this.lastMovementLTE90Days = lastMovementLTE90Days;
	}

	public Integer getLastMovementLTE180Days() {
		return lastMovementLTE180Days;
	}

	public void setLastMovementLTE180Days(Integer lastMovementLTE180Days) {
		this.lastMovementLTE180Days = lastMovementLTE180Days;
	}

	public Integer getLastMovementGT180Days() {
		return lastMovementGT180Days;
	}

	public void setLastMovementGT180Days(Integer lastMovementGT180Days) {
		this.lastMovementGT180Days = lastMovementGT180Days;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public Integer getCarTypeEquals() {
		return carTypeEquals;
	}

	public void setCarTypeEquals(Integer carTypeEquals) {
		this.carTypeEquals = carTypeEquals;
	}

	public Integer getInitialMaxPercentByRoad() {
		return initialMaxPercentByRoad;
	}

	public void setInitialMaxPercentByRoad(Integer initialMaxPercentByRoad) {
		this.initialMaxPercentByRoad = initialMaxPercentByRoad;
	}

	public Integer getInitialEscalationDelay() {
		return initialEscalationDelay;
	}

	public void setInitialEscalationDelay(Integer initialEscalationDelay) {
		this.initialEscalationDelay = initialEscalationDelay;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE, false);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false, EWDomainAbstract.class);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, EWDomainAbstract.class);
	}
}
