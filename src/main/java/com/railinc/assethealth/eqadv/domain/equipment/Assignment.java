package com.railinc.assethealth.eqadv.domain.equipment;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.time.DateUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.railinc.assethealth.eqadv.domain.Constants;
import com.railinc.assethealth.eqadv.domain.exception.InvalidAssignedEquipmentException;
import com.railinc.assethealth.eqadv.domain.mark.Mark;
import com.railinc.assethealth.eqadv.domain.mark.MarkFactory;
import com.railinc.assethealth.eqadv.domain.notice.Notice;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * AssignedEquipment are immutable.  They are read from the DB and instantiated
 * from table values.  Use the static Builder to construct an AssignedEquipment.
 * @author ron taylor
 *
 */
@JsonPropertyOrder(value= {"id", "severity", "noticeNumber", "ein", "equipmentId"
		, "assignedDate", "assignedReporter", "escalationDate", "escalationLevel"})
@JsonRootName(value = "assignedEquipment")
public class Assignment extends Equipment {
	private static final long serialVersionUID = -1982231834193099777L;
	
	int	noticeNumber;
	Notice  notice;
	Date	assignedDate;
	Date	escalationDate;
	Mark	assignmentReporter;
	SeverityLevel severity;
	SeverityLevel escalationLevel;
	Integer delay;

	private Assignment(Equipment equipment) { 
		super(equipment);
	}
	
	public int getNoticeNumber() {
		return noticeNumber;
	}
	
	public void setNoticeNumber(int noticeNumber) {
		this.noticeNumber = noticeNumber;
	}

	@JsonIgnore
	public String getStencilMark() {
		return super.getRoadMark();
	}
	
	@Override
	@JsonIgnore
	public String getNumberAsJustifiedString() {
		return super.getNumberAsJustifiedString();
	}
	
	public String getAssignmentReporter() {
		return assignmentReporter.getValueAsString();
	}
	
	public Date getAssignedDate() {
		return assignedDate;
	}	

	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}

	@JsonInclude(Include.NON_NULL)
	@JsonFormat(pattern=Constants.DATE_FORMAT, timezone=Constants.TIME_ZONE)
	public Date getEscalationDate() {
		if(null == escalationDate) {
			if((null != notice && this.notice.hasNeverBeenPublished() 
					|| (noticeNumber >= Constants.DRAFT_NOTICE_NUMBER_START)) && null != this.delay && 0 < this.delay) {
				return DateUtils.addDays(new Date(), this.delay); 
			} else {
				return this.severity.getEscalationDate(assignedDate);
			}
		}
		return escalationDate;
	}
	
	@JsonIgnore
	public String getEscalationDateString() {
		if (null == this.getEscalationDate()) {
			return null;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		return simpleDateFormat.format(this.getEscalationDate());
	}
	
	@JsonFormat(pattern=Constants.DATE_FORMAT, timezone=Constants.TIME_ZONE)
	public void setEscalationDate(Date date) {
		this.escalationDate = date;
	}
		
	@JsonInclude(Include.NON_NULL)
	public SeverityLevel getSeverity() {
		return severity;
	}
	
	@JsonInclude(Include.NON_NULL)
	public SeverityLevel getEscalationLevel() {
		if(null != notice && null != notice.getSeverityLevels()) {
			return notice.getSeverityLevels().getEscalationLevel(this);
		}
		// doesn't escalate
		return null;
	}
	
	public void setEscalationLevel(SeverityLevel escalationLevel) {
		this.escalationLevel = escalationLevel;
	}

	public Integer getDelay() {
		return delay;
	}

	public void setDelay(Integer delay) {
		this.delay = delay;
	}
	
	public boolean isTimeToEscalate() {
		return null != getEscalationDate() && getEscalationDate().compareTo(new Date()) <= 0;
	}	

	public boolean  escalate() {
		if(isTimeToEscalate()) {
			severity = this.escalationLevel;
			assignedDate = new Date();
		} 
	
		return isTimeToEscalate();
	} 
	

	/**
	 * This AssignedEquipment Builder ensures the immutable AssignedEquipment is built with
	 * the required attributes set.
	 * @author ron taylor
	 *
	 */
	public static class Builder{
		long	id;
		Equipment	equipment;
		Notice	notice;
		int		noticeNumber;
		long 	noticeId;
		Date	assignedTimestamp;
		Integer delay;
		String 	createUserName;
		String createCompany;
		Date 	createTimestamp;
		Date	escalationDate;
		Mark	assignedCompany;
		SeverityLevel severity;
		SeverityLevel escalationLevel;
		String 	modifyUserName;
		String modifyCompany;
		Date 	modifyTimestamp;
		Assignment assignment;

		public Builder(Equipment equipment) {
            this.equipment = equipment;
        }
		public Builder(Equipment equipment, Assignment assignment) {
            this.equipment = equipment;
            this.assignment = assignment;
        }
		public Builder(Assignment assignment) {
            this.assignment = assignment;
        }
		public Builder(String equipmentMark, long equipmentId, long ein) {
            this.equipment = new Equipment(equipmentMark, equipmentId, ein);
        }
		public Builder withId(long id) {
			this.id = id;
			return this;
		}
		public Builder escalatesOn(Date escalationDate) {
			this.escalationDate = escalationDate;
			return this;
		}
		public Builder escalatesTo(SeverityLevel escalationLevel) {
			this.escalationLevel = escalationLevel;
			return this;
		}
        public Builder onNotice(int noticeNumber){
        	this.noticeNumber = noticeNumber;
            this.notice = new Notice();
            this.notice.setNumber(noticeNumber);
            return this;  
        }
        public Builder onNoticeWithId(long noticeId){
        	this.noticeId = noticeId;
            this.notice = new Notice();
            this.notice.setId(noticeId);
            return this;  
        }
        public Builder onNotice(Notice notice){
            this.notice = notice;
            this.noticeId = notice == null ? 0 : notice.getId();
            this.noticeNumber = notice == null ? 0 : notice.getNumber();
            return this;  
        }
        public Builder assignedOn(Date timestamp){
            this.assignedTimestamp = timestamp;
            return this;
        }
        public Builder withDelay(Integer delay){
            this.delay = delay;
            return this;
        }
        public Builder assignedBy(String mark) {
        	this.assignedCompany = MarkFactory.newMark(mark);
        	return this;
        }
        public Builder withSeverityLevel(SeverityLevel code) {
        	this.severity = code;
        	return this;
        }
        public Builder withSeverityCode(String code) {
        	this.severity = new SeverityLevel(code);
        	return this;
        }        
        public Builder withModifyUserName(String modifyUserName) {
			this.modifyUserName = modifyUserName;
			return this;
		}
		public Builder withModifyTimestamp(Date modifyTimestamp) {
			this.modifyTimestamp = modifyTimestamp;
			return this;
		}
		public Builder withCreateUserName(String createUserName) {
			this.createUserName = createUserName;
			return this;
		}
		public Builder withCreateCompany(String createCompany) {
			this.createCompany = createCompany;
			return this;
		}
		public Builder withCreateTimestamp(Date createTimestamp) {
			this.createTimestamp = createTimestamp;
			return this;
		}
		public Builder withModifyCompany(String modifyCompany) {
			this.modifyCompany = modifyCompany;
			return this;
		}
		private Assignment build(Assignment from, Assignment to) {
			to.id = from.id;
        	to.noticeNumber = from.noticeNumber;
        	to.notice = from.notice;
        	to.assignmentReporter = MarkFactory.newMark(from.assignmentReporter);
        	to.assignedDate = from.assignedDate;
        	to.delay = from.delay;
        	to.severity = from.severity;
    		to.escalationLevel = from.escalationLevel;
    		to.escalationDate = from.escalationDate;
        	to.createUsername = from.createUsername;
        	to.createCompany = from.createCompany;
        	to.createdTimestamp = from.createdTimestamp;
        	to.modifyUsername = from.modifyUsername;
        	to.modifyCompany = from.modifyCompany;
        	to.modifiedTimestamp = from.modifiedTimestamp;
        	return to;
			
		}
        public Assignment build() throws InvalidAssignedEquipmentException {
        	if(null != assignment && null != equipment) {
        		return this.build(assignment, new Assignment(equipment));
        	}
        	if(null != assignment) {
        		return this.build(assignment, new Assignment(assignment));
        	}
        	if((null == notice || (0 == noticeId && 0 == noticeNumber))) {
        		throw new InvalidAssignedEquipmentException("The value[" + noticeNumber +"] is not a valid Notice Number!");        		
        	}
        	if(null == equipment) {
        		throw new InvalidAssignedEquipmentException("Equipment must be set to create and Assign Equipment!");        		        		
        	}
        	if(null == severity) {
        		throw new InvalidAssignedEquipmentException("Severity must be set to create and Assign Equipment!");        		        		        		        		
        	}
        	
        	Assignment assignedEquipment = new Assignment(equipment);
        	assignedEquipment.id = id;
        	assignedEquipment.notice = notice;
        	assignedEquipment.noticeId = noticeId;
        	assignedEquipment.noticeNumber = noticeNumber;
        	assignedEquipment.assignmentReporter = assignedCompany;
        	assignedEquipment.assignedDate = assignedTimestamp;
        	assignedEquipment.delay = delay;
        	assignedEquipment.severity = severity;
   			assignedEquipment.escalationLevel = (null != severity.getEscalationLevel() ? severity.getEscalationLevel() : escalationLevel);
        	assignedEquipment.escalationDate = escalationDate;
        	assignedEquipment.createCompany = createCompany;
        	assignedEquipment.createUsername = createUserName;
        	assignedEquipment.createdTimestamp = createTimestamp;
			assignedEquipment.modifyCompany = modifyCompany;
        	assignedEquipment.modifyUsername = modifyUserName;
        	assignedEquipment.modifiedTimestamp = modifyTimestamp;
        	return assignedEquipment;
        }
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
	
	@Override
    public int hashCode() {
          return HashCodeBuilder.reflectionHashCode(this);
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false, super.getClass(), "id", "notice");
    }
}
