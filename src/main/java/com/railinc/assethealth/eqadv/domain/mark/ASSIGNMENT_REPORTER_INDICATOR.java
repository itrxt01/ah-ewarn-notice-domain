package com.railinc.assethealth.eqadv.domain.mark;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.railinc.assethealth.eqadv.domain.exception.InvalidAssignmentReporterIndicatorException;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonIgnoreProperties({ "isRetrieveFromDatabase" })
@JsonRootName("AssignmentReporterIndicator")
@JsonDeserialize(using=AssignmentReporterIndicatorDeserializer.class)
public enum ASSIGNMENT_REPORTER_INDICATOR {
	SPECIFIED      ("S", true,  "Specified",                       "Only a specified list of marks can assign equipment to this notice."),
	AAR_ONLY       ("A", false, "AAR Only",                        "Only the AAR can assign equipment to this notice."),
	EQUIPMENT_OWNER("E", false, "Equipment Owner",                 "Only the equipment owner in Umler can assign equipment to this notice."),
	INTERNAL_ONLY  ("I", true,  "Internal Rail Inc. Systems Only", "Only specified internal RailInc systems can assign.");
	
	public final String code;
	public final boolean isRetrieveFromDatabase;
	public final String value;
	public final String description;
	
	private static final int EXACT_IND_LENGTH = 1;
	private static final String MSG_FORMAT = "The value [%s] is not a valid Assignment Reporter Indicator!";
	
	private ASSIGNMENT_REPORTER_INDICATOR(String ind, boolean getFromDB, String desc, String tooltip){
		this.code = ind;
		this.isRetrieveFromDatabase = getFromDB;
		this.value = desc;
		this.description = tooltip;
	}
	
	public static ASSIGNMENT_REPORTER_INDICATOR forIndicator(String ind) throws InvalidAssignmentReporterIndicatorException {
		if(StringUtils.isBlank(ind)) {
			throw new InvalidAssignmentReporterIndicatorException(String.format(MSG_FORMAT, ind));
		}
		
		String trimmed = ind.trim().toUpperCase();
		if(EXACT_IND_LENGTH != trimmed.length()) {
			throw new InvalidAssignmentReporterIndicatorException(String.format(MSG_FORMAT, ind));
		}
		
		for(ASSIGNMENT_REPORTER_INDICATOR indicator : ASSIGNMENT_REPORTER_INDICATOR.values()) {
			if(StringUtils.equals(indicator.code, trimmed)) {
				return indicator;
			}
		}
		
		throw new InvalidAssignmentReporterIndicatorException(String.format(MSG_FORMAT, ind));
	}	
}
