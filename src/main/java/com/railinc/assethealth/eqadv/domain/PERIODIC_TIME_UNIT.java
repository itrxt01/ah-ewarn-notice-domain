package com.railinc.assethealth.eqadv.domain;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonPropertyOrder({ "typeName", "daysInPeriod" })
public enum PERIODIC_TIME_UNIT {
	YEARS(365, "Years"),
	MONTHS(30, "Months"),
	DAYS(1, "Days");
	
	public	final String 	units;
	public final int 		daysPerUnit;
	public final String		daysAsString;
	private static final String ERROR_MSG = "The value [%s] is not a valid value for a Time Period Unit (years, months, days)!";
	
	PERIODIC_TIME_UNIT(int days, String typeName){
		this.daysPerUnit = days;
		this.units = typeName;
		this.daysAsString = Integer.toString(days);
	}
	
	public static PERIODIC_TIME_UNIT getTimeUnitForFrequency(int days) {
		if (days >= 365 && 0 == days % 365) {
			return YEARS;
		}		
		if (days >= 30 && 0 == days % 30) {
			return MONTHS;
		}		
		return DAYS;
	}

	public static PERIODIC_TIME_UNIT getTimeUnitForName(String name) throws InvalidPeriodicTimeUnitException {
		if (StringUtils.isBlank(name)) {
			throw new InvalidPeriodicTimeUnitException(String.format(ERROR_MSG, name));
		}

		String trimmed = name.trim().toLowerCase();
		for(PERIODIC_TIME_UNIT ptu : PERIODIC_TIME_UNIT.values()) {
			if (StringUtils.equalsIgnoreCase(ptu.units, trimmed)) {
				return ptu;
			}
		}		
		throw new InvalidPeriodicTimeUnitException(String.format(ERROR_MSG, name));
	}
	
	public int getNumberOfTimeUnits(int totalDays) {
		return totalDays / this.daysPerUnit;
	}	
	
	public int getNumberOfDays(int timeUnits) {
		return timeUnits * this.daysPerUnit;
	}	
}
