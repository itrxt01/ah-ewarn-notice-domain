package com.railinc.assethealth.eqadv.domain.pdf;

import java.util.Arrays;

import com.railinc.assethealth.eqadv.domain.exception.InvalidContentTypeException;

public enum CONTENT_TYPE {

	TXT("txt"),
	PDF("pdf");

	private String fileType;

	CONTENT_TYPE(String fileType) {
		this.fileType = fileType;
	}

	public String getFileType() {
		return this.fileType;
	}

	public static CONTENT_TYPE forType(String type) throws InvalidContentTypeException {
		return Arrays.stream(CONTENT_TYPE.values())
				.filter(contentType -> contentType.getFileType().equalsIgnoreCase(type))
				.findFirst()
				.orElseThrow(() -> new InvalidContentTypeException("Content type " + type + " is not valid"));
	}
}
