package com.railinc.assethealth.eqadv.domain.mark;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.railinc.assethealth.eqadv.domain.EWNoticeAttribute;
import com.railinc.assethealth.eqadv.domain.notice.NoticeSetItem;

/**
 * Marks must be no longer than 4 characters in length and match the patterns:
 * AAAA - road mark
 * AAA  - shop mark
 * Cxxx, Dxxx - Shop mark
 * Axxx - Running Repair agent mark
 * 
 * This is just an initial validation.  Final validation is against the list of marks retrieved from Umler.
 * 
 * @author ron taylor
 *
 */
@JsonIgnoreType
public abstract class Mark extends EWNoticeAttribute implements NoticeSetItem, Comparable<Mark> {
	private static final Logger LOGGER = LoggerFactory.getLogger(Mark.class);
    private static final Set<String> AAR_MARK_STRINGS= new HashSet<>(Arrays.asList("AAR", "RAIL"));
    private static final long serialVersionUID = 1L;
	private static final char PAD_CHARACTER = ' ';
	private static final int MAX_LENGTH = 4;	
	
	private String initial;
	private String description;
	private Mark parent;
	
	protected abstract String getRegex();
	protected abstract String getErrorMessageFormat();
	protected abstract boolean getIsEquipmentOwnerMark();
	
	protected Mark newInstance(String mark) {
		return MarkFactory.newMark(mark);
	}
	
	protected Mark(String mark) {
		if(!this.isValid(mark)) {
			LOGGER.warn(String.format(this.getErrorMessageFormat(), mark));				// NOSONAR - flag evaluated in library
		} else if(StringUtils.isNotBlank(mark)) {
			this.initial = mark.trim().toUpperCase();			
		} else {
			LOGGER.warn(String.format(this.getErrorMessageFormat(), mark));				// NOSONAR - flag evaluated in library
			this.initial = null;
		}
	}
	
	protected Mark(String mark, Mark parent) {
		if(!this.isValid(mark)) {
			LOGGER.warn(String.format(this.getErrorMessageFormat(), mark));				// NOSONAR - flag evaluated in library
		} else if(StringUtils.isNotBlank(mark)) {
			this.initial = mark.trim().toUpperCase();			
		} else {
			LOGGER.warn(String.format(this.getErrorMessageFormat(), mark));				// NOSONAR - flag evaluated in library
			this.initial = null;
		}
		this.parent = parent;
	}
	
	protected Mark(String mark, String parent) {
		if(!this.isValid(mark)) {
			LOGGER.warn(String.format(this.getErrorMessageFormat(), mark));				// NOSONAR - flag evaluated in library
		}
		
		if(!this.isValid(parent)) {
			LOGGER.warn(String.format(this.getErrorMessageFormat(), parent));			// NOSONAR - flag evaluated in library
		}
		
		this.initial = mark.trim().toUpperCase();
		this.parent = this.newInstance(mark);		
	}

	protected Mark(Mark mark) {
		if(null == mark) {
			LOGGER.warn(String.format(this.getErrorMessageFormat(), mark));				// NOSONAR - flag evaluated in library
		} else {
			this.initial = mark.initial;
			this.parent = mark.parent;			
		}
	}

	@JsonIgnore
	public boolean isValid() {
		if(null != parent) {
			return isValid(this.initial) && parent.isValid();
		}
		return isValid(this.initial);
	}
	
	
	@JsonIgnore
	protected boolean isValid(String mark) {
		return StringUtils.isNotBlank(mark) && mark.toUpperCase().trim().length() <= MAX_LENGTH && mark.toUpperCase().trim().matches(this.getRegexPattern());  
	}
	
	public void setParent(Mark parent) {
		this.parent = parent;
	}
	
	@JsonIgnore
	public boolean isParent(Mark parent) {
		return this.parent.equals(parent);
	}
	
	@JsonIgnore
	public Mark getParent() {
		return this.parent;
	}
	
	@JsonIgnore
	public boolean isAAR() {
		return  AAR_MARK_STRINGS.stream().anyMatch(m -> m.equalsIgnoreCase(initial) || (null != this.getParent() && m.equalsIgnoreCase(this.getParent().initial)));
	}
	
	@JsonIgnore
	public String getDescription() {
		return this.description;
	}
	
	@JsonIgnore
	public void setDescription(String desc) {
		this.description = desc;
	}
	
	@Override
	@JsonProperty(value="mark")
	public String getValueAsString() {
		return initial;
	}

	@Override
	@JsonIgnore
	public String getValueAsJustifiedString() {
		return StringUtils.rightPad(initial, MAX_LENGTH, PAD_CHARACTER);
	}
	
	@JsonIgnore
	public String getRegexPattern() {
		return this.getRegex();
	}
	
	@JsonIgnore
	protected String getMarkInitial() {
		return initial;
	}
	
	@JsonIgnore
	public boolean isEquipmentOwningMark() {
		return this.getIsEquipmentOwnerMark();
	}

	@Override
	public int compareTo(Mark obj) {
		final int EQUAL = 0;
		final int GREATER = 1;

        if (this == obj) return EQUAL;
        if (obj == null) return GREATER;
        Mark that = obj;
		return this.initial.compareTo(that.initial);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false, Mark.class, "description", "parent");
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, Mark.class, "description", "parent");
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
