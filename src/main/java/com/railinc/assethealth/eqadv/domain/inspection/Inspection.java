package com.railinc.assethealth.eqadv.domain.inspection;

import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.time.DateUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;
import com.railinc.assethealth.eqadv.domain.EWDomainWithID;
import com.railinc.assethealth.eqadv.domain.equipment.Assignment;
import com.railinc.assethealth.eqadv.domain.equipment.Equipment;
import com.railinc.assethealth.eqadv.domain.equipment.EquipmentUtils;

public class Inspection extends EWDomainWithID {

	private static final long serialVersionUID = -8176003249684467221L;
	private Long noticeId;
	private Integer noticeNumber;

	private static final int EIN_LEN = 10;
	private static final int EQUIP_NUMBER_LEN = 10;

	private String noticeCategory;
    private String inspectionCode;
    private Long ein;
    private String equipInitial;
    private Long equipNumber;
    private Date inspectionDate;
    private String inspectionReporter;
    private String activeFlag;
    
    @JsonIgnore
    private Equipment equipment;


    public Inspection() {}
    
    // Inspections are always related to an assignment
    public Inspection(Assignment assignment) {
    	this.noticeNumber = assignment.getNoticeNumber();
    	this.ein = assignment.getEin();
    	if(StringUtils.isNotBlank(assignment.getEquipmentId())) {
        	String equipmentId = assignment.getEquipmentId();    	
        	this.equipInitial = EquipmentUtils.extractMark(equipmentId);    	
        	this.equipNumber = EquipmentUtils.extractNumberAsLong(equipmentId);    		
    	}
    }

    public Inspection(Equipment equipment, Inspection copyInspection) {
        this.equipment = equipment;
        id = copyInspection.getId();
        ein = copyInspection.getEin();
        noticeId = copyInspection.getNoticeId();
        noticeNumber = copyInspection.getNoticeNumber();
        noticeCategory = copyInspection.getNoticeCategory();
        inspectionCode = copyInspection.getInspectionCode();
        inspectionDate = copyInspection.getActivityDate();
        inspectionReporter = copyInspection.getActivityReporter();
        activeFlag = copyInspection.getActiveFlag();
        createCompany = copyInspection.getCreateCompany();
        createUsername = copyInspection.getCreateUsername();
        modifyCompany = copyInspection.getModifyCompany();
        modifyUsername = copyInspection.getModifyUsername();
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }
    
    @JsonIgnore
    public String getEquipmentId() {
    	return equipment.getEquipmentId();
    }
    
    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(long noticeId) {
        this.noticeId = noticeId;
    }

    public Integer getNoticeNumber() {
		return noticeNumber;
	}

	public void setNoticeNumber(int noticeNumber) {
		this.noticeNumber = noticeNumber;
	}

    public String getNoticeCategory() {
        return noticeCategory;
    }

    public void setNoticeCategory(String noticeCategory) {
        this.noticeCategory = noticeCategory;
    }

    public String getInspectionCode() {
        return inspectionCode;
    }

    public void setInspectionCode(String inspectionCode) {
        this.inspectionCode = inspectionCode;
    }

    public Long getEin() {
        return ein;
    }

    @JsonIgnore
    public String getEinAsString() {
        return padNumberWithLeadingZeros(ein, EIN_LEN);
    }

    public void setEin(long ein) {
        this.ein = ein;
    }

    public String getEquipInitial() {
        return equipInitial;
    }

    public void setEquipInitial(String equipInitial) {
        this.equipInitial = equipInitial;
    }

    public Long getEquipNumber() {
        return equipNumber;
    }

    public void setEquipNumber(long equipNumber) {
        this.equipNumber = equipNumber;
    }

    @JsonIgnore
    public String getEquipNumberAsString() {
        return padNumberWithLeadingZeros(equipNumber, EQUIP_NUMBER_LEN);
    }

    public Date getActivityDate() {
        return inspectionDate;
    }

    public void setActivityDate(Date activityDate) {
        this.inspectionDate = activityDate;
    }

    public String getActivityReporter() {
        return inspectionReporter;
    }

    public void setActivityReporter(String activityReporter) {
        this.inspectionReporter = activityReporter;
    }

    public String getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    @Override
    public boolean equals(Object obj) {
		boolean result = EqualsBuilder.reflectionEquals(this, obj, false, EWDomainAbstract.class, "inspectionDate");
		
		// Only want to compare the date portion excluding the time element
		if (obj == this)
            return true;
        if (!(obj instanceof Inspection))
            return false;
        Inspection other = (Inspection) obj;
        result = result && ((this.inspectionDate == null && other.inspectionDate == null) 
        		|| (this.inspectionDate != null && DateUtils.isSameDay(this.inspectionDate, other.inspectionDate)));
        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, noticeId, inspectionCode, ein, equipInitial, equipNumber, inspectionDate, inspectionReporter, activeFlag);
    }
}
