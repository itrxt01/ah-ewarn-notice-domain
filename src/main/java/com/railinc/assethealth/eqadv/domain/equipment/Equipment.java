package com.railinc.assethealth.eqadv.domain.equipment;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.railinc.assethealth.eqadv.domain.EWNoticeAttributeWithID;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.mark.Mark;
import com.railinc.assethealth.eqadv.domain.mark.MarkFactory;
import com.railinc.assethealth.eqadv.domain.notice.NoticeSetItem;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Equipment is semi-immutable.   The identifying characteristics may not be changed.
 * However, the EIN lineage and equipment designation may be updated in Umler, so
 * they are lazy loaded from Umler if they are needed.  Equipment Designation may 
 * be required upon assignment if the notice is restricted to particular equipment.
 * The EIN Lineage is always retrieved when assigning equipment to a notice and
 * when an inspection is reported.
 *   
 * @author ron taylor
 *
 */
@JsonPropertyOrder(value= {"mark", "number", "ein"})
@SuppressWarnings({"squid:S2629"})
public class Equipment  extends EWNoticeAttributeWithID implements NoticeSetItem, Comparable<Equipment> {
	private static final Logger LOGGER = LoggerFactory.getLogger(Equipment.class);
	private static final long serialVersionUID = -5942982619006559606L;
	
	private static final String INVALID_NUMNBER_FORMAT = "The value[%d] is not a valid equipment number!";
	private static final String INVALID_EIN_FORMAT = "The value[%d] is not a valid EIN!";
	private static final String INVALID_MARK_FORMAT = "The mark[%s] is not a valid equipment road mark!";
	
	
	// These are for both EIN and Equipment ID validation.
	private static final long MIN_NUMBER = 1L;
	private static final long MAX_NUMBER = 9_999_999_999L;
	private static final int EQUIPMENT_NUMBER_LEN = 10;
	
	Mark	umlerOwner = null;
	Mark	stencilMark = null;
	Long	number = null;
	Long	ein = null;
	String 	equipmentGroup = null;
	String  lessee = null;
	String  maintenanceParty = null;
	String  umlerStatus = null;
	Date	lastMovement;
	boolean	lastCommodityHazmat;
	String commodity;
	String handlingCarrier;

	MechanicalDesignation mechanicalDesignation = null;
	transient EquipmentSet<Equipment> einLineage = new EquipmentSet<>();
	
	// Only used to facilitate the Supplier interface for a deep copy of a collection of Equipment.
	protected Equipment() {}
	
	public Equipment(Long ein)  {
		this.setEin(ein);
	}
	
	public Equipment(String mark, Long number) {
		this.setStencilMark(mark);
		this.setNumber(number);
	}
	
	public Equipment(String mark, Long number, Long ein) {
		this.setStencilMark(mark);
		this.setNumber(number);
		this.setEin(ein);
	}
	
	public Equipment(String equipmentId) {
		this.setStencilMark(EquipmentUtils.extractMark(equipmentId));
		this.setNumber(EquipmentUtils.extractNumberAsLong(equipmentId));
	}
	
	public Equipment(Equipment eq) {
		this.umlerOwner = eq.umlerOwner;
		this.stencilMark = eq.stencilMark;
		this.number = eq.number;
		this.ein = eq.ein;
		this.setMechanicalDesignation(eq.getMechanicalDesignation());
		this.equipmentGroup = eq.equipmentGroup;
		this.lessee = eq.lessee;
		this.maintenanceParty = eq.maintenanceParty;
		this.umlerStatus = eq.umlerStatus;
	}
	
	private void setStencilMark(String stencil) {
		Mark temp = null;
		temp = MarkFactory.newMark(stencil);
		if(null == temp || !temp.isEquipmentOwningMark()) {
			LOGGER.warn(String.format(INVALID_MARK_FORMAT, stencil));				// NOSONAR - is warning check done in library
		}
		this.stencilMark = temp;
	}
	
	private void setNumber(Long number) {
		if(number < MIN_NUMBER || number > MAX_NUMBER) {
			LOGGER.warn(String.format(INVALID_NUMNBER_FORMAT, number));				// NOSONAR - is warning check done in library
		}		
		this.number = number;
	}
	
	public void setEin(Long ein) {
		if(ein < MIN_NUMBER || ein > MAX_NUMBER) {
			LOGGER.warn(String.format(INVALID_EIN_FORMAT, ein));					// NOSONAR - is warning check done in library
		}		
		this.ein = ein;
	}
	
	private boolean isValidEin() {
		return (null != ein && ein >= MIN_NUMBER && ein <= MAX_NUMBER);
	}

	private boolean isValidNumber() {
		return (null != number && number >= MIN_NUMBER && number <= MAX_NUMBER);
	}
	
	@JsonIgnore
	public void setUmlerOwner(String mark) {
		Mark temp = MarkFactory.newMark(mark);
		if(!temp.isEquipmentOwningMark()) {
			LOGGER.warn(String.format(INVALID_MARK_FORMAT, mark));					// NOSONAR - is warning check done in library
		}
		this.umlerOwner = temp;
	}
	
	@JsonIgnore
	public String getUmlerOwner() {
		return this.umlerOwner.getValueAsString();
	}
		
	/**
	 * Creates a String for Equipment ID like:
	 * NS  0000000001
	 * BNSF0000000001
	 * @return EquipmentID
	 */
	public String getEquipmentId() {
		if(null != stencilMark && StringUtils.isNotBlank(stencilMark.getValueAsJustifiedString())) {
			return stencilMark.getValueAsJustifiedString() + padNumberWithLeadingZeros(number, EQUIPMENT_NUMBER_LEN);
		}
		return null;
	}

	public Mark getMark() {
		return stencilMark;
	}
	
	/**
	 * Creates a String for Equipment ID like:
	 * NS    1
	 * BNSF 51
	 * @return EquipmentID
	 */
	public String getJustifiedEquipmentId(int maxNumberLen) {
		return stencilMark.getValueAsJustifiedString() + rightJustifyNumber(number, maxNumberLen);
	}
	
	
	/**
	 * Creates a String for Equipment ID like:
	 * NS1
	 * BNSF1
	 * @return Abbreviated EquipmentID
	 */
	@JsonIgnore
	public String getShortId() {
		return stencilMark.getValueAsString() + numberAsString(number);
	}

	@JsonIgnore
	public String getEinAsString() {
		return padNumberWithLeadingZeros(ein, EQUIPMENT_NUMBER_LEN);
	}
	
	@JsonProperty("equipInitial")
	public String getRoadMark() {
		return stencilMark.getValueAsString();
	}
	
	@JsonProperty("equipNumber")
	public Long getNumber() {
		return number;
	}

	public Long getEin() {
		return ein;
	}

	@JsonIgnore
	public String getEquipmentGroup() {
		return equipmentGroup;
	}

	public void setEquipmentGroup(String equipmentGroup) {
		this.equipmentGroup = equipmentGroup;
	}
	@JsonIgnore
	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	@JsonIgnore
	public String getHandlingCarrier() {
		return handlingCarrier;
	}

	public void setHandlingCarrier(String handlingCarrier) {
		this.handlingCarrier = handlingCarrier;
	}

	@Override
	@JsonIgnore
	public String getValueAsJustifiedString() {
		return stencilMark.getValueAsJustifiedString() + rightJustifyNumber(number, EQUIPMENT_NUMBER_LEN);
	}	
	
	@JsonIgnore
	public String getMarkAsJustifiedString() {
		return stencilMark.getValueAsJustifiedString();
	}	
	
	@JsonIgnore
	public String getValueAsJustifiedString(int len) {
		return stencilMark.getValueAsJustifiedString() + rightJustifyNumber(number, len);
	}	
	
	@JsonIgnore
	public String getNumberAsJustifiedString(int len) {
		return rightJustifyNumber(number, len);
	}	

	
	@JsonIgnore
	public String getNumberAsJustifiedString() {
		return padNumberWithLeadingZeros(number, EQUIPMENT_NUMBER_LEN);
	}	
	
	public void setEinLineage(EquipmentSet<Equipment> equipment){
		this.einLineage = equipment;
	}
	
	public void setMechanicalDesignation(MechanicalDesignation designation) {
		this.mechanicalDesignation = designation;
	}

	public void setMechanicalDesignation(String designation) throws InvalidMechanicalDesignationException {
		this.mechanicalDesignation = new MechanicalDesignation(designation);
	}
	
	@JsonIgnore
	public boolean isMechanicalDesignation(MechanicalDesignation designation) {
		return this.mechanicalDesignation.equals(designation);
	}
	
	@JsonIgnore
	public boolean isMechanicalDesignation(String designation) throws InvalidMechanicalDesignationException {
		return this.isMechanicalDesignation(new MechanicalDesignation(designation));
	}

	@JsonIgnore
	public MechanicalDesignation getMechanicalDesignation() {
		return mechanicalDesignation;
	}
	
	@JsonInclude(Include.NON_NULL)
	public String getMechanicalDesignationCode() {
		if(null != this.mechanicalDesignation && StringUtils.isNotBlank(this.mechanicalDesignation.getValueAsString())) {
			return this.mechanicalDesignation.getValueAsString();
		}
		return null;
	}
	
	@Override
	@JsonIgnore
	public String getValueAsString() {
		return this.getShortId();
	}
	
	@JsonIgnore
	public boolean isValid() {
		return null != this.stencilMark && this.stencilMark.isValid()
				&& this.isValidEin() && this.isValidNumber();
	}
	
	public String getLessee() {
		return lessee;
	}

	public void setLessee(String lessee) {
		this.lessee = lessee;
	}

	public String getMaintenanceParty() {
		return maintenanceParty;
	}

	public void setMaintenanceParty(String maintenanceParty) {
		this.maintenanceParty = maintenanceParty;
	}

	public String getStatus() {
		return umlerStatus;
	}

	public void setStatus(String status) {
		this.umlerStatus = status;
	}

	public Date getLastMovement() {
		return lastMovement;
	}

	public void setLastMovement(Date lastMovement) {
		this.lastMovement = lastMovement;
	}

	public boolean isLastCommodityHazmat() {
		return lastCommodityHazmat;
	}

	public void setLastCommodityHazmat(boolean lastCommodityHazmat) {
		this.lastCommodityHazmat = lastCommodityHazmat;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
	
	@Override
    public int hashCode() {
          return HashCodeBuilder.reflectionHashCode(this);
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass(), "equipmentGroup", "mechanicalDesignation", "einLineage"
        		  , "umlerOwner", "lessee", "maintenanceParty", "status", "lastMovement", "lastCommodityHazmat");
    }

	@Override
	public int compareTo(Equipment obj) {
		final int EQUAL = 0;
		final int GREATER = 1;
		final int LESS = -1;

        if (this == obj) return EQUAL;
        if (obj == null || getClass() != obj.getClass()) return GREATER;
        Equipment that = obj;
        
        // Marks are equal
        if(null == this.stencilMark && null == that.stencilMark) {
        	// compare only ein makes sense.
        	return compareLongs(this.ein, that.ein);
        }
        
        if(null == this.stencilMark && null != that.stencilMark) {
        	// This one is null
    		return LESS;
        }
        
        if(null != this.stencilMark && null == that.stencilMark) {
        	// Other one is null
    		return GREATER;
        }

        if(0 == this.stencilMark.compareTo(that.stencilMark)) {		// NOSONAR - Stencil mark can't be null at this point
        	// Number is equal
        	if(0 == compareLongs(this.number, that.number)) {
        		// Compare the EINs
            	return compareLongs(this.ein, that.ein);        		        			
        	} 
        	// Compare the numbers
   			return compareLongs(this.number, that.number); 	
        }
        // Compare the marks
       	return this.stencilMark.compareTo(that.stencilMark);
	}
	
	private int compareLongs(Long lhs, Long rhs) {
		final int EQUAL = 0;
		final int GREATER = 1;
		final int LESS = -1;

		if(null == lhs && null == rhs) {
			return EQUAL;
		}
		
		if(null == lhs ^ null == rhs) {
			return (lhs == null ? LESS : GREATER);
		}
		
		return lhs.compareTo(rhs);		
	}
}
