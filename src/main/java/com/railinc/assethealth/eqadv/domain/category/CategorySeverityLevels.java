package com.railinc.assethealth.eqadv.domain.category;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class CategorySeverityLevels implements Serializable {
	private static final long serialVersionUID = 4457550895398089492L;
	private List<Category> categories = new ArrayList<>();
	
	public CategorySeverityLevels(List<CategorySeverityLevel> levels) {
		if(CollectionUtils.isNotEmpty(levels)) {
			for(CategorySeverityLevel l : levels) {
				this.add(l);
			}
		}
	}
	
	@JsonUnwrapped
	public List<Category> getCategorySeverityLevels(){
		return categories;
	}
	
	public void add(CategorySeverityLevel severityLevel) {
		boolean added = false;
		if(CollectionUtils.isNotEmpty(categories)) {
			for(Category c : categories) {
				if(c.getCode().equals(severityLevel.getCategory().getCode())) {
					c.addSeverityLevel(severityLevel.getSeverityLevel());
					added = true;
					break;
				}
			}
		} 
		
		if(!added) {
			severityLevel.category.addSeverityLevel(severityLevel.getSeverityLevel());
			categories.add(severityLevel.category);
		}
	}
}
