package com.railinc.assethealth.eqadv.domain.equipment;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.railinc.assethealth.eqadv.domain.ORACLE_BOOLEAN;
import com.railinc.assethealth.eqadv.domain.exception.InvalidOracleBooleanFlagException;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonIgnoreProperties({ "isRetrieveFromDatabase", "flag" })
@JsonRootName("MechanicalDesignationIndicator")
@JsonDeserialize(using=MechanicalDesignationIndicatorDeserializer.class)
public enum MECHANICAL_DESIGNATION_INDICATOR {
	OPEN(ORACLE_BOOLEAN.FALSE, "O", "Open", "Any equipment mechanical designation can be assigned to this notice."),
	SPECIFIED(ORACLE_BOOLEAN.TRUE, "S", "Specified", "Only equipment of the specified mechanical designations can be assigned to this notice.");
	
	public final ORACLE_BOOLEAN flag;
	public final String code;
	public final String value;
	public final boolean isRetrieveFromDatabase;	
	public final String description;
	private static final String MSG_FORMAT = "The value [%s]is not a valid Mechanical Designation flag!";
	
	MECHANICAL_DESIGNATION_INDICATOR(ORACLE_BOOLEAN flag, String code, String typeName, String desc) {
		this.flag = flag;
		this.code = code;
		this.isRetrieveFromDatabase = flag.value;
		this.value = typeName;
		this.description = desc;
	}
	
	public static MECHANICAL_DESIGNATION_INDICATOR forFlag(boolean flag) {
		if(flag) {
			return SPECIFIED;
		} else {
			return OPEN;
		}
	}

	public static MECHANICAL_DESIGNATION_INDICATOR forCode(String code) throws InvalidOracleBooleanFlagException {
		for(MECHANICAL_DESIGNATION_INDICATOR indicator : MECHANICAL_DESIGNATION_INDICATOR.values()) {
			if(indicator.code.equals(code)) {
				return indicator;
			}
		}			
		throw new InvalidOracleBooleanFlagException(String.format(MSG_FORMAT, code));
	}

	public static MECHANICAL_DESIGNATION_INDICATOR forFlag(String flag) throws InvalidOracleBooleanFlagException {
		if(StringUtils.isBlank(flag)) {
			throw new InvalidOracleBooleanFlagException(String.format(MSG_FORMAT, flag));
		}
		
		try {
			ORACLE_BOOLEAN temp = ORACLE_BOOLEAN.valueFor(flag);
			for(MECHANICAL_DESIGNATION_INDICATOR indicator : MECHANICAL_DESIGNATION_INDICATOR.values()) {
				if(indicator.flag == temp) {
					return indicator;
				}
			}			
		} catch(InvalidOracleBooleanFlagException e) {
			return MECHANICAL_DESIGNATION_INDICATOR.forCode(flag);
		}
		throw new InvalidOracleBooleanFlagException(String.format(MSG_FORMAT, flag));
	}	
}
