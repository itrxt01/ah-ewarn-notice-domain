package com.railinc.assethealth.eqadv.domain.mark;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.railinc.assethealth.eqadv.domain.exception.EWDeserializationException;

public class AssignmentReporterIndicatorDeserializer extends StdDeserializer<ASSIGNMENT_REPORTER_INDICATOR>{
	private static final long serialVersionUID = -4867661038564660186L;

	public AssignmentReporterIndicatorDeserializer() { 
		this(null); 
	} 

	public AssignmentReporterIndicatorDeserializer(Class<ASSIGNMENT_REPORTER_INDICATOR> indicator) { 
		super(indicator); 
	}

	@Override
	public ASSIGNMENT_REPORTER_INDICATOR deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
		JsonNode node = jp.getCodec().readTree(jp);
		String indicator = null;
		if(node.isObject()) {
			indicator = node.get("code").asText();
		} else {
			indicator =  node.asText();	
		}
		
		try {
			return ASSIGNMENT_REPORTER_INDICATOR.forIndicator(indicator);
		} catch (Exception e) {
			throw new EWDeserializationException(e, String.format("Problem creating new ASSIGNMENT_REPORTER_INDICATOR from '%s'", indicator));
		}
	}
}
