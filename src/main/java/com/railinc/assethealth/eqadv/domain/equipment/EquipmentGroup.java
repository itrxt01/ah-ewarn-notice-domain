package com.railinc.assethealth.eqadv.domain.equipment;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"name", "description"})
public class EquipmentGroup implements Serializable {
	private static final long serialVersionUID = -6230190396957065778L;

	String name;
	String description;
	
	public EquipmentGroup(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public String getGroup() {
		return name;
	}
	public void setGroup(String group) {
		this.name = group;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
