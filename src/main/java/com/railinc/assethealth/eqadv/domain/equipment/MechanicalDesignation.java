package com.railinc.assethealth.eqadv.domain.equipment;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.railinc.assethealth.eqadv.domain.EWNoticeAttribute;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.notice.NoticeSetItem;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"desig", "desc"})
public class MechanicalDesignation extends EWNoticeAttribute implements NoticeSetItem, Comparable<MechanicalDesignation> {
	private static final long serialVersionUID = 5843226587547598019L;
	private static final String REGEX_PATTERN = "^([a-z, A-Z]{1,4})$";
	private static final int MAX_LENGTH = 4;
	private static final String MSG_FORMAT = "The value[%s] is not a valid equipment mechanical designation! Expecting String of alpa characters with a max length of 4.";

	private String equipmentDesignation;
	private String description;
	
	public MechanicalDesignation(String desig) throws InvalidMechanicalDesignationException {
		this.init(desig);
	}
	
	public MechanicalDesignation(String desig, String desc) throws InvalidMechanicalDesignationException {
		this.init(desig);
		this.description = desc;
	}
	
	public MechanicalDesignation(MechanicalDesignation desig) throws InvalidMechanicalDesignationException {
		if(null == desig) {
			throw new InvalidMechanicalDesignationException(String.format(MSG_FORMAT, desig));						
		}
		// No need to validate since these are immutable and are validated upon construction		
		this.equipmentDesignation = desig.equipmentDesignation;
	}

	private void init(String desig) throws InvalidMechanicalDesignationException {
		if(StringUtils.isBlank(desig)) {
			throw new InvalidMechanicalDesignationException(String.format(MSG_FORMAT, desig));			
		}
		String temp = desig.toUpperCase().trim();
		if(temp.length() > MAX_LENGTH || !temp.matches(REGEX_PATTERN)) {
			throw new InvalidMechanicalDesignationException(String.format(MSG_FORMAT, desig));			
		}
		this.equipmentDesignation = temp;
		
	}
	
	@Override
	@JsonProperty(value="desig")
	public String getValueAsString() {
		return equipmentDesignation;
	}

	@Override
	@JsonIgnore
	public String getValueAsJustifiedString() {
		return leftJustifyString(equipmentDesignation, MAX_LENGTH);
	}
	
	@JsonProperty(value="desc")
	@JsonInclude(Include.NON_EMPTY)
	public String getDescription() {
		return this.description;
	}
		
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
	
	@Override
    public int hashCode() {
          return HashCodeBuilder.reflectionHashCode(this);
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass(), "description");
    }

	@Override
	public int compareTo(MechanicalDesignation o) {
		return this.equipmentDesignation.compareTo(o.equipmentDesignation);
	}
}
