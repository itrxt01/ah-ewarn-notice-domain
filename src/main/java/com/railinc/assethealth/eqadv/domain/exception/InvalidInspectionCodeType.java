package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidInspectionCodeType extends Exception {
	private static final long serialVersionUID = 6558391835667912443L;

	public InvalidInspectionCodeType(String msg) {
		super(msg);
	}
}
