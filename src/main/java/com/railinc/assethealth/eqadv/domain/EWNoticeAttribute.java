package com.railinc.assethealth.eqadv.domain;


import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public abstract class EWNoticeAttribute extends EWDomainAbstract implements Serializable {
	private static final long serialVersionUID = -8621228873230080470L;
	protected Long noticeId = null;
	
	protected EWNoticeAttribute() {}
	
	protected EWNoticeAttribute(EWNoticeAttribute obj){
		super(obj);
		if(null != obj) {
			this.noticeId = obj.noticeId;
		}
	}
	
	@JsonInclude(Include.NON_NULL)
	public Long getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(long noticeId) {
		this.noticeId = Long.valueOf(noticeId);
	}

	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}
	
	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false);
    }

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false);
	}	
}
