package com.railinc.assethealth.eqadv.domain.mark;

public class IndustryMark extends Mark {
	private static final long serialVersionUID = 351087469485661728L;
	static final String INDUSTRY_MARK_REGEX_PATTERN = "^EWS&$";
	private static final String INDUSTRY_MARK_ERROR_MSG_FORMAT = "The value[%s] is not a valid industry mark!";
	private static final boolean IS_INDUSTRY_MARK_EQUIPMENT_OWNER = false;
	
	protected IndustryMark(String mark) {
		super(mark);
	}
	
	@Override
	protected String getRegex() {
		return INDUSTRY_MARK_REGEX_PATTERN;
	}
	
	@Override
	protected String getErrorMessageFormat() {
		return INDUSTRY_MARK_ERROR_MSG_FORMAT;
	}
	
	@Override
	protected boolean getIsEquipmentOwnerMark() {
		return IS_INDUSTRY_MARK_EQUIPMENT_OWNER;
	}
}
