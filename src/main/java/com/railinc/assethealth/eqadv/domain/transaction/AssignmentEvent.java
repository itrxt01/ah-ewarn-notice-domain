package com.railinc.assethealth.eqadv.domain.transaction;

import java.util.Date;

import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;

public class AssignmentEvent extends EWDomainAbstract {

    private static final long serialVersionUID = 7265349167021041314L;

    private Long eventId;
    private Long assignmentId;
    private Date assignmentDate;
    private String assignmentReporter;
    private String severityCode;
    private Integer delay;
    private Date escalationDate;
    private String escalationSevCode;
    private Integer noticeNbr;
    private String ein;
    private String equipUnitInitCode;
    private String equipUnitNbr;
    private String mechanicalDesignation;
    private String umlerOwner;
    private String lessee;
    private String maintenanceParty;
    private String equipStatus;
    private String markOwner;

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    public Date getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(Date assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public String getAssignmentReporter() {
        return assignmentReporter;
    }

    public void setAssignmentReporter(String assignmentReporter) {
        this.assignmentReporter = assignmentReporter;
    }

    public String getSeverityCode() {
        return severityCode;
    }

    public void setSeverityCode(String severityCode) {
        this.severityCode = severityCode;
    }

    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }

    public Date getEscalationDate() {
        return escalationDate;
    }

    public void setEscalationDate(Date escalationDate) {
        this.escalationDate = escalationDate;
    }

    public String getEscalationSevCode() {
        return escalationSevCode;
    }

    public void setEscalationSevCode(String escalationSevCode) {
        this.escalationSevCode = escalationSevCode;
    }

    public Integer getNoticeNbr() {
        return noticeNbr;
    }

    public void setNoticeNbr(Integer noticeNbr) {
        this.noticeNbr = noticeNbr;
    }

    public String getEin() {
        return ein;
    }

    public void setEin(String ein) {
        this.ein = ein;
    }

    public String getEquipUnitInitCode() {
        return equipUnitInitCode;
    }

    public void setEquipUnitInitCode(String equipUnitInitCode) {
        this.equipUnitInitCode = equipUnitInitCode;
    }

    public String getEquipUnitNbr() {
        return equipUnitNbr;
    }

    public void setEquipUnitNbr(String equipUnitNbr) {
        this.equipUnitNbr = equipUnitNbr;
    }

    public String getMechanicalDesignation() {
        return mechanicalDesignation;
    }

    public void setMechanicalDesignation(String mechanicalDesignation) {
        this.mechanicalDesignation = mechanicalDesignation;
    }

    public String getUmlerOwner() {
        return umlerOwner;
    }

    public void setUmlerOwner(String umlerOwner) {
        this.umlerOwner = umlerOwner;
    }

    public String getLessee() {
        return lessee;
    }

    public void setLessee(String lessee) {
        this.lessee = lessee;
    }

    public String getMaintenanceParty() {
        return maintenanceParty;
    }

    public void setMaintenanceParty(String maintenanceParty) {
        this.maintenanceParty = maintenanceParty;
    }

    public String getEquipStatus() {
        return equipStatus;
    }

    public void setEquipStatus(String equipStatus) {
        this.equipStatus = equipStatus;
    }

    public String getMarkOwner() {
        return markOwner;
    }

    public void setMarkOwner(String markOwner) {
        this.markOwner = markOwner;
    }

    @Override
    public String toString() {
        return "AssignmentEvent{" +
                "eventId=" + eventId +
                ", assignmentId=" + assignmentId +
                ", assignmentDate=" + assignmentDate +
                ", assignmentReporter='" + assignmentReporter + '\'' +
                ", severityCode='" + severityCode + '\'' +
                ", delay=" + delay +
                ", escalationDate=" + escalationDate +
                ", escalationSevCode='" + escalationSevCode + '\'' +
                ", noticeNbr=" + noticeNbr +
                ", ein='" + ein + '\'' +
                ", equipUnitInitCode='" + equipUnitInitCode + '\'' +
                ", equipUnitNbr='" + equipUnitNbr + '\'' +
                ", mechanicalDesignation='" + mechanicalDesignation + '\'' +
                ", umlerOwner='" + umlerOwner + '\'' +
                ", lessee='" + lessee + '\'' +
                ", maintenanceParty='" + maintenanceParty + '\'' +
                ", equipStatus='" + equipStatus + '\'' +
                ", markOwner='" + markOwner + '\'' +
                ", createCompany='" + createCompany + '\'' +
                ", createUserName='" + createUsername + '\'' +
                ", createdTs=" + createdTimestamp +
                ", modifyCompany='" + modifyCompany + '\'' +
                ", modifyUserName='" + modifyUsername + '\'' +
                ", modifiedTs=" + modifiedTimestamp +
                '}';
    }
}
