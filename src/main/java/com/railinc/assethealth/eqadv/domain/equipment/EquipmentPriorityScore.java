package com.railinc.assethealth.eqadv.domain.equipment;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;
import com.railinc.assethealth.eqadv.domain.notice.NoticeEqmtScoreFactors;

/**
 * An assignment priority score is calculated from the notice score factors and
 * data retrieved about the equipment when it is assigned to the notice.
 * 
 * @author ron
 *
 */
@JsonPropertyOrder(value = { "noticeNumber", "equipmentId", "commodityScore", "movementScore", "carTypeScore",
		"totalScore" })
@JsonRootName(value = "escalationPriorityScore")
public class EquipmentPriorityScore extends EWDomainAbstract {
	private static final long serialVersionUID = 1244553878183709032L;

	@JsonIgnore
	Long assignmentId;
	Integer noticeNumber;
	Integer carTypeScore;
	Integer movementScore;
	Integer commodityScore;
	Integer totalScore;
	Equipment equipment;

	public EquipmentPriorityScore calculateScore(Equipment equipment, NoticeEqmtScoreFactors factors) {
		this.equipment = equipment;
		if(null != factors) {
			commodityScore = calculateCommodityScore(factors);
			carTypeScore = calculateCarTypeScore(factors);
			movementScore = calculateMovementScore(factors);
			totalScore = calculateTotalScore();
		} else {
			commodityScore = 0;
			carTypeScore = 0;
			movementScore = 0;
			totalScore = 0;			
		}
		return this;
	}

	private int calculateTotalScore() {
		return carTypeScore + movementScore + commodityScore;
	}
	
	private int calculateCarTypeScore(NoticeEqmtScoreFactors factors) {
		if (StringUtils.isNotBlank(equipment.getEquipmentGroup())
				&& equipment.getEquipmentGroup().equalsIgnoreCase(factors.getCarType())) {
			return factors.getCarTypeEquals();
		} else {
			return 0;
		}
	}

	private int calculateMovementScore(NoticeEqmtScoreFactors factors) {
		if (null == equipment.getLastMovement()) {
			return 0;
		} else {
			LocalDateTime now = LocalDateTime.now();
			LocalDateTime lastMovement = new java.sql.Timestamp(equipment.getLastMovement().getTime()).toLocalDateTime();
			int daysSinceLastMovement = Math.abs(Math.toIntExact(ChronoUnit.DAYS.between(now, lastMovement)));
			if (daysSinceLastMovement <= 90) {
				return factors.getLastMovementLTE90Days();
			} else if (daysSinceLastMovement <= 180) {
				return factors.getLastMovementLTE180Days();
			} else if (daysSinceLastMovement > 180) {
				return factors.getLastMovementGT180Days();
			} else {
				return 0;
			}
		}
	}

	private int calculateCommodityScore(NoticeEqmtScoreFactors factors) {
		if (equipment.isLastCommodityHazmat()) {
			return factors.getHazmatCarried();
		} else {
			return factors.getNonHazmatCarried();
		}
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public Long getAssignmentId() {
		return assignmentId;
	}

	public void setAssignmentId(Long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public Integer getCarTypeScore() {
		return carTypeScore;
	}

	public void setCarTypeScore(Integer carTypeScore) {
		this.carTypeScore = carTypeScore;
	}

	public Integer getCommodityScore() {
		return commodityScore;
	}

	public void setCommodityScore(Integer commodityScore) {
		this.commodityScore = commodityScore;
	}

	public Integer getMovementScore() {
		return movementScore;
	}

	public void setMovementScore(Integer movementScore) {
		this.movementScore = movementScore;
	}

	public Integer getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}

	public Integer getNoticeNumber() {
		return noticeNumber;
	}

	public void setNoticeNumber(Integer noticeNumber) {
		this.noticeNumber = noticeNumber;
	}

	public String getEquipmentId() {
		return equipment.getEquipmentId();
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE, false);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false, EWDomainAbstract.class);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, EWDomainAbstract.class);
	}
}
