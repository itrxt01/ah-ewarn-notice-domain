package com.railinc.assethealth.eqadv.domain.mark;

/**
 * Marks must be no longer than 4 characters in length and match the pattern:
 * 
 * Axxx - running repair agent mark
 * 
 * This is just an initial validation.  Final validation is against the list of marks retrieved from Umler.
 * 
 * @author ron taylor
 *
 */
public class RepairShopMark extends Mark {
	private static final long serialVersionUID = 20822137216122779L;
	static final String REPAIR_SHOP_REGEX_PATTERN = "^(([a-z, A-Z]{3})||([(c,d,C,D)]([0-9]{3})))$";
	private static final String REPAIR_SHOP_ERROR_MSG_FORMAT = "The value[%s] is not a valid repair shop mark!";
	private static final boolean IS_REPAIR_SHOP_EQUIPMENT_OWNER = false;
	
	public RepairShopMark(String mark) {
		super(mark);
	}
	
	RepairShopMark(String mark, Mark parent) {
		super(mark, parent);
	}
	
	RepairShopMark(Mark mark) {
		super(mark);
	}
	
	@Override
	protected String getRegex() {
		return REPAIR_SHOP_REGEX_PATTERN;
	}

	@Override
	protected String getErrorMessageFormat() {
		return REPAIR_SHOP_ERROR_MSG_FORMAT;
	}

	@Override
	protected boolean getIsEquipmentOwnerMark() {
		return IS_REPAIR_SHOP_EQUIPMENT_OWNER;
	}
}
