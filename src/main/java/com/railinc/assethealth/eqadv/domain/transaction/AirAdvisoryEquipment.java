package com.railinc.assethealth.eqadv.domain.transaction;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.railinc.assethealth.eqadv.domain.EWDomainWithID;

public class AirAdvisoryEquipment extends EWDomainWithID {
	
	private static final long serialVersionUID = 2304118947573145285L;
	
    private String categoryCode;
    private Integer noticeNumber;
    private String equipmentId;
    private String ein;
    private String equipmentUnitInitCode;
    private String equipmentUnitNumber;
    private Date assignmentDate;
    private String assignmentReporter;
    private String severityCode;
	
	public String getCategoryCode() {
		return categoryCode;
	}
	
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	
	public Integer getNoticeNumber() {
		return noticeNumber;
	}
	
	public void setNoticeNumber(Integer noticeNumber) {
		this.noticeNumber = noticeNumber;
	}
	
	public String getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}
	
	public String getEin() {
		return ein;
	}
	
	public void setEin(String ein) {
		this.ein = ein;
	}
	
	public String getEquipmentUnitInitCode() {
		return equipmentUnitInitCode;
	}
	
	public void setEquipmentUnitInitCode(String equipmentUnitInitCode) {
		this.equipmentUnitInitCode = equipmentUnitInitCode;
	}
	
	public String getEquipmentUnitNumber() {
		return equipmentUnitNumber;
	}
	
	public void setEquipmentUnitNumber(String equipmentUnitNumber) {
		this.equipmentUnitNumber = equipmentUnitNumber;
	}
	
	public Date getAssignmentDate() {
		return assignmentDate;
	}
	
	public void setAssignmentDate(Date assignmentDate) {
		this.assignmentDate = assignmentDate;
	}
	
	public String getAssignmentReporter() {
		return assignmentReporter;
	}
	
	public void setAssignmentReporter(String assignmentReporter) {
		this.assignmentReporter = assignmentReporter;
	}
	
	public String getSeverityCode() {
		return severityCode;
	}
	
	public void setSeverityCode(String severityCode) {
		this.severityCode = severityCode;
	}
	
	@Override
    public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass(), "description");
    }
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
