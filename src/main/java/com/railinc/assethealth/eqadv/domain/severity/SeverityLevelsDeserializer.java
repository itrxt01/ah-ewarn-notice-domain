package com.railinc.assethealth.eqadv.domain.severity;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class SeverityLevelsDeserializer extends StdDeserializer<SeverityLevels> { 
	private static final long serialVersionUID = 5690703706738320767L;

	public SeverityLevelsDeserializer() { 
		this(null); 
	} 

	public SeverityLevelsDeserializer(Class<SeverityLevels> vc) { 
		super(vc); 
	}

	@Override
	public SeverityLevels deserialize(JsonParser jp, DeserializationContext ctxt) 
			throws IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		List<SeverityLevel> severityLevelList = mapper.readValue(jp, new TypeReference<List<SeverityLevel>>() {});
		return new SeverityLevels(severityLevelList);
	}
}
