package com.railinc.assethealth.eqadv.domain.transaction;

public class NoticeInspectionCodeEvent extends NoticeEvent {

    private static final long serialVersionUID = 3643619323521284408L;

    private String severityCode;
    private String inspectionCode;
    private String isFinalFlag;
    private Long frequency;
    private String frequencyUnits;

    public String getSeverityCode() {
        return severityCode;
    }

    public void setSeverityCode(String severityCode) {
        this.severityCode = severityCode;
    }

    public String getInspectionCode() {
        return inspectionCode;
    }

    public void setInspectionCode(String inspectionCode) {
        this.inspectionCode = inspectionCode;
    }

    public String getIsFinalFlag() {
        return isFinalFlag;
    }

    public void setIsFinalFlag(String isFinalFlag) {
        this.isFinalFlag = isFinalFlag;
    }

    public Long getFrequency() {
        return frequency;
    }

    public void setFrequency(Long frequency) {
        this.frequency = frequency;
    }

    public String getFrequencyUnits() {
        return frequencyUnits;
    }

    public void setFrequencyUnits(String frequencyUnits) {
        this.frequencyUnits = frequencyUnits;
    }
}
