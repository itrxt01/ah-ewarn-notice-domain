package com.railinc.assethealth.eqadv.domain.pdf;

import com.railinc.assethealth.eqadv.domain.exception.InvalidContentTypeException;

public class NoticeContent extends NoticeFile {
	private static final long serialVersionUID = -2627028519081034911L;
	
	public CONTENT_TYPE getTemplateContentType() throws InvalidContentTypeException {
		return CONTENT_TYPE.forType(super.getFileExtension());
	}

	public void setTemplateContentType(CONTENT_TYPE type) {		
		super.setFileExtension(type.getFileType());
	}
	
	@Override
	public boolean isValid() {
		return super.isValid() && isValidPDF();
	}
	
	private boolean isValidPDF() {
		return PDFValidator.isValidPDF(super.getFileContent());
	}
	
	
}
