package com.railinc.assethealth.eqadv.domain.exception;

public class InspectionCodeMCNotSupportedException extends Exception {
	private static final long serialVersionUID = 5022730397591215925L;

	public InspectionCodeMCNotSupportedException(String msg){
		super(msg);
	}
}
