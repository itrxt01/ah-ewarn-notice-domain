package com.railinc.assethealth.eqadv.domain.mark;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MarkFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(MarkFactory.class);
	
	private static final String MSG_ERR_DOES_NOT_EVALUATE_VALID = "The value[%s] does not evaluate to a valid mark!";
	private static final String MSG_ERR_DOES_NOT_EVALUATE_RECOGNIZED = "The value[%s] does not evaluate to a recognized mark!";

	// All methods are static, factory has no place being instantiated
	private MarkFactory() {
		throw new AssertionError("Suppress default constructor for non-instantiability");
	}
	
	public static Mark newMark(String mark) {
		if(StringUtils.isBlank(mark)) {
			LOGGER.warn(String.format(MSG_ERR_DOES_NOT_EVALUATE_VALID, mark));		// NOSONAR - flag evaluated in library
			return null;
		}
		if(mark.matches(RoadMark.ROAD_MARK_REGEX_PATTERN)) {
			return new RoadMark(mark);
		}		
		if(mark.matches(RepairShopMark.REPAIR_SHOP_REGEX_PATTERN)) {
			return new RepairShopMark(mark);
		}		
		if(mark.matches(RunningRepairAgentMark.RUNNING_REPAIR_AGENT_REGEX_PATTERN)) {
			return new RunningRepairAgentMark(mark);
		}
		if(mark.matches(IndustryMark.INDUSTRY_MARK_REGEX_PATTERN)) {
			return new IndustryMark(mark);
		}
		LOGGER.warn(String.format(MSG_ERR_DOES_NOT_EVALUATE_VALID, mark));				// NOSONAR - flag evaluated in library
		return new RoadMark(mark);
	}

	public static Mark newMark(String mark, Mark parent) {
		if(mark.matches(RoadMark.ROAD_MARK_REGEX_PATTERN)) {
			return new RoadMark(mark, parent);
		}		
		if(mark.matches(RepairShopMark.REPAIR_SHOP_REGEX_PATTERN)) {
			return new RepairShopMark(mark, parent);
		}		
		if(mark.matches(RunningRepairAgentMark.RUNNING_REPAIR_AGENT_REGEX_PATTERN)) {
			return new RunningRepairAgentMark(mark, parent);
		}		
		LOGGER.warn(String.format(MSG_ERR_DOES_NOT_EVALUATE_VALID, mark));				// NOSONAR - flag evaluated in library
		return new RoadMark(mark);		
	}

	public static Mark newMark(String mark, String parent) {
		return newMark(mark, newMark(parent));
	}
	
	public static Mark newMark(Mark mark) {
		if(mark instanceof RoadMark) {
			return new RoadMark(mark);
		}
		if(mark instanceof RepairShopMark) {
			return new RepairShopMark(mark);
		}
		if(mark instanceof RunningRepairAgentMark) {
			return new RunningRepairAgentMark(mark);
		}
		LOGGER.warn(String.format(MSG_ERR_DOES_NOT_EVALUATE_RECOGNIZED, mark));			// NOSONAR - flag evaluated in library
		return new RoadMark(mark);		
	}
}
