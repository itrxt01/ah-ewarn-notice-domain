package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidRoadMarkException extends Exception {
	private static final long serialVersionUID = 2469487014141099170L;

	public InvalidRoadMarkException(String msg) {
		super(msg);
	}

}
