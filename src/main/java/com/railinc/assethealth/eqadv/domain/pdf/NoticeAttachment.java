package com.railinc.assethealth.eqadv.domain.pdf;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class NoticeAttachment extends NoticeFile {
	private static final long serialVersionUID = -2740228915410311032L;

	private Long id;
	private String comment;
	
	public NoticeAttachment() {}
	
	public NoticeAttachment(NoticeFile file) {
		super(file);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public boolean equals(Object o) {
		return new EqualsBuilder().appendSuper(super.equals(o)).reflectionAppend(this, o).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().appendSuper(super.hashCode()).append("comment").toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).appendSuper(super.toString()).toString();
	}

}
