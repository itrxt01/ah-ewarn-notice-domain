package com.railinc.assethealth.eqadv.domain.status;

import org.apache.commons.lang3.StringUtils;

import com.railinc.assethealth.eqadv.domain.exception.InvalidStatusException;

public enum NOTICE_STATUS {
	DRAFT("DRAFT", "Draft Notice not published to industry.", "Draft Notice - Not visible outside creating mark.")
	, PUBLISHED("PUBLISHED", "Published Notice public to industry.", "Published Notice - visible to the industry.")
	, ARCHIVED("ARCHIVED", "Archived Notice.", "Archived Notice - Not visible.");
	
	private static final String MSG_FORMAT = "The status[%s] doesn't resolve to a valid notice status!";

	public final String statusName;
	public final String desc;
	public final String tooltip;

	private NOTICE_STATUS(String typeName, String desc, String tooltip){
		this.statusName = typeName;
		this.desc = desc;
		this.tooltip = tooltip;
	}
	
	public static NOTICE_STATUS forCode(String code) throws InvalidStatusException {
		if(StringUtils.isBlank(code)) {
			throw new InvalidStatusException(String.format(MSG_FORMAT, code));
		}
		String trimmed = code.trim();
		for(NOTICE_STATUS status : NOTICE_STATUS.values()){
			if (StringUtils.equalsIgnoreCase(status.statusName, trimmed)){
				return status;
			}
		}
		throw new InvalidStatusException(String.format(MSG_FORMAT, code));
	}
}
