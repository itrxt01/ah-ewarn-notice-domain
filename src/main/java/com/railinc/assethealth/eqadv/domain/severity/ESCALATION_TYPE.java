package com.railinc.assethealth.eqadv.domain.severity;

public enum ESCALATION_TYPE {
	INTERVAL("I")
	, DATE("D")
	, QUOTA("Q");
	
	public final String value;
	ESCALATION_TYPE(String value){
		this.value = value;
	}
}
