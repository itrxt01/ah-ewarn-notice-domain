package com.railinc.assethealth.eqadv.domain.exception;

public class InspectionFrequencyInvalidException extends Exception {
	private static final long serialVersionUID = 826630333761118848L;

	public InspectionFrequencyInvalidException(String msg){
		super(msg);
	}
}
