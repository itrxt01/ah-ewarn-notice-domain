package com.railinc.assethealth.eqadv.domain.exception;

public class UnsupportedAttachmentTypeException extends Exception {
	private static final long serialVersionUID = 6577987266999473663L;

	public UnsupportedAttachmentTypeException(String msg){
		super(msg);
	}
}