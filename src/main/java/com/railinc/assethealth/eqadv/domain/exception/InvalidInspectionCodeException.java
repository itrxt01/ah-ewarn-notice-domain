package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidInspectionCodeException extends Exception {
	private static final long serialVersionUID = -574216959007715101L;

	public InvalidInspectionCodeException(String msg) {
		super(msg);
	}
}
