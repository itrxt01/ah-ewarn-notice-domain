package com.railinc.assethealth.eqadv.domain.transaction;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;

import java.util.Date;

public class InspectionEvent extends EWDomainAbstract {

    private static final long serialVersionUID = -6345039947058939620L;

    private Long eventId;
    private Long inspectionId;
    private String inspectionCode;
    private Date inspectionDate;
    private String inspectionReporter;
    private String statusInd;
    private Long eqmtAssignmentId;
    private Integer noticeNbr;
    private String categoryCode;
    private String ein;
    private String equipUnitInitCode;
    private String equipUnitNbr;
    private String mechanicalDesignation;
    private String umlerOwner;
    private String lessee;
    private String maintenanceParty;
    private String equipStatus;
    private String markOwner;

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(Long inspectionId) {
        this.inspectionId = inspectionId;
    }

    public String getInspectionCode() {
        return inspectionCode;
    }

    public void setInspectionCode(String inspectionCode) {
        this.inspectionCode = inspectionCode;
    }

    public Date getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(Date inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getInspectionReporter() {
        return inspectionReporter;
    }

    public void setInspectionReporter(String inspectionReporter) {
        this.inspectionReporter = inspectionReporter;
    }

    public String getStatusInd() {
        return statusInd;
    }

    public void setStatusInd(String statusInd) {
        this.statusInd = statusInd;
    }

    public Long getEqmtAssignmentId() {
        return eqmtAssignmentId;
    }

    public void setEqmtAssignmentId(Long eqmtAssignmentId) {
        this.eqmtAssignmentId = eqmtAssignmentId;
    }

    public Integer getNoticeNbr() {
        return noticeNbr;
    }

    public void setNoticeNbr(Integer noticeNbr) {
        this.noticeNbr = noticeNbr;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getEin() {
        return ein;
    }

    public void setEin(String ein) {
        this.ein = ein;
    }

    public String getEquipUnitInitCode() {
        return equipUnitInitCode;
    }

    public void setEquipUnitInitCode(String equipUnitInitCode) {
        this.equipUnitInitCode = equipUnitInitCode;
    }

    public String getEquipUnitNbr() {
        return equipUnitNbr;
    }

    public void setEquipUnitNbr(String equipUnitNbr) {
        this.equipUnitNbr = equipUnitNbr;
    }

    public String getMechanicalDesignation() {
        return mechanicalDesignation;
    }

    public void setMechanicalDesignation(String mechanicalDesignation) {
        this.mechanicalDesignation = mechanicalDesignation;
    }

    public String getUmlerOwner() {
        return umlerOwner;
    }

    public void setUmlerOwner(String umlerOwner) {
        this.umlerOwner = umlerOwner;
    }

    public String getLessee() {
        return lessee;
    }

    public void setLessee(String lessee) {
        this.lessee = lessee;
    }

    public String getMaintenanceParty() {
        return maintenanceParty;
    }

    public void setMaintenanceParty(String maintenanceParty) {
        this.maintenanceParty = maintenanceParty;
    }

    public String getEquipStatus() {
        return equipStatus;
    }

    public void setEquipStatus(String equipStatus) {
        this.equipStatus = equipStatus;
    }

    public String getMarkOwner() {
        return markOwner;
    }

    public void setMarkOwner(String markOwner) {
        this.markOwner = markOwner;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE, false);
    }
}
