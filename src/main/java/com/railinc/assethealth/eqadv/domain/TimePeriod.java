package com.railinc.assethealth.eqadv.domain;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.time.DateUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;

@JsonDeserialize(using=TimePeriodDeserializer.class)
public class TimePeriod implements Serializable {
	private static final long serialVersionUID = 4872910005064585469L;
	int period;
	PERIODIC_TIME_UNIT units;
	
	public TimePeriod() { /* Empty zero arg c'tor for deserialization */ }
	
	public TimePeriod(int length, PERIODIC_TIME_UNIT units) {
		this.period = length;
		this.units = units;
	}

	public TimePeriod(int length, String units) throws InvalidPeriodicTimeUnitException {
		this.period = length;
		this.units = PERIODIC_TIME_UNIT.getTimeUnitForName(units);
	}
	
	public int getTimePeriod() {
		return period;
	}
	
	public void setTimePeriod(int period) {
		this.period = period;
	}
	
	public PERIODIC_TIME_UNIT getTimeUnit() {
		return units;
	}
	
	public void setTimeUnit(String units) throws InvalidPeriodicTimeUnitException {
		this.units = PERIODIC_TIME_UNIT.getTimeUnitForName(units);
	}
	
	@JsonIgnore
	public boolean isValid() {
		return period > 0 && null != units;
	}
	
	@JsonIgnore
	public Date addPeriod(Date date) {
		return DateUtils.addDays(date, units.getNumberOfDays(period));
	}
	
	@JsonIgnore
	public boolean isElapsed(Date date) {
		return this.addPeriod(date).before(new Date());
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass());
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}
