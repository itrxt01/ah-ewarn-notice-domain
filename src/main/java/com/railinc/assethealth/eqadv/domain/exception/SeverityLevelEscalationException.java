package com.railinc.assethealth.eqadv.domain.exception;

public class SeverityLevelEscalationException extends Exception {
	private static final long serialVersionUID = 1807495019661032354L;

	public SeverityLevelEscalationException(String msg) {
		super(msg);
	}
}
