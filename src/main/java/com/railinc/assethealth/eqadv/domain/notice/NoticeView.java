package com.railinc.assethealth.eqadv.domain.notice;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.category.Category;

@JsonPropertyOrder({"title", "number", "noticeId", "noticeStatus", "noticeCategory", "fileNumber", "supplementNumber", "equipmentAssigned", "equipmentCurrentlyAssigned"
	, "equipCount_XX", "equipCount_A1", "equipCount_A2"})
public class NoticeView {
	private static final Logger LOGGER = LoggerFactory.getLogger(NoticeView.class);	
	
	private enum SEVERITY_CODE {
		XX, A1, A2, A9;
	}
	
	private Notice notice;
	Map<SEVERITY_CODE, Integer> nbrEquipmentAtSeverityLevel = new EnumMap<>(SEVERITY_CODE.class);

	public NoticeView(Notice notice) {
		this.notice = notice;
	}

	public NoticeView(NoticeView notice) {
		this.notice = notice.notice;
		this.nbrEquipmentAtSeverityLevel.putAll(notice.nbrEquipmentAtSeverityLevel);
	}	
	
	@JsonIgnore
	public Notice getNotice() {
		return notice;
	}

	@JsonProperty("noticeCategory")
	public String getNoticeCategory() {
		Category category = this.notice.getCategory();	
		return null == category ? "" : this.notice.getCategory().getCode();
	}
	
	@JsonProperty("number")
	public int getNoticeNumber() {
		return this.notice.getNumber();
	}
	
	@JsonProperty("noticeId")
	public Long getNoticeId() {
		return this.notice.getId();
	}

	@JsonProperty("supplementNumber")
	public Integer getSupplementNumber() {
		return this.notice.getSupplementNumber();
	}
	
	@JsonProperty("equipmentAssigned")
	public long getEquipmentAssigned() {
		return this.notice.getLifeTimeNumberEquipmentAssigned();
	}
	
	@JsonProperty("equipmentCurrentlyAssigned")
	public long getEquipmentCurrentlyAssigned() {
		int numberAssigned = nbrEquipmentAtSeverityLevel.values().stream().mapToInt(Integer::intValue).sum();
		if(notice.getNumberEquipmentCurrentlyAssigned() != numberAssigned) {
			this.notice.setEquipmentNumberCurrentlyAssigned(numberAssigned);
		}
		return numberAssigned;
	}
	
	public void setEquipmentCurrentlyAssigned(int numAssigned) {
		this.notice.setEquipmentNumberCurrentlyAssigned(numAssigned);
	}	
	
	@JsonProperty("fileNumber")
	public String getFileNumber() {
		return this.notice.getFileNumber();
	}
	
	@JsonProperty("noticeStatus")
	public String getStatus() {
		return this.notice.getStatus();
	}
	
	@JsonProperty("title")
	public String getTitle() {
		return this.notice.getTitle();
	}
	
	private int getNumberAtSeverityLevel(SEVERITY_CODE code) {
		if(null != nbrEquipmentAtSeverityLevel) {
			return nbrEquipmentAtSeverityLevel.getOrDefault(code, 0);
		}
		return 0;
	}

	@JsonProperty("equipCount_XX")
	public int getNumXX() {
		return getNumberAtSeverityLevel(SEVERITY_CODE.XX);
	}

	public void setNumXX(int numXX) {
		this.nbrEquipmentAtSeverityLevel.put(SEVERITY_CODE.XX, Integer.valueOf(numXX));
	}

	@JsonProperty("equipCount_A2")
	public int getNumA2() {
		return getNumberAtSeverityLevel(SEVERITY_CODE.A2);
	}

	public void setNumA2(int numA2) {
		this.nbrEquipmentAtSeverityLevel.put(SEVERITY_CODE.A2, Integer.valueOf(numA2));
	}

	@JsonProperty("equipCount_A1")
	public int getNumA1() {
		return getNumberAtSeverityLevel(SEVERITY_CODE.A1);
	}

	public void setNumA1(int numA1) {
		this.nbrEquipmentAtSeverityLevel.put(SEVERITY_CODE.A1, Integer.valueOf(numA1));

	}

	@JsonProperty("equipCount_A9")
	public int getNumA9() {
		return getNumberAtSeverityLevel(SEVERITY_CODE.A9);
	}

	public void setNumA9(int numA9) {
		this.nbrEquipmentAtSeverityLevel.put(SEVERITY_CODE.A9, Integer.valueOf(numA9));

	}

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			LOGGER.error("Json processing exception while creating notice view.");
			return "";
		}
	}	
}
