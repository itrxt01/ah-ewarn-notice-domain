package com.railinc.assethealth.eqadv.domain.exception;

public class InspectionNotSupportedException extends Exception {
	private static final long serialVersionUID = -6357113741547193573L;

	public InspectionNotSupportedException(String msg){
		super(msg);
	}
}
