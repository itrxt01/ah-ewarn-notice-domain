package com.railinc.assethealth.eqadv.domain.transaction;

import java.sql.Timestamp;
import java.util.Date;

import com.railinc.assethealth.eqadv.domain.EWDomainWithID;

public class LogEvent extends EWDomainWithID {

    private static final long serialVersionUID = -4908111064786488292L;

    private String eventType;
    private String eventSource;
    private Timestamp eventTs;
    private String categoryCode;
    private Integer noticeNbr;
    private String EIN;
    private String equipUnitInitCode;
    private String equipUnitNbr;
    private Date inspectionDate;
    private String inspectionCode;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventSource() {
        return eventSource;
    }

    public void setEventSource(String eventSource) {
        this.eventSource = eventSource;
    }

    public Timestamp getEventTs() {
        return eventTs;
    }

    public void setEventTs(Timestamp eventTs) {
        this.eventTs = eventTs;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public Integer getNoticeNbr() {
        return noticeNbr;
    }

    public void setNoticeNbr(Integer noticeNbr) {
        this.noticeNbr = noticeNbr;
    }

    public String getEIN() {
        return EIN;
    }

    public void setEIN(String EIN) {
        this.EIN = EIN;
    }

    public String getEquipUnitInitCode() {
        return equipUnitInitCode;
    }

    public void setEquipUnitInitCode(String equipUnitInitCode) {
        this.equipUnitInitCode = equipUnitInitCode;
    }

    public String getEquipUnitNbr() {
        return equipUnitNbr;
    }

    public void setEquipUnitNbr(String equipUnitNbr) {
        this.equipUnitNbr = equipUnitNbr;
    }

    public Date getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(Date inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getInspectionCode() {
        return inspectionCode;
    }

    public void setInspectionCode(String inspectionCode) {
        this.inspectionCode = inspectionCode;
    }

    @Override
    public String toString() {
        return "LogEvent{" +
                "eventId=" + id +
                ", eventType='" + eventType + '\'' +
                ", eventSource='" + eventSource + '\'' +
                ", eventTs=" + eventTs +
                ", categoryCode='" + categoryCode + '\'' +
                ", noticeNbr=" + noticeNbr +
                ", EIN='" + EIN + '\'' +
                ", equipUnitInitCode='" + equipUnitInitCode + '\'' +
                ", equipUnitNbr='" + equipUnitNbr + '\'' +
                ", inspectionDate=" + inspectionDate +
                ", inspectionCode='" + inspectionCode + '\'' +
                ", createCompany='" + createCompany + '\'' +
                ", createUserName='" + createUsername + '\'' +
                ", createdTs=" + createdTimestamp +
                ", modifyCompany='" + modifyCompany + '\'' +
                ", modifyUserName='" + modifyUsername + '\'' +
                ", modifiedTs=" + modifiedTimestamp +
                '}';
    }
}
