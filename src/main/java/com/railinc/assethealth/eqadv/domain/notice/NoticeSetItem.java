package com.railinc.assethealth.eqadv.domain.notice;

public interface NoticeSetItem {
	public String getValueAsString();
	public String getValueAsJustifiedString();
}
