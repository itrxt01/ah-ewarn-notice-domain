package com.railinc.assethealth.eqadv.domain.errors;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ErrorDetails {
    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm:ss";

    private LocalDateTime timestamp;
    private String code;
    private String message;
    private List<SubError> subErrors = new ArrayList<>();

    public ErrorDetails() {
        timestamp = LocalDateTime.now();
    }

	public ErrorDetails(String message) {
        this();
        this.message = message;
    }

	public ErrorDetails(String message, String subError) {
        this();
        this.message = message;
        this.subErrors.add(new ErrorMessage(subError));
    }
	
	public ErrorDetails(String message, List<SubError> subErrors) {
        this();
        this.message = message;
        this.subErrors.addAll(subErrors);
    }
	
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@JsonProperty("timestamp")
    public String getTimestampString() {
	    return  timestamp.format( DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
    }

    public void addSubError(SubError subError) {
        if (subErrors == null) {
            subErrors = new ArrayList<>();
        }
        subErrors.add(subError);
    }

    public ErrorDetails addSubError(String subError) {
        if (subErrors == null) {
            subErrors = new ArrayList<>();
        }
        subErrors.add(new ErrorMessage(subError));
        return this;
    }
    
    public List<SubError> getSubErrors() {
		return this.subErrors;
	}

    public void addSubErrors(List<SubError> errors) {
        if (subErrors == null) {
            subErrors = new ArrayList<>();
        }
        subErrors.addAll(errors);
    }

    
    public boolean hasSubErrors() {
    	return CollectionUtils.isNotEmpty(this.subErrors);
    }
    
	@JsonIgnore
	public String getJson() throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(this);	
	}    
}