package com.railinc.assethealth.eqadv.domain.notice;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public interface NoticeItemSet<T extends NoticeSetItem> {
	default String getDelimitedString(Set<T> set, String delimiter, boolean justified){
		StringBuilder sb = new StringBuilder();
		for(NoticeSetItem i : set){
			if(StringUtils.isNotBlank(i.getValueAsString())) {
				if(sb.length() > 0) sb.append(delimiter); 
				if(justified) {
					sb.append(i.getValueAsJustifiedString());
				} else {
					sb.append(i.getValueAsString());				
				}
			}
		}
		return sb.toString();
	}
}

