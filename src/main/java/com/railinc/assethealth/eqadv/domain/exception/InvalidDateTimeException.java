package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidDateTimeException extends EWDeserializationException {	// NOSONAR - Approve 6 parents for this exception.
	private static final long serialVersionUID = -3236240201843915666L;

	public InvalidDateTimeException(String msg){
		super(msg);
	}
}
