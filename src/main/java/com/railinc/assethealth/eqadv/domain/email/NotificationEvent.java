package com.railinc.assethealth.eqadv.domain.email;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;

public class NotificationEvent extends EWDomainAbstract implements Serializable {

	private static final long serialVersionUID = -6255528187953447020L;

	private Integer notificationEventId; 
	private String description;
	
	public NotificationEvent(Integer notificationEventId, String description) {
		super();
		this.notificationEventId = notificationEventId;
		this.description = description;
	}

	public Integer getNotificationEventId() {
		return notificationEventId;
	}

	public void setNotificationEventId(Integer notificationEventId) {
		this.notificationEventId = notificationEventId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, false);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass());
	}
}
