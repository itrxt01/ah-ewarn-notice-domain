package com.railinc.assethealth.eqadv.domain.equipment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EquipmentUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(EquipmentUtils.class);

    private static final String MARK_PADDING_CHAR = " ";
    private static final String NUMBER_PADDING_CHAR = "0";
    private static final int MARK_LENGTH = 4;
    private static final int NUMBER_LENGTH = 10;
    private static final Pattern MARK_PATTERN = Pattern.compile("([a-z]){2,4}", Pattern.CASE_INSENSITIVE);
    private static final Pattern NUMBER_PATTERN = Pattern.compile("([0-9]){1,10}");
    private static final Pattern EQUIP_ID_PATTERN = Pattern.compile("([a-z]){2,4}(\\s){0,2}([0-9]){1,10}", Pattern.CASE_INSENSITIVE);


    private EquipmentUtils() {
        throw new UnsupportedOperationException("This class should not be instantiated");
    }


    public static boolean isValidEquipmentId(String equipment) {
        return EQUIP_ID_PATTERN.matcher(equipment).matches();
    }


    public static boolean isValidNumber(String number) {
        return NUMBER_PATTERN.matcher(number).matches();
    }


    public static String extractMark(String equipment) {
        Matcher markMatcher = MARK_PATTERN.matcher(equipment);
        if(markMatcher.find()) {
            return markMatcher.group();
        }
        LOGGER.debug("Equipment mark not found in equipment {}", equipment);
        return "";
    }


    public static String extractNumber(String equipment) {
        Matcher numberMatcher = NUMBER_PATTERN.matcher(equipment);
        if(numberMatcher.find()) {
            return numberMatcher.group();
        }
        LOGGER.debug("Equipment number not found in equipment {}", equipment);
        return "";
    }

    public static Long extractNumberAsLong(String equipment) {
    	try {
    		return Long.parseLong(extractNumber(equipment));
    	} catch(NumberFormatException e) {
    		return null;
    	}
    }

    public static String extractNormalizedMark(String equipment) {
        return normalizeMark(extractMark(equipment));
    }


    public static String extractNormalizedNumber(String equipment) {
        return normalizeNumber(extractNumber(equipment));
    }


    public static String normalizeMark(String mark) {
        return StringUtils.rightPad(mark.toUpperCase(), MARK_LENGTH, MARK_PADDING_CHAR);
    }


    public static String normalizeNumber(String number) {
        return StringUtils.leftPad(number, NUMBER_LENGTH, NUMBER_PADDING_CHAR);
    }


    public static String normalizeEquipment(String equipment) {
        String mark = extractMark(equipment);
        String number = extractNumber(equipment);

        mark = StringUtils.isBlank(mark) ? mark : normalizeMark(mark);
        number = StringUtils.isBlank(number) ? number : normalizeNumber(number);

        return mark+number;
    }

    public static String getNormalizedEquipmentId(String mark, String number) {
        String equipMark = StringUtils.isBlank(mark) ? mark : normalizeMark(mark);
        String equipNumber = StringUtils.isBlank(number) ? number : normalizeNumber(number);

        return equipMark+equipNumber;
    }

	public static String normalizeEin(long ein) {
    	try {
    		return normalizeNumber(Long.toString(ein));
    	} catch(NumberFormatException e) {
    		return null;
    	}
	}

	public static String normalizeEquipmentId(long equipmentId) {
    	try {
    		return normalizeNumber(Long.toString(equipmentId));
    	} catch(NumberFormatException e) {
    		return null;
    	}
	}

}
