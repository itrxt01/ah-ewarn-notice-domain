package com.railinc.assethealth.eqadv.domain;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.railinc.assethealth.eqadv.domain.exception.InvalidOracleBooleanFlagException;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonIgnoreProperties({ "dbValue", "trueFalseName" })
public enum ORACLE_BOOLEAN {
	TRUE(true, "Y", "true"),
	FALSE(false, "N", "false");

	public final boolean 	value;
	public final String		dbValue;
	public final String		typeName;
	
	private static final String ERROR_MSG = "The value [%s] is not a valid value for an Oracle boolean flag!";

	ORACLE_BOOLEAN(boolean value, String initial, String trueFalse){
		this.value = value;
		this.dbValue = initial;
		this.typeName = trueFalse;
	}

	public static ORACLE_BOOLEAN valueFor(String s) throws InvalidOracleBooleanFlagException {
		if (StringUtils.isBlank(s)) {
			throw new InvalidOracleBooleanFlagException(String.format(ERROR_MSG, s));
		}

		String trimmed = s.trim();

		for(ORACLE_BOOLEAN b : ORACLE_BOOLEAN.values()) {
			// Faster than that compareTo; doesn't have to add up values of strings, stops at first mis-matched character.
			if (StringUtils.equalsIgnoreCase(b.dbValue, trimmed) ||
					StringUtils.equalsIgnoreCase(b.typeName, trimmed)) {
				return b;
			}
		}
		throw new InvalidOracleBooleanFlagException(String.format(ERROR_MSG, s));
	}

	public static ORACLE_BOOLEAN valueFor(boolean b) throws InvalidOracleBooleanFlagException {
		for(ORACLE_BOOLEAN ob : ORACLE_BOOLEAN.values()) {
			if(ob.value == b) {
				return ob;
			}
		}
		throw new InvalidOracleBooleanFlagException(String.format(ERROR_MSG, b));
	}
}
