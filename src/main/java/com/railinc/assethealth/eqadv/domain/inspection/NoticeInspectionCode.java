package com.railinc.assethealth.eqadv.domain.inspection;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.railinc.assethealth.eqadv.domain.EWNoticeAttribute;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;

/**
 * This represents a permitted inspection for a notice at a Severity level.
 * @author ron taylor
 */
public class NoticeInspectionCode extends EWNoticeAttribute {
	private static final long serialVersionUID = -7039064255422190351L;
	private InspectionCode inspectionCode;
    private SeverityCode severityCode;
    private TimePeriod period;

    public NoticeInspectionCode(){ /* Zero arg c'tor for deserialzation */}

    public NoticeInspectionCode(InspectionCode code, SeverityCode severity){
    	this.severityCode = severity;
    	this.inspectionCode = code;
    }
    
    public InspectionCode getInspectionCode() {
        return inspectionCode;
    }

    public void setInspectionCode(InspectionCode inspectionCode) {
        this.inspectionCode = inspectionCode;
    }
    
    public String getCode() {
    	return inspectionCode.getCode();
    }

    public boolean isPeriodic() {
    	return inspectionCode.isPeriodic();
    }
    
    public SeverityCode getSeverityCode() {
        return severityCode;
    }

    public void setSeverityCode(SeverityCode severityCode) {
        this.severityCode = severityCode;
    }

    public boolean isFinal() {
        return inspectionCode.isFinal();
    }

    public void setFrequency(TimePeriod period) {
    	this.period = period;
    }
    
    public int getFrequency() {
    	return null == period ? 0 : period.getTimePeriod();
    }
    
    public String getFrequencyUnits() {
    	return null == period ? null : period.getTimeUnit().units;
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false);
    }

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false);
	}	
}
