package com.railinc.assethealth.eqadv.domain.category;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.railinc.assethealth.eqadv.domain.Constants;
import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;
import com.railinc.assethealth.eqadv.domain.exception.InvalidCategoryException;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevels;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Notice category.
 * 
 * @author ron taylor
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonRootName("category")
@JsonPropertyOrder({"code", "description", "severitylevels"})
public final class Category extends EWDomainAbstract implements Serializable {
	private static final long serialVersionUID = -3210317466605272306L;
	private static final String VALIDATION_MSG_FORMAT = String.format("%%s advisory number must be a positive number greater than [%d]", Constants.PUBLISHED_NOTICE_NUMBER_START);
	private static final String MINIMUM = "Minimum";
	private static final String MAXIMUM = "Maximum";
	private String description;
	private String code;
	private Integer minAdvisoryNumber;
	private Integer maxAdvisoryNumber;
	private SeverityLevels severityLevels;

	public Category() {}
	
	public Category(String code) {
		this.code = code;
		description = "";
	}
	
	public Category(String code, String desc) {
		this.code = code;
		description = desc;
	}
	
	public Category(String code, String desc, Integer min, Integer max) throws InvalidCategoryException {
		this.code = code;
		description = desc;
		this.setMinAdvisoryNumber(min);
		this.setMaxAdvisoryNumber(max);
		validateMinMax();
	}

	public void setDescription(String desc) {
		this.description = desc;
	}
	public String getDescription() {
		return description;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	
	@JsonIgnore
	public Integer getMinAdvisoryNumber() {
		return minAdvisoryNumber;
	}

	public void setMinAdvisoryNumber(Integer min) throws InvalidCategoryException {
		validateValue(MINIMUM, min);
		this.minAdvisoryNumber = min;
		if(null != maxAdvisoryNumber) {
			validateMinMax();
		}
	}

	@JsonIgnore
	public Integer getMaxAdvisoryNumber() {
		return maxAdvisoryNumber;
	}

	public void setMaxAdvisoryNumber(Integer max) throws InvalidCategoryException {
		validateValue(MAXIMUM, max);
		this.maxAdvisoryNumber = max;
		if(null != minAdvisoryNumber) {
			validateMinMax();
		}
	}
	
	@JsonIgnore
	public boolean isValidAdvisoryNumber(Integer advisoryNumber) {
		try {
			// Can't assume min and max were set
			validateValue(MINIMUM, this.minAdvisoryNumber);
			validateValue(MAXIMUM, this.maxAdvisoryNumber);
			return advisoryNumber >= minAdvisoryNumber && advisoryNumber <= maxAdvisoryNumber;
		} catch (InvalidCategoryException e) {
			throw new UnsupportedOperationException("Minimum and Maximum Advisory Numbers must be set in order to evaluate", e);			
		}		
	}
	
	
	private void validateValue(String name, Integer value) throws InvalidCategoryException {
		if(null == value || value <= 0 || value >= Constants.DRAFT_NOTICE_NUMBER_START) {
			throw new InvalidCategoryException(String.format(VALIDATION_MSG_FORMAT, name));			
		}
	}
		
	private void validateMinMax() throws InvalidCategoryException {
		if (!isValidMinAndMax()) {
			throw new InvalidCategoryException(
					String.format("Advisory numbers must be positive numbers and minimum Advisory Number[%d] "
							+ "must be less than or equal to the maximum Advisory Number[%d]!", minAdvisoryNumber, maxAdvisoryNumber));
		}
	}
	
	private boolean isValidMinAndMax() throws InvalidCategoryException {
		validateValue(MINIMUM, this.minAdvisoryNumber);
		validateValue(MAXIMUM, this.maxAdvisoryNumber);
		return minAdvisoryNumber <= maxAdvisoryNumber;
	}

	@JsonInclude(Include.NON_NULL)
	@JsonUnwrapped
	public SeverityLevels getSeverityLevels() {
		return severityLevels;
	}
	
	public void addSeverityLevel(SeverityLevel severityLevel) {
		if(null == this.severityLevels) {
			this.severityLevels = new SeverityLevels();
		}
		this.severityLevels.addSeverityLevel(severityLevel);
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
	
	@Override
    public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(31, 7, this, false, Category.class, "description");
    }

	@Override
    public boolean equals(Object obj) {
          return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass(), "description");
    }	
}
