package com.railinc.assethealth.eqadv.domain.mark;

/**
 * Marks must be no longer than 4 characters in length and match the pattern:
 * 
 * AAA, Cxxx, Dxxx - shop mark
 * 
 * This is just an initial validation.  Final validation is against the list of marks retrieved from Umler.
 * 
 * @author ron taylor
 *
 */
public class RunningRepairAgentMark extends Mark {
	private static final long serialVersionUID = -2243725716643830849L;
	static final String RUNNING_REPAIR_AGENT_REGEX_PATTERN = "^([(a,A)]([0-9]{3}))$";
	private static final String RUNNING_REPAIR_AGENT_ERROR_MSG_FORMAT = "The value[%s] is not a valid running repair agent mark!";
	private static final boolean IS_RUNNING_REPAIR_AGENT_EQUIPMENT_OWNER = false;
	
	RunningRepairAgentMark(String mark) {
		super(mark);
	}
	
	RunningRepairAgentMark(String mark, Mark parent) {
		super(mark, parent);
	}
	
	RunningRepairAgentMark(Mark mark) {
		super(mark);
	}
	
	@Override
	protected String getRegex() {
		return RUNNING_REPAIR_AGENT_REGEX_PATTERN;
	}

	@Override
	protected String getErrorMessageFormat() {
		return RUNNING_REPAIR_AGENT_ERROR_MSG_FORMAT;
	}

	@Override
	protected boolean getIsEquipmentOwnerMark() {
		return IS_RUNNING_REPAIR_AGENT_EQUIPMENT_OWNER;
	}
}
