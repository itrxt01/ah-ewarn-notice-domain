package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidMarkException extends Exception {
	private static final long serialVersionUID = 6901536979493351380L;

	public InvalidMarkException(String msg) {
		super(msg);
	}

}
