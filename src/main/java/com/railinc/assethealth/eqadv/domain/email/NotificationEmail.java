package com.railinc.assethealth.eqadv.domain.email;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;

public class NotificationEmail extends EWDomainAbstract implements Serializable {

	private static final long serialVersionUID = -4737692258823914450L;
	
	private String company;
	private int notificationEventId;
	private String email;

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getNotificationEventId() {
		return notificationEventId;
	}

	public void setNotificationEventId(int notificationEventId) {
		this.notificationEventId = notificationEventId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, false);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass());
	}
}
