package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidEquipmentIdException extends Exception {
	private static final long serialVersionUID = -6090744668538349009L;

	public InvalidEquipmentIdException(String msg) {
		super(msg);
	}

}
