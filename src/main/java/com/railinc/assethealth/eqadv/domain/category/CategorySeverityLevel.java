package com.railinc.assethealth.eqadv.domain.category;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;

/**
 * This class captures the severity level defaults specified in the
 * database for a specific category.
 * 
 * @author ron taylor
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonRootName("CategorySeverityLevel")
@JsonPropertyOrder({"category", "severityLevel"})
public class CategorySeverityLevel extends EWDomainAbstract implements Serializable {
	private static final long serialVersionUID = -5684568950771782940L;
	SeverityLevel severityLevel;
	Category	category;

	
	public CategorySeverityLevel(Category category, SeverityLevel severityLevel){
		this.category = category;
		this.severityLevel = severityLevel;
	}


	public SeverityLevel getSeverityLevel() {
		return severityLevel;
	}


	public void setSeverityLevel(SeverityLevel severityLevel) {
		this.severityLevel = severityLevel;
	}


	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
	
	@Override
    public int hashCode() {
          return HashCodeBuilder.reflectionHashCode(this);
    }

	@Override
    public boolean equals(Object obj) {
		// Only care if the actual code is the same for equality
		return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass());
    }
	
}
