package com.railinc.assethealth.eqadv.domain.severity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.railinc.assethealth.eqadv.domain.equipment.Assignment;

@JsonDeserialize(using=SeverityLevelsDeserializer.class)
public class SeverityLevels implements Serializable {
	private static final long serialVersionUID = 4457550895398089492L;
	
	private List<SeverityLevel> severityLevelsList = new ArrayList<>();
	
	public SeverityLevels() { /* Zero arg c'tor for object mapper */ }
	
	public SeverityLevels(Collection<SeverityLevel> severityLevels){
		this.severityLevelsList.addAll(severityLevels);
		Collections.sort(this.severityLevelsList);
		linkLevels(this.severityLevelsList);
		Collections.reverse(this.severityLevelsList);
	}
	
	public void addSeverityLevel(SeverityLevel severityLevel) {
		severityLevelsList.add(severityLevel);
		Collections.sort(this.severityLevelsList);
		linkLevels(this.severityLevelsList);
		Collections.reverse(this.severityLevelsList);
	}

	// Link severity levels when adding to collection
	private void linkLevels(List<SeverityLevel> levels) {
		// Link ascending sort of levels
		for(SeverityLevel level : levels) {
			SeverityLevel escalationLevel = level.getEscalationLevel();
			if(null != escalationLevel) {
				for(SeverityLevel escLevel : levels) {
					if(escLevel.getSeverityCode().equals(escalationLevel.getSeverityCode())) {
						level.setEscalationLevel(escLevel);
						break;
					}
				}
			}
		}		
	}
	
	public List<SeverityLevel> getSeverityLevels(){
		return severityLevelsList;
	}
	
	public void setSeverityLevels(SeverityLevel[] levels){
		for(SeverityLevel level : levels) {
			this.addSeverityLevel(level);
		}
		linkLevels(this.severityLevelsList);
	}
	
	public void setSeverityLevels(Collection<SeverityLevel> levels){
		for(SeverityLevel level : levels) {
			this.addSeverityLevel(level);
		}
		linkLevels(this.severityLevelsList);
	}
	
	@JsonIgnore
	public boolean isValid() {
		return CollectionUtils.isNotEmpty(severityLevelsList);
	}

	public boolean hasSeverity(String code) {
		return getSeverityLevel(code) != null;
	}
	
	public SeverityLevel getSeverityLevel(String code) {
		return severityLevelsList
				.stream()
				.filter(s -> s.getSeverityLevelCode().equals(code))
				.findFirst()
				.orElse(null);
	}	
	
	public SeverityLevel getEscalationLevel(SeverityLevel level) {
		SeverityLevel escalationLevel =  severityLevelsList
				.stream()
				.filter(s -> null != level && s.getSeverityCode().equals(level.getSeverityCode()) && null != s.getEscalationLevel())
				.findFirst()
				.orElse(null);
		return (escalationLevel != null ? escalationLevel.getEscalationLevel() : null);
	}	
	
	public SeverityLevel getEscalationLevel(Assignment equipment) {
		SeverityLevel severity =  getSeverityLevel(equipment.getSeverity().getSeverityLevelCode());
		return (severity != null ? severity.getEscalationLevel() : null);
	}
	
	public Date getEscalationDate(Assignment equipment) {
		SeverityLevel severity =  getSeverityLevel(equipment.getSeverity().getSeverityLevelCode());
		return (severity != null ? severity.getEscalationDate(equipment.getAssignedDate()) : null);
	}

	public boolean isTimeToEscalate(Assignment equipment) {
		SeverityLevel severity =  getSeverityLevel(equipment.getSeverity().getSeverityLevelCode());
		return severity != null && severity.isTimeToEscalate(equipment.getAssignedDate());
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}		
	
	@Override
    public int hashCode() {
        int value = 0;
	    for(SeverityLevel level : severityLevelsList) {
	    	  value += level.hashCode();
	    }
	    return value;
    }

	@Override
	public boolean equals(Object obj) { // NOSONAR - equals builder tests type of Object parameter
		boolean equals = EqualsBuilder.reflectionEquals(this, obj, false, this.getClass(), "description",
				"severityLevels");
		if (equals && null != obj && obj.getClass() == this.getClass()) {
			SeverityLevels that = (SeverityLevels) obj;
			List<SeverityLevel> levels = new ArrayList<>(severityLevelsList);
			if (levels.size() == that.getSeverityLevels().size()) {
				levels.removeAll((that.getSeverityLevels()));
				return levels.isEmpty();
			}
		}
		return false;
	}

}
