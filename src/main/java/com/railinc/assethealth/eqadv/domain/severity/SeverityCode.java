/**
 * Copyright (c) Railinc - 2018. All rights reserved.
 */
package com.railinc.assethealth.eqadv.domain.severity;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonRootName;


/**
 * @author G.B.
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonRootName("SeverityCode")
public final class SeverityCode implements Serializable {
	private static final long serialVersionUID = 7956655153835925282L;
	private final String code;
	private String description;
	
	public SeverityCode(String severityCode, String description){
		this.code = severityCode;
		this.description = description;
	}
	
	public SeverityCode(String severityCode){
		this.code = severityCode;
		this.description = null;
	}
	
	public String getSeverityCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String desc) {
		this.description = desc;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
	
	@Override
    public int hashCode() {
          return HashCodeBuilder.reflectionHashCode(this, "description");
    }

	@Override
    public boolean equals(Object obj) {
		// Only care if the actual code is the same for equality
		return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass(), "description");
    }

}
