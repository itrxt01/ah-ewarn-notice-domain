package com.railinc.assethealth.eqadv.domain.equipment;

import org.apache.commons.lang3.StringUtils;

import com.railinc.assethealth.eqadv.domain.exception.InvalidEquipmentStatusException;

public enum EQUIPMENT_STATUS {
	PREREGISTRED("P", "Pre-registered", "Pre-registered in Umler.")
	, ACTIVE("A", "Active", "Active in Umler.")
	, INACTIVE("I", "Inactive", "Inactive in Umler");

	public final String code;  // NOSONAR - immutable enum
	public final String desc;  // NOSONAR - immutable enum
	public final String tooltip; // NOSONAR - immutable enum
	
	EQUIPMENT_STATUS(String code, String desc, String tooltip){
		this.code = code;
		this.desc = desc;
		this.tooltip = tooltip;
	}

	public static EQUIPMENT_STATUS forString(String str) throws InvalidEquipmentStatusException {
		if(StringUtils.isBlank(str)) {
			throw new InvalidEquipmentStatusException("The value[null] is not a recognized equipment status!");			
		}
		String temp = str.toUpperCase().trim();
		for(EQUIPMENT_STATUS status : EQUIPMENT_STATUS.values()) {
			if(status.code.equals(temp)) {
				return status;
			}
		}
		throw new InvalidEquipmentStatusException(String.format("The value[%s] is not a recognized equipment status!", str));
	}
}
