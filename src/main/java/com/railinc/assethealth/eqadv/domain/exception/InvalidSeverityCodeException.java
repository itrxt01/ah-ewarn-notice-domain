package com.railinc.assethealth.eqadv.domain.exception;

public class InvalidSeverityCodeException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidSeverityCodeException(String msg) {
		super(msg);
	}

}
