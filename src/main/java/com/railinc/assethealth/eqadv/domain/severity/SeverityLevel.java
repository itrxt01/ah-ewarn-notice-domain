package com.railinc.assethealth.eqadv.domain.severity;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.railinc.assethealth.eqadv.domain.EWNoticeAttribute;
import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"severityLevel", "qty", "description", "escalationLevel", "escalationPeriod", "escalationPeriodUnit", "escalationType", "escalationQty"})
public class SeverityLevel extends EWNoticeAttribute implements Comparable<SeverityLevel> {
	private static final long serialVersionUID = -8213738595974451408L;
	private TimePeriod escalationPeriod = null;
	private SeverityCode severityCode;
	private SeverityLevel escalationLevel = null;
	private int qty;
	private String escalationType;
	private int escalationQty;
	
	public SeverityLevel(){ /* Zero arg c'tor for object mapper */ }
	
	public SeverityLevel(String level){
		this.severityCode = new SeverityCode(level);
		this.escalationPeriod = null;
	}

	public SeverityLevel(String level, String description){
		this.severityCode = new SeverityCode(level, description);
		this.escalationPeriod = null;
	}

	public SeverityLevel(SeverityCode level){
		this.severityCode = level;
	}	
	
	public SeverityLevel(String level, String escalationLevel, int period, String units) throws InvalidPeriodicTimeUnitException{
		this.severityCode = new SeverityCode(level);
		this.escalationLevel = new SeverityLevel(escalationLevel);
		this.escalationPeriod = new TimePeriod(period, PERIODIC_TIME_UNIT.getTimeUnitForName(units));	
		this.escalationType = ESCALATION_TYPE.DATE.value;
	}
	
	public SeverityLevel(SeverityCode level, SeverityLevel escalationLevel, TimePeriod escalationPeriod){
		this.severityCode = level;
		this.escalationLevel = escalationLevel;
		this.escalationPeriod = escalationPeriod;
		this.escalationType = ESCALATION_TYPE.DATE.value;
	}
	
	public SeverityLevel(SeverityCode level, SeverityLevel escalationLevel, TimePeriod escalationPeriod, String escalationType , int escalationQty){
		this.severityCode = level;
		this.escalationLevel = escalationLevel;
		this.escalationPeriod = escalationPeriod;
		this.escalationType = escalationType;
		this.escalationQty = escalationQty;
	}
	
	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
	
	@JsonIgnore
	public boolean isEscalates() {
		return null != this.escalationLevel && StringUtils.isNotBlank(escalationType);
	}

	@JsonIgnore
	public SeverityCode getSeverityCode() {
		return severityCode;
	}
	
	@JsonProperty("severityLevel")
	public void setSeverityLevel(SeverityCode code) {
		this.severityCode = code;
	}

	@JsonProperty("severityLevel")
	public String getSeverityLevelCode() {
		return severityCode.getSeverityCode();
	}

	@JsonProperty("description")
	public String getSeverityLevelDesc() {
		return severityCode.getDescription();
	}

	@JsonProperty("description")
	public void setSeverityLevelDesc(String desc) {
		severityCode.setDescription(desc);
	}
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("escalationLevel")
	public String getEscalationLevelCode() {
		if(null == this.escalationLevel || StringUtils.isBlank(this.escalationLevel.getSeverityCode().getSeverityCode())) {
			return null;
		}
		return escalationLevel.getSeverityCode().getSeverityCode();
	}
	
	@JsonProperty("escalationLevel")
	public void setEscalationLevel(SeverityLevel level) {
		this.escalationLevel = level;
	}

	@JsonInclude(Include.NON_DEFAULT)
	@JsonProperty("escalationPeriod")
	public int getEscalationPeriodValue() {
		if(null == escalationPeriod || null == escalationLevel) {
			return 0;
		}
		return escalationPeriod.getTimePeriod();
	}	
	
	public void setEscalationPeriodValue(int period) {
		if(null == escalationPeriod) {
			escalationPeriod = new TimePeriod();
		}
		escalationPeriod.setTimePeriod(period);		
	}
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("escalationPeriodUnit")
	public String getEscalationPeriodUnit() {
		if(null == escalationPeriod || null == escalationLevel) {
			return null;
		}
		return escalationPeriod.getTimeUnit().units;
	}	
	
	public void setEscalationPeriodUnit(String unit) throws InvalidPeriodicTimeUnitException {
		if(null == escalationPeriod) {
			escalationPeriod = new TimePeriod();
		}
		escalationPeriod.setTimeUnit(unit);		
	}
	
	@JsonIgnore
	public TimePeriod getEscalationPeriod() {
		return escalationPeriod;
	}
	
	public void setEscalationPeriod(TimePeriod period) {
		this.escalationPeriod = period;
	}
	
	@JsonIgnore
	public boolean isTimeToEscalate(Date date) {
		if(null != escalationLevel && null != escalationPeriod) {
			return escalationPeriod.isElapsed(date);			
		}
		return false;
	}
	
	@JsonIgnore
	public Date getEscalationDate(Date date) {
		if(null != escalationLevel && null != escalationPeriod) {
			return escalationPeriod.addPeriod(date);			
		}
		return null;
	}

	public SeverityLevel getEscalationLevel() {
		return escalationLevel;			
	}
	
	@JsonProperty("escalationType")
	public String getEscalationType() {
		return escalationType;
	}

	public void setEscalationType(String escalationType) {
		this.escalationType = escalationType;
	}
	
	@JsonProperty("escalationQty")
	public int getEscalationQty() {
		return escalationQty;
	}

	public void setEscalationQty(int escalationQty) {
		this.escalationQty = escalationQty;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, this.getClass());
	}

	@Override
	// Ascending lowest to highest.
	public int compareTo(SeverityLevel o) {
		final int EQUAL = 0;
		final int GREATER = 1;
		final int LESS = -1;

        if (this == o) return EQUAL;
        if (o == null || getClass() != o.getClass()) return GREATER;
        SeverityLevel that = o;
        if( this.severityCode.equals(that.severityCode) ) {
        	return EQUAL;  // Same severity code
        }         	
        
        if(!this.isEscalates() && that.isEscalates()) {
        	return GREATER;  // This one doesn't escalate, so this is higher level
        } else if(this.isEscalates() && !that.isEscalates()) {
        	return LESS;  // The other one doesn't escalate, so it is higher level 
        } else if(!this.isEscalates() && !that.isEscalates()) {
        	return EQUAL;  // Neither escalates, so both at highest level
        }

        // Both escalate
		if(this.escalationLevel.severityCode.equals(that.severityCode)) {
			return LESS;  // This escalates to the other, so the other is higher level
		} else if(that.escalationLevel.severityCode.equals(this.severityCode)) {
			return GREATER;  // The other escalates to this, so this is higher level
		} else if(this.escalationLevel.severityCode.equals(that.escalationLevel.severityCode)) {
			return EQUAL;  // Both escalate to the same level
		} else {
			return this.escalationLevel.compareTo(that.escalationLevel);
		}        
	}
}
