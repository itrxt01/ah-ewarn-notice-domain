package com.railinc.assethealth.eqadv.domain.mark;

import java.io.Serializable;
import java.util.Objects;

public class ReportingMark implements Serializable {
	private static final long serialVersionUID = -3468564465866443687L;
	private String mark;
    private String ownerOfInitial;
    private String markEntry;
    private String description;

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getOwnerOfInitial() {
        return ownerOfInitial;
    }

    public void setOwnerOfInitial(String ownerOfInitial) {
        this.ownerOfInitial = ownerOfInitial;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMarkEntry() {
        return markEntry;
    }

    public void setMarkEntry(String markEntry) {
        this.markEntry = markEntry;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportingMark that = (ReportingMark) o;
        return Objects.equals(mark, that.mark) &&
                Objects.equals(ownerOfInitial, that.ownerOfInitial) &&
                Objects.equals(markEntry, that.markEntry);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mark, ownerOfInitial, markEntry);
    }
}
