package com.railinc.assethealth.eqadv.domain.status;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidStatusException;
import com.railinc.assethealth.eqadv.domain.status.NOTICE_STATUS;

public class NOTICE_STATUSTest {
	
	@Test
	public void testHappyPath() throws InvalidStatusException {
		assertEquals(NOTICE_STATUS.DRAFT, NOTICE_STATUS.forCode("Draft"));
		assertEquals(NOTICE_STATUS.PUBLISHED, NOTICE_STATUS.forCode("Published"));
	}

	@Test
	public void testTrimming() throws InvalidStatusException {
		assertEquals(NOTICE_STATUS.DRAFT, NOTICE_STATUS.forCode("  Draft"));
		assertEquals(NOTICE_STATUS.DRAFT, NOTICE_STATUS.forCode("Draft  "));
		assertEquals(NOTICE_STATUS.DRAFT, NOTICE_STATUS.forCode("  Draft  "));
		
		assertEquals(NOTICE_STATUS.PUBLISHED, NOTICE_STATUS.forCode("  Published"));
		assertEquals(NOTICE_STATUS.PUBLISHED, NOTICE_STATUS.forCode("Published  "));
		assertEquals(NOTICE_STATUS.PUBLISHED, NOTICE_STATUS.forCode("  Published  "));
	}

	@Test
	public void testIgnoresCase() throws InvalidStatusException {
		assertEquals(NOTICE_STATUS.DRAFT, NOTICE_STATUS.forCode("draft"));
		assertEquals(NOTICE_STATUS.DRAFT, NOTICE_STATUS.forCode("DRAFT"));
		
		assertEquals(NOTICE_STATUS.PUBLISHED, NOTICE_STATUS.forCode("PUBLISHED"));
		assertEquals(NOTICE_STATUS.PUBLISHED, NOTICE_STATUS.forCode("published"));
	}

	@Test(expected=InvalidStatusException.class)
	public void testNull() throws InvalidStatusException {
		String nullString = null;
		NOTICE_STATUS.forCode(nullString);
	}

	@Test(expected=InvalidStatusException.class)
	public void testEmptyString() throws InvalidStatusException {
		String emptyString = "";
		NOTICE_STATUS.forCode(emptyString);
	}

	@Test(expected=InvalidStatusException.class)
	public void testBlankString() throws InvalidStatusException {
		String blankString = "         ";
		NOTICE_STATUS.forCode(blankString);
	}

	@Test(expected=InvalidStatusException.class)
	public void testInvalidCode() throws InvalidStatusException {
		String invalidCode = "PANCAKES AND PICKLES";
		NOTICE_STATUS.forCode(invalidCode);
	}
}