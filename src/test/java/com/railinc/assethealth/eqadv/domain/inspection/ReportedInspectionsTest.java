package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.*;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.inspection.INSPECTION_TYPE;
import com.railinc.assethealth.eqadv.domain.inspection.InspectionCode;
import com.railinc.assethealth.eqadv.domain.inspection.ReportedInspections;

public class ReportedInspectionsTest {
	
	@Test
	public void testAddInspectionQty() {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "FINAL");
		ReportedInspections inspections = new ReportedInspections();
		inspections.addInspectionQty(code, 123_456);
		assertEquals(Integer.valueOf(123_456), inspections.getReportedInspections().get(code.getCode()));
	}

	@Test
	public void testGetReportedInspections() {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "FINAL");
		ReportedInspections inspections = new ReportedInspections();
		assertNotNull(inspections.getReportedInspections());
		assertTrue(inspections.getReportedInspections().isEmpty());
		inspections.addInspectionQty(code, 4);
		assertEquals(Integer.valueOf(4), inspections.getReportedInspections().get(code.getCode()));
	}

}
