package com.railinc.assethealth.eqadv.domain.errors;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.errors.ErrorMessage;

public class ErrorMessageTest {

	@Test
	public void testErrorMessage_NULL_MESSAGE() {
		ErrorMessage msg = new ErrorMessage();
		assertNull(msg.getMessage());
	}

	@Test
	public void testErrorMessage_EMPTY_MESSAGE() {
		ErrorMessage msg = new ErrorMessage("");
		assertTrue(StringUtils.isBlank(msg.getMessage()));
	}

	@Test
	public void testErrorMessage_BLANK_MESSAGE() {
		ErrorMessage msg = new ErrorMessage("   ");
		assertTrue(StringUtils.isBlank(msg.getMessage()));
	}
	
	@Test
	public void testErrorMessage() {
		ErrorMessage msg = new ErrorMessage("This is a test message");
		assertTrue(msg.getMessage().equals("This is a test message"));
	}

	@Test
	public void testSetErrorMessage() {
		ErrorMessage msg = new ErrorMessage("This is a test message");
		msg.setMessage("This is a different message");
		assertTrue(msg.getMessage().equals("This is a different message"));
	}

}
