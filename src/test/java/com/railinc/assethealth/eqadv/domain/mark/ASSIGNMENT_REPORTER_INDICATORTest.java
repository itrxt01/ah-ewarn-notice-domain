package com.railinc.assethealth.eqadv.domain.mark;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.exception.EWDeserializationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidAssignmentReporterIndicatorException;
import com.railinc.assethealth.eqadv.domain.mark.ASSIGNMENT_REPORTER_INDICATOR;

public class ASSIGNMENT_REPORTER_INDICATORTest {
	@Test
	public void testHappyPath() throws InvalidAssignmentReporterIndicatorException {
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY, ASSIGNMENT_REPORTER_INDICATOR.forIndicator("A"));
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.EQUIPMENT_OWNER, ASSIGNMENT_REPORTER_INDICATOR.forIndicator("E"));
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.INTERNAL_ONLY, ASSIGNMENT_REPORTER_INDICATOR.forIndicator("I"));
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.SPECIFIED, ASSIGNMENT_REPORTER_INDICATOR.forIndicator("S"));
	}
	
	@Test
	public void testTrim() throws InvalidAssignmentReporterIndicatorException {
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY, ASSIGNMENT_REPORTER_INDICATOR.forIndicator(" A"));
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY, ASSIGNMENT_REPORTER_INDICATOR.forIndicator("A "));
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY, ASSIGNMENT_REPORTER_INDICATOR.forIndicator(" A "));
	}
	
	@Test
	public void testCase() throws InvalidAssignmentReporterIndicatorException {
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY, ASSIGNMENT_REPORTER_INDICATOR.forIndicator("a"));
	}
	
	@Test(expected=InvalidAssignmentReporterIndicatorException.class)
	public void testNullIndicator() throws InvalidAssignmentReporterIndicatorException {
		String nullString = null;
		ASSIGNMENT_REPORTER_INDICATOR.forIndicator(nullString);
	}
	@Test(expected=InvalidAssignmentReporterIndicatorException.class)
	public void testEmptyIndicator() throws InvalidAssignmentReporterIndicatorException {
		String emptyString = "";
		ASSIGNMENT_REPORTER_INDICATOR.forIndicator(emptyString);
	}
	@Test(expected=InvalidAssignmentReporterIndicatorException.class)
	public void testBlankIndicator() throws InvalidAssignmentReporterIndicatorException {
		String blankString = "    ";
		ASSIGNMENT_REPORTER_INDICATOR.forIndicator(blankString);
	}
	@Test(expected=InvalidAssignmentReporterIndicatorException.class)
	public void testTooLong() throws InvalidAssignmentReporterIndicatorException {
		String twoCharacterIndicator = "12";
		ASSIGNMENT_REPORTER_INDICATOR.forIndicator(twoCharacterIndicator);
	}

	@Test(expected=InvalidAssignmentReporterIndicatorException.class)
	public void testUnknownIndicator() throws InvalidAssignmentReporterIndicatorException {
		String unkonwnIndicator = "1";
		ASSIGNMENT_REPORTER_INDICATOR.forIndicator(unkonwnIndicator);
	}
	
	@Test
	public void testMarshallUnmarshall() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		
		String JSON = String.format("\"%s\"", ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY.code);
		System.out.println("\"assignmentReporterInd\":" + JSON);
		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY, unmarshalled);
	}

	@Test
	public void testDeserializeINTERNAL() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		
		String JSON = "{\n" + 
				"  \"code\" : \"I\"\n" + 
				"} \n";
		System.out.println("\"assignmentReporterInd\":" + JSON);
		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.INTERNAL_ONLY, unmarshalled);
	}
	@Test
	public void testDeserializeSPECIFIED() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		
		String JSON = "{\n" + 
				"  \"code\" : \"S\"\n" + 
				"} \n";
		System.out.println("\"assignmentReporterInd\":" + JSON);
		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.SPECIFIED, unmarshalled);
	}
	@Test
	public void testDeserializeAAR_ONLY() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		
		String JSON = "{\n" + 
				"  \"code\" : \"A\"\n" + 
				"} \n";
		System.out.println("\"assignmentReporterInd\":" + JSON);
		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY, unmarshalled);
	}
	@Test
	public void testDeserialize_EQUIPMENT_OWNER() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		
		String JSON = "{\n" + 
				"  \"code\" : \"E\"\n" + 
				"} \n";
		System.out.println("\"assignmentReporterInd\":" + JSON);
		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.EQUIPMENT_OWNER, unmarshalled);
	}
	@Test(expected=EWDeserializationException.class)
	public void testDeserialize_INVALID() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		
		String JSON = "{\n" + 
				"  \"code\" : \"u\"\n" + 
				"} \n";
		System.out.println("\"assignmentReporterInd\":" + JSON);
		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.EQUIPMENT_OWNER, unmarshalled);
	}
}
