package com.railinc.assethealth.eqadv.domain.transaction;

import com.railinc.assethealth.eqadv.domain.transaction.NoticeEvent;

abstract class NoticeEventTestHelper<T extends NoticeEvent> {

    static final Long EVENT_ID = 12345L;
    static final Long NOTICE_ID = 67890L;

    abstract T getNoticeEvent();
}
