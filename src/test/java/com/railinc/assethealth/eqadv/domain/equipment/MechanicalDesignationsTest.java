package com.railinc.assethealth.eqadv.domain.equipment;

import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignation;
import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignations;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;

public class MechanicalDesignationsTest {
	TreeSet<String> stringDesignations = new TreeSet<>();
	MechanicalDesignation d1, d2, d3, d4, d5;
	MechanicalDesignations desigs;

	char getRandomChar() {
		int max = 90;
		int min = 65;
		double rand = Math.random();
		char returnVal = 0;
		returnVal = (char) ((rand * ((max - min) + 1)) + min);
		return returnVal;
	}

	@Test
	public void createDesignations() {
		int max_length = 4;
		try {
			for (int count = 0; count < 500; count++) {
				int length = max_length - count % 4;
				StringBuilder sb = new StringBuilder("");
				switch (length) {
				case 4:
					sb.append(this.getRandomChar());
				case 3:
					sb.append(this.getRandomChar());
				case 2:
					sb.append(this.getRandomChar());
				case 1:
					sb.append(this.getRandomChar());
				default:
					break;
				}
				
				if (count % 100 == 0) {
					d1 = new MechanicalDesignation(sb.toString());
				}
				if (count % 200 == 0) {
					d2 = new MechanicalDesignation(sb.toString());
				}
				if (count % 300 == 0) {
					d3 = new MechanicalDesignation(sb.toString());
				}
				if (count % 400 == 0) {
					d4 = new MechanicalDesignation(sb.toString());
				}
				if (count % 500 == 0) {
					d5 = new MechanicalDesignation(sb.toString());
				}

				stringDesignations.add(sb.toString());
			}
		} catch (InvalidMechanicalDesignationException e) {
			Assert.fail("Exception creating a valid mechanical designation.");
		}
	}
}
