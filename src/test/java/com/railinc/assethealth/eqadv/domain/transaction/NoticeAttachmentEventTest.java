package com.railinc.assethealth.eqadv.domain.transaction;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.transaction.NoticeAttachmentEvent;

import static org.junit.Assert.assertEquals;

public class NoticeAttachmentEventTest extends NoticeEventTestHelper<NoticeAttachmentEvent> {

    private static final String FILE_NAME = "fileName";
    private static final String FILE_COMMENT = "fileComment";
    private static final Integer FILE_SIZE = 5000;
    private static final byte[] FILE_CONTENT = new byte[]{'C', 'o', 'n', 't', 'e', 'n', 't'};
    private static final String FILE_TYPE = "pdf";
    private static final String MD5 = "a1b2c3d4e5f6";

    @Test
    public void testNoticeAttachmentEvent() {
        NoticeAttachmentEvent noticeAttachmentEvent = getNoticeEvent();

        assertEquals(EVENT_ID, noticeAttachmentEvent.getEventId());
        assertEquals(NOTICE_ID, noticeAttachmentEvent.getNoticeId());
        assertEquals(FILE_NAME, noticeAttachmentEvent.getFileName());
        assertEquals(FILE_COMMENT, noticeAttachmentEvent.getFileComment());
        assertEquals(FILE_SIZE, noticeAttachmentEvent.getFileSize());
        assertEquals(FILE_CONTENT, noticeAttachmentEvent.getFileContent());
        assertEquals(FILE_TYPE, noticeAttachmentEvent.getFileType());
        assertEquals(MD5, noticeAttachmentEvent.getMd5Checksum());
    }

    @Override
    NoticeAttachmentEvent getNoticeEvent() {
        NoticeAttachmentEvent noticeAttachmentEvent = new NoticeAttachmentEvent();
        noticeAttachmentEvent.setEventId(EVENT_ID);
        noticeAttachmentEvent.setNoticeId(NOTICE_ID);
        noticeAttachmentEvent.setFileName(FILE_NAME);
        noticeAttachmentEvent.setFileComment(FILE_COMMENT);
        noticeAttachmentEvent.setFileSize(FILE_SIZE);
        noticeAttachmentEvent.setFileContent(FILE_CONTENT);
        noticeAttachmentEvent.setFileType(FILE_TYPE);
        noticeAttachmentEvent.setMd5Checksum(MD5);
        return noticeAttachmentEvent;
    }
}
