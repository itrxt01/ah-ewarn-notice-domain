package com.railinc.assethealth.eqadv.domain;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.Constants;

public class ConstantsTest {

	@Test
	public void testConstants() {
		assertEquals(Integer.valueOf(500_000), Integer.valueOf(Constants.DRAFT_NOTICE_NUMBER_START));
		assertEquals(Integer.valueOf(1), Integer.valueOf(Constants.PUBLISHED_NOTICE_NUMBER_START));
		assertTrue(StringUtils.isNotBlank(Constants.DRAFT));
		assertTrue(StringUtils.isNotBlank(Constants.SUPPLEMENT));
	}
}
