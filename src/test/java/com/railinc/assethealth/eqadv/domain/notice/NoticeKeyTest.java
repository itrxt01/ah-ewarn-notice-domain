package com.railinc.assethealth.eqadv.domain.notice;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.exception.InvalidStatusException;
import com.railinc.assethealth.eqadv.domain.notice.NoticeKey;
import com.railinc.assethealth.eqadv.domain.status.NOTICE_STATUS;

public class NoticeKeyTest {

	private static Category category = new Category("FF", "Category description"); 
	@Test
	public void testNoticeKey() throws InvalidStatusException {
		NoticeKey key = new NoticeKey();
		key.setId(1234L);
		key.setNumber(5678);
		key.setStatus("DRAFT");
		key.setSupplementNumber(1);
		assertEquals((Long)1234L, key.getId());
		assertEquals((Integer)5678, key.getNumber());
		assertEquals("DRAFT", key.getStatus());
		assertEquals((Integer)1, key.getSupplementNumber());
		
	}

	@Test
	public void testNoticeKey_ARGS() {
		NoticeKey key = new NoticeKey(1234L, 567, 3, NOTICE_STATUS.ARCHIVED);
		key.setCategory(category);
		assertEquals((Long)1234L, key.getId());
		assertEquals((Integer)567, key.getNumber());
		assertEquals(category, key.getCategory());
		assertEquals(category.getCode(), key.getCategory().getCode());
		assertEquals("ARCHIVED", key.getStatus());
		assertEquals((Integer)3, key.getSupplementNumber());
		
	}
	
	@Test
	public void testGetJson() throws JsonProcessingException {
		String json = "{\"id\":1234,\"category\":\"FF\",\"number\":567,\"supplementNumber\":3,\"status\":\"ARCHIVED\"}";
		NoticeKey key = new NoticeKey(1234L, 567, 3, NOTICE_STATUS.ARCHIVED);
		key.setCategory(category);
		assertEquals(json, key.getJson());
	}
	
	@Test
	public void testGetJsonWithoutCategory() throws JsonProcessingException {
		String json = "{\"id\":1234,\"number\":567,\"supplementNumber\":3,\"status\":\"ARCHIVED\"}";
		NoticeKey key = new NoticeKey(1234L, 567, 3, NOTICE_STATUS.ARCHIVED);
		assertEquals(json, key.getJson());
	}

}
