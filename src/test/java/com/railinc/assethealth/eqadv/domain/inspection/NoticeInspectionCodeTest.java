package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.*;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.inspection.INSPECTION_TYPE;
import com.railinc.assethealth.eqadv.domain.inspection.InspectionCode;
import com.railinc.assethealth.eqadv.domain.inspection.NoticeInspectionCode;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;

public class NoticeInspectionCodeTest {

	@Test
	public void testNoticeInspectionCode() {
		NoticeInspectionCode inspectionCode = new NoticeInspectionCode();
		assertNotNull(inspectionCode);
	}

	@Test
	public void testNoticeInspectionCodeInspectionCodeSeverityCode() {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MC");
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode inspectionCode = new NoticeInspectionCode(inspCode, sevCode);
		assertNotNull(inspectionCode);
		assertEquals(sevCode, inspectionCode.getSeverityCode());
		assertEquals(inspCode, inspectionCode.getInspectionCode());
	}

	@Test
	public void testSetInspectionCode() {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MC");
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode inspectionCode = new NoticeInspectionCode();
		inspectionCode.setInspectionCode(inspCode);
		inspectionCode.setSeverityCode(sevCode);
		assertNotNull(inspectionCode);
		assertEquals(sevCode, inspectionCode.getSeverityCode());
		assertEquals(inspCode, inspectionCode.getInspectionCode());
	}

	@Test
	public void testGetCode() {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MC");
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode inspectionCode = new NoticeInspectionCode(inspCode, sevCode);
		assertNotNull(inspectionCode.getCode());
		assertEquals("MC", inspectionCode.getCode());
	}

	@Test
	public void testIsFinal() {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode inspectionCode = new NoticeInspectionCode();
		inspectionCode.setInspectionCode(inspCode);
		inspectionCode.setSeverityCode(sevCode);
		assertTrue(inspectionCode.isFinal());
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MN");
		inspectionCode.setInspectionCode(inspCode_2);
		assertFalse(inspectionCode.isFinal());
		assertFalse(inspectionCode.isPeriodic());
		assertNull(inspectionCode.getFrequencyUnits());
		assertEquals(0, inspectionCode.getFrequency());
	}

	@Test
	public void testSetFrequency() {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MT");
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode inspectionCode = new NoticeInspectionCode();
		inspectionCode.setInspectionCode(inspCode);
		inspectionCode.setSeverityCode(sevCode);
		TimePeriod tp = new TimePeriod(123, PERIODIC_TIME_UNIT.DAYS);
		inspectionCode.setFrequency(tp);
		assertEquals(tp.getTimeUnit().units, inspectionCode.getFrequencyUnits());
		assertEquals(tp.getTimePeriod(), inspectionCode.getFrequency());
		assertFalse(inspectionCode.isPeriodic());
	}
}
