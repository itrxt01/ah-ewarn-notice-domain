package com.railinc.assethealth.eqadv.domain.transaction;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.transaction.InspectionEvent;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class InspectionEventTest {

    @Test
    public void testInspectionEvent() {
        Long eventId = 12345L;
        Long inspectionId = 54321L;
        String inspectionCode = "AA";
        Date inspectionDate = new Date();
        String inspectionReporter = "BBBB";
        String statusInd = "C";
        Long eqmtAssignmentId = 10101L;
        Integer noticeNbr = 4444;
        String categoryType = "MA";
        String ein = "0000055555";
        String equipUnitInitCode = "DDDD";
        String equipUnitNbr = "2468";
        String mechanicalDesignation = "EEEE";
        String umlerOwner = "FFFF";
        String lessee = "GGGG";
        String maintenanceParty = "HHHH";
        String equipStatus = "I";
        String markOwner = "JJJJ";

        InspectionEvent inspectionEvent = new InspectionEvent();
        inspectionEvent.setEventId(eventId);
        inspectionEvent.setInspectionId(inspectionId);
        inspectionEvent.setInspectionCode(inspectionCode);
        inspectionEvent.setInspectionDate(inspectionDate);
        inspectionEvent.setInspectionReporter(inspectionReporter);
        inspectionEvent.setStatusInd(statusInd);
        inspectionEvent.setEqmtAssignmentId(eqmtAssignmentId);
        inspectionEvent.setNoticeNbr(noticeNbr);
        inspectionEvent.setCategoryCode(categoryType);
        inspectionEvent.setEin(ein);
        inspectionEvent.setEquipUnitInitCode(equipUnitInitCode);
        inspectionEvent.setEquipUnitNbr(equipUnitNbr);
        inspectionEvent.setMechanicalDesignation(mechanicalDesignation);
        inspectionEvent.setUmlerOwner(umlerOwner);
        inspectionEvent.setLessee(lessee);
        inspectionEvent.setMaintenanceParty(maintenanceParty);
        inspectionEvent.setEquipStatus(equipStatus);
        inspectionEvent.setMarkOwner(markOwner);

        assertEquals(eventId, inspectionEvent.getEventId());
        assertEquals(inspectionId, inspectionEvent.getInspectionId());
        assertEquals(inspectionCode, inspectionEvent.getInspectionCode());
        assertEquals(inspectionDate, inspectionEvent.getInspectionDate());
        assertEquals(inspectionReporter, inspectionEvent.getInspectionReporter());
        assertEquals(statusInd, inspectionEvent.getStatusInd());
        assertEquals(eqmtAssignmentId, inspectionEvent.getEqmtAssignmentId());
        assertEquals(noticeNbr, inspectionEvent.getNoticeNbr());
        assertEquals(categoryType, inspectionEvent.getCategoryCode());
        assertEquals(ein, inspectionEvent.getEin());
        assertEquals(equipUnitInitCode, inspectionEvent.getEquipUnitInitCode());
        assertEquals(equipUnitNbr, inspectionEvent.getEquipUnitNbr());
        assertEquals(mechanicalDesignation, inspectionEvent.getMechanicalDesignation());
        assertEquals(umlerOwner, inspectionEvent.getUmlerOwner());
        assertEquals(lessee, inspectionEvent.getLessee());
        assertEquals(maintenanceParty, inspectionEvent.getMaintenanceParty());
        assertEquals(equipStatus, inspectionEvent.getEquipStatus());
        assertEquals(markOwner, inspectionEvent.getMarkOwner());
    }
}
