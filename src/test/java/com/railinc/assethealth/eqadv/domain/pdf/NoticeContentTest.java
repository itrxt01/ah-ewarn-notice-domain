package com.railinc.assethealth.eqadv.domain.pdf;

import static org.junit.Assert.*;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidContentTypeException;
import com.railinc.assethealth.eqadv.domain.pdf.CONTENT_TYPE;
import com.railinc.assethealth.eqadv.domain.pdf.NoticeContent;

public class NoticeContentTest {

	@Test(expected=Test.None.class)
	public void testTemplateContentType() throws InvalidContentTypeException {
		NoticeContent content = new NoticeContent();		
		content.setTemplateContentType(CONTENT_TYPE.TXT);		
		assertEquals(CONTENT_TYPE.TXT, content.getTemplateContentType());
		content.setTemplateContentType(CONTENT_TYPE.PDF);		
		assertEquals(CONTENT_TYPE.PDF, content.getTemplateContentType());
	}

	@Test(expected=InvalidContentTypeException.class)
	public void testInvalidTemplateContentTypeText() throws InvalidContentTypeException {
		NoticeContent content = new NoticeContent();		
		content.setTemplateContentType(CONTENT_TYPE.forType("INVALID"));		
	}
}
