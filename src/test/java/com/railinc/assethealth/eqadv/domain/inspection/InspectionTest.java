package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.ORACLE_BOOLEAN;
import com.railinc.assethealth.eqadv.domain.equipment.Assignment;
import com.railinc.assethealth.eqadv.domain.equipment.Equipment;
import com.railinc.assethealth.eqadv.domain.exception.InvalidAssignedEquipmentException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidEquipmentIdException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;
import com.railinc.assethealth.eqadv.domain.notice.Notice;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevels;

public class InspectionTest {
	private static final Integer NOTICE_NUMBER = 100_001_005;
	private static final Long NOTICE_ID = 500_000_001L;
	private static final Long EIN = 1_001_002_003L;
	private static final String MARK = "MARK";
	private static final Long EQUIP_NUMBER = 987_654_321L;
	private static final Date NOW = new Date();
	private static final String USER_NAME = "USER_NAME";
	private static final String EQUIP_MECH_DESIG = "T";
	private static final String REPORTER = "reporter";
	private static final String COMPANY = "COMPANY";
	private static final long ID = 321_654_987L;
	private static final String INSPECTION_CODE = "EDOC";
	

	@Test
	public void testInspection() {
		Inspection insp = getInspection();
		assertNotNull(insp);
		assertEquals(ORACLE_BOOLEAN.TRUE.dbValue, insp.getActiveFlag());
		assertEquals(NOW, insp.getActivityDate());
		assertEquals(REPORTER, insp.getActivityReporter());
		assertEquals(COMPANY, insp.getCreateCompany());
		assertEquals(NOW, insp.getCreatedTimestamp());
		assertEquals(USER_NAME, insp.getCreateUsername());
		assertEquals(EIN, insp.getEin());
		assertEquals(MARK, insp.getEquipInitial());
		assertEquals(EQUIP_NUMBER, insp.getEquipNumber());
		assertEquals(Long.valueOf(ID), insp.getId());
		assertEquals(INSPECTION_CODE, insp.getInspectionCode());
		assertEquals(DateUtils.addDays(NOW, -1), insp.getModifiedTimestamp());
		assertEquals(COMPANY, insp.getModifyCompany());
		assertEquals(USER_NAME, insp.getModifyUsername());
		assertEquals(NOTICE_ID, insp.getNoticeId());
		assertEquals(NOTICE_NUMBER, insp.getNoticeNumber());
	}
	
	@Test(expected=Test.None.class)
	public void testInspectionAssignment() throws InvalidAssignedEquipmentException, InvalidMechanicalDesignationException, InvalidPeriodicTimeUnitException, 			InvalidEquipmentIdException {
		List<SeverityLevel> severityLevels = new ArrayList<>();
		severityLevels.add(new SeverityLevel("XX"));
		severityLevels.add(new SeverityLevel("A1", "XX", 180, "days"));
		severityLevels.add(new SeverityLevel("A2", "A1", 36, "months"));
		
		Notice notice = new Notice();
		notice.setSeverityLevels(new SeverityLevels(severityLevels));
		notice.setNumber(5766);
		notice.setId(NOTICE_ID);
		Equipment equipment = new Equipment(MARK, EQUIP_NUMBER, EIN);		
		equipment.setMechanicalDesignation(EQUIP_MECH_DESIG);
		Assignment assignment = new Assignment.Builder(equipment)
			.assignedOn(NOW)
			.withDelay(0)
			.assignedBy(MARK)
			.onNotice(notice)
			.withSeverityCode("XX")
			.withModifyTimestamp(NOW)
			.withModifyUserName(USER_NAME)
			.withCreateUserName(USER_NAME)
			.withCreateTimestamp(NOW)
			.build();
		Inspection inspection = new Inspection(assignment);
		assertNotNull(inspection);
		assertEquals(EIN, inspection.getEin());
		assertEquals(Integer.valueOf(5766), inspection.getNoticeNumber());
		assertEquals(MARK, inspection.getEquipInitial());
		assertEquals(EQUIP_NUMBER, inspection.getEquipNumber());
		
	}
	
	@Test
	public void testHashCode() {
		Inspection inspOne = this.getInspection();
		Inspection inspTwo = this.getInspection();
		assertEquals(inspOne.hashCode(), inspTwo.hashCode());
		assertEquals(inspTwo.hashCode(), inspOne.hashCode());
		inspTwo.setEin(EQUIP_NUMBER);
		assertNotEquals(inspOne.hashCode(), inspTwo.hashCode());
		assertNotEquals(inspTwo.hashCode(), inspOne.hashCode());
		inspTwo.setEin(EIN);
		inspTwo.setEquipInitial("ABCD");
		assertNotEquals(inspOne.hashCode(), inspTwo.hashCode());
		assertNotEquals(inspTwo.hashCode(), inspOne.hashCode());
		inspTwo.setEquipInitial(MARK);
		inspTwo.setEquipNumber(123_456);
		assertNotEquals(inspOne.hashCode(), inspTwo.hashCode());
		assertNotEquals(inspTwo.hashCode(), inspOne.hashCode());
		inspTwo.setEquipNumber(EQUIP_NUMBER);
		assertEquals(inspOne.hashCode(), inspTwo.hashCode());
		assertEquals(inspTwo.hashCode(), inspOne.hashCode());
	}
	
	@Test
	public void testEquals() {
		Inspection inspOne = this.getInspection();
		Inspection inspTwo = this.getInspection();
		assertEquals(inspOne, inspTwo);
		assertEquals(inspTwo, inspOne);
		inspTwo.setEin(EQUIP_NUMBER);
		assertNotEquals(inspOne, inspTwo);
		assertNotEquals(inspTwo, inspOne);
		inspTwo.setEin(EIN);
		inspTwo.setEquipInitial("ABCD");
		assertNotEquals(inspOne, inspTwo);
		assertNotEquals(inspTwo, inspOne);
		inspTwo.setEquipInitial(MARK);
		inspTwo.setEquipNumber(123_456);
		assertNotEquals(inspOne, inspTwo);
		assertNotEquals(inspTwo, inspOne);
		inspTwo.setEquipNumber(EQUIP_NUMBER);
		inspTwo.setNoticeId(1);
		assertNotEquals(inspOne, inspTwo);
		assertNotEquals(inspTwo, inspOne);
		inspTwo.setNoticeId(NOTICE_ID);
		inspTwo.setNoticeNumber(1);
		assertNotEquals(inspOne, inspTwo);
		assertNotEquals(inspTwo, inspOne);
		inspTwo.setNoticeNumber(NOTICE_NUMBER);
		inspTwo.setActiveFlag(ORACLE_BOOLEAN.FALSE.dbValue);
		assertNotEquals(inspOne, inspTwo);
		assertNotEquals(inspTwo, inspOne);
		inspTwo.setActiveFlag(ORACLE_BOOLEAN.TRUE.dbValue);
		inspTwo.setInspectionCode("AB");
		assertNotEquals(inspOne, inspTwo);
		assertNotEquals(inspTwo, inspOne);
		inspTwo.setInspectionCode(INSPECTION_CODE);
		assertEquals(inspOne, inspTwo);
		assertEquals(inspTwo, inspOne);
	}
	private Inspection getInspection() {
		Equipment equip = new Equipment (MARK, EQUIP_NUMBER, EIN);
		Inspection insp = new Inspection();
		insp.setEquipment(equip);
		insp.setActiveFlag(ORACLE_BOOLEAN.TRUE.dbValue);
		insp.setActivityDate(NOW);
		insp.setActivityReporter(REPORTER);
		insp.setCreateCompany(COMPANY);
		insp.setCreatedTimestamp(NOW);
		insp.setCreateUsername(USER_NAME);
		insp.setEin(EIN);
		insp.setEquipInitial(MARK);
		insp.setEquipNumber(EQUIP_NUMBER);
		insp.setId(ID);
		insp.setInspectionCode(INSPECTION_CODE);
		insp.setModifiedTimestamp(DateUtils.addDays(NOW, -1));
		insp.setModifyCompany(COMPANY);
		insp.setModifyUsername(USER_NAME);
		insp.setNoticeId(NOTICE_ID);
		insp.setNoticeNumber(NOTICE_NUMBER);
		return insp;
	}

	@Test
	public void testGetPaddedEquipNumber() {
		Long equipNumber = 321L;
		Inspection inspection = new Inspection();
		inspection.setEquipNumber(equipNumber);

		assertEquals("0000000321", inspection.getEquipNumberAsString());
	}
	
	@Test(expected=Test.None.class)
	public void testMarshalUnmarshal() throws IOException {
		Inspection inspection = getInspection();
		
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(inspection);
		System.out.println(JSON);


		Inspection unmarshalled = objectMapper.readValue(JSON, Inspection.class);
		assertEquals(inspection.getCreateCompany(), unmarshalled.getCreateCompany());
		assertEquals(inspection.getCreateUsername(), unmarshalled.getCreateUsername());
		assertEquals(inspection.getModifyUsername(), unmarshalled.getModifyUsername());
		assertEquals(inspection.getModifyCompany(), unmarshalled.getModifyCompany());
		assertEquals(inspection.getId(), unmarshalled.getId());
		assertEquals(inspection.getNoticeNumber(), unmarshalled.getNoticeNumber());
		assertEquals(inspection.getNoticeCategory(), unmarshalled.getNoticeCategory());
		assertEquals(inspection.getInspectionCode(), unmarshalled.getInspectionCode());
		assertEquals(inspection.getEin(), unmarshalled.getEin());
		assertEquals(inspection.getEquipInitial(), unmarshalled.getEquipInitial());
		assertEquals(inspection.getEquipNumber(), unmarshalled.getEquipNumber());
		assertEquals(inspection.getActiveFlag(), unmarshalled.getActiveFlag());
		assertEquals(inspection.getActivityReporter(), unmarshalled.getActivityReporter());
		assertEquals(inspection.getActivityDate(), unmarshalled.getActivityDate());		
	}
}
