package com.railinc.assethealth.eqadv.domain.email;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.email.NotificationEvent;

public class NotificationEventTest {
	private static String EVENT_DESC = "Event Description";
	private static Integer ID = Integer.valueOf(1);
	
	NotificationEvent validEvent;
	
	
	@Before
	public void init() {
		validEvent = new NotificationEvent(ID, EVENT_DESC);
	}
	
	@Test
	public void testHashCode() {
		NotificationEvent event = new NotificationEvent(ID, EVENT_DESC);
		assertEquals(event.hashCode(), validEvent.hashCode());
		assertEquals(validEvent.hashCode(), event.hashCode());
		validEvent.setDescription("A different desc");
		assertNotEquals(event.hashCode(), validEvent.hashCode());
		assertNotEquals(validEvent.hashCode(), event.hashCode());
		event = new NotificationEvent(3, EVENT_DESC);
		assertNotEquals(event.hashCode(), validEvent.hashCode());
		assertNotEquals(validEvent.hashCode(), event.hashCode());
	}

	@Test
	public void testNotificationEvent() {
		assertNotNull(validEvent);
		assertEquals(Integer.valueOf(1), validEvent.getNotificationEventId());
		assertEquals(EVENT_DESC, validEvent.getDescription());
	}

	@Test
	public void testSetNotificationEventId() {
		Integer id = validEvent.getNotificationEventId();
		validEvent.setNotificationEventId(2);
		assertEquals(Integer.valueOf(2), validEvent.getNotificationEventId());
		validEvent.setNotificationEventId(id);
		assertEquals(ID, validEvent.getNotificationEventId());
	}

	@Test
	public void testGetDescription() {
		String desc = validEvent.getDescription();
		validEvent.setDescription("A different desc");
		assertEquals("A different desc", validEvent.getDescription());
		validEvent.setDescription(desc);
		assertEquals(EVENT_DESC, validEvent.getDescription());
	}


	@Test
	public void testGetSerialVersionUid() {
		assertNotNull(NotificationEvent.getSerialversionuid());
		assertTrue(0 != NotificationEvent.getSerialversionuid());
	}


	@Test
	public void testEqualsObject() {
		NotificationEvent event = new NotificationEvent(ID, EVENT_DESC);
		assertEquals(event, validEvent);
		assertEquals(validEvent, event);
		validEvent.setDescription("A different desc");
		assertNotEquals(event, validEvent);
		assertNotEquals(validEvent, event);
		event = new NotificationEvent(3, EVENT_DESC);
		assertNotEquals(event, validEvent);
		assertNotEquals(validEvent, event);
	}
}
