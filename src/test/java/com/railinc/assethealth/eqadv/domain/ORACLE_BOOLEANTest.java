package com.railinc.assethealth.eqadv.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.ORACLE_BOOLEAN;
import com.railinc.assethealth.eqadv.domain.exception.EWDeserializationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidOracleBooleanFlagException;

public class ORACLE_BOOLEANTest {
	@Test
	public void testY() throws InvalidOracleBooleanFlagException {
		assertEquals(ORACLE_BOOLEAN.TRUE, ORACLE_BOOLEAN.valueFor("y"));
		assertEquals(ORACLE_BOOLEAN.TRUE, ORACLE_BOOLEAN.valueFor("Y"));
		assertEquals(ORACLE_BOOLEAN.TRUE, ORACLE_BOOLEAN.valueFor(" Y"));
		assertEquals(ORACLE_BOOLEAN.TRUE, ORACLE_BOOLEAN.valueFor("Y "));
		assertEquals(ORACLE_BOOLEAN.TRUE, ORACLE_BOOLEAN.valueFor(" Y "));
	}

	// Tests case, trim
	@Test
	public void testN() throws InvalidOracleBooleanFlagException {
		assertEquals(ORACLE_BOOLEAN.FALSE, ORACLE_BOOLEAN.valueFor("n"));
		assertEquals(ORACLE_BOOLEAN.FALSE, ORACLE_BOOLEAN.valueFor("N"));
		assertEquals(ORACLE_BOOLEAN.FALSE, ORACLE_BOOLEAN.valueFor(" N"));
		assertEquals(ORACLE_BOOLEAN.FALSE, ORACLE_BOOLEAN.valueFor("N "));
		assertEquals(ORACLE_BOOLEAN.FALSE, ORACLE_BOOLEAN.valueFor(" N "));
	}

	@Test
	public void testTrue() throws EWDeserializationException {
		assertEquals(ORACLE_BOOLEAN.TRUE, ORACLE_BOOLEAN.valueFor(true));
	}

	@Test
	public void testFalse() throws EWDeserializationException {
		assertEquals(ORACLE_BOOLEAN.FALSE, ORACLE_BOOLEAN.valueFor(false));
	}

	@Test(expected=InvalidOracleBooleanFlagException.class)
	public void TestInvalidBooleanString() throws InvalidOracleBooleanFlagException {
		ORACLE_BOOLEAN.valueFor("K");
	}

	@Test(expected=InvalidOracleBooleanFlagException.class)
	public void TestEmptyBooleanString() throws InvalidOracleBooleanFlagException {
		String nullString = "";
		ORACLE_BOOLEAN.valueFor(nullString);
	}

	@Test(expected=InvalidOracleBooleanFlagException.class)
	public void TestWhiteSpaceBooleanString() throws InvalidOracleBooleanFlagException {
		String nullString = "    ";
		ORACLE_BOOLEAN.valueFor(nullString);
	}

	@Test(expected=InvalidOracleBooleanFlagException.class)
	public void TestNullBooleanString() throws InvalidOracleBooleanFlagException {
		String nullString = null;
		ORACLE_BOOLEAN.valueFor(nullString);
	}
}
