package com.railinc.assethealth.eqadv.domain.equipment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.equipment.MECHANICAL_DESIGNATION_INDICATOR;
import com.railinc.assethealth.eqadv.domain.exception.InvalidOracleBooleanFlagException;

public class MECHANICAL_DESIGNATION_INDICATORTest {
	@Test
	public void testHappyPath () throws InvalidOracleBooleanFlagException {
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, MECHANICAL_DESIGNATION_INDICATOR.forFlag("false"));
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, MECHANICAL_DESIGNATION_INDICATOR.forFlag("N"));
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED, MECHANICAL_DESIGNATION_INDICATOR.forFlag("true"));
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED, MECHANICAL_DESIGNATION_INDICATOR.forFlag("Y"));
	}
	
	@Test
	public void testCase() throws InvalidOracleBooleanFlagException {
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, MECHANICAL_DESIGNATION_INDICATOR.forFlag("FALSE"));
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, MECHANICAL_DESIGNATION_INDICATOR.forFlag("fAlSe"));
	}

	@Test
	public void testTrim() throws InvalidOracleBooleanFlagException {
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, MECHANICAL_DESIGNATION_INDICATOR.forFlag(" false"));
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, MECHANICAL_DESIGNATION_INDICATOR.forFlag("false "));
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, MECHANICAL_DESIGNATION_INDICATOR.forFlag(" false "));
	}
	
	@Test
	public void testForBoolean() throws InvalidOracleBooleanFlagException {
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, MECHANICAL_DESIGNATION_INDICATOR.forFlag(false));
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED, MECHANICAL_DESIGNATION_INDICATOR.forFlag(true));
	}

	@Test(expected=InvalidOracleBooleanFlagException.class)
	public void testNullFlag() throws InvalidOracleBooleanFlagException {
		String nullFlag = null;
		MECHANICAL_DESIGNATION_INDICATOR.forFlag(nullFlag);
	}

	@Test(expected=InvalidOracleBooleanFlagException.class)
	public void testEmptyFlag() throws InvalidOracleBooleanFlagException {
		String emptyFlag = "";
		MECHANICAL_DESIGNATION_INDICATOR.forFlag(emptyFlag);
	}

	@Test(expected=InvalidOracleBooleanFlagException.class)
	public void testBlankFlag() throws InvalidOracleBooleanFlagException {
		String blankFlag = "   ";
		MECHANICAL_DESIGNATION_INDICATOR.forFlag(blankFlag);
	}

	@Test(expected=InvalidOracleBooleanFlagException.class)
	public void testUnknownFlag() throws InvalidOracleBooleanFlagException {
		String unknownFlag = "PANCAKES";
		MECHANICAL_DESIGNATION_INDICATOR.forFlag(unknownFlag);
	}
	
	
	@Test
	public void testMarshalUnmarshalOpen() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String JSON = String.format("\"%s\"", MECHANICAL_DESIGNATION_INDICATOR.OPEN.flag);
		System.out.println("\"mechanicalDesignationInd\":" + JSON);
		MECHANICAL_DESIGNATION_INDICATOR result = objectMapper.readValue(JSON, MECHANICAL_DESIGNATION_INDICATOR.class);
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, result);
	}

	@Test
	public void testMarshalUnmarshalSpecified() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String JSON = String.format("\"%s\"", MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED.flag);
		System.out.println("\"mechanicalDesignationInd\":" + JSON);
		MECHANICAL_DESIGNATION_INDICATOR result = objectMapper.readValue(JSON, MECHANICAL_DESIGNATION_INDICATOR.class);
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED, result);
	}

	@Test
	public void testMarshalUnmarshalNull() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String JSON = String.format("null");
		System.out.println("\"mechanicalDesignationInd\":" + JSON);
		MECHANICAL_DESIGNATION_INDICATOR result = objectMapper.readValue(JSON, MECHANICAL_DESIGNATION_INDICATOR.class);
		assertNull(result);
	}


}