package com.railinc.assethealth.eqadv.domain.mark;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;

public class MarksTest {

	@Test
	public void testCreateMarks() throws InvalidRoadMarkException {
		List<Mark> markList = createMarks();
		Marks marks = new Marks(markList);
		Assert.assertNotNull(marks);
		Assert.assertTrue(marks.size() == 100);
		assertTrue(marks.hasMarks());
		assertTrue(marks.getListAsDelimitedString(true).length() == 600 - 2);
		String temp = marks.getListAsDelimitedString(false);
		assertTrue(temp.split(",").length == 100);
		for (Mark m : markList) {
			assertTrue(marks.isMarkListed(m));
		}
	}

	@Test
	public void testToString() throws InvalidRoadMarkException {
		List<Mark> markList = createMarks();
		Marks marks = new Marks(markList);
		Assert.assertNotNull(marks);
		Assert.assertTrue(marks.size() == 100);
		assertTrue(marks.hasMarks());
		assertTrue(marks.getListAsDelimitedString(true).length() == 600 - 2);
		String temp = marks.getListAsDelimitedString(false);
		assertTrue(temp.split(",").length == 100);
		for (Mark m : markList) {
			assertTrue(marks.isMarkListed(m));
		}
	}

	@Test
	public void testCreateMarksParent() throws InvalidRoadMarkException {
		Mark ns = MarkFactory.newMark("NS");
		Marks marks = new Marks(createMarks());
		for (Mark mark : marks.getMarksList()) {
			mark.setParent(ns);
		}

		for (Mark mark : marks.getMarksList()) {
			assertTrue(mark.getParent() == ns);
		}

		marks.addMark(ns);
		assertTrue(marks.getMarksList().size() == 101);
		assertTrue(marks.isMarkListed(ns));
		String delimitedStr = marks.getListAsDelimitedString("#");
		assertTrue(delimitedStr.split("#").length == 101);
		String marksStr = marks.getMarksString();
		assertFalse(marksStr.contains("#"));
		assertTrue(marksStr.split(",").length == 101);
		assertTrue(delimitedStr.split(",").length == 1);
	}

	@Test
	public void testMarks() throws InvalidRoadMarkException {
		Mark ns = MarkFactory.newMark("NS");
		Marks marks = new Marks();
		marks.addMark(ns);
		assertTrue(marks.isMarkListed(ns));

		Marks copy = new Marks(marks);
		assertTrue(copy.isMarkListed(ns));
	}

	@Test
	public void testMarshallUnmarshall() throws IOException, InvalidRoadMarkException {
		List<Mark> markList = createMarks();
		Marks marks = new Marks(markList);

		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(marks);
		System.out.println(JSON);

		SimpleModule module = new SimpleModule();
		module.addDeserializer(Marks.class, new MarksDeserializer());
		objectMapper.registerModule(module);

		Marks unmarshalled = objectMapper.readValue(JSON, Marks.class);

		for (Mark m : markList) {
			assertTrue(unmarshalled.isMarkListed(m));
		}
	}

	@Test(expected = Test.None.class)
	public void testDeserialize_INVALID() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = "{\n" + "  \"marksString\" : \"1234\"\n" + "} \n";
		System.out.println(JSON);
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Marks.class, new MarksDeserializer());
		objectMapper.registerModule(module);

		Marks unmarshalled = objectMapper.readValue(JSON, Marks.class);
		assertEquals(unmarshalled, unmarshalled);
	}

	@Test(expected = Test.None.class)
	public void testDeserialize_INVALID_MARK() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = "{\n" + "  \"marksString\" : \"ABCD1234\"\n" + "} \n";
		System.out.println(JSON);
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Marks.class, new MarksDeserializer());
		objectMapper.registerModule(module);

		Marks unmarshalled = objectMapper.readValue(JSON, Marks.class);
		assertEquals(unmarshalled, unmarshalled);
	}

	@Test(expected = Test.None.class)
	public void testDeserialize_NULL() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = "{\n" + "  \"marksString\" : null\n" + "} \n";
		System.out.println(JSON);
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Marks.class, new MarksDeserializer());
		objectMapper.registerModule(module);

		Marks unmarshalled = objectMapper.readValue(JSON, Marks.class);
		assertEquals(unmarshalled, unmarshalled);
	}

	@Test(expected = Test.None.class)
	public void testDeserialize_EMPTY() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = "{\n" + "  \"marksString\" : \"\"\n" + "} \n";
		System.out.println(JSON);
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Marks.class, new MarksDeserializer());
		objectMapper.registerModule(module);

		Marks unmarshalled = objectMapper.readValue(JSON, Marks.class);
		assertEquals(unmarshalled, unmarshalled);
	}

	public List<Mark> createMarks() throws InvalidRoadMarkException {
		Random r = new Random();
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String format = "%c%03d";

		List<Mark> markList = new ArrayList<>();
		for (int i = 90; i < 190; i++) {
			String markName;
			if (i % 5 == 0) {
				markName = String.format(format, 'A', i);
			} else if (i % 3 == 0) {
				markName = String.format(format, 'c', i);
			} else if (i % 2 == 0) {
				markName = String.format(format, 'D', i);
			} else {
				StringBuilder sb = new StringBuilder();
				for (int j = 0; j < 4; j++) {
					sb.append(alphabet.charAt(r.nextInt(alphabet.length())));
				}
				markName = sb.toString();
			}
			markList.add(MarkFactory.newMark(markName));
		}
		return markList;
	}

}
