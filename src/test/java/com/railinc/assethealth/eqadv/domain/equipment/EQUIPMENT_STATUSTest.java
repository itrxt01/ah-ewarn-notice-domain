package com.railinc.assethealth.eqadv.domain.equipment;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.equipment.EQUIPMENT_STATUS;
import com.railinc.assethealth.eqadv.domain.exception.InvalidEquipmentStatusException;

public class EQUIPMENT_STATUSTest {

	@Test
	public void testEQUIPMENT_STATUS() {
		for(EQUIPMENT_STATUS s : EQUIPMENT_STATUS.values()) {
			assertTrue(StringUtils.isNotBlank(s.code));
			assertEquals(1, s.code.length());
		}			
	}

	@Test(expected=Test.None.class)
	public void testForString() throws InvalidEquipmentStatusException {
		EQUIPMENT_STATUS s = EQUIPMENT_STATUS.forString("i");
		assertNotNull(s);
		assertEquals(EQUIPMENT_STATUS.INACTIVE, s);
		s = EQUIPMENT_STATUS.forString("P");
		assertNotNull(s);
		assertEquals(EQUIPMENT_STATUS.PREREGISTRED, s);
		s = EQUIPMENT_STATUS.forString("A");
		assertNotNull(s);
		assertEquals(EQUIPMENT_STATUS.ACTIVE, s);
	}
	
	@Test(expected=InvalidEquipmentStatusException.class)
	public void testForInvalidString() throws InvalidEquipmentStatusException {
		EQUIPMENT_STATUS.forString("b");
	}

	@Test(expected=InvalidEquipmentStatusException.class)
	public void testForEmptyString() throws InvalidEquipmentStatusException {
		EQUIPMENT_STATUS.forString("");
	}

	@Test(expected=InvalidEquipmentStatusException.class)
	public void testForNullString() throws InvalidEquipmentStatusException {
		EQUIPMENT_STATUS.forString((String)null);
	}
	
	@Test(expected=InvalidEquipmentStatusException.class)
	public void testForBlankString() throws InvalidEquipmentStatusException {
		EQUIPMENT_STATUS.forString("   ");
	}
}
