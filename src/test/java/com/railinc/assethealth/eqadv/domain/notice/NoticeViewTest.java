package com.railinc.assethealth.eqadv.domain.notice;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.exception.InvalidCategoryException;
import com.railinc.assethealth.eqadv.domain.notice.Notice;
import com.railinc.assethealth.eqadv.domain.notice.NoticeView;
import com.railinc.assethealth.eqadv.domain.status.NOTICE_STATUS;

public class NoticeViewTest {
	@Test
	public void testMarshall() throws IOException, InvalidCategoryException {
		Notice notice = new Notice();
		notice.setTitle("Fake Notice Title");
		notice.setNumber(500);
		notice.setFileNumber("AC-100");
		notice.setSupplementNumber(15);
		notice.setCategory(new Category("MS"));
		notice.setStatus(NOTICE_STATUS.DRAFT);

		NoticeView noticeView = new NoticeView(notice);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(noticeView);
		System.out.println(jsonString);
		Map<String, Object> map = new HashMap<>();
		map = mapper.readValue(jsonString, new TypeReference<Map<String, String>>(){});
		assertEquals("Fake Notice Title", map.get("title"));
		assertEquals("500", map.get("number"));
		assertEquals("AC-100", map.get("fileNumber"));
		assertEquals("15", map.get("supplementNumber"));
		assertEquals("MS", map.get("noticeCategory"));
	}
}
