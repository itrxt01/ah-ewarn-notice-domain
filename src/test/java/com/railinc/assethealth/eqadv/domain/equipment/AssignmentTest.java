package com.railinc.assethealth.eqadv.domain.equipment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.equipment.Assignment;
import com.railinc.assethealth.eqadv.domain.equipment.Equipment;
import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignation;
import com.railinc.assethealth.eqadv.domain.exception.InvalidAssignedEquipmentException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidEquipmentIdException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidSeverityCodeException;
import com.railinc.assethealth.eqadv.domain.exception.SeverityLevelEscalationException;
import com.railinc.assethealth.eqadv.domain.notice.Notice;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevels;

public class AssignmentTest {
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String USER_NAME = "TESTER";
	private static final String VALID_MARK = "ABCD";
	private static final Long EQUIP_NUMBER = 10001L;
	private static final String EQUIP_NUM_STRING = "0000010001";
	private static final Long EQUIP_EIN = 22_222_222L;
	private static final String EIN_STRING = "0022222222";
	private static final String EQUIP_MECH_DESIG = "T";
	private static final String EQUIP_ID = VALID_MARK + EQUIP_NUM_STRING;
	private static final int NOTICE_NUM = 5001;
	private static final int DRAFT_NOTICE_NUM = 500_167;
	private static final long NOTICE_ID = 1234567890L;
	private static final int DELAY = 30;
	private static final List<SeverityLevel> severityLevels = new ArrayList<>();
	private static Equipment equipment;
	private static final Date NOW = DateUtils.addDays(new Date(), -1);
	private static final Date TIMESTAMP = new Date();
	
	@Before
	public void init() throws InvalidPeriodicTimeUnitException, InvalidEquipmentIdException, InvalidRoadMarkException {
		severityLevels.add(new SeverityLevel("XX"));
		severityLevels.add(new SeverityLevel("A1", "XX", 180, "days"));
		severityLevels.add(new SeverityLevel("A2", "A1", 36, "months"));
		equipment = new Equipment(VALID_MARK, EQUIP_NUMBER, EQUIP_EIN);
	}
	
	private SeverityLevel getCode(String code) {
		return severityLevels.stream().filter(c -> c.getSeverityCode().getSeverityCode().equals(code)).findFirst().orElse(null);
	}

	@Test
	public void testBuildPublishedAssignment() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException, InvalidMechanicalDesignationException, SeverityLevelEscalationException {
		Assignment assignedEquipment = this.getPublishedAssignment();
		this.validateAssignment(assignedEquipment, "A1", "XX");
		assertTrue(0 == assignedEquipment.getDelay());
		assertEquals(NOTICE_NUM, assignedEquipment.getNoticeNumber());
		assertEquals(DateUtils.addDays(NOW, 180), assignedEquipment.getEscalationDate());
		assertEquals(EQUIP_MECH_DESIG, assignedEquipment.getMechanicalDesignationCode());
		assertEquals(new MechanicalDesignation(EQUIP_MECH_DESIG), assignedEquipment.getMechanicalDesignation());
		String pattern = DATE_FORMAT;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		assertEquals(simpleDateFormat.format(DateUtils.addDays(NOW, 180)), assignedEquipment.getEscalationDateString());				
	}

	@Test
	public void testBuildDraftAssignment() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException, InvalidMechanicalDesignationException, SeverityLevelEscalationException {
		Assignment assignedEquipment = this.getDraftAssignment();
		this.validateAssignment(assignedEquipment, "A1", "XX");
		assertTrue(DELAY == assignedEquipment.getDelay());
		assertEquals(DRAFT_NOTICE_NUM, assignedEquipment.getNoticeNumber());
		assertTrue(DateUtils.isSameDay(DateUtils.addDays(new Date(), DELAY), assignedEquipment.getEscalationDate()));
		assertEquals(EQUIP_MECH_DESIG, assignedEquipment.getMechanicalDesignationCode());
		assertEquals(new MechanicalDesignation(EQUIP_MECH_DESIG), assignedEquipment.getMechanicalDesignation());
		String pattern = DATE_FORMAT;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		assertTrue(simpleDateFormat.format(DateUtils.addDays(new Date(), DELAY)).equals(simpleDateFormat.format(assignedEquipment.getEscalationDate())));		
	}	

	@Test
	public void testSetNoticeNumber() throws InvalidMechanicalDesignationException, InvalidEquipmentIdException, InvalidRoadMarkException, InvalidAssignedEquipmentException, ParseException, InvalidSeverityCodeException, SeverityLevelEscalationException {
		Assignment assignedEquipment = this.getPublishedAssignment();
		assignedEquipment.setNoticeNumber(1024);
		this.validateAssignment(assignedEquipment, "A1", "XX");
		assertEquals(1024, assignedEquipment.getNoticeNumber());
		assertEquals(DateUtils.addDays(NOW, 180), assignedEquipment.getEscalationDate());
	}

	@Test
	public void testSetAssignedDate() throws InvalidMechanicalDesignationException, InvalidEquipmentIdException, InvalidRoadMarkException, InvalidAssignedEquipmentException, ParseException, InvalidSeverityCodeException, SeverityLevelEscalationException {
		Assignment assignedEquipment = this.getPublishedAssignment();
		this.validateAssignment(assignedEquipment, "A1", "XX");
		assignedEquipment.setAssignedDate(DateUtils.addDays(NOW, -30));
		assertEquals(DateUtils.addDays(NOW, -30), assignedEquipment.getAssignedDate());
	}

	@Test
	public void testSetEscalationDate() throws InvalidMechanicalDesignationException, InvalidEquipmentIdException, InvalidRoadMarkException, InvalidAssignedEquipmentException, ParseException, InvalidSeverityCodeException, SeverityLevelEscalationException {
		Assignment assignedEquipment = this.getDoesNotEscalateAssignment();		
		this.validateAssignment(assignedEquipment, "XX", null);
		assignedEquipment.setEscalationDate(DateUtils.addDays(NOW, 180));
		assignedEquipment.setDelay(0);
		assertEquals(DateUtils.addDays(NOW, 180), assignedEquipment.getEscalationDate());
		String pattern = DATE_FORMAT;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		assertEquals(simpleDateFormat.format(DateUtils.addDays(NOW, 180)), assignedEquipment.getEscalationDateString());		
	}

	@Test(expected=Test.None.class)
	public void testNullEscalationDate() throws InvalidMechanicalDesignationException, InvalidEquipmentIdException, InvalidRoadMarkException, InvalidAssignedEquipmentException, ParseException, InvalidSeverityCodeException, SeverityLevelEscalationException {
		Assignment assignedEquipment = this.getDoesNotEscalateAssignment();		
		this.validateAssignment(assignedEquipment, "XX", null);
		assignedEquipment.setDelay(0);
		assignedEquipment.setEscalationDate(null);
		assertNull(assignedEquipment.getEscalationDate());
	}
	
	@Test
	public void testSetDelay() throws InvalidMechanicalDesignationException, InvalidEquipmentIdException, InvalidRoadMarkException, InvalidAssignedEquipmentException, ParseException, InvalidSeverityCodeException, SeverityLevelEscalationException {
		Assignment assignedEquipment = this.getDraftAssignment();
		assignedEquipment.setDelay(120);		
		this.validateAssignment(assignedEquipment, "A1", "XX");
		assertEquals(Integer.valueOf(120), assignedEquipment.getDelay());
		assertEquals(DRAFT_NOTICE_NUM, assignedEquipment.getNoticeNumber());
		LocalDate expected = EWDateUtils.toLocalDate(DateUtils.addDays(new Date(), 120));
		assertEquals(expected, EWDateUtils.toLocalDate(assignedEquipment.getEscalationDate()));
		String pattern = DATE_FORMAT;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		assertEquals(simpleDateFormat.format(DateUtils.addDays(new Date(), 120)), simpleDateFormat.format(assignedEquipment.getEscalationDate()));		
	}

	@Test
	public void testToString() throws InvalidMechanicalDesignationException, InvalidEquipmentIdException, InvalidRoadMarkException, InvalidAssignedEquipmentException, ParseException, InvalidSeverityCodeException, SeverityLevelEscalationException {
		Assignment assignedEquipment = this.getPublishedAssignment();
		assignedEquipment.setDelay(120);		
		this.validateAssignment(assignedEquipment, "A1", "XX");
		assertTrue(StringUtils.isNotBlank(assignedEquipment.toString()));
	}

	@Test
	public void testHashCode() throws InvalidMechanicalDesignationException, InvalidEquipmentIdException, InvalidRoadMarkException, InvalidAssignedEquipmentException, ParseException, InvalidSeverityCodeException, SeverityLevelEscalationException {
		Assignment assignment_1 = this.getPublishedAssignment();
		assignment_1.setDelay(120);		
		Assignment assignment_2 = this.getPublishedAssignment();
		assignment_2.setDelay(120);		
		this.validateAssignment(assignment_1, "A1", "XX");
		this.validateAssignment(assignment_2, "A1", "XX");
		assertEquals(assignment_1.hashCode(), assignment_2.hashCode());
		assignment_2.setDelay(121);		
		assertNotEquals(assignment_1.hashCode(), assignment_2.hashCode());
	}

	@Test
	public void testEquals() throws InvalidMechanicalDesignationException, InvalidEquipmentIdException, InvalidRoadMarkException, InvalidAssignedEquipmentException, ParseException, InvalidSeverityCodeException, SeverityLevelEscalationException {
		Assignment assignment_1 = this.getPublishedAssignment();
		assignment_1.setDelay(120);		
		Assignment assignment_2 = this.getPublishedAssignment();
		assignment_2.setDelay(120);		
		this.validateAssignment(assignment_1, "A1", "XX");
		this.validateAssignment(assignment_2, "A1", "XX");
		assertTrue(assignment_1.equals(assignment_2));
		assertTrue(assignment_2.equals(assignment_1));
		assignment_2.setDelay(121);		
		assertFalse(assignment_1.equals(assignment_2));
		assertFalse(assignment_2.equals(assignment_1));
		assertFalse(assignment_1.equals((Assignment)null));
		assertFalse(assignment_1.equals(this.getDraftAssignment()));
	}
	
	
	@Test
	public void testBuilderMarkNumberEin() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException, InvalidMechanicalDesignationException, SeverityLevelEscalationException {
		Assignment assignment = new Assignment.Builder(VALID_MARK, EQUIP_NUMBER, EQUIP_EIN)
		.assignedOn(NOW)
		.withId(1_123_456_789L)
		.assignedBy(VALID_MARK)
		.onNoticeWithId(NOTICE_ID)
		.escalatesOn(DateUtils.addDays(NOW, 180))
		.withSeverityCode("XX")
		.withModifyTimestamp(TIMESTAMP)
		.withModifyUserName(USER_NAME)
		.withCreateUserName(USER_NAME)
		.withCreateTimestamp(TIMESTAMP)
		.build();
		this.validateAssignment(assignment, "XX", null);
		assertEquals(Long.valueOf(1_123_456_789L), assignment.getId());
		assignment.setDelay(0);
		assertEquals(DateUtils.addDays(NOW, 180), assignment.getEscalationDate());
		String pattern = DATE_FORMAT;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		assertEquals(simpleDateFormat.format(DateUtils.addDays(NOW, 180)), assignment.getEscalationDateString());		
	}

	@Test(expected=Test.None.class)
	public void testBuilderInvalidReporterMark() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException, InvalidMechanicalDesignationException, SeverityLevelEscalationException {
		new Assignment.Builder(VALID_MARK, EQUIP_NUMBER, EQUIP_EIN)
		.assignedOn(NOW)
		.withId(1_123_456_789L)
		.assignedBy("1234")
		.onNoticeWithId(NOTICE_ID)
		.escalatesOn(DateUtils.addDays(NOW, 180))
		.withSeverityCode("XX")
		.withModifyTimestamp(TIMESTAMP)
		.withModifyUserName(USER_NAME)
		.withCreateUserName(USER_NAME)
		.withCreateTimestamp(TIMESTAMP)
		.build();
	}
	
	
	@Test(expected=Test.None.class)
	public void testBuilderAssignmentAssignment() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException, InvalidMechanicalDesignationException, SeverityLevelEscalationException {
		Assignment assignmentFrom = this.getPublishedAssignment();
		Equipment equipment = new Equipment(VALID_MARK, EQUIP_NUMBER, EQUIP_EIN);
		Assignment result = new Assignment.Builder(equipment, assignmentFrom).build();		
		this.validateAssignment(result, "A1", "XX");
	}
	
	
	@Test(expected=InvalidAssignedEquipmentException.class)
	public void testNullNotice() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException {
		Equipment equipment = new Equipment(VALID_MARK, EQUIP_NUMBER, EQUIP_EIN);
		new Assignment.Builder(equipment)
		.assignedOn(new Date())
		.assignedBy(VALID_MARK)
		.onNotice(null)
		.withSeverityLevel(getCode("A1"))
		.build();
	}
	
	
	@Test(expected=InvalidAssignedEquipmentException.class)
	public void testNoticeNumber0() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException {
		Equipment equipment = new Equipment(VALID_MARK, EQUIP_NUMBER, EQUIP_EIN);
		new Assignment.Builder(equipment)
		.assignedOn(new Date())
		.assignedBy(VALID_MARK)
		.onNotice(0)
		.withSeverityLevel(getCode("A1"))
		.build();
	}
	
	@Test(expected=InvalidAssignedEquipmentException.class)
	public void testNullEquipment() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException {
		new Assignment.Builder(null)
		.assignedOn(new Date())
		.assignedBy(VALID_MARK)
		.onNotice(NOTICE_NUM)
		.withSeverityLevel(getCode("A1"))
		.build();
	}

	@Test(expected=InvalidAssignedEquipmentException.class)
	public void testNullSeverityCode() throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException {
		Equipment equipment = new Equipment(VALID_MARK, EQUIP_NUMBER, EQUIP_EIN);
		Date now = new Date();
		new Assignment.Builder(equipment)
		.assignedOn(now)
		.assignedBy(VALID_MARK)
		.onNotice(NOTICE_NUM)
		.build();
	}

	private Assignment getAssignment(Notice notice, int delay, String severityCode, String escalationCode) throws InvalidEquipmentIdException, InvalidRoadMarkException, InvalidMechanicalDesignationException, InvalidAssignedEquipmentException {
		equipment.setMechanicalDesignation(EQUIP_MECH_DESIG);
		return new Assignment.Builder(equipment)
		.assignedOn(NOW)
		.withDelay(delay)
		.assignedBy(VALID_MARK)
		.onNotice(notice)
		.withSeverityLevel(getCode(severityCode))
		.escalatesTo(getCode(escalationCode))
		.withModifyTimestamp(TIMESTAMP)
		.withModifyUserName(USER_NAME)
		.withCreateUserName(USER_NAME)
		.withCreateTimestamp(TIMESTAMP)
		.build();		
	}
	
	private Assignment getPublishedAssignment() throws InvalidEquipmentIdException, InvalidRoadMarkException, InvalidMechanicalDesignationException, InvalidAssignedEquipmentException {
		Notice notice = new Notice();
		notice.setSeverityLevels(new SeverityLevels(severityLevels));
		notice.setNumber(NOTICE_NUM);
		notice.setId(NOTICE_ID);
		return getAssignment(notice, 0, "A1", "XX");
	}
	
	private Assignment getDraftAssignment() throws InvalidEquipmentIdException, InvalidRoadMarkException, InvalidMechanicalDesignationException, InvalidAssignedEquipmentException {
		Notice notice = new Notice();
		notice.setSeverityLevels(new SeverityLevels(severityLevels));
		notice.setNumber(DRAFT_NOTICE_NUM);
		notice.setId(NOTICE_ID);
		return this.getAssignment(notice, DELAY, "A1", "XX");
	}	
	
	private Assignment getDoesNotEscalateAssignment() throws InvalidEquipmentIdException, InvalidRoadMarkException, InvalidMechanicalDesignationException, InvalidAssignedEquipmentException {
		Notice notice = new Notice();
		notice.setSeverityLevels(new SeverityLevels(severityLevels));
		notice.setNumber(DRAFT_NOTICE_NUM);
		notice.setId(NOTICE_ID);
		return this.getAssignment(notice, DELAY, "XX", null);
	}	
	
	
	public void validateAssignment(Assignment assignedEquipment, String severityCode, String escalationCode) throws InvalidRoadMarkException, InvalidEquipmentIdException, ParseException, InvalidAssignedEquipmentException, InvalidSeverityCodeException, InvalidMechanicalDesignationException, SeverityLevelEscalationException {
		assertEquals(getCode(severityCode), assignedEquipment.getSeverity());
		assertEquals(NOW, assignedEquipment.getAssignedDate());
		assertEquals(EQUIP_NUMBER, assignedEquipment.getNumber());
		assertEquals(EQUIP_EIN, assignedEquipment.getEin());
		assertEquals(EIN_STRING, assignedEquipment.getEinAsString());
		assertEquals(EQUIP_NUM_STRING, assignedEquipment.getNumberAsJustifiedString());
		assertEquals(EQUIP_EIN, assignedEquipment.getEin());
		assertEquals(EQUIP_ID, assignedEquipment.getEquipmentId());
		assertEquals(VALID_MARK, assignedEquipment.getRoadMark());
		assertEquals(VALID_MARK, assignedEquipment.getStencilMark());
		assertEquals(VALID_MARK, assignedEquipment.getAssignmentReporter());
		assertEquals(NOW, assignedEquipment.getAssignedDate());
		assertEquals(TIMESTAMP, assignedEquipment.getCreatedTimestamp());
		assertEquals(USER_NAME, assignedEquipment.getCreateUsername());		
		assertEquals(TIMESTAMP, assignedEquipment.getModifiedTimestamp());
		assertEquals(USER_NAME, assignedEquipment.getModifyUsername());
		if(null == escalationCode) {
			assertEquals(escalationCode, assignedEquipment.getEscalationLevel());			
		} else {
			assertEquals(escalationCode, assignedEquipment.getEscalationLevel().getSeverityLevelCode());	
		}
	}
}
