package com.railinc.assethealth.eqadv.domain.equipment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.equipment.Equipment;
import com.railinc.assethealth.eqadv.domain.equipment.EquipmentSet;
import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignation;
import com.railinc.assethealth.eqadv.domain.exception.InvalidEquipmentIdException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;
import com.railinc.assethealth.eqadv.domain.mark.MarkFactory;

public class EquipmentTest {

	@Test(expected = Test.None.class /* no exception expected */)
	public void testHashCode() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_890L);
		assertNotNull(eq1.hashCode());
		assertNotNull(eq2.hashCode());
		assertNotNull(eq3.hashCode());
		assertNotEquals(eq1.hashCode(), eq2.hashCode());
		assertNotEquals(eq2.hashCode(), eq3.hashCode());
		assertEquals(eq1.hashCode(), eq3.hashCode());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testUmlerOwnerCompany() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setUmlerOwner("TEST");
		assertEquals("TEST", eq1.getUmlerOwner());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCommodity() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment equipment = new Equipment("BNSF", 1L, 100L);
		equipment.setCommodity("29366");
		assertTrue(equipment.isValid());
		assertNotNull(equipment.getCommodity());
		assertEquals("29366", equipment.getCommodity());
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testalastCommodityHazmat() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setLastCommodityHazmat(false);
		assertFalse(eq1.isLastCommodityHazmat());
		eq1.setLastCommodityHazmat(true);
		assertTrue(eq1.isLastCommodityHazmat());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testIsLastCommodityHazmat() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setCommodity("28123");
		assertEquals("28123", eq1.getCommodity());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testHandlingCarrier() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment equipment = new Equipment("BNSF", 1L, 100L);
		equipment.setHandlingCarrier("BNSF");
		assertTrue(equipment.isValid());
		assertNotNull(equipment.getHandlingCarrier());
		assertEquals("BNSF", equipment.getHandlingCarrier());
	}	

	@Test(expected = Test.None.class /* no exception expected */)
	public void testLastMovementDate() {
		Date date = new Date();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setLastMovement(DateUtils.addDays(date, +120));
		assertEquals(date, DateUtils.addDays(eq1.getLastMovement(), -120));
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void tesUmlerOwner() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setUmlerOwner("RAIL");
		assertEquals("RAIL", eq1.getUmlerOwner());
		eq1.setUmlerOwner("A123");
		assertEquals("A123", eq1.getUmlerOwner());
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testModifyCompany() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setModifyCompany("TEST");
		assertEquals("TEST", eq1.getModifyCompany());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEquipmentIdCtor() {
		Equipment eq1 = new Equipment("RAIL00053");
		assertEquals(MarkFactory.newMark("RAIL"), eq1.getMark());
		assertEquals(MarkFactory.newMark("RAIL").getValueAsString(), eq1.getMark().getValueAsString());
		assertEquals(Long.valueOf(53L), eq1.getNumber());
		assertEquals("RAIL        53", eq1.getValueAsJustifiedString());
		assertEquals("RAIL53", eq1.getValueAsString());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testValidEin() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Equipment eq1 = new Equipment("RAIL", 01L, 01L);
		Method m = eq1.getClass().getDeclaredMethod("isValidEin");
		m.setAccessible(true);
		Object o = m.invoke(eq1);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.TRUE, b);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEinLTZero() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Equipment eq1 = new Equipment("RAIL", 01L, -1_234_567_890L);
		Method m = eq1.getClass().getDeclaredMethod("isValidEin");
		m.setAccessible(true);
		Object o = m.invoke(eq1);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.FALSE, b);
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testZeroEin() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Equipment eq1 = new Equipment("RAIL", 01L, 0L);
		Method m = eq1.getClass().getDeclaredMethod("isValidEin");
		m.setAccessible(true);
		Object o = m.invoke(eq1);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.FALSE, b);
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testEinGTMaxValue() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Equipment eq1 = new Equipment("RAIL", 01L, 11_111_111_111L);
		Method m = eq1.getClass().getDeclaredMethod("isValidEin");
		m.setAccessible(true);
		Object o = m.invoke(eq1);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.FALSE, b);
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testValidNumber() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Method m = eq1.getClass().getDeclaredMethod("isValidNumber");
		m.setAccessible(true);
		Object o = m.invoke(eq1);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.TRUE, b);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testNumberLTZero() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Equipment eq1 = new Equipment("RAIL", -01L, 1_234_567_890L);
		Method m = eq1.getClass().getDeclaredMethod("isValidNumber");
		m.setAccessible(true);
		Object o = m.invoke(eq1);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.FALSE, b);
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testZeroNumber() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Equipment eq1 = new Equipment("RAIL", 0L, 0L);
		Method m = eq1.getClass().getDeclaredMethod("isValidNumber");
		m.setAccessible(true);
		Object o = m.invoke(eq1);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.FALSE, b);
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testNumberGTMaxValue() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Equipment eq1 = new Equipment("RAIL", 11_111_111_111L, 01L);
		Method m = eq1.getClass().getDeclaredMethod("isValidNumber");
		m.setAccessible(true);
		Object o = m.invoke(eq1);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.FALSE, b);
	}
	
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testModifyUsername() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setModifyUsername("TEST_ID");
		assertEquals("TEST_ID", eq1.getModifyUsername());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testModifyTimestamp() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Date now = new Date();
		eq1.setModifiedTimestamp(new Date());
		assertEquals(now, eq1.getModifiedTimestamp());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCreateCompany() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setCreateCompany("TEST");
		assertEquals("TEST", eq1.getCreateCompany());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCreateUsername() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		eq1.setCreateUsername("TEST_ID");
		assertEquals("TEST_ID", eq1.getCreateUsername());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCreateTimestamp() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Date now = new Date();
		eq1.setCreatedTimestamp(now);
		assertEquals(now, eq1.getCreatedTimestamp());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testToString() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		assertNotNull(eq1.toString());
		assertTrue(StringUtils.isNotBlank(eq1.toString()));
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testIsValidNumber() {
		Equipment eq1 = new Equipment("RAIL", 0L, 1_234_567_890L);
		assertFalse(eq1.isValid());
		eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		assertTrue(eq1.isValid());
		eq1 = new Equipment("RAIL", 11_111_111_111_111L, 1_234_567_890L);
		assertFalse(eq1.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testIsValidEin() {
		Equipment eq1 = new Equipment("RAIL", 1L, 0L);
		assertFalse(eq1.isValid());
		eq1 = new Equipment("RAIL", 1L, 1_234_567_890L);
		assertTrue(eq1.isValid());
		eq1 = new Equipment("RAIL", 1L, 11_111_111_111_111L);
		assertFalse(eq1.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEqualsObject() {
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_890L);
		assertNotNull(eq1);
		assertNotNull(eq2);
		assertNotNull(eq3);
		assertNotEquals(eq1, eq2);
		assertNotEquals(eq2, eq3);
		assertEquals(eq1, eq3);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEquipment() {
		Equipment eq = new Equipment();
		assertNotNull(eq);
		assertNull(eq.getMark());
		assertNull(eq.getNumber());
		assertNull(eq.getEin());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEquipmentLong() {
		Equipment eq = new Equipment(1_111_111_111L);
		assertNotNull(eq);
		assertNull(eq.getMark());
		assertNull(eq.getNumber());
		assertEquals(Long.valueOf(1_111_111_111L), eq.getEin());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEquipmentStringLong() {
		Equipment eq = new Equipment("RAIL", 2L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertNull(eq.getEin());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEquipmentStringLongLong() {
		Equipment eq = new Equipment("RAIL", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testSetEin() {
		Equipment eq = new Equipment("RAIL", 2L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertNull(eq.getEin());
		eq.setEin(2_999_999_999L);
		assertEquals(Long.valueOf(2_999_999_999L), eq.getEin());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEquipmentEquipment() {
		Equipment eq1 = new Equipment("RAIL", 2L, 1_111_111_111L);
		Equipment eq2 = new Equipment(eq1);
		assertNotNull(eq2);
		assertNotNull(eq2.getMark());
		assertEquals(eq2.getMark().getValueAsString(), eq1.getMark().getValueAsString());
		assertTrue(eq1.getNumber() == eq2.getNumber());
		assertTrue(eq2.getEin() == eq1.getEin());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testEquipmentEquipmentEquipmentArg() {
		Equipment eq1 = new Equipment("RAIL00001");
		Equipment eq2 = new Equipment("RAIL0000000001");
		assertNotNull(eq2);
		assertNotNull(eq2.getMark());
		assertEquals(eq2.getMark().getValueAsString(), eq1.getMark().getValueAsString());
		assertTrue(eq1.getNumber() == eq2.getNumber());
		assertTrue(eq2.getEin() == eq1.getEin());
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testEquipmentWithUmlerFields() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment();
		eq1.setEquipmentGroup("EG");
		eq1.setLessee("LESS");
		eq1.setMaintenanceParty("MP");
		eq1.setUmlerOwner("BNSF");		
		eq1.setStatus("A");

		Equipment eq2 = new Equipment(eq1);

		assertNotNull(eq2);
		assertEquals(eq1.getEquipmentGroup(), eq2.getEquipmentGroup());
		assertEquals(eq1.getUmlerOwner(), eq2.getUmlerOwner());		
		assertEquals(eq1.getLessee(), eq2.getLessee());
		assertEquals(eq1.getMaintenanceParty(), eq2.getMaintenanceParty());
		assertEquals(eq1.getStatus(), eq2.getStatus());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetEquipmentId() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("RAIL", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getEquipmentId().equals("RAIL0000000002"));		
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testSetUmlerOwner() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("RAIL000001");
		eq.setUmlerOwner("NANX");
		assertNotNull(eq.getUmlerOwner());
		assertEquals("NANX", eq.getUmlerOwner());
	}		
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetEquipmentIdInvalidMark() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("A2", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertNull(eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertNull(eq.getEquipmentId());		
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetJustifiedEquipmentId() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("NS", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("NS", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getEquipmentId().equals("NS  0000000002"));
		assertTrue(eq.getJustifiedEquipmentId(7).equals("NS        2"));
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetShortId() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("RAIL", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getShortId().equals("RAIL2"));
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetValueAsString() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("RAIL", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getValueAsString().equals("RAIL2"));
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetEinAsString() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("RAIL", 2L, 111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 111L);
		assertTrue(eq.getEinAsString().equals("0000000111"));
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetRoadMark() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("RAIL", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("RAIL"));
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetEquipmentGroup() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("RAIL", 2L, 1_111_111_111L);
		eq.setEquipmentGroup("GROUP");
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("RAIL"));
		assertEquals("GROUP", eq.getEquipmentGroup());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetValueAsJustifiedString() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("RAIL", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("RAIL", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("RAIL"));
		assertEquals("RAIL         2", eq.getValueAsJustifiedString());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetMarkAsJustifiedString() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("UP", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("UP", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("UP"));
		assertEquals("UP  ", eq.getMarkAsJustifiedString());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetValueAsJustifiedStringInt() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("UP", 2L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("UP", eq.getMark().getValueAsString());
		assertTrue(2L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("UP"));
		assertEquals("UP                     2", eq.getValueAsJustifiedString(20));
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testGetNumberAsJustifiedStringInt() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("UP", 1012L, 1_111_111_111L);
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("UP", eq.getMark().getValueAsString());
		assertTrue(1012L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("UP"));
		assertEquals(" 1012", eq.getNumberAsJustifiedString(5));
		assertEquals("           1012", eq.getNumberAsJustifiedString(15));
		assertEquals("0000001012", eq.getNumberAsJustifiedString());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testSetEinLineage() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq = new Equipment("UP", 1012L, 1_111_111_111L);
		EquipmentSet<Equipment> set = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 1012L, 1_111_111_112L);
		Equipment eq2 = new Equipment("BNSF", 1012L, 1_111_111_113L);
		set.add(eq1);
		set.add(eq2);
		eq.setEinLineage(set);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testSetMechanicalDesignationMechanicalDesignation() throws InvalidEquipmentIdException, InvalidRoadMarkException, InvalidMechanicalDesignationException {
		Equipment eq = new Equipment("UP", 1012L, 1_111_111_111L);
		MechanicalDesignation desig = new MechanicalDesignation("D");
		eq.setMechanicalDesignation( desig );
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("UP", eq.getMark().getValueAsString());
		assertTrue(1012L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("UP"));
		assertEquals(eq.getMechanicalDesignation(), desig);
		assertTrue(eq.isMechanicalDesignation(desig));
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testSetMechanicalDesignationString() throws InvalidEquipmentIdException, InvalidRoadMarkException, InvalidMechanicalDesignationException {
		Equipment eq = new Equipment("UP", 1012L, 1_111_111_111L);
		eq.setMechanicalDesignation( "T" );
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("UP", eq.getMark().getValueAsString());
		assertTrue(1012L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("UP"));
		assertEquals("T", eq.getMechanicalDesignationCode());
		assertTrue(eq.isMechanicalDesignation("T"));
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToEqual() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment("RAIL", 2L, 1_111_111_111L);
		Equipment eq2 = new Equipment(eq1);
		assertNotNull(eq1);
		assertNotNull(eq2);
		assertTrue(eq1.compareTo(eq2) == 0);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToEqualNullEin() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment("RAIL", 2L);
		Equipment eq2 = new Equipment(eq1);
		assertNotNull(eq1);
		assertNotNull(eq2);
		assertTrue(eq1.compareTo(eq2) == 0);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToEqualNullObject() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment();
		assertNotNull(eq1);
		assertTrue(eq1.compareTo(null) == 1);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToMark() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment("BNSF", 2L);
		Equipment eq2 = new Equipment("RAIL", 2L);
		assertNotNull(eq1);
		assertNotNull(eq2);
		assertTrue(eq1.compareTo(eq2) < 0);
		assertTrue(eq2.compareTo(eq1) > 0);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToMarksNull() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment();
		eq1.setEin(1L);
		Equipment eq2 = new Equipment();
		eq2.setEin(2L);
		assertNotNull(eq1);
		assertNotNull(eq2);
		assertTrue(eq1.compareTo(eq2) < 0);
		assertTrue(eq2.compareTo(eq1) > 0);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToNullMark() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment();
		eq1.setEin(1L);
		Equipment eq2 = new Equipment("ABCD", 2L);
		assertNotNull(eq1);
		assertNotNull(eq2);
		assertTrue(eq1.compareTo(eq2) < 0);
		assertTrue(eq2.compareTo(eq1) > 0);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToNumber() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment("RAIL", 1L, 1L);
		Equipment eq2 = new Equipment("RAIL", 2L, 2L);
		assertNotNull(eq1);
		assertNotNull(eq2);
		assertTrue(eq1.compareTo(eq2) < 0);
		assertTrue(eq2.compareTo(eq1) > 0);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToEin() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment eq1 = new Equipment("RAIL", 1L, 1020L);
		Equipment eq2 = new Equipment("RAIL", 1L, 1021L);
		assertNotNull(eq1);
		assertNotNull(eq2);
		assertTrue(eq1.compareTo(eq2) < 0);
		assertTrue(eq2.compareTo(eq1) > 0);
	}
	
	@SuppressWarnings("rawtypes")
	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareLongsLT() throws InvalidEquipmentIdException, InvalidRoadMarkException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Equipment eq1 = new Equipment("RAIL", 1L, 1020L);
		Class[] parameterTypes = new Class[2];
		parameterTypes[0] = Long.class;
		parameterTypes[1] = Long.class;
		
		Method m = eq1.getClass().getDeclaredMethod("compareLongs", parameterTypes);
		m.setAccessible(true);
		
		Object[] parameters = new Object[2];
		parameters[0] = 1L;
		parameters[1] = 2L;
		
		Object o = m.invoke(eq1, parameters);
		Integer result = (Integer)o;
		assertTrue(result<0);
	}
	
	@SuppressWarnings("rawtypes")
	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareLongsGT() throws InvalidEquipmentIdException, InvalidRoadMarkException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Equipment eq1 = new Equipment("RAIL", 1L, 1020L);
		Class[] parameterTypes = new Class[2];
		parameterTypes[0] = Long.class;
		parameterTypes[1] = Long.class;
		
		Method m = eq1.getClass().getDeclaredMethod("compareLongs", parameterTypes);
		m.setAccessible(true);
		
		Object[] parameters = new Object[2];
		parameters[0] = 2L;
		parameters[1] = 1L;
		
		Object o = m.invoke(eq1, parameters);
		Integer result = (Integer)o;
		assertTrue(result>0);
	}
	
	@SuppressWarnings("rawtypes")
	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareLongsEqual() throws InvalidEquipmentIdException, InvalidRoadMarkException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Equipment eq1 = new Equipment("RAIL", 1L, 1020L);
		Class[] parameterTypes = new Class[2];
		parameterTypes[0] = Long.class;
		parameterTypes[1] = Long.class;
		
		Method m = eq1.getClass().getDeclaredMethod("compareLongs", parameterTypes);
		m.setAccessible(true);
		
		Object[] parameters = new Object[2];
		parameters[0] = 2L;
		parameters[1] = 2L;
		
		Object o = m.invoke(eq1, parameters);
		Integer result = (Integer)o;
		assertEquals(Integer.valueOf(0), result);
	}
	
	@SuppressWarnings("rawtypes")
	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareLongsNull_1() throws InvalidEquipmentIdException, InvalidRoadMarkException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Equipment eq1 = new Equipment("RAIL", 1L, 1020L);
		Class[] parameterTypes = new Class[2];
		parameterTypes[0] = Long.class;
		parameterTypes[1] = Long.class;
		
		Method m = eq1.getClass().getDeclaredMethod("compareLongs", parameterTypes);
		m.setAccessible(true);
		
		Object[] parameters = new Object[2];
		parameters[0] = (Long)null;
		parameters[1] = 2L;
		
		Object o = m.invoke(eq1, parameters);
		Integer result = (Integer)o;
		assertTrue(result<0);
	}
	@SuppressWarnings("rawtypes")
	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareLongsNull_2() throws InvalidEquipmentIdException, InvalidRoadMarkException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Equipment eq1 = new Equipment("RAIL", 1L, 1020L);
		Class[] parameterTypes = new Class[2];
		parameterTypes[0] = Long.class;
		parameterTypes[1] = Long.class;
		
		Method m = eq1.getClass().getDeclaredMethod("compareLongs", parameterTypes);
		m.setAccessible(true);
		
		Object[] parameters = new Object[2];
		parameters[0] = 2L;
		parameters[1] = (Long)null;
		
		Object o = m.invoke(eq1, parameters);
		Integer result = (Integer)o;
		assertTrue(result>0);
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testIsValid() throws InvalidRoadMarkException, InvalidEquipmentIdException {
		Equipment equipment = new Equipment("RAIL", 2L, 1L);
		assertTrue(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testInvalidLowEin() throws InvalidRoadMarkException, InvalidEquipmentIdException {
		Equipment equipment = new Equipment("RAIL", 2L, 0L);
		assertFalse(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testInvalidHighEin() throws InvalidRoadMarkException, InvalidEquipmentIdException {
		Equipment equipment = new Equipment("RAIL", 2L, 10_000_000_000L);
		assertFalse(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testInvalidLowNumber() throws InvalidRoadMarkException, InvalidEquipmentIdException {
		Equipment equipment = new Equipment("RAIL", 0L, 2L);
		assertFalse(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testInvalidHighNumber() throws InvalidRoadMarkException, InvalidEquipmentIdException {
		Equipment equipment = new Equipment("RAIL", 10_000_000_000L, 2L);
		assertFalse(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testInvalidLowNumber2() throws InvalidRoadMarkException, InvalidEquipmentIdException {
		Equipment equipment = new Equipment("RAIL", 0L);
		assertFalse(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testInvalidHighNumber2() throws InvalidRoadMarkException, InvalidEquipmentIdException {
		Equipment equipment = new Equipment("RAIL", 10_000_000_000L);
		assertFalse(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToMarkDigits() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment equipment = new Equipment("1234", 1L);
		assertFalse(equipment.isValid());
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToMarkEmpty() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment equipment = new Equipment("", 1L);
		assertFalse(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToMarkNull() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment equipment = new Equipment(null, 1L);
		assertFalse(equipment.isValid());
	}

	@Test(expected = Test.None.class /* no exception expected */)
	public void testCompareToMarkTooLong() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment equipment = new Equipment("BNSFX", 1L);
		assertFalse(equipment.isValid());
	}
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void testRepairAgentMark() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		Equipment equipment = new Equipment("C123", 1L);
		assertFalse(equipment.isValid());
	}
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void testSetMechanicalDesignationMechanicalDesignationNull() throws InvalidEquipmentIdException, InvalidRoadMarkException, InvalidMechanicalDesignationException {
		Equipment eq = new Equipment("UP", 1012L, 1_111_111_111L);
		MechanicalDesignation desig = new MechanicalDesignation("");
		eq.setMechanicalDesignation( desig );
		assertNotNull(eq);
		assertNotNull(eq.getMark());
		assertEquals("UP", eq.getMark().getValueAsString());
		assertTrue(1012L == eq.getNumber());
		assertTrue(eq.getEin() == 1_111_111_111L);
		assertTrue(eq.getRoadMark().equals("UP"));
		assertNull(eq.getMechanicalDesignation());
	}	
}
