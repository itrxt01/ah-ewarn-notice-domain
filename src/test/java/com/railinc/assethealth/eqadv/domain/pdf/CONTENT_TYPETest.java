package com.railinc.assethealth.eqadv.domain.pdf;

import static org.junit.Assert.*;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidContentTypeException;
import com.railinc.assethealth.eqadv.domain.pdf.CONTENT_TYPE;

public class CONTENT_TYPETest {

	@Test
	public void testForType_PDF() throws InvalidContentTypeException {
		CONTENT_TYPE type = CONTENT_TYPE.forType("pdf");
		assertEquals(CONTENT_TYPE.PDF, type);
	}

	@Test
	public void testForType_TXT() throws InvalidContentTypeException {
		CONTENT_TYPE type = CONTENT_TYPE.forType("txt");
		assertEquals(CONTENT_TYPE.TXT, type);
	}
	
	@Test(expected=InvalidContentTypeException.class)
	public void testForType_UNKNOWN() throws InvalidContentTypeException {
		CONTENT_TYPE type = CONTENT_TYPE.forType("bmpt");
		assertEquals(CONTENT_TYPE.TXT, type);
	}
}
