package com.railinc.assethealth.eqadv.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;

public class TimePeriodTest {

	@Test(expected=InvalidPeriodicTimeUnitException.class)
	public void testIsValidException() throws InvalidPeriodicTimeUnitException {
		TimePeriod period = new TimePeriod();
		period.setTimePeriod(0);
		period.setTimeUnit("lightYears");
		
	}

	@Test(expected=InvalidPeriodicTimeUnitException.class)
	public void testIsValidExceptionNonZeroValue() throws InvalidPeriodicTimeUnitException {
		TimePeriod period = new TimePeriod();
		period.setTimePeriod(5);
		period.setTimeUnit("minutes");
		assertFalse(period.isValid());
	}

	@Test(expected=Test.None.class)
	public void testIsValid() throws InvalidPeriodicTimeUnitException {
		TimePeriod period = new TimePeriod();
		period.setTimePeriod(0);
		period.setTimeUnit("years");
		assertFalse(period.isValid());
		period.setTimePeriod(5);
		assertTrue(period.isValid());
	}
}
