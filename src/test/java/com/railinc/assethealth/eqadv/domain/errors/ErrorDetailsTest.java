package com.railinc.assethealth.eqadv.domain.errors;

import static org.junit.Assert.*;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.errors.ErrorDetails;
import com.railinc.assethealth.eqadv.domain.errors.ErrorMessage;

public class ErrorDetailsTest {

	@Test
	public void testErrorDetails() {
		ErrorDetails ed = new ErrorDetails();
		ed.setMessage("This is a test message");
		ed.setCode("00");
		assertEquals("This is a test message", ed.getMessage());
		assertEquals("00", ed.getCode());
		ed.addSubError(new ErrorMessage("This is a sub error"));
		assertTrue(ed.hasSubErrors());
		assertEquals(1, ed.getSubErrors().size()); 
	}
}
