package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.ORACLE_BOOLEAN;
import com.railinc.assethealth.eqadv.domain.exception.InvalidInspectionCodeType;
import com.railinc.assethealth.eqadv.domain.inspection.INSPECTION_TYPE;

public class INSPECTION_TYPETest {

	@Test
	public void testINSPECTION_TYPE() {
		for(INSPECTION_TYPE t : INSPECTION_TYPE.values()) {
			assertTrue(StringUtils.isNotBlank(t.code));
			assertEquals(2, t.code.length());
		}			
	}

	@Test(expected=Test.None.class)
	public void testForCode() throws InvalidInspectionCodeType {
		INSPECTION_TYPE t = INSPECTION_TYPE.forCode("pi");
		assertNotNull(t);
		assertEquals(INSPECTION_TYPE.PRELIMINARY, t);
		t = INSPECTION_TYPE.forCode("fi");
		assertNotNull(t);
		assertEquals(INSPECTION_TYPE.FINAL, t);
	}
	
	@Test(expected=Test.None.class)
	public void testForFlag() throws InvalidInspectionCodeType {
		INSPECTION_TYPE t = INSPECTION_TYPE.forFlag(ORACLE_BOOLEAN.FALSE.value);
		assertNotNull(t);
		assertEquals(INSPECTION_TYPE.PRELIMINARY, t);
		t = INSPECTION_TYPE.forFlag(ORACLE_BOOLEAN.TRUE.value);
		assertNotNull(t);
		assertEquals(INSPECTION_TYPE.FINAL, t);
	}

	@Test(expected=InvalidInspectionCodeType.class)
	public void testForInvalidString() throws InvalidInspectionCodeType {
		INSPECTION_TYPE.forCode("b");
	}

	@Test(expected=InvalidInspectionCodeType.class)
	public void testForEmptyString() throws InvalidInspectionCodeType {
		INSPECTION_TYPE.forCode("");
	}

	@Test(expected=InvalidInspectionCodeType.class)
	public void testForNullString() throws InvalidInspectionCodeType {
		INSPECTION_TYPE.forCode((String)null);
	}
	
	@Test(expected=InvalidInspectionCodeType.class)
	public void testForBlankString() throws InvalidInspectionCodeType {
		INSPECTION_TYPE.forCode("   ");
	}
}
