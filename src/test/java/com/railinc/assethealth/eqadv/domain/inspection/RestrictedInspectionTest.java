package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;
import com.railinc.assethealth.eqadv.domain.inspection.INSPECTION_TYPE;
import com.railinc.assethealth.eqadv.domain.inspection.InspectionCode;
import com.railinc.assethealth.eqadv.domain.inspection.RestrictedInspection;
import com.railinc.assethealth.eqadv.domain.mark.Mark;
import com.railinc.assethealth.eqadv.domain.mark.MarkFactory;

public class RestrictedInspectionTest {

	@Test
	public void testRestrictedInspection() {
		RestrictedInspection restrictedInspection = new RestrictedInspection();
		assertNotNull(restrictedInspection);
	}

	@Test(expected=Test.None.class)
	public void testRestrictedInspectionInspectionCodeMark() throws InvalidRoadMarkException {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "DESC", true, true);
		Mark mark = MarkFactory.newMark("MARk");
		RestrictedInspection restrictedInspection = new RestrictedInspection(code, mark);
		assertNotNull(restrictedInspection);
		assertEquals(mark, restrictedInspection.getMark());
		assertEquals(code, restrictedInspection.getInspectionCode());
		assertEquals(code.getCode(), restrictedInspection.getCode());
		assertEquals(mark.getValueAsString(), restrictedInspection.getMarkString());
	}

	@Test(expected=Test.None.class)
	public void testGetInspectionCode() throws InvalidRoadMarkException {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "DESC", true, true);
		Mark mark = MarkFactory.newMark("MARk");
		RestrictedInspection restrictedInspection = new RestrictedInspection(code, mark);
		assertEquals(code, restrictedInspection.getInspectionCode());
	}

	@Test(expected=Test.None.class)
	public void testSetInspectionCode() throws InvalidRoadMarkException {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "DESC", true, true);
		InspectionCode code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ZZ", "DESC", true, true);
		Mark mark = MarkFactory.newMark("MARk");
		RestrictedInspection restrictedInspection = new RestrictedInspection(code, mark);
		assertEquals(code, restrictedInspection.getInspectionCode());
		restrictedInspection.setInspectionCode(code_2);
		assertEquals(code_2, restrictedInspection.getInspectionCode());
	}

	@Test(expected=Test.None.class)
	public void testGetCode() throws InvalidRoadMarkException {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "DESC", true, true);
		Mark mark = MarkFactory.newMark("MARk");
		RestrictedInspection restrictedInspection = new RestrictedInspection(code, mark);
		assertNotNull(restrictedInspection);
		assertEquals(code.getCode(), restrictedInspection.getCode());
	}

	@Test(expected=Test.None.class)
	public void testSetMark() throws InvalidRoadMarkException {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "DESC", true, true);
		Mark mark = MarkFactory.newMark("MARk");
		Mark mark_2 = MarkFactory.newMark("XXXX");
		RestrictedInspection restrictedInspection = new RestrictedInspection(code, mark);
		assertEquals(mark, restrictedInspection.getMark());
		restrictedInspection.setMark(mark_2);
		assertEquals(mark_2, restrictedInspection.getMark());		
		restrictedInspection.setMark(mark);
		assertEquals(mark, restrictedInspection.getMark());
	}

	@Test(expected=Test.None.class)
	public void testGetMark() throws InvalidRoadMarkException {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "DESC", true, true);
		Mark mark = MarkFactory.newMark("MARk");
		RestrictedInspection restrictedInspection = new RestrictedInspection(code, mark);
		assertEquals(mark, restrictedInspection.getMark());
	}

	@Test(expected=Test.None.class)
	public void testGetMarkString() throws InvalidRoadMarkException {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "DESC", true, true);
		Mark mark = MarkFactory.newMark("MARk");
		RestrictedInspection restrictedInspection = new RestrictedInspection(code, mark);
		assertEquals(mark.getValueAsString(), restrictedInspection.getMarkString());
	}

}