package com.railinc.assethealth.eqadv.domain.mark;


import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;

public class MarkFactoryTest {
	
	@Test
	public void validRoadMarks() {
		Mark parent = MarkFactory.newMark("bnsf");
		assertNotNull(parent);
		assertNotNull(MarkFactory.newMark("a"));
		assertNotNull(MarkFactory.newMark("AAAA"));
		assertNotNull(MarkFactory.newMark("a", "bnsf"));
		assertNotNull(MarkFactory.newMark("AAAA", parent));
		assertTrue(StringUtils.isNotBlank(MarkFactory.newMark("AAAA", parent).toString()));
		assertTrue(MarkFactory.newMark("AAAA", parent).toString().contains("AAAA"));
	}

	@Test(expected=Test.None.class)
	public void testInvalidRoadMarkPattern() {
		Mark parent = MarkFactory.newMark("bnsf");
		assertNotNull(parent);
		Mark mark = MarkFactory.newMark("T123", parent);
		assertFalse(mark.isValid());
	}

	@Test(expected=Test.None.class)
	public void testInvalidRoadMarkLength() {
		Mark parent = MarkFactory.newMark("bnsf");
		assertNotNull(parent);
		Mark mark = MarkFactory.newMark("AAAAA", parent);
		assertFalse(mark.isValid());
	}

	@Test
	public void validShopMarks() {
		Mark parent = MarkFactory.newMark("bnsf");
		assertNotNull(parent);
		assertTrue(parent.isEquipmentOwningMark());
		Mark mark;
		assertNotNull(mark = MarkFactory.newMark("C123"));
		assertFalse(mark.isEquipmentOwningMark());
		assertNotNull(mark = MarkFactory.newMark("c123"));
		assertFalse(mark.isEquipmentOwningMark());
		assertNotNull(mark = MarkFactory.newMark("D123"));
		assertFalse(mark.isEquipmentOwningMark());
		assertNotNull(mark = MarkFactory.newMark("d123"));
		assertFalse(mark.isEquipmentOwningMark());
		assertNotNull(mark = MarkFactory.newMark("a123"));
		assertFalse(mark.isEquipmentOwningMark());
		assertNotNull(mark = MarkFactory.newMark("A123"));
		assertFalse(mark.isEquipmentOwningMark());
		assertNotNull(mark = MarkFactory.newMark("C123", parent));
		assertFalse(mark.isEquipmentOwningMark());
		assertTrue(mark.isParent(parent));
		assertNotNull(mark = MarkFactory.newMark("c123", parent));
		assertFalse(mark.isEquipmentOwningMark());
		assertTrue(mark.isParent(parent));
		assertNotNull(mark = MarkFactory.newMark("D123", parent));
		assertFalse(mark.isEquipmentOwningMark());
		assertTrue(mark.isParent(parent));
		assertNotNull(mark = MarkFactory.newMark("d123", parent));
		assertFalse(mark.isEquipmentOwningMark());
		assertTrue(mark.isParent(parent));
		assertNotNull(mark = MarkFactory.newMark("a123", parent));
		assertFalse(mark.isEquipmentOwningMark());
		assertTrue(mark.isParent(parent));
		assertNotNull(mark = MarkFactory.newMark("A123", parent));
		assertFalse(mark.isEquipmentOwningMark());
		assertTrue(mark.isParent(parent));
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_1() throws InvalidRoadMarkException {
		Mark mark = MarkFactory.newMark("23");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_2() throws InvalidRoadMarkException {
		Mark mark = MarkFactory.newMark("AB.D");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_3() throws InvalidRoadMarkException {
		Mark mark = MarkFactory.newMark("AB.D");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_4() throws InvalidRoadMarkException {
		Mark mark = MarkFactory.newMark("234a");
		assertFalse(mark.isValid());
	}

	@Test(expected=Test.None.class)
	public void inValidMarks_11() throws InvalidRoadMarkException {
		Mark mark = MarkFactory.newMark("23", (Mark)null);
		assertFalse(mark.isValid());
	}

	@Test(expected=Test.None.class)
	public void inValidMarks_21() throws InvalidRoadMarkException {
		Mark mark = MarkFactory.newMark("AB.D", (Mark)null);
		assertFalse(mark.isValid());
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_31() throws InvalidRoadMarkException {
		Mark mark = MarkFactory.newMark("AB.D", (Mark)null);
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_41() throws InvalidRoadMarkException {
		Mark mark = MarkFactory.newMark("234a", (Mark)null);
		assertFalse(mark.isValid());
	}
	
	
	@Test
	public void testCopyConstructors() {
		Mark mark, mark2, parent1, parent2;
		mark = MarkFactory.newMark("C123");
		mark2 = MarkFactory.newMark(mark);
		assertTrue(mark.equals(mark2) && mark2.equals(mark));
		mark = MarkFactory.newMark("d999");
		assertFalse(mark.equals(mark2) && mark2.equals(mark));
		mark2 = MarkFactory.newMark(mark);
		assertTrue(mark.equals(mark2) && mark2.equals(mark));
		mark = MarkFactory.newMark("a999");
		assertFalse(mark.equals(mark2) && mark2.equals(mark));
		mark2 = MarkFactory.newMark(mark);
		assertTrue(mark.equals(mark2) && mark2.equals(mark));
		mark = MarkFactory.newMark("NS");
		assertFalse(mark.equals(mark2) && mark2.equals(mark));
		mark2 = MarkFactory.newMark(mark);
		assertTrue(mark.equals(mark2) && mark2.equals(mark));

		parent1 = MarkFactory.newMark("BNSF");
		parent2 = MarkFactory.newMark("NS");
		
		mark = MarkFactory.newMark("C123", parent1);
		mark2 = MarkFactory.newMark(mark);
		assertTrue(mark.equals(mark2) && mark2.equals(mark));
		assertTrue(mark.getParent().equals(mark2.getParent()) && mark2.getParent().equals(mark.getParent()));
		mark = MarkFactory.newMark("d999", parent2);
		assertFalse(mark.equals(mark2) && mark2.equals(mark));
		mark2 = MarkFactory.newMark(mark);
		assertTrue(mark.equals(mark2) && mark2.equals(mark));
		assertTrue(mark.getParent().equals(mark2.getParent()) && mark2.getParent().equals(mark.getParent()));
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_a() {
		MarkFactory.newMark((Mark)null);
	}
	
	@Test
	public void testJustifiedString() {
		Mark mark = MarkFactory.newMark("EWS&");
		assertThat(mark.getValueAsJustifiedString(), is("EWS&"));
	}
}
