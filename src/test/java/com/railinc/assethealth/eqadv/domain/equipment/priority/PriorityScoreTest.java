package com.railinc.assethealth.eqadv.domain.equipment.priority;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.equipment.Equipment;
import com.railinc.assethealth.eqadv.domain.equipment.EquipmentPriorityScore;
import com.railinc.assethealth.eqadv.domain.notice.NoticeEqmtScoreFactors;

public class PriorityScoreTest {
	@Test(expected=Test.None.class)
	public void testPriorityScore() {
		EquipmentPriorityScore score = new EquipmentPriorityScore();
		score.setCommodityScore(50);
		score.setCarTypeScore(20);
		score.setMovementScore(100);
		assertEquals(Integer.valueOf(20), score.getCarTypeScore());
		assertEquals(Integer.valueOf(50), score.getCommodityScore());
		assertEquals(Integer.valueOf(100), score.getMovementScore());
	}	

	@Test(expected=Test.None.class)
	public void testCalculateScore() {
		EquipmentPriorityScore score = new EquipmentPriorityScore();
		NoticeEqmtScoreFactors factors = new NoticeEqmtScoreFactors();
		factors.setCarType("BLAH");
		factors.setCarTypeEquals(20);
		factors.setHazmatCarried(100);
		factors.setNonHazmatCarried(10);
		factors.setLastMovementLTE90Days(90);
		factors.setLastMovementLTE180Days(50);
		factors.setLastMovementGT180Days(10);
		Equipment equipment = new Equipment("RAIL", 1L, 1L);
		equipment.setLastMovement(DateUtils.addDays(new Date(), -5));
		equipment.setEquipmentGroup("TANK");
		equipment.setLastCommodityHazmat(false);
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(0), score.getCarTypeScore());
		assertEquals(Integer.valueOf(10), score.getCommodityScore());
		assertEquals(Integer.valueOf(90), score.getMovementScore());
		assertEquals(Integer.valueOf(100), score.getTotalScore());
	}	

	@Test(expected=Test.None.class)
	public void testCommodityScore() {
		EquipmentPriorityScore score = new EquipmentPriorityScore();
		NoticeEqmtScoreFactors factors = new NoticeEqmtScoreFactors();
		factors.setCarType("BLAH");
		factors.setCarTypeEquals(20);
		factors.setHazmatCarried(66);
		factors.setNonHazmatCarried(10);
		factors.setLastMovementLTE90Days(90);
		factors.setLastMovementLTE180Days(50);
		factors.setLastMovementGT180Days(10);
		Equipment equipment = new Equipment("RAIL", 1L, 1L);
		equipment.setLastMovement(DateUtils.addDays(new Date(), -5));
		equipment.setEquipmentGroup("TANK");
		equipment.setLastCommodityHazmat(true);
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(66), score.getCommodityScore());
		equipment.setLastCommodityHazmat(false);
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(10), score.getCommodityScore());
	}	
	
	@Test(expected=Test.None.class)
	public void testCarTypeScore() {
		EquipmentPriorityScore score = new EquipmentPriorityScore();
		NoticeEqmtScoreFactors factors = new NoticeEqmtScoreFactors();
		factors.setCarType("BLAH");
		factors.setCarTypeEquals(20);
		factors.setHazmatCarried(100);
		factors.setNonHazmatCarried(10);
		factors.setLastMovementLTE90Days(90);
		factors.setLastMovementLTE180Days(50);
		factors.setLastMovementGT180Days(10);
		Equipment equipment = new Equipment("RAIL", 1L, 1L);
		equipment.setLastMovement(DateUtils.addDays(new Date(), -5));
		equipment.setEquipmentGroup("TANK");
		equipment.setLastCommodityHazmat(false);
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(0), score.getCarTypeScore());
		equipment.setEquipmentGroup("BLAH");
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(20), score.getCarTypeScore());
	}	

	@Test(expected=Test.None.class)
	public void testMovementScore() {
		EquipmentPriorityScore score = new EquipmentPriorityScore();
		NoticeEqmtScoreFactors factors = new NoticeEqmtScoreFactors();
		factors.setCarType("BLAH");
		factors.setCarTypeEquals(20);
		factors.setHazmatCarried(100);
		factors.setNonHazmatCarried(10);
		factors.setLastMovementLTE90Days(91);
		factors.setLastMovementLTE180Days(52);
		factors.setLastMovementGT180Days(13);
		Equipment equipment = new Equipment("RAIL", 1L, 1L);
		equipment.setLastMovement(DateUtils.addDays(new Date(), -5));
		equipment.setEquipmentGroup("TANK");
		equipment.setLastCommodityHazmat(false);
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(91), score.getMovementScore());
		equipment.setLastMovement(DateUtils.addDays(new Date(), -90));
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(91), score.getMovementScore());
		equipment.setLastMovement(DateUtils.addDays(new Date(), -180));
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(52), score.getMovementScore());
		equipment.setLastMovement(DateUtils.addDays(new Date(), -181));
		score.calculateScore(equipment, factors);
		assertEquals(Integer.valueOf(13), score.getMovementScore());
	}	
	
}
