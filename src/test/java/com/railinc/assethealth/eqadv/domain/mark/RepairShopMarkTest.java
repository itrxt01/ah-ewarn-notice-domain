package com.railinc.assethealth.eqadv.domain.mark;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;

public class RepairShopMarkTest {
	
	@Test
	public void validRepairShopMarks() {
		RepairShopMark mark = new RepairShopMark("d222");
		assertNotNull(mark);
		assertFalse(mark.isEquipmentOwningMark());
		assertEquals("D222", mark.getValueAsString());
		assertEquals(mark.getMarkInitial(), mark.getValueAsString());
		assertEquals("D222", mark.getValueAsJustifiedString());
		mark = new RepairShopMark("C123");
		assertNotNull(mark);
		assertFalse(mark.isEquipmentOwningMark());
		assertEquals("C123", mark.getValueAsString());
		assertEquals(mark.getMarkInitial(), mark.getValueAsString());
		assertEquals("C123", mark.getValueAsJustifiedString());
		mark = new RepairShopMark("ABC");
		assertNotNull(mark);
		assertFalse(mark.isEquipmentOwningMark());
		assertEquals("ABC", mark.getValueAsString());
		assertEquals(mark.getMarkInitial(), mark.getValueAsString());
		assertEquals("ABC ", mark.getValueAsJustifiedString());
	}
	
	@Test
	public void validMarkParent() {
		Mark parent = new RoadMark("BNSF");
		assertNotNull(parent);
		assertTrue(parent.isEquipmentOwningMark());
		assertEquals("BNSF", parent.getValueAsString());
		RepairShopMark mark = new RepairShopMark("d222", parent);
		assertNotNull(mark);
		assertFalse(mark.isEquipmentOwningMark());
		assertTrue(mark.isParent(parent));
	}
	

	@Test
	public void testCopyConstructor() {
		RepairShopMark mark = new RepairShopMark("C123");
		RepairShopMark mark2 = new RepairShopMark(mark);
		assertNotNull(mark2);
		assertTrue(mark2.equals(mark) && mark.equals(mark2));
		assertEquals(mark2.getMarkInitial(), mark.getValueAsString());
	}
	
	
	@Test(expected=Test.None.class)
	public void inValidMarks_1() throws InvalidRoadMarkException {
		Mark mark = new RepairShopMark("123C");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_2() throws InvalidRoadMarkException {
		Mark mark = new RepairShopMark("A123");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_7() throws InvalidRoadMarkException {
		Mark mark = new RepairShopMark("A1%3");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_8() throws InvalidRoadMarkException {
		Mark mark = new RepairShopMark("A13345");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_3() throws InvalidRoadMarkException {
		Mark mark = new RepairShopMark("    ");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_4() throws InvalidRoadMarkException {
		Mark mark = new RepairShopMark("");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_5() throws InvalidRoadMarkException {
		Mark mark = new RepairShopMark((String)null);
		assertFalse(mark.isValid());
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_6() throws InvalidRoadMarkException {
		Mark mark = new RepairShopMark((Mark)null);
		assertFalse(mark.isValid());
	}
	
	@Test
	public void testEquals() throws InvalidRoadMarkException {
		RepairShopMark mark = new RepairShopMark("C234");
		RepairShopMark mark2 = new RepairShopMark("c234");
		assertTrue(mark.equals(mark2));
		assertTrue(mark2.equals(mark));
		mark2 = new RepairShopMark("C234");
		assertTrue(mark.equals(mark2));
		assertTrue(mark2.equals(mark));
		mark2 = new RepairShopMark("c000");
		assertFalse(mark.equals(mark2));
		assertFalse(mark2.equals(mark));
		assertFalse(mark.equals("C234"));
		assertFalse(mark.equals(null));
	}

	@Test
	public void testHashcode() throws InvalidRoadMarkException {
		RepairShopMark mark = new RepairShopMark("c234");
		RepairShopMark mark2 = new RepairShopMark("C234");
		assertTrue(mark.hashCode() == mark2.hashCode());
		mark = new RepairShopMark("d009");
		mark2 = new RepairShopMark("D009");
		assertTrue(mark.hashCode() == mark2.hashCode());
		mark = new RepairShopMark("d009");
		mark2 = new RepairShopMark("D000");
		assertTrue(mark.hashCode() != mark2.hashCode());
	}
	
	@Test
	public void testToString() throws InvalidRoadMarkException {
		RepairShopMark mark = new RepairShopMark("d000");
		assertTrue(StringUtils.isNotBlank(mark.toString()));
		assertTrue(mark.toString().contains("D000"));
	}
}
