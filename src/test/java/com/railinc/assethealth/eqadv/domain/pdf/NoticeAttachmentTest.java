package com.railinc.assethealth.eqadv.domain.pdf;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSettersExcluding;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NoticeAttachmentTest {

	 @Test
	    public void test_HasValidProperties() {
	        assertThat(NoticeAttachment.class, allOf(
	                hasValidBeanConstructor(),
	                hasValidGettersAndSettersExcluding("noticeName", "nextNoticeName"
	                		, "status", "noticeNumber", "fileType", "valid", "supportedFileType", "validPDF")));
	    }

	    @Test
	    public void test_Equals() {
	    	NoticeAttachment n1 = new NoticeAttachment();
	        n1.setFileName("filename");
	        n1.setComment("valid");
	        NoticeAttachment n2 = new NoticeAttachment();
	        n2.setFileName("filename");
	        n2.setComment("valid");
	        assertThat(n1.equals(n2), is(true));
	    }

	    @Test
	    public void testCopyCtor() {
	    	NoticeAttachment n1 = new NoticeAttachment();
	        n1.setFileName("filename");
	        n1.setFileContent("FILE CONTENTS".getBytes());
	        n1.setFileExtension("pdf");
	        n1.setFileSize(123);
	        NoticeAttachment n2 = new NoticeAttachment(n1);
	        assertEquals(n1.getFileName(), n2.getFileName());
	        assertEquals(n1.getFileExtension(), n2.getFileExtension());
	        assertEquals(n1.getFileSize(), n2.getFileSize());
	        assertEquals(n1.getFileContent(), n1.getFileContent());
	    }

	    @Test
	    public void test_Hashcode() {
	    	NoticeAttachment n1 = new NoticeAttachment();
	        n1.setFileName("filename");
	        n1.setComment("valid");
	        NoticeAttachment n2 = new NoticeAttachment();
	        n2.setFileName("filename");
	        n2.setComment("valid");
	        assertThat(n1.hashCode() == n2.hashCode(), is(true));
	        assertThat(n1.hashCode() == n2.hashCode(), is(true));
	    }
	    
	    @Test
	    public void test_ToString() {
	    	NoticeAttachment n1 = new NoticeAttachment();
	    	n1.setFileName("filename");
	        n1.setComment("valid");
	        assertThat(n1.toString(), containsString("valid"));
	    }

}
