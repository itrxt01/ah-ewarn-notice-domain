package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.railinc.assethealth.eqadv.domain.inspection.FINAL_INSPECTION_INDICATOR;
import com.railinc.assethealth.eqadv.domain.inspection.FinalInspectionIndicatorDeserializer;

public class FinalInspectionIndicatorDeserializerTest {

		@Test
		public void testFinalInspectionIndicatorDeserializer() {
			FinalInspectionIndicatorDeserializer des = new FinalInspectionIndicatorDeserializer();
			assertNotNull(des);
		}

		@Test
		public void testFinalInspectionIndicatorDeserializerClassOfFinalInspectionIndicator() {
			FinalInspectionIndicatorDeserializer des = new FinalInspectionIndicatorDeserializer(FINAL_INSPECTION_INDICATOR.class);
			assertNotNull(des);
		}

		@Test
		public void testMarshallUnmarshall_INTERNAL() throws IOException {
			FINAL_INSPECTION_INDICATOR indicator = FINAL_INSPECTION_INDICATOR.INTERNAL;
			
			ObjectMapper objectMapper = new ObjectMapper();

			String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(indicator);
			System.out.println(JSON);

			SimpleModule module = new SimpleModule();
			module.addDeserializer(FINAL_INSPECTION_INDICATOR.class, new FinalInspectionIndicatorDeserializer());
			objectMapper.registerModule(module);

			FINAL_INSPECTION_INDICATOR unmarshalled = objectMapper.readValue(JSON, FINAL_INSPECTION_INDICATOR.class);
			assertEquals(FINAL_INSPECTION_INDICATOR.INTERNAL, unmarshalled);
		}

		@Test
		public void testMarshallUnmarshall_OPEN() throws IOException {
			FINAL_INSPECTION_INDICATOR indicator = FINAL_INSPECTION_INDICATOR.OPEN;
			
			ObjectMapper objectMapper = new ObjectMapper();

			String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(indicator);
			System.out.println(JSON);

			SimpleModule module = new SimpleModule();
			module.addDeserializer(FINAL_INSPECTION_INDICATOR.class, new FinalInspectionIndicatorDeserializer());
			objectMapper.registerModule(module);

			FINAL_INSPECTION_INDICATOR unmarshalled = objectMapper.readValue(JSON, FINAL_INSPECTION_INDICATOR.class);
			assertEquals(FINAL_INSPECTION_INDICATOR.OPEN, unmarshalled);
		}

		@Test
		public void testMarshallUnmarshall_SPECIFIED() throws IOException {
			FINAL_INSPECTION_INDICATOR indicator = FINAL_INSPECTION_INDICATOR.SPECIFIED;
			
			ObjectMapper objectMapper = new ObjectMapper();

			String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(indicator);
			System.out.println(JSON);

			SimpleModule module = new SimpleModule();
			module.addDeserializer(FINAL_INSPECTION_INDICATOR.class, new FinalInspectionIndicatorDeserializer());
			objectMapper.registerModule(module);

			FINAL_INSPECTION_INDICATOR unmarshalled = objectMapper.readValue(JSON, FINAL_INSPECTION_INDICATOR.class);
			assertEquals(FINAL_INSPECTION_INDICATOR.SPECIFIED, unmarshalled);
		}
}
