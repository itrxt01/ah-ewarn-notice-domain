package com.railinc.assethealth.eqadv.domain.category;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.category.CategorySeverityLevel;
import com.railinc.assethealth.eqadv.domain.category.CategorySeverityLevels;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;
import com.railinc.assethealth.eqadv.domain.exception.SeverityLevelEscalationException;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevels;

public class CategorySeverityLevelsTest {

	@Test
	public void testCategorySeverityLevels() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		SeverityLevel s1 = new SeverityLevel("ZZ");
		SeverityLevel s2 = new SeverityLevel("YY", "ZZ", 180, "days");
		s2.setEscalationType("D");
		SeverityLevel s3 = new SeverityLevel("XX", "YY", 180, "months");
		s3.setEscalationType("D");
		
		CategorySeverityLevel level1 = new CategorySeverityLevel(new Category("AB"), s1);
		CategorySeverityLevel level2 = new CategorySeverityLevel(new Category("AB"), s2);
		CategorySeverityLevel level3 = new CategorySeverityLevel(new Category("AB"), s3);
		List<CategorySeverityLevel> levels = new ArrayList<>();
		levels.add(level2);
		levels.add(level1);
		levels.add(level3);
		CategorySeverityLevels categorySeverityLevels = new CategorySeverityLevels(levels);
		Category category = categorySeverityLevels.getCategorySeverityLevels().get(0);
		SeverityLevels severityLevels = category.getSeverityLevels();
		assertNotNull(severityLevels.getSeverityLevel("ZZ"));
		assertFalse(severityLevels.getSeverityLevel("ZZ").isEscalates());
		assertNotNull(severityLevels.getSeverityLevel("YY"));
		assertTrue(severityLevels.getSeverityLevel("YY").isEscalates());
		
		severityLevels.getSeverityLevel("XX");
		assertNotNull(severityLevels.getSeverityLevel("XX"));
		assertTrue(severityLevels.getSeverityLevel("XX").isEscalates());
	}

	@Test
	public void testCategorySeverityLevelsEmptyCollection() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<CategorySeverityLevel> levels = new ArrayList<>();
		CategorySeverityLevels categorySeverityLevels = new CategorySeverityLevels(levels);
		assertNotNull(categorySeverityLevels.getCategorySeverityLevels());
		assertTrue(categorySeverityLevels.getCategorySeverityLevels().isEmpty());

		CategorySeverityLevel level1 = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ"));
		CategorySeverityLevel level2 = new CategorySeverityLevel(new Category("AC"), new SeverityLevel("YY", "ZZ", 180, "days"));
		categorySeverityLevels.add(level1);
		categorySeverityLevels.add(level2);
		List<Category> categories = categorySeverityLevels.getCategorySeverityLevels();
		assertTrue(categories.contains(level1.getCategory()));
		assertTrue(categories.contains(level2.getCategory()));
	}
}
