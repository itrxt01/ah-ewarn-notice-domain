package com.railinc.assethealth.eqadv.domain.equipment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.exception.InvalidAssignedEquipmentException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidEquipmentIdException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;
import com.railinc.assethealth.eqadv.domain.notice.Notice;

public class EquipmentViewTest {
	private static final String USER_NAME = "TESTER";
	private static final String VALID_MARK = "ABCD";
	private static final long EQUIP_NUMBER = 10001L;
	private static final String EQUIP_NUM_STRING = "0000010001";
	private static final long EQUIP_EIN = 22_222_222L;
	private static final String EIN_STRING = "0022222222";
	private static final String EQUIP_ID = VALID_MARK + EQUIP_NUM_STRING;
	private static final Date NOW = new Date();
	private static final Date TIMESTAMP = new Date();
	
	
	@Test
	public void testConstructor() throws InvalidAssignedEquipmentException, InvalidRoadMarkException, InvalidEquipmentIdException {
		Notice notice = new Notice();
		notice.setNumber(1234);
		notice.setComponentRegistryNotice(true);
		notice.setId(9_876_543_210L);
		Assignment assignment = new Assignment.Builder(VALID_MARK, EQUIP_NUMBER, EQUIP_EIN)
		.assignedOn(NOW)
		.onNotice(notice)
		.withId(1_123_456_789L)
		.assignedBy(VALID_MARK)
		.escalatesOn(DateUtils.addDays(NOW, 180))
		.withSeverityCode("XX")
		.withModifyTimestamp(TIMESTAMP)
		.withModifyUserName(USER_NAME)
		.withCreateUserName(USER_NAME)
		.withCreateTimestamp(TIMESTAMP)
		.build();

		EquipmentView view = new EquipmentView(assignment);
		assertEquals(NOW, view.getAssignmentDate());
		assertEquals(9_876_543_210L, view.getNoticeId());
		assertEquals(EIN_STRING, view.getEin());
		assertEquals(EQUIP_ID, view.getEquipmentId());
		assertEquals(NOW, view.getAssignmentDate());
		assertEquals("XX", view.getSeverityLevel());
	}
	
	@Test
	public void testMarshalUnmarshal() throws IOException, ParseException {
		
		Date now = new Date();
		ObjectMapper mapper = new ObjectMapper();
		
		EquipmentView originalEV = new EquipmentView();
		originalEV.setAssignmentDate(now);
		originalEV.setComponentInfo("componentInfo");
		originalEV.setEin("0010285911");
		originalEV.setEquipmentId("0000000022");
		originalEV.setEquipmentStatus("I");
		originalEV.setLessee("RAIL-L");
		originalEV.setMarkOwner("RAIL-M");
		originalEV.setMechanicalDesignation("MS");
		originalEV.setNoticeId(5171);
		originalEV.setUmlerOwner("RAIL");

		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(originalEV);
		
		EquipmentView newEV = mapper.readValue(json, EquipmentView.class);
		
		assertTrue(DateUtils.isSameDay(originalEV.getAssignmentDate(), newEV.getAssignmentDate()));
		assertEquals(originalEV.getComponentInfo(), newEV.getComponentInfo());
		assertEquals(originalEV.getEin(), newEV.getEin());
		assertEquals(originalEV.getEquipmentId(), newEV.getEquipmentId());
		assertEquals(originalEV.getEquipmentStatus(), newEV.getEquipmentStatus());
		assertEquals(originalEV.getLessee(), newEV.getLessee());
		assertEquals(originalEV.getMarkOwner(), newEV.getMarkOwner());
		assertEquals(originalEV.getMechanicalDesignation(), newEV.getMechanicalDesignation());
		assertEquals(originalEV.getNoticeId(), newEV.getNoticeId());
		assertEquals(originalEV.getUmlerOwner(), newEV.getUmlerOwner());
	}
}
