package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.exception.InvalidFinalInspectionIndicatorException;
import com.railinc.assethealth.eqadv.domain.inspection.FINAL_INSPECTION_INDICATOR;

public class FINAL_INSPECTION_INDICATORTest {
	@Test
	public void testHappyPath() throws InvalidFinalInspectionIndicatorException {
		assertEquals(FINAL_INSPECTION_INDICATOR.OPEN, FINAL_INSPECTION_INDICATOR.forIndicator("O"));
		assertEquals(FINAL_INSPECTION_INDICATOR.SPECIFIED, FINAL_INSPECTION_INDICATOR.forIndicator("S"));
		assertEquals(FINAL_INSPECTION_INDICATOR.INTERNAL, FINAL_INSPECTION_INDICATOR.forIndicator("I"));
	}
	
	@Test
	public void testTrim()  throws InvalidFinalInspectionIndicatorException {
		assertEquals(FINAL_INSPECTION_INDICATOR.OPEN, FINAL_INSPECTION_INDICATOR.forIndicator(" O"));
		assertEquals(FINAL_INSPECTION_INDICATOR.OPEN, FINAL_INSPECTION_INDICATOR.forIndicator("O "));
		assertEquals(FINAL_INSPECTION_INDICATOR.OPEN, FINAL_INSPECTION_INDICATOR.forIndicator(" O "));
	}

	@Test
	public void testCase()  throws InvalidFinalInspectionIndicatorException {
		assertEquals(FINAL_INSPECTION_INDICATOR.OPEN, FINAL_INSPECTION_INDICATOR.forIndicator("o"));
	}

	@Test(expected=InvalidFinalInspectionIndicatorException.class)
	public void testNullIndicator() throws InvalidFinalInspectionIndicatorException {
		String nullIndicator = null;
		FINAL_INSPECTION_INDICATOR.forIndicator(nullIndicator);
	}
	
	@Test(expected=InvalidFinalInspectionIndicatorException.class)
	public void testEmptyIndicator() throws InvalidFinalInspectionIndicatorException {
		String emptyIndicator = "";
		FINAL_INSPECTION_INDICATOR.forIndicator(emptyIndicator);
	}
	
	@Test(expected=InvalidFinalInspectionIndicatorException.class)
	public void testBlankIndicator() throws InvalidFinalInspectionIndicatorException {
		String blankIndicator = "   ";
		FINAL_INSPECTION_INDICATOR.forIndicator(blankIndicator);
	}
	
	@Test(expected=InvalidFinalInspectionIndicatorException.class)
	public void testTooLongIndicator() throws InvalidFinalInspectionIndicatorException {
		String tooLongIndicator = "AA";
		FINAL_INSPECTION_INDICATOR.forIndicator(tooLongIndicator);
	}
	
	@Test(expected=InvalidFinalInspectionIndicatorException.class)
	public void testUnknownIndicator() throws InvalidFinalInspectionIndicatorException {
		String unknownIndicator = "Z";
		FINAL_INSPECTION_INDICATOR.forIndicator(unknownIndicator);
	}
	
	@Test
	public void testMarshallUnmarshal() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String JSON = String.format("\"%s\"", FINAL_INSPECTION_INDICATOR.INTERNAL.code);
		System.out.println("\"finalInspectionInd\":" + JSON);
		FINAL_INSPECTION_INDICATOR result = objectMapper.readValue(JSON, FINAL_INSPECTION_INDICATOR.class);
		
		assertEquals(FINAL_INSPECTION_INDICATOR.INTERNAL, result);
		
	}
}
