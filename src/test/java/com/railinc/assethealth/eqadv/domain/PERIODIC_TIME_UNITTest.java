package com.railinc.assethealth.eqadv.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;

public class PERIODIC_TIME_UNITTest {
	@Test
	public void testHappyPath() throws InvalidPeriodicTimeUnitException {
		assertEquals(PERIODIC_TIME_UNIT.MONTHS, PERIODIC_TIME_UNIT.getTimeUnitForFrequency(30));
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForFrequency(7));

		assertEquals(PERIODIC_TIME_UNIT.YEARS, PERIODIC_TIME_UNIT.getTimeUnitForFrequency(730));
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForFrequency(735));
		assertEquals(PERIODIC_TIME_UNIT.MONTHS, PERIODIC_TIME_UNIT.getTimeUnitForFrequency(60));
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForFrequency(63));
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForFrequency(14));

		assertEquals(PERIODIC_TIME_UNIT.YEARS, PERIODIC_TIME_UNIT.getTimeUnitForName("years"));
		assertEquals(PERIODIC_TIME_UNIT.MONTHS, PERIODIC_TIME_UNIT.getTimeUnitForName("months"));
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName("days"));
	}

	@Test
	public void testGetNumberOfDays() {
		assertEquals(0, PERIODIC_TIME_UNIT.YEARS.getNumberOfDays(0));
		assertEquals(365, PERIODIC_TIME_UNIT.YEARS.getNumberOfDays(1));
		assertEquals(730, PERIODIC_TIME_UNIT.YEARS.getNumberOfDays(2));

		assertEquals(0, PERIODIC_TIME_UNIT.MONTHS.getNumberOfDays(0));
		assertEquals(30, PERIODIC_TIME_UNIT.MONTHS.getNumberOfDays(1));
		assertEquals(60, PERIODIC_TIME_UNIT.MONTHS.getNumberOfDays(2));

		assertEquals(0, PERIODIC_TIME_UNIT.DAYS.getNumberOfDays(0));
		assertEquals(1, PERIODIC_TIME_UNIT.DAYS.getNumberOfDays(1));
		assertEquals(2, PERIODIC_TIME_UNIT.DAYS.getNumberOfDays(2));
	}

	@Test
	public void testGetNumberOfTimeUnits() {
		assertEquals(0, PERIODIC_TIME_UNIT.YEARS.getNumberOfTimeUnits(364));
		assertEquals(1, PERIODIC_TIME_UNIT.YEARS.getNumberOfTimeUnits(365));
		assertEquals(2, PERIODIC_TIME_UNIT.YEARS.getNumberOfTimeUnits(730));

		assertEquals(0, PERIODIC_TIME_UNIT.MONTHS.getNumberOfTimeUnits(29));
		assertEquals(1, PERIODIC_TIME_UNIT.MONTHS.getNumberOfTimeUnits(30));
		assertEquals(2, PERIODIC_TIME_UNIT.MONTHS.getNumberOfTimeUnits(60));

		assertEquals(0, PERIODIC_TIME_UNIT.DAYS.getNumberOfTimeUnits(0));
		assertEquals(1, PERIODIC_TIME_UNIT.DAYS.getNumberOfTimeUnits(1));
		assertEquals(2, PERIODIC_TIME_UNIT.DAYS.getNumberOfTimeUnits(2));
	}

	@Test
	public void testTrim() throws InvalidPeriodicTimeUnitException {
		assertEquals(PERIODIC_TIME_UNIT.YEARS, PERIODIC_TIME_UNIT.getTimeUnitForName(" years"));
		assertEquals(PERIODIC_TIME_UNIT.YEARS, PERIODIC_TIME_UNIT.getTimeUnitForName("years "));
		assertEquals(PERIODIC_TIME_UNIT.YEARS, PERIODIC_TIME_UNIT.getTimeUnitForName(" years "));

		assertEquals(PERIODIC_TIME_UNIT.MONTHS, PERIODIC_TIME_UNIT.getTimeUnitForName(" months"));
		assertEquals(PERIODIC_TIME_UNIT.MONTHS, PERIODIC_TIME_UNIT.getTimeUnitForName("months "));
		assertEquals(PERIODIC_TIME_UNIT.MONTHS, PERIODIC_TIME_UNIT.getTimeUnitForName(" months "));

		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName(" days"));
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName("days "));
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName(" days "));
	}

	@Test
	public void testCase() throws InvalidPeriodicTimeUnitException {
		assertEquals(PERIODIC_TIME_UNIT.YEARS, PERIODIC_TIME_UNIT.getTimeUnitForName("YEARS"));
		assertEquals(PERIODIC_TIME_UNIT.MONTHS, PERIODIC_TIME_UNIT.getTimeUnitForName("MONTHS"));
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName("DAYS"));
	}

	@Test(expected=InvalidPeriodicTimeUnitException.class)
	public void testUnknownName() throws InvalidPeriodicTimeUnitException {
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName("PANCAKES"));
	}
	
	@Test(expected=InvalidPeriodicTimeUnitException.class)
	public void testEmptyString() throws InvalidPeriodicTimeUnitException {
		String emptyString = "";
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName(emptyString));
	}
	
	@Test(expected=InvalidPeriodicTimeUnitException.class)
	public void testBlankName() throws InvalidPeriodicTimeUnitException {
				String blankString = "  ";
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName(blankString));
	}
	
	@Test(expected=InvalidPeriodicTimeUnitException.class)
	public void testNullName() throws InvalidPeriodicTimeUnitException {	
		String nullString = null;
		assertEquals(PERIODIC_TIME_UNIT.DAYS, PERIODIC_TIME_UNIT.getTimeUnitForName(nullString));
	}
}
