package com.railinc.assethealth.eqadv.domain.category;

import static org.junit.Assert.*;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.exception.InvalidCategoryException;
import com.railinc.assethealth.eqadv.domain.exception.SeverityLevelEscalationException;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;

public class CategoryTest {

	@Test
	public void testConstructorCODE() {
		Category cat = new Category("SM");
		assertTrue(cat.getCode().equals("SM"));
	}

	@Test
	public void testConstructorDESC() {
		Category cat = new Category("SI", "Another description");
		assertTrue("SI".equals(cat.getCode()));
		assertTrue(cat.getDescription().equals("Another description"));
	}

	@Test(expected=Test.None.class)
	public void testFourArgCtor() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description", 25, 60);
		assertTrue("ff".equals(cat.getCode()));
		assertTrue(cat.getDescription().equals("Another SILLY description"));
		assertEquals(Integer.valueOf(25), cat.getMinAdvisoryNumber());
		assertEquals(Integer.valueOf(60), cat.getMaxAdvisoryNumber());		
	}

	@Test(expected=InvalidCategoryException.class)
	public void testFourArgCtorNullMax() throws InvalidCategoryException {
		new Category("ff", "Another SILLY description", 25, null);
	}

	@Test(expected=InvalidCategoryException.class)
	public void testFourArgCtorNullMin() throws InvalidCategoryException {
		new Category("ff", "Another SILLY description", null, 25);
	}

	@Test(expected=InvalidCategoryException.class)
	public void testFourArgCtorNullArgs() throws InvalidCategoryException {
		new Category("ff", "Another SILLY description", null, null);
	}

	@Test(expected=InvalidCategoryException.class)
	public void testFourArgCtorInvalidArgs() throws InvalidCategoryException {
		new Category("ff", "Another SILLY description", 500, 25);
	}

	@Test(expected=Test.None.class)
	public void testIsValidAdvisoryNumber() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description", 25, 60);
		assertTrue(cat.isValidAdvisoryNumber(25));
		assertTrue(cat.isValidAdvisoryNumber(60));
		assertFalse(cat.isValidAdvisoryNumber(-5));
		assertFalse(cat.isValidAdvisoryNumber(500_000));
		assertFalse(cat.isValidAdvisoryNumber(61));
		assertFalse(cat.isValidAdvisoryNumber(24));
	}

	@Test(expected=Test.None.class)
	public void testIsValidAdvisoryNumberSetMinMax() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMaxAdvisoryNumber(5);
		cat.setMinAdvisoryNumber(1);
		assertTrue(cat.isValidAdvisoryNumber(1));
		assertTrue(cat.isValidAdvisoryNumber(4));
		assertFalse(cat.isValidAdvisoryNumber(-5));
		assertFalse(cat.isValidAdvisoryNumber(500_000));
		assertFalse(cat.isValidAdvisoryNumber(6));
		assertFalse(cat.isValidAdvisoryNumber(0));
	}

	@Test(expected=InvalidCategoryException.class)
	public void testIsValidAdvisoryNumberSetInvalidMin() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMaxAdvisoryNumber(5);
		cat.setMinAdvisoryNumber(6);
	}

	@Test(expected=InvalidCategoryException.class)
	public void testIsValidAdvisoryNumberSetInvalidMax() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMinAdvisoryNumber(6);
		cat.setMaxAdvisoryNumber(5);
	}

	@Test(expected=Test.None.class)
	public void testIsValidAdvisoryNumberMinMax() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMinAdvisoryNumber(1);
		cat.setMaxAdvisoryNumber(500);
		assertTrue(cat.isValidAdvisoryNumber(1));
		assertTrue(cat.isValidAdvisoryNumber(499));
		assertFalse(cat.isValidAdvisoryNumber(501));
		assertFalse(cat.isValidAdvisoryNumber(-1));
	}

	@Test(expected=Test.None.class)
	public void testIsValidAdvisoryNumberMaxMin() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMaxAdvisoryNumber(500);
		cat.setMinAdvisoryNumber(1);
		assertTrue(cat.isValidAdvisoryNumber(1));
		assertTrue(cat.isValidAdvisoryNumber(499));
		assertFalse(cat.isValidAdvisoryNumber(501));
		assertFalse(cat.isValidAdvisoryNumber(-1));
	}

	@Test(expected=InvalidCategoryException.class)
	public void testIsValidAdvisoryNumberInvalidMin() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMinAdvisoryNumber(-1);
	}

	@Test(expected=InvalidCategoryException.class)
	public void testIsValidAdvisoryNumberInvalidMax() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMaxAdvisoryNumber(500_000);
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testIsValidAdvisoryNumberMinNotSet() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMaxAdvisoryNumber(100);
		assertFalse(cat.isValidAdvisoryNumber(25));
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void testIsValidAdvisoryNumberMaxNotSet() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		cat.setMinAdvisoryNumber(25);
		assertFalse(cat.isValidAdvisoryNumber(25));
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testIsValidAdvisoryNumberMinMaxNotSet() throws InvalidCategoryException {
		Category cat = new Category("ff", "Another SILLY description");
		assertFalse(cat.isValidAdvisoryNumber(25));
	}

	@Test(expected=Test.None.class)
	public void testHashCode() throws InvalidCategoryException {
		Category cat = new Category();
		Category cat2 = new Category();
		assertTrue(cat.hashCode() == cat2.hashCode());
		cat.setCode("MS");
		assertTrue(cat.hashCode() != cat2.hashCode());
		cat2.setCode("MS");
		assertTrue(cat.hashCode() == cat2.hashCode());
		cat.setDescription("Description");		// Description excluded from hash
		assertTrue(cat.hashCode() == cat2.hashCode());
		cat2.setDescription("Description");
		assertTrue(cat.hashCode() == cat2.hashCode());
		cat.addSeverityLevel(new SeverityLevel(new SeverityCode("A1")));
		assertTrue(cat.hashCode() != cat2.hashCode());
		cat2.addSeverityLevel(new SeverityLevel(new SeverityCode("A1")));
		assertTrue(cat.hashCode() == cat2.hashCode());
		cat.setMinAdvisoryNumber(1);
		cat2.setMinAdvisoryNumber(1);
		assertTrue(cat.hashCode() == cat2.hashCode());
		cat2.setMinAdvisoryNumber(1000);
		assertFalse(cat.hashCode() == cat2.hashCode());
		cat.setMinAdvisoryNumber(1000);
		cat.setMaxAdvisoryNumber(1999);
		cat2.setMaxAdvisoryNumber(1999);
		assertTrue(cat.hashCode() == cat2.hashCode());		
	}

	@Test(expected=Test.None.class)
	public void testEqualsObject() throws InvalidCategoryException {
		Category cat = new Category();
		Category cat2 = new Category();
		assertTrue(cat.equals(cat2));
		cat.setCode("MS");
		assertTrue(!cat.equals(cat2));
		cat2.setCode("MS");
		assertTrue(cat.equals(cat2));
		cat.setDescription("Description");		// Description excluded from equals
		assertTrue(cat.equals(cat2));
		cat2.setDescription("Description");
		assertTrue(cat.equals(cat2));
		cat.addSeverityLevel(new SeverityLevel(new SeverityCode("A1")));
		assertTrue(!cat.equals(cat2));
		cat2.addSeverityLevel(new SeverityLevel(new SeverityCode("A1")));
		assertTrue(cat.equals(cat2));
		cat.setMinAdvisoryNumber(1);
		cat2.setMinAdvisoryNumber(1);
		assertTrue(cat.equals(cat2) && cat2.equals(cat));
		cat2.setMinAdvisoryNumber(1000);
		assertFalse(cat.equals(cat2) && cat2.equals(cat));
		cat.setMinAdvisoryNumber(1000);
		cat.setMaxAdvisoryNumber(1999);
		cat2.setMaxAdvisoryNumber(1999);
		assertTrue(cat.equals(cat2) && cat2.equals(cat));		
	}

	@Test
	public void testToString() {
		Category cat = new Category();
		cat.setDescription("A reasonable description");
		cat.setCode("SI");
		cat.addSeverityLevel(new SeverityLevel(new SeverityCode("A1")));
		String s = cat.toString();
		assertTrue(s.contains("SI"));
		assertTrue(s.contains("A reasonable description"));
		assertTrue(s.contains("A1"));
	}
	
	
	@Test
	public void testGetDescription() {
		Category cat = new Category();
		cat.setDescription("A reasonable description");
		assertTrue(cat.getDescription().equals("A reasonable description"));
	}

	@Test
	public void testCode() {
		Category cat = new Category();
		cat.setCode("SM");
		assertTrue(cat.getCode().equals("SM"));
	}

	@Test
	public void testSeverityLevel() throws SeverityLevelEscalationException {
		Category cat = new Category();
		cat.addSeverityLevel(new SeverityLevel(new SeverityCode("A1")));
		assertTrue(null != cat.getSeverityLevels().getSeverityLevel("A1"));
	}
}
