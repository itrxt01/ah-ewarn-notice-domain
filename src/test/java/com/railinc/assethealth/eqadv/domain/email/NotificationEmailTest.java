package com.railinc.assethealth.eqadv.domain.email;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.email.NotificationEmail;

public class NotificationEmailTest {

	private static String VALID_EMAIL_ADDR = "tester@tester.com";
	private static Integer NOTIFICATION_ID = Integer.valueOf(1);
	
	NotificationEmail validEmail;
	
	
	@Before
	public void init() {
		validEmail = new NotificationEmail();
		validEmail.setNotificationEventId(NOTIFICATION_ID);
		validEmail.setEmail(VALID_EMAIL_ADDR);
	}
	
	@Test
	public void testHashCode() {
		NotificationEmail email = new NotificationEmail();
		email.setNotificationEventId(3);
		email.setEmail(VALID_EMAIL_ADDR);
		assertNotEquals(validEmail.hashCode(), email.hashCode());
		assertNotEquals(email.hashCode(), validEmail.hashCode());
		email.setNotificationEventId(NOTIFICATION_ID);
		email.setEmail("tester_2@tester.com");
		assertNotEquals(email.hashCode(), validEmail.hashCode());
		assertNotEquals(validEmail.hashCode(), email.hashCode());
		email.setEmail(VALID_EMAIL_ADDR);
		assertEquals(email.hashCode(), validEmail.hashCode());
		assertEquals(validEmail.hashCode(), email.hashCode());
	}

	@Test
	public void testGetNotificationEventId() {
		int id = validEmail.getNotificationEventId();
		validEmail.setNotificationEventId(3);
		assertEquals(3, validEmail.getNotificationEventId());
		validEmail.setNotificationEventId(id);
		assertEquals(id, validEmail.getNotificationEventId());
	}

	@Test
	public void testGetEmail() {
		String email = validEmail.getEmail();
		validEmail.setEmail("another@test.com");;
		assertEquals("another@test.com", validEmail.getEmail());
		validEmail.setEmail(email);
		assertEquals(email, validEmail.getEmail());
	}

	@Test
	public void testGetCompany() {
		String company = validEmail.getCompany();
		validEmail.setCompany("anotherCompany");
		assertEquals("anotherCompany", validEmail.getCompany());
		validEmail.setCompany(company);
		assertEquals(company, validEmail.getCompany());
	}

	@Test
	public void testEqualsObject() {
		NotificationEmail email = new NotificationEmail();
		email.setNotificationEventId(3);
		email.setEmail(VALID_EMAIL_ADDR);
		assertNotEquals(validEmail, email);
		assertNotEquals(email, validEmail);
		email.setNotificationEventId(NOTIFICATION_ID);
		email.setEmail("tester_2@tester.com");
		assertNotEquals(email, validEmail);
		assertNotEquals(validEmail, email);
		email.setEmail(VALID_EMAIL_ADDR);
		assertEquals(email, validEmail);
		assertEquals(validEmail, email);
	}

}
