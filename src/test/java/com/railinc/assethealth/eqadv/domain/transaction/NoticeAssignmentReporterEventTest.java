package com.railinc.assethealth.eqadv.domain.transaction;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.transaction.NoticeAssignmentReporterEvent;

import static org.junit.Assert.assertEquals;

public class NoticeAssignmentReporterEventTest extends NoticeEventTestHelper<NoticeAssignmentReporterEvent> {

    private static final String MARK = "MARK";

    @Test
    public void testNoticeAssignmentReporterEvent() {
        NoticeAssignmentReporterEvent noticeAssignmentReporterEvent = getNoticeEvent();

        assertEquals(EVENT_ID, noticeAssignmentReporterEvent.getEventId());
        assertEquals(NOTICE_ID, noticeAssignmentReporterEvent.getNoticeId());
        assertEquals(MARK, noticeAssignmentReporterEvent.getMark());
    }

    @Override
    NoticeAssignmentReporterEvent getNoticeEvent() {
        NoticeAssignmentReporterEvent noticeAssignmentReporterEvent = new NoticeAssignmentReporterEvent();
        noticeAssignmentReporterEvent.setEventId(EVENT_ID);
        noticeAssignmentReporterEvent.setNoticeId(NOTICE_ID);
        noticeAssignmentReporterEvent.setMark(MARK);
        return noticeAssignmentReporterEvent;
    }
}
