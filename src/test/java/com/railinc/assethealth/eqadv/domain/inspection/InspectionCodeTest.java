package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.inspection.INSPECTION_TYPE;
import com.railinc.assethealth.eqadv.domain.inspection.InspectionCode;

public class InspectionCodeTest {

	@Test
	public void testHashCode() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, false);
		InspectionCode code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, false);
		assertTrue(code_1.hashCode() == code_2.hashCode());		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCE", "abcd description", false, false);
		assertFalse(code_1.hashCode() == code_2.hashCode());		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abce description", false, false);
		assertFalse(code_1.hashCode() == code_2.hashCode());		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", true, false);
		assertFalse(code_1.hashCode() == code_2.hashCode());		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, true);
		assertFalse(code_1.hashCode() == code_2.hashCode());		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", true, true);
		assertFalse(code_1.hashCode() == code_2.hashCode());		
	}

	@Test
	public void testToString() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, false);
		String testStr = code_1.toString();
		assertNotNull(testStr);
		assertTrue(StringUtils.isNotBlank(testStr));
		assertTrue(testStr.contains("ABCD"));
		assertTrue(testStr.contains("abcd description"));
	}

	@Test
	public void testEqualsObject() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, false);
		InspectionCode code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, false);
		assertEquals(code_1, code_2);		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCE", "abcd description", false, false);
		assertNotEquals(code_1, code_2);		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abce description", false, false);
		assertEquals(code_1, code_2);		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", true, false);
		assertNotEquals(code_1, code_2);		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, true);
		assertNotEquals(code_1, code_2);		
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", true, true);
		assertNotEquals(code_1, code_2);		
		assertNotEquals(code_1, null);		
		assertNotEquals(null, code_2);		
		
		assertNotEquals(code_1, "ABCD");		
		assertNotEquals("ABCD", code_2);		
	}

	@Test
	public void testGetCode() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "ABCD", "abcd description", false, false);
		assertNotNull(code_1.getCode());
		assertEquals("ABCD", code_1.getCode());
		assertFalse(code_1.isFinal());
		assertFalse(code_1.isPeriodic());
	}

	@Test
	public void testInspectionCodeINSPECTION_TYPEString() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCDE");
		assertNotNull(code_1.getCode());
		assertEquals("ABCDE", code_1.getCode());
		assertTrue(code_1.isFinal());
		assertFalse(code_1.isPeriodic());
	}

	@Test
	public void testGetInspectionType() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCDE");
		assertEquals(INSPECTION_TYPE.FINAL, code_1.getInspectionType());
	}

	@Test
	public void testIsActive() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCDE", "DESC", true, true);
		assertTrue(code_1.isActive());
	}

	@Test
	public void testIsBackout() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MF", "DESC", true, true);
		assertTrue(code_1.isBackout());
		code_1 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MP", "DESC", true, true);
		assertTrue(code_1.isBackout());
		code_1 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "OO", "DESC", true, true);
		assertFalse(code_1.isBackout());
	}
	
	@Test
	public void testIsEquipmentIncorrectlyAssigned() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "DESC", true, true);
		assertTrue(code_1.isEquipmentIncorrectlyAssigned());
		code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "MH", "DESC", true, true);
		assertFalse(code_1.isEquipmentIncorrectlyAssigned());
		code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "OO", "DESC", true, true);
		assertFalse(code_1.isEquipmentIncorrectlyAssigned());
	}

	@Test
	public void testIsEscalationActivityCode() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MY", "DESC", true, true);
		assertTrue(code_1.isEscalationActivityCode());
		code_1 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MH", "DESC", true, true);
		assertFalse(code_1.isEscalationActivityCode());
		code_1 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "OO", "DESC", true, true);
		assertFalse(code_1.isEscalationActivityCode());
	}
	
	
	@Test
	public void testCompareTo() {
		InspectionCode code_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, false);
		InspectionCode code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCD", "abcd description", false, false);
		assertEquals(0, code_1.compareTo(code_2));
		assertEquals(0, code_2.compareTo(code_1));
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "AB", "abcd description", false, false);
		assertTrue(0 < code_1.compareTo(code_2));
		assertTrue(0 > code_2.compareTo(code_1));
		code_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "ABCDE");
		assertTrue(0 > code_1.compareTo(code_2));
		assertTrue(0 < code_2.compareTo(code_1));
		assertTrue(0 > code_1.compareTo(null));
		assertTrue(0 > code_2.compareTo(null));
		
	}

}
