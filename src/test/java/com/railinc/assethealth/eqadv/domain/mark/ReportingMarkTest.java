package com.railinc.assethealth.eqadv.domain.mark;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.mark.ReportingMark;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanEqualsExcluding;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanHashCodeExcluding;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.MatcherAssert.assertThat;

public class ReportingMarkTest {

    @Test
    public void test_HasValidProperties() {
        assertThat(ReportingMark.class, allOf(
                hasValidBeanEqualsExcluding("description"),
                hasValidBeanHashCodeExcluding("description"),
                hasValidBeanConstructor(),
                hasValidGettersAndSetters()));
    }

}
