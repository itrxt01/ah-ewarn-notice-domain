package com.railinc.assethealth.eqadv.domain.pdf;


import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.UnsupportedAttachmentTypeException;
import com.railinc.assethealth.eqadv.domain.pdf.ATTACHMENT_TYPE;

public class ATTACHMENT_TYPETest {
	private static String[] extensions = {"bmp", "csv", "doc", "docx", "jpg", "pdf", "png", "ppt", "pptx", "txt", "xls", "xlsx"};
	private static String[] unknowns = {"exe", "js", "sql", "sh", "do", "jar", "vb", "vbs", "bat", "dll"};
	
	@Test
	public void testATTACHMENT_TYPE() {
		assertTrue(ATTACHMENT_TYPE.BMP != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.CSV != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.DOC != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.DOCX != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.JPG != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.PDF != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.PNG != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.PPT != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.PPTX != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.TXT != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.XLS != ATTACHMENT_TYPE.UNKNOWN);
		assertTrue(ATTACHMENT_TYPE.XLSX != ATTACHMENT_TYPE.UNKNOWN);
	}

	@Test
	public void testForExtension() {
		// Test all supported extensions
		for(String ext : extensions) {
			assertTrue(ATTACHMENT_TYPE.forExtension(ext) != ATTACHMENT_TYPE.UNKNOWN);
		}
		
		// Test all supported extensions UPPER case
		for(String ext : extensions) {
			assertTrue(ATTACHMENT_TYPE.forExtension(ext.toUpperCase()) != ATTACHMENT_TYPE.UNKNOWN);
		}
	}

	@Test
	public void testForExtensionUNKNOWN() {
		// Test all supported extensions
		for(String ext : unknowns) {
			assertTrue(ATTACHMENT_TYPE.forExtension(ext) == ATTACHMENT_TYPE.UNKNOWN);
		}
	}
	
	@Test
	public void testForExtensionBLANK() {
		assertTrue(ATTACHMENT_TYPE.forExtension("") == ATTACHMENT_TYPE.UNKNOWN);
	}

	@Test
	public void testForExtensionBLANKS() {
		assertTrue(ATTACHMENT_TYPE.forExtension("   ") == ATTACHMENT_TYPE.UNKNOWN);
	}

	@Test
	public void testForExtensionNULL() {
		assertTrue(ATTACHMENT_TYPE.forExtension(null) == ATTACHMENT_TYPE.UNKNOWN);
	}

	@Test
	public void testGetMimeType() throws UnsupportedAttachmentTypeException {
		// Test all supported extensions UPPER case
		for(String ext : extensions) {
			assertTrue(StringUtils.isNotBlank(ATTACHMENT_TYPE.forExtension(ext).getMimeType()));
		}
	}

	@Test(expected=UnsupportedAttachmentTypeException.class)
	public void testGetMimeTypeUNKNOWN() throws UnsupportedAttachmentTypeException {
		ATTACHMENT_TYPE.UNKNOWN.getMimeType();
	}

	@Test
	public void testForMimeType() throws UnsupportedAttachmentTypeException {
		// Test all supported extensions UPPER case
		for(ATTACHMENT_TYPE type : ATTACHMENT_TYPE.values()) {
			if(type != ATTACHMENT_TYPE.UNKNOWN) {
				assertTrue(ATTACHMENT_TYPE.forMimeType(type.getMimeType()) == type);				
			}
		}
		for(ATTACHMENT_TYPE type : ATTACHMENT_TYPE.values()) {
			if(type != ATTACHMENT_TYPE.UNKNOWN) {
				assertTrue(ATTACHMENT_TYPE.forMimeType(type.getMimeType().toUpperCase()) == type);
			}
		}
	}

	@Test
	public void testForMimeTypeUNKNOWN() throws UnsupportedAttachmentTypeException {
		// Test all supported extensions UPPER case
		for(String unknown : unknowns) {
			assertTrue(ATTACHMENT_TYPE.forMimeType(unknown) == ATTACHMENT_TYPE.UNKNOWN);
		}
	}
	
	
	@Test
	public void testGetMimeTypeUNKNOWNS() throws UnsupportedAttachmentTypeException {
		// Test all supported extensions UPPER case
		for(String ext : unknowns) {
			assertTrue(ATTACHMENT_TYPE.forMimeType(ext) == ATTACHMENT_TYPE.UNKNOWN);
		}
	}

	@Test
	public void testGetMimeTypeBlank() throws UnsupportedAttachmentTypeException {
		assertTrue(ATTACHMENT_TYPE.forMimeType("") == ATTACHMENT_TYPE.UNKNOWN);
	}

	@Test
	public void testGetMimeTypeBlanks() throws UnsupportedAttachmentTypeException {
		assertTrue(ATTACHMENT_TYPE.forMimeType("   ") == ATTACHMENT_TYPE.UNKNOWN);
	}

	@Test
	public void testGetMimeTypeNULL() throws UnsupportedAttachmentTypeException {
		assertTrue(ATTACHMENT_TYPE.forMimeType(null) == ATTACHMENT_TYPE.UNKNOWN);
	}
	
	
	@Test
	public void testGetMimeTypes() throws UnsupportedAttachmentTypeException {
		// Test all supported extensions UPPER case
		for(String ext : extensions) {
			assertTrue(ATTACHMENT_TYPE.forExtension(ext).getMimeTypes().length > 0);
		}
	}

	@Test(expected=UnsupportedAttachmentTypeException.class)
	public void testGetMimeTypesUNKNOWN() throws UnsupportedAttachmentTypeException {
		ATTACHMENT_TYPE.UNKNOWN.getMimeTypes();
	}
	
}
