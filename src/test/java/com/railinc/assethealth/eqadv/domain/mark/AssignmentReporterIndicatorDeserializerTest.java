package com.railinc.assethealth.eqadv.domain.mark;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class AssignmentReporterIndicatorDeserializerTest {

	@Test
	public void testAssignmentReporterIndicatorDeserializer() {
		AssignmentReporterIndicatorDeserializer des = new AssignmentReporterIndicatorDeserializer();
		assertNotNull(des);
	}

	@Test
	public void testAssignmentReporterIndicatorDeserializerClassOfASSIGNMENT_REPORTER_INDICATOR() {
		AssignmentReporterIndicatorDeserializer des = new AssignmentReporterIndicatorDeserializer(ASSIGNMENT_REPORTER_INDICATOR.class);
		assertNotNull(des);
	}

	@Test
	public void testMarshallUnmarshall_AAR() throws IOException {
		ASSIGNMENT_REPORTER_INDICATOR indicator = ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY;
		
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(indicator);
		System.out.println(JSON);

		SimpleModule module = new SimpleModule();
		module.addDeserializer(ASSIGNMENT_REPORTER_INDICATOR.class, new AssignmentReporterIndicatorDeserializer());
		objectMapper.registerModule(module);

		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY, unmarshalled);
	}

	@Test
	public void testMarshallUnmarshall_EQUIPMENT_OWNER() throws IOException {
		ASSIGNMENT_REPORTER_INDICATOR indicator = ASSIGNMENT_REPORTER_INDICATOR.EQUIPMENT_OWNER;
		
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(indicator);
		System.out.println(JSON);

		SimpleModule module = new SimpleModule();
		module.addDeserializer(ASSIGNMENT_REPORTER_INDICATOR.class, new AssignmentReporterIndicatorDeserializer());
		objectMapper.registerModule(module);

		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.EQUIPMENT_OWNER, unmarshalled);
	}

	@Test
	public void testMarshallUnmarshall_INTERNAL_ONLY() throws IOException {
		ASSIGNMENT_REPORTER_INDICATOR indicator = ASSIGNMENT_REPORTER_INDICATOR.INTERNAL_ONLY;
		
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(indicator);
		System.out.println(JSON);

		SimpleModule module = new SimpleModule();
		module.addDeserializer(ASSIGNMENT_REPORTER_INDICATOR.class, new AssignmentReporterIndicatorDeserializer());
		objectMapper.registerModule(module);

		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.INTERNAL_ONLY, unmarshalled);
	}
	
	@Test
	public void testMarshallUnmarshall_SPECIFIED() throws IOException {
		ASSIGNMENT_REPORTER_INDICATOR indicator = ASSIGNMENT_REPORTER_INDICATOR.SPECIFIED;
		
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(indicator);
		System.out.println(JSON);

		SimpleModule module = new SimpleModule();
		module.addDeserializer(ASSIGNMENT_REPORTER_INDICATOR.class, new AssignmentReporterIndicatorDeserializer());
		objectMapper.registerModule(module);

		ASSIGNMENT_REPORTER_INDICATOR unmarshalled = objectMapper.readValue(JSON, ASSIGNMENT_REPORTER_INDICATOR.class);
		assertEquals(ASSIGNMENT_REPORTER_INDICATOR.SPECIFIED, unmarshalled);
	}
}
