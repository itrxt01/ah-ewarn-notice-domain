package com.railinc.assethealth.eqadv.domain.equipment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.equipment.Equipment;
import com.railinc.assethealth.eqadv.domain.equipment.EquipmentSet;
import com.railinc.assethealth.eqadv.domain.exception.InvalidEquipmentIdException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;

public class EquipmentSetTest {

	@Test
	public void testEquipmentSet() {
		EquipmentSet<Equipment> set = new EquipmentSet<>();
		assertNotNull(set);
		assertTrue(set.isEmpty());
	}

	@Test(expected = Test.None.class)
	public void testEquipmentSetCollectionOfT() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_990L);
		List<Equipment> list = new ArrayList<>();
		list.add(eq1);
		list.add(eq2);
		list.add(eq3);
		set.addAll(list);
		assertEquals(3, set.size());
		Equipment eq4 = new Equipment("RAIL", 01L, 1_234_567_891L);
		set.add(eq4);
		assertEquals(4, set.size());
		set.replaceAll(list);
		assertEquals(3, set.size());
	}

	@Test(expected = Test.None.class)
	public void testContains() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_990L);
		List<Equipment> list = new ArrayList<>();
		list.add(eq1);
		list.add(eq2);
		list.add(eq3);
		set.addAll(list);
		assertTrue(set.contains(eq1));
		Equipment eq4 = new Equipment("RAIL", 01L, 1_234_567_891L);
		set.add(eq4);
		assertTrue(set.contains(eq4));
		set.replaceAll(list);
		assertTrue(set.contains(eq1));
		assertFalse(set.contains(eq4));
	}

	@Test(expected = Test.None.class)
	public void testEquipmentSetExclusivity() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_890L);  // Duplicate
		List<Equipment> list = new ArrayList<>();
		list.add(eq1);
		list.add(eq2);
		list.add(eq3);
		set.addAll(list);
		assertTrue(set.contains(eq1));
		assertEquals(2, set.size());
		Equipment eq4 = new Equipment("RAIL", 01L, 1_234_567_891L);
		set.add(eq4);
		assertTrue(set.contains(eq4));
		assertEquals(3, set.size());
	}

	@Test(expected = Test.None.class)
	public void testGetEquipmentList() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_891L); 
		List<Equipment> list = new ArrayList<>();
		list.add(eq1);
		list.add(eq2);
		list.add(eq3);
		int added = set.addAll(list);
		List<Equipment> returnedList = set.getEquipment();
		assertEquals(3, added);
		assertTrue(returnedList.contains(eq1));
		assertTrue(returnedList.contains(eq2));
		assertTrue(returnedList.contains(eq3));
		assertEquals(3, set.size());
	}

	@Test(expected = Test.None.class)
	public void testAddAllEmptyCollection() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set = new EquipmentSet<>();
		List<Equipment> list = new ArrayList<>();
		int added = set.addAll(list);
		assertEquals(0, added);
		assertEquals(0, set.size());
	}
	
	@Test(expected = Test.None.class)
	public void testGetEquipment() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_891L); 
		List<Equipment> list = new ArrayList<>();
		list.add(eq1);
		list.add(eq2);
		list.add(eq3);
		int added = set.addAll(list);
		assertEquals(3, added);
		Equipment eq = set.getEquipment("RAIL", 1);
		assertEquals("RAIL", eq.getMarkAsJustifiedString());
		assertEquals(Long.valueOf(1L), eq.getNumber());
		eq = set.getEquipment("BNSF", 1);
		assertNull(eq);
		eq = set.getEquipment("RAIL", 9_999_999_990L);
		assertNull(eq);
	}

	@Test(expected = Test.None.class)
	public void testEquipmentSetCollection_T() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set_1 = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_891L); 
		List<Equipment> list = new ArrayList<>();
		list.add(eq1);
		list.add(eq2);
		list.add(eq3);
		int added = set_1.addAll(list);		
		assertEquals(3, added);
		EquipmentSet<Equipment> set_2 = new EquipmentSet<>(list);
		Equipment eq = set_2.getEquipment("RAIL", 1);
		assertNotNull(eq);
		eq = set_2.getEquipment("RAIL", 2);
		assertNotNull(eq);
		eq = set_2.getEquipment("RAIL", 2);
		assertNotNull(eq);
	}

	@Test(expected = Test.None.class)
	public void testEquipmentSetEquipmentSet_T() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set_1 = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_891L); 
		List<Equipment> list = new ArrayList<>();
		list.add(eq1);
		list.add(eq2);
		list.add(eq3);
		int added = set_1.addAll(list);		
		assertEquals(3, added);
		EquipmentSet<Equipment> set_2 = new EquipmentSet<>(set_1);
		Equipment eq = set_2.getEquipment("RAIL", 1);
		assertNotNull(eq);
		eq = set_2.getEquipment("RAIL", 2);
		assertNotNull(eq);
		eq = set_2.getEquipment("RAIL", 2);
		assertNotNull(eq);
	}
	
	@Test(expected = Test.None.class)
	public void testEquipmentSetEquipmentSet_T_null() throws InvalidEquipmentIdException, InvalidRoadMarkException {
		EquipmentSet<Equipment> set_1 = new EquipmentSet<>();
		Equipment eq1 = new Equipment("RAIL", 01L, 1_234_567_890L);
		Equipment eq2 = new Equipment("RAIL", 02L, 9_999_999_999L);
		Equipment eq3 = new Equipment("RAIL", 01L, 1_234_567_891L); 
		List<Equipment> list = new ArrayList<>();
		list.add(eq1);
		list.add(eq2);
		list.add(eq3);
		int added = set_1.addAll(list);		
		assertEquals(3, added);
		EquipmentSet<Equipment> set_2 = new EquipmentSet<Equipment>((EquipmentSet<Equipment>)null);
		assertNotNull(set_2);
		assertTrue(set_2.isEmpty());
	}
	
}
