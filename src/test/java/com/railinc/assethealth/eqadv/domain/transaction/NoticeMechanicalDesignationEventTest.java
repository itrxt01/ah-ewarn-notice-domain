package com.railinc.assethealth.eqadv.domain.transaction;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.transaction.NoticeMechanicalDesignationEvent;

import static org.junit.Assert.assertEquals;

public class NoticeMechanicalDesignationEventTest extends NoticeEventTestHelper<NoticeMechanicalDesignationEvent> {

    private static final String MECHANICAL_DESIGNATION = "XM";

    @Test
    public void testNoticeMechanicalDesignationEvent() {
        NoticeMechanicalDesignationEvent noticeMechanicalDesignationEvent = getNoticeEvent();

        assertEquals(EVENT_ID, noticeMechanicalDesignationEvent.getEventId());
        assertEquals(NOTICE_ID, noticeMechanicalDesignationEvent.getNoticeId());
        assertEquals(MECHANICAL_DESIGNATION, noticeMechanicalDesignationEvent.getMechanicalDesignation());
    }

    @Override
    NoticeMechanicalDesignationEvent getNoticeEvent() {
        NoticeMechanicalDesignationEvent noticeMechanicalDesignationEvent = new NoticeMechanicalDesignationEvent();
        noticeMechanicalDesignationEvent.setEventId(EVENT_ID);
        noticeMechanicalDesignationEvent.setNoticeId(NOTICE_ID);
        noticeMechanicalDesignationEvent.setMechanicalDesignation(MECHANICAL_DESIGNATION);
        return noticeMechanicalDesignationEvent;
    }
}
