package com.railinc.assethealth.eqadv.domain.transaction;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.transaction.NoticeContentEvent;

import static org.junit.Assert.assertEquals;

public class NoticeContentEventTest extends NoticeEventTestHelper<NoticeContentEvent> {

    private static final String FILE_NAME = "fileName";
    private static final Integer FILE_SIZE = 5000;
    private static final byte[] FILE_CONTENT = new byte[]{'C', 'o', 'n', 't', 'e', 'n', 't'};
    private static final String FILE_TYPE = "pdf";
    private static final String MD5 = "a1b2c3d4e5f6";

    @Test
    public void testNoticeContentEvent() {
        NoticeContentEvent noticeContentEvent = getNoticeEvent();

        assertEquals(EVENT_ID, noticeContentEvent.getEventId());
        assertEquals(NOTICE_ID, noticeContentEvent.getNoticeId());
        assertEquals(FILE_NAME, noticeContentEvent.getFileName());
        assertEquals(FILE_SIZE, noticeContentEvent.getFileSize());
        assertEquals(FILE_CONTENT, noticeContentEvent.getFileContent());
        assertEquals(FILE_TYPE, noticeContentEvent.getFileType());
        assertEquals(MD5, noticeContentEvent.getMd5Checksum());
    }

    @Override
    NoticeContentEvent getNoticeEvent() {
        NoticeContentEvent noticeContentEvent = new NoticeContentEvent();
        noticeContentEvent.setEventId(EVENT_ID);
        noticeContentEvent.setNoticeId(NOTICE_ID);
        noticeContentEvent.setFileName(FILE_NAME);
        noticeContentEvent.setFileSize(FILE_SIZE);
        noticeContentEvent.setFileContent(FILE_CONTENT);
        noticeContentEvent.setFileType(FILE_TYPE);
        noticeContentEvent.setMd5Checksum(MD5);
        return noticeContentEvent;
    }
}
