package com.railinc.assethealth.eqadv.domain.transaction;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.transaction.NoticeSeverityEvent;

import static org.junit.Assert.assertEquals;

public class NoticeSeverityEventTest extends NoticeEventTestHelper<NoticeSeverityEvent> {

    private static final String SEVERITY_CODE = "A2";
    private static final Integer ESCALATION_PERIOD = 180;
    private static final String ESCALATION_UNIT = "Days";
    private static final String ESCALATION_SEV_CODE = "A1";
    private static final String ESCALATION_TYPE = "Q";
    private static final Integer ESCALATION_QTY = 454;

    @Test
    public void testNoticeSeverityEvent() {
        NoticeSeverityEvent noticeSeverityEvent = getNoticeEvent();

        assertEquals(EVENT_ID, noticeSeverityEvent.getEventId());
        assertEquals(NOTICE_ID, noticeSeverityEvent.getNoticeId());
        assertEquals(SEVERITY_CODE, noticeSeverityEvent.getSeverityCode());
        assertEquals(ESCALATION_PERIOD, noticeSeverityEvent.getEscalationPeriod());
        assertEquals(ESCALATION_UNIT, noticeSeverityEvent.getEscalationUnit());
        assertEquals(ESCALATION_SEV_CODE, noticeSeverityEvent.getEscalationSevCode());
        assertEquals(ESCALATION_TYPE, noticeSeverityEvent.getEscalationType());
        assertEquals(ESCALATION_QTY, noticeSeverityEvent.getEscalationQty());
    }

    @Override
    NoticeSeverityEvent getNoticeEvent() {
        NoticeSeverityEvent noticeSeverityEvent = new NoticeSeverityEvent();
        noticeSeverityEvent.setEventId(EVENT_ID);
        noticeSeverityEvent.setNoticeId(NOTICE_ID);
        noticeSeverityEvent.setSeverityCode(SEVERITY_CODE);
        noticeSeverityEvent.setEscalationPeriod(ESCALATION_PERIOD);
        noticeSeverityEvent.setEscalationUnit(ESCALATION_UNIT);
        noticeSeverityEvent.setEscalationSevCode(ESCALATION_SEV_CODE);
        noticeSeverityEvent.setEscalationType(ESCALATION_TYPE);
        noticeSeverityEvent.setEscalationQty(ESCALATION_QTY);
        return noticeSeverityEvent;
    }
}
