package com.railinc.assethealth.eqadv.domain.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.category.CategorySeverityLevel;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;

public class CategorySeverityLevelTest {

	
	@Test
	public void testCategorySeverityLevel() throws InvalidPeriodicTimeUnitException {
		CategorySeverityLevel severity = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ"));
		assertTrue(severity.getCategory().equals(new Category("AB")));
		assertTrue(severity.severityLevel.equals(new SeverityLevel("ZZ")));
		assertTrue(severity.getSeverityLevel().equals(new SeverityLevel("ZZ")));
		severity.setCategory(new Category("BB"));
		assertTrue(severity.getCategory().getCode().equals("BB"));
		severity.setSeverityLevel(new SeverityLevel("yy", "XX", 180, "days"));
		assertTrue(severity.getSeverityLevel().getSeverityLevelCode().equals("yy"));
	}
	
	@Test
	public void testEqualsObject() throws InvalidPeriodicTimeUnitException {
		CategorySeverityLevel lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ"));
		CategorySeverityLevel rhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ"));
		assertTrue(lhs.equals(rhs));
		rhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 180, "days"));
		assertFalse(lhs.equals(rhs));
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("yy", "YY", 180, "days"));
		assertFalse(lhs.equals(rhs));
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "XX", 180, "days"));
		assertFalse(lhs.equals(rhs));
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 1, "days"));
		assertFalse(lhs.equals(rhs));
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 180, "months"));
		assertFalse(lhs.equals(rhs));
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 180, "days"));
		assertTrue(lhs.equals(rhs));
	}

	@Test
	public void testToString() throws InvalidPeriodicTimeUnitException {
		CategorySeverityLevel lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ"));
		assertTrue(lhs.toString().contains("AB"));
		assertTrue(lhs.toString().contains("ZZ"));
		lhs = new CategorySeverityLevel(new Category("CC"), new SeverityLevel("YY", "DD", 180, "days"));
		assertTrue(lhs.toString().contains("CC"));
		assertTrue(lhs.toString().contains("DD"));
	}

	@Test
	public void testHashcode() throws InvalidPeriodicTimeUnitException {
		CategorySeverityLevel lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ"));
		CategorySeverityLevel rhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ"));
		assertEquals(lhs.hashCode(), rhs.hashCode());
		rhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 180, "days"));
		assertNotEquals(lhs.hashCode(), rhs.hashCode());
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 0, "days"));
		assertNotEquals(lhs.hashCode(), rhs.hashCode());
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 180, "months"));
		assertNotEquals(lhs.hashCode(), rhs.hashCode());
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 180, "years"));
		assertNotEquals(lhs.hashCode(), rhs.hashCode());
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "ZZ", 180, "days"));
		assertNotEquals(lhs.hashCode(), rhs.hashCode());
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("ZZ", "YY", 180, "days"));
		assertEquals(lhs.hashCode(), rhs.hashCode());
		rhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("YY", "ZZ", 180, "days"));
		assertNotEquals(lhs.hashCode(), rhs.hashCode());
		lhs = new CategorySeverityLevel(new Category("AB"), new SeverityLevel("YY", "ZZ", 180, "days"));
		assertEquals(lhs.hashCode(), rhs.hashCode());
	}

	@Test
	public void testMarshall() throws IOException {
		CategorySeverityLevel original = new CategorySeverityLevel(new Category("AA"), new SeverityLevel("A1", "XX", 6, "months"));

		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writer().writeValueAsString(original);
		System.out.println(JSON);
		assertEquals("{\"category\":{\"code\":\"AA\",\"description\":\"\"},\"severityLevel\":{\"severityLevel\":\"A1\",\"qty\":0,\"description\":null,\"escalationLevel\":\"XX\",\"escalationPeriod\":6,\"escalationPeriodUnit\":\"Months\",\"escalationType\":\"D\",\"escalationQty\":0}}", JSON);
	}
}
