package com.railinc.assethealth.eqadv.domain.severity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.equipment.Assignment;
import com.railinc.assethealth.eqadv.domain.equipment.Equipment;
import com.railinc.assethealth.eqadv.domain.exception.InvalidAssignedEquipmentException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidEquipmentIdException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;
import com.railinc.assethealth.eqadv.domain.exception.SeverityLevelEscalationException;
import com.railinc.assethealth.eqadv.domain.notice.Notice;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevelDeserializer;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevels;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevelsDeserializer;

public class SeverityLevelsTest {

	@Test
	public void testSeverityLevels() {
		SeverityLevels severityLevels = new SeverityLevels();
		assertFalse(severityLevels.isValid());
		SeverityLevel level1 = new SeverityLevel("XX");
		severityLevels.addSeverityLevel(level1);
		assertTrue(severityLevels.isValid());
	}

	@Test
	public void testSeverityLevelsCtorCollectionOfSeverityLevel() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationLevel().equals(new SeverityLevel("Z1", "ZZ", 1, "days")));
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationLevelCode().equals("Z1"));
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationPeriod().getTimePeriod() == 83);
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationPeriod().getTimeUnit().units.equals("Days"));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationLevel().equals(new SeverityLevel("ZZ")));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationLevelCode().equals("ZZ"));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationPeriod().getTimePeriod() == 1);
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationPeriod().getTimeUnit().units.equals("Days"));
	}

	@Test
	public void testSeverityCtorCollectionOfSeverityLevel() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels();
		severityLevels.setSeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationLevel().equals(new SeverityLevel("Z1", "ZZ", 1, "days")));
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationLevelCode().equals("Z1"));
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationPeriod().getTimePeriod() == 83);
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationPeriod().getTimeUnit().units.equals("Days"));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationLevel().equals(new SeverityLevel("ZZ")));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationLevelCode().equals("ZZ"));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationPeriod().getTimePeriod() == 1);
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationPeriod().getTimeUnit().units.equals("Days"));
	}

	@Test
	public void testSeverityLevelsArraySeverityLevel() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		SeverityLevel[] levels = new SeverityLevel[3];
		levels[0] = new SeverityLevel("ABC");
		levels[1] = new SeverityLevel("ZS1", "ABC", 1, "days");
		levels[2] = new SeverityLevel("ZS2", "ZS1", 83, "days");
		SeverityLevels severityLevels = new SeverityLevels();
		severityLevels.setSeverityLevels(levels);
		
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertTrue(severityLevels.getSeverityLevel("ZS2").getEscalationLevel().equals(new SeverityLevel("ZS1", "ABC", 1, "days")));
		assertTrue(severityLevels.getSeverityLevel("ZS2").getEscalationLevelCode().equals("ZS1"));
		assertTrue(severityLevels.getSeverityLevel("ZS2").getEscalationPeriod().getTimePeriod() == 83);
		assertTrue(severityLevels.getSeverityLevel("ZS2").getEscalationPeriod().getTimeUnit().units.equals("Days"));
		assertTrue(severityLevels.getSeverityLevel("ZS1").getEscalationLevel().equals(new SeverityLevel("ABC")));
		assertTrue(severityLevels.getSeverityLevel("ZS1").getEscalationLevelCode().equals("ABC"));
		assertTrue(severityLevels.getSeverityLevel("ZS1").getEscalationPeriod().getTimePeriod() == 1);
		assertTrue(severityLevels.getSeverityLevel("ZS1").getEscalationPeriod().getTimeUnit().units.equals("Days"));
	}

	@Test
	public void testGetEscalationLevelByCode() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationLevel().equals(new SeverityLevel("Z1", "ZZ", 1, "days")));
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationLevelCode().equals("Z1"));
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationPeriod().getTimePeriod() == 83);
		assertTrue(severityLevels.getSeverityLevel("Z2").getEscalationPeriod().getTimeUnit().units.equals("Days"));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationLevel().equals(new SeverityLevel("ZZ")));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationLevelCode().equals("ZZ"));
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationPeriod().getTimePeriod() == 1);
		assertTrue(severityLevels.getSeverityLevel("Z1").getEscalationPeriod().getTimeUnit().units.equals("Days"));
	}

	@Test(expected=Test.None.class)
	public void testGetEscalationLevelNOT_FOUND() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertNull(severityLevels.getSeverityLevel("XX"));
	}

	@Test(expected=Test.None.class)
	public void testGetEscalationLevelNULL() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertNull(severityLevels.getSeverityLevel(null));
	}
	
	@Test(expected=Test.None.class)
	public void testGetEscalationLevelEMPTY() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertNull(severityLevels.getSeverityLevel(""));
	}
	
	@Test(expected=Test.None.class)
	public void testGetEscalationLevelBLANK() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertNull(severityLevels.getSeverityLevel("   "));
	}
	
	@Test
	public void testGetEscalationLevelByLevel() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertTrue(severityLevels.getEscalationLevel(new SeverityLevel("Z2", "Z1", 83, "days")).equals(new SeverityLevel("Z1", "ZZ", 1, "days")));
		assertTrue(severityLevels.getEscalationLevel(new SeverityLevel("Z2", "Z1", 83, "days")).getSeverityCode().getSeverityCode().equals("Z1"));
		assertTrue(severityLevels.getEscalationLevel(new SeverityLevel("Z2", "Z1", 83, "days")).getEscalationPeriod().getTimePeriod() == 1);
		assertTrue(severityLevels.getEscalationLevel(new SeverityLevel("Z2", "Z1", 83, "days")).getEscalationPeriod().getTimeUnit().units.equals("Days"));
		assertTrue(severityLevels.getEscalationLevel(new SeverityLevel("Z1", "ZZ", 1, "days")).equals(new SeverityLevel("ZZ")));
		assertTrue(severityLevels.getEscalationLevel(new SeverityLevel("Z1", "ZZ", 1, "days")).getSeverityCode().getSeverityCode().equals("ZZ"));
		assertTrue(severityLevels.getEscalationLevel(new SeverityLevel("Z1", "ZZ", 1, "days")).getEscalationPeriod() == null);
		assertTrue(severityLevels.getEscalationLevel(new SeverityLevel("Z1", "ZZ", 1, "days")).getEscalationPeriod() == null);
	}

	@Test(expected=Test.None.class)
	public void testGetEscalationLevelByLevelNOT_FOUND() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertNull(severityLevels.getEscalationLevel(new SeverityLevel("XX", "Z1", 83, "days")));
	}

	@Test(expected=Test.None.class)
	public void testGetEscalationLevelbyLevelNULL() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertNull(severityLevels.getEscalationLevel((SeverityLevel)null));
	}
	
	@Test(expected=Test.None.class)
	public void testGetEscalationLevelByLevelEMPTY() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertNull(severityLevels.getEscalationLevel((SeverityLevel)(new SeverityLevel(""))));
	}
	
	@Test(expected=Test.None.class)
	public void testGetEscalationLevelByLevelBLANK() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		assertNull(severityLevels.getEscalationLevel((SeverityLevel)(new SeverityLevel("    "))));
	}

	@Test
	public void testGetEscalationLevelForAssignedEquipment() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException, InvalidAssignedEquipmentException, InvalidEquipmentIdException, InvalidRoadMarkException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		
		Assignment eq = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("Z2").build();
		assertTrue(severityLevels.getEscalationLevel(eq).equals(new SeverityLevel("Z1", "ZZ", 1, "days")));
		Assignment eq2 = new Assignment.Builder(new Equipment("rail", 2L)).onNotice(1).withSeverityCode("Z1").build();
		assertTrue(severityLevels.getEscalationLevel(eq2).equals(new SeverityLevel("ZZ")));
	}

	@Test(expected=Test.None.class)
	public void testGetEscalationLevelForAssignedEquipmentDOESNT_ESCALATE() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException, InvalidAssignedEquipmentException, InvalidEquipmentIdException, InvalidRoadMarkException {
		Notice notice = new Notice();
		notice.setNumber(5);
		notice.setId(1L);
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 1, "days")); 
		levels.add(new SeverityLevel("Z2", "Z1", 83, "days"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		notice.setSeverityLevels(severityLevels);
		
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		
		Assignment eq3 = new Assignment.Builder(new Equipment("rail", 3L)).onNotice(notice).withSeverityCode("XX").build();
		assertNull(severityLevels.getEscalationLevel(eq3));
	}

	
	@Test
	public void testGetEscalationDateForAssignedEquipment() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException, InvalidAssignedEquipmentException, InvalidEquipmentIdException, InvalidRoadMarkException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 101, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 200, "years"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		
		Assignment eq = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("Z2").assignedOn(new Date()).build();
		assertTrue(DateUtils.isSameDay(severityLevels.getEscalationDate(eq), DateUtils.addDays(new Date(), 200 * 365)));  // Note: EW year is 365 days
		Assignment eq2 = new Assignment.Builder(new Equipment("rail", 2L)).onNotice(1).withSeverityCode("Z1").assignedOn(new Date()).build();
		assertTrue(DateUtils.isSameDay(severityLevels.getEscalationDate(eq2), DateUtils.addDays(new Date(), 101)));
	}

	@Test(expected=Test.None.class)
	public void testGetEscalationDateForAssignedEquipmentDOESNT_ESCALATE() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException, InvalidAssignedEquipmentException, InvalidEquipmentIdException, InvalidRoadMarkException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 101, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 200, "years"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		
		Assignment eq = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("ZZ").assignedOn(new Date()).build();
		assertNull(severityLevels.getEscalationDate(eq));
	}	

	@Test
	public void testIsTimeToEscalateAssignedEquipment() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException, InvalidAssignedEquipmentException, InvalidEquipmentIdException, InvalidRoadMarkException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 101, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 200, "years"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		
		Assignment eq = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("Z2")
				.assignedOn(DateUtils.addDays(new Date(), (-200 * 365) -1)).build();
		assertTrue(severityLevels.isTimeToEscalate(eq));
		Assignment eq2 = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("Z1")
				.assignedOn(DateUtils.addDays(new Date(), -102)).build();
		assertTrue(severityLevels.isTimeToEscalate(eq2));
		Assignment eq3 = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("Z2")
				.assignedOn(DateUtils.addDays(new Date(), (-200 * 365) + 1)).build();
		assertFalse(severityLevels.isTimeToEscalate(eq3));
		Assignment eq4 = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("Z1")
				.assignedOn(DateUtils.addDays(new Date(), -100)).build();
		assertFalse(severityLevels.isTimeToEscalate(eq4));
	}

	@Test(expected=Test.None.class)
	public void testIsTimeToEscalateAssignedEquipmentNEVER_ESCALATEs() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException, InvalidAssignedEquipmentException, InvalidEquipmentIdException, InvalidRoadMarkException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 101, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 200, "years"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		
		Assignment eq = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("ZZ")
				.assignedOn(DateUtils.addYears(new Date(), -1000)).build();
		assertFalse(severityLevels.isTimeToEscalate(eq));
	}	
	
	@Test(expected=Test.None.class)
	public void testIsTimeToEscalateAssignedEquipmentNOT_FOUND() throws InvalidPeriodicTimeUnitException, SeverityLevelEscalationException, InvalidAssignedEquipmentException, InvalidEquipmentIdException, InvalidRoadMarkException {
		List<SeverityLevel> levels = new ArrayList<>();
		levels.add(new SeverityLevel("ZZ"));
		levels.add(new SeverityLevel("Z1", "ZZ", 101, "days"));
		levels.add(new SeverityLevel("Z2", "Z1", 200, "years"));
		SeverityLevels severityLevels = new SeverityLevels(levels);
		assertTrue(severityLevels.isValid());
		assertTrue(3 == severityLevels.getSeverityLevels().size());
		
		Assignment eq = new Assignment.Builder(new Equipment("rail", 1L)).onNotice(1).withSeverityCode("YY").assignedOn(new Date()).build();
		assertNull(severityLevels.getEscalationDate(eq));
	}	

	@Test
	public void testMarshall() throws IOException, SeverityLevelEscalationException {
		List<SeverityLevel> levels = new ArrayList<>();
		SeverityLevel level1 = new SeverityLevel("ZZ");
		SeverityLevel level2 = new SeverityLevel("Z1", "ZZ", 101, "days");
		SeverityLevel level3 = new SeverityLevel("Z2", "Z1", 200, "years");
		
		levels.add(level1);
		levels.add(level2);
		levels.add(level3);
		SeverityLevels severityLevels = new SeverityLevels(levels);

		ObjectMapper objectMapper = new ObjectMapper();
		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(severityLevels);
		System.out.println(JSON);
		
		assertTrue(StringUtils.isNotBlank(JSON));
		assertTrue(JSON.contains("\"severityLevel\" : \"ZZ\""));
		assertTrue(JSON.contains("\"severityLevel\" : \"Z1\""));
		assertTrue(JSON.contains("\"severityLevel\" : \"Z2\""));
		assertTrue(JSON.contains("\"escalationLevel\" : \"ZZ\""));
		assertTrue(JSON.contains("\"escalationLevel\" : \"Z1\""));
		assertTrue(JSON.contains("\"escalationPeriod\" : 101"));
		assertTrue(JSON.contains("\"escalationPeriod\" : 200"));
		assertTrue(JSON.contains("\"escalationPeriodUnit\" : \"Days\""));
		assertTrue(JSON.contains("\"escalationPeriodUnit\" : \"Years\""));
	}		

	@Test
	public void testMarshallUnmarshall() throws IOException, SeverityLevelEscalationException {
		SeverityLevel level1 = new SeverityLevel("ZZ");
		SeverityLevel level2 = new SeverityLevel(new SeverityCode("Z1"), level1, new TimePeriod(101, "days"));
		level2.setEscalationType("D");
		SeverityLevel level3 = new SeverityLevel(new SeverityCode("Z2"), level2, new TimePeriod(200, "years"));
		level3.setEscalationType("D");
		String JSON = "[ {\n" + 
				"    \"severityLevel\" : \"ZZ\",\n" + 
				"    \"qty\" : 0,\n" + 
				"    \"description\" : null\n" + 
				"  }, {\n" + 
				"    \"severityLevel\" : \"Z1\",\n" + 
				"    \"qty\" : 0,\n" + 
				"    \"description\" : null,\n" + 
				"    \"escalationLevel\" : \"ZZ\",\n" + 
				"    \"escalationPeriod\" : 101,\n" + 
				"    \"escalationPeriodUnit\" : \"Days\",\n" + 
				"    \"escalationType\" : \"D\"\n" + 
				"  }, {\n" + 
				"    \"severityLevel\" : \"Z2\",\n" + 
				"    \"qty\" : 0,\n" + 
				"    \"description\" : null,\n" + 
				"    \"escalationLevel\" : \"Z1\",\n" + 
				"    \"escalationPeriod\" : 200,\n" + 
				"    \"escalationPeriodUnit\" : \"Years\",\n" + 
				"    \"escalationType\" : \"D\"\n" + 
				"  } ]\n" + 
				"";
		ObjectMapper objectMapper = new ObjectMapper();
		
		SimpleModule module = new SimpleModule();
		module.addDeserializer(SeverityLevel.class, new SeverityLevelDeserializer());
		module.addDeserializer(SeverityLevels.class, new SeverityLevelsDeserializer());
		objectMapper.registerModule(module);
		
		SeverityLevels result = objectMapper.readValue(JSON, SeverityLevels.class);
		assertTrue(result.isValid());
		assertTrue(3 == result.getSeverityLevels().size());
		assertTrue(result.getSeverityLevel("ZZ").equals(level1));
		assertTrue(result.getSeverityLevel("Z1").equals(level2));
		assertTrue(result.getSeverityLevel("Z2").equals(level3));
		assertFalse(result.getSeverityLevel("ZZ").isEscalates());
		assertTrue(result.getSeverityLevel("Z1").isEscalates());
		assertTrue(result.getSeverityLevel("Z2").isEscalates());
		assertFalse(result.getSeverityLevel("Z1").isTimeToEscalate(new Date()));
		assertFalse(result.getSeverityLevel("Z2").isTimeToEscalate(new Date()));
		assertEquals(result.getSeverityLevel("Z1").getEscalationLevel(), level1);
		assertEquals(result.getSeverityLevel("Z2").getEscalationLevel(), level2);
		assertEquals(101, result.getSeverityLevel("Z1").getEscalationPeriodValue());
		assertEquals(200, result.getSeverityLevel("Z2").getEscalationPeriodValue());
		assertEquals("Days", result.getSeverityLevel("Z1").getEscalationPeriodUnit());
		assertEquals("Years", result.getSeverityLevel("Z2").getEscalationPeriodUnit());
	}		
}
