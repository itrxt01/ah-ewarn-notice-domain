package com.railinc.assethealth.eqadv.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.TimePeriodDeserializer;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;

public class TimePeriodDeserializerTest {

	@Test
	public void testTimePeriodDeserializer() {
		TimePeriodDeserializer des = new TimePeriodDeserializer();
		assertNotNull(des);
	}

	@Test
	public void testTimePeriodDeserializerClassOfTimePeriod() {
		TimePeriodDeserializer des = new TimePeriodDeserializer(TimePeriod.class);
		assertNotNull(des);

	}

	@Ignore
	public void testMarshallUnmarshall() throws IOException, InvalidRoadMarkException {
		TimePeriod timePeriod = new TimePeriod(5, PERIODIC_TIME_UNIT.DAYS);
		
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(timePeriod);
		System.out.println(JSON);

		SimpleModule module = new SimpleModule();
		module.addDeserializer(TimePeriod.class, new TimePeriodDeserializer(TimePeriod.class));
		objectMapper.registerModule(module);

		TimePeriod unmarshalled = objectMapper.readValue(JSON, TimePeriod.class);
		assertEquals(5, unmarshalled.getTimePeriod());
		assertEquals(PERIODIC_TIME_UNIT.DAYS, unmarshalled.getTimeUnit());
	}
}
