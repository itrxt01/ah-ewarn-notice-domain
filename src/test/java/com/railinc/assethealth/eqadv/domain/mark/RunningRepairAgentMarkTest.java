package com.railinc.assethealth.eqadv.domain.mark;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;

public class RunningRepairAgentMarkTest {
	
	@Test
	public void validRepairShopMarks() {
		RunningRepairAgentMark mark = new RunningRepairAgentMark("a222");
		assertNotNull(mark);
		assertFalse(mark.isEquipmentOwningMark());
		assertEquals("A222", mark.getValueAsString());
		assertEquals("A222", mark.getValueAsJustifiedString());
		mark = new RunningRepairAgentMark("A123");
		assertNotNull(mark);
		assertFalse(mark.isEquipmentOwningMark());
		assertEquals("A123", mark.getValueAsString());
		assertEquals("A123", mark.getValueAsJustifiedString());
		mark = new RunningRepairAgentMark("a000");
		assertNotNull(mark);
		assertFalse(mark.isEquipmentOwningMark());
		assertEquals("A000", mark.getValueAsString());
		assertEquals("A000", mark.getValueAsJustifiedString());
	}
	
	@Test
	public void validMarkParent() {
		Mark parent = new RoadMark("BNSF");
		assertNotNull(parent);
		assertTrue(parent.isEquipmentOwningMark());
		assertEquals("BNSF", parent.getValueAsString());
		RunningRepairAgentMark mark = new RunningRepairAgentMark("A222", parent);
		assertNotNull(mark);
		assertFalse(mark.isEquipmentOwningMark());
		assertTrue(mark.isParent(parent));
	}

	

	@Test
	public void testCopyConstructor() {
		RunningRepairAgentMark mark = new RunningRepairAgentMark("A123");
		RunningRepairAgentMark mark2 = new RunningRepairAgentMark(mark);
		assertNotNull(mark2);
		assertTrue(mark2.equals(mark) && mark.equals(mark2));
	}
	
	
	@Test(expected=Test.None.class)
	public void inValidMarks_1() throws InvalidRoadMarkException {
		Mark mark = new RunningRepairAgentMark("123A");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_2() throws InvalidRoadMarkException {
		Mark mark = new RunningRepairAgentMark("C123");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_7() throws InvalidRoadMarkException {
		Mark mark = new RunningRepairAgentMark("A1%3");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_8() throws InvalidRoadMarkException {
		Mark mark = new RunningRepairAgentMark("A13345");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_3() throws InvalidRoadMarkException {
		Mark mark = new RunningRepairAgentMark("    ");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_4() throws InvalidRoadMarkException {
		Mark mark = new RunningRepairAgentMark("");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_5() throws InvalidRoadMarkException {
		Mark mark = new RunningRepairAgentMark((String)null);
		assertFalse(mark.isValid());
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_6() throws InvalidRoadMarkException {
		Mark mark = new RunningRepairAgentMark((Mark)null);
		assertFalse(mark.isValid());
	}
	
	@Test
	public void testEquals() throws InvalidRoadMarkException {
		RunningRepairAgentMark mark = new RunningRepairAgentMark("A234");
		RunningRepairAgentMark mark2 = new RunningRepairAgentMark("a234");
		assertTrue(mark.equals(mark2));
		assertTrue(mark2.equals(mark));
		mark2 = new RunningRepairAgentMark("A234");
		assertTrue(mark.equals(mark2));
		assertTrue(mark2.equals(mark));
		mark2 = new RunningRepairAgentMark("a000");
		assertFalse(mark.equals(mark2));
		assertFalse(mark2.equals(mark));
		assertFalse(mark.equals("a234"));
		assertFalse(mark.equals(null));
	}

	@Test
	public void testHashcode() throws InvalidRoadMarkException {
		RunningRepairAgentMark mark = new RunningRepairAgentMark("a234");
		RunningRepairAgentMark mark2 = new RunningRepairAgentMark("a234");
		assertTrue(mark.hashCode() == mark2.hashCode());
		mark = new RunningRepairAgentMark("a009");
		mark2 = new RunningRepairAgentMark("A009");
		assertTrue(mark.hashCode() == mark2.hashCode());
		mark = new RunningRepairAgentMark("a009");
		mark2 = new RunningRepairAgentMark("A000");
		assertTrue(mark.hashCode() != mark2.hashCode());
	}
	
	@Test
	public void testToString() throws InvalidRoadMarkException {
		RunningRepairAgentMark mark = new RunningRepairAgentMark("a000");
		assertTrue(StringUtils.isNotBlank(mark.toString()));
		assertTrue(mark.toString().contains("A000"));
	}
}
