package com.railinc.assethealth.eqadv.domain.pdf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.notice.Notice;
import com.railinc.assethealth.eqadv.domain.pdf.ATTACHMENT_TYPE;
import com.railinc.assethealth.eqadv.domain.pdf.NoticeFile;
import com.railinc.assethealth.eqadv.domain.pdf.PDFValidator;
import com.railinc.assethealth.eqadv.domain.status.NOTICE_STATUS;

public class NoticeFileTest {
	
	private static final byte[] VALID_PDF = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x33, 0x25, 0x25, 0x45, 0x4f, 0x46};  // "%PDF-1.3"
	private static final byte[] INVALID_PDF = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x1E, 0x25, 0x25, 0x45, 0x4f, 0x46};  // "Invalid"

	private Notice notice;
	private NoticeFile file;
	
	@Before
	public void setup() throws IOException {
		notice = new Notice();
		initializeNotice(notice);
		file = new NoticeFile();
		initializeFile(notice, file);
	}

	private void initializeNotice(Notice notice) throws IOException {
		notice.setNumber(1000);
		notice.setId(3L);
		notice.setCategory(new Category("AB"));
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		notice.setSupplementNumber(1);
		file = new NoticeFile();
		initializeFile(notice, file);
	}

	private void initializeFile(Notice notice, NoticeFile file) throws IOException {
		file.setNotice(notice);
		file.setFileName("fileName");
		file.setFileContent("fileContent".getBytes());
		file.setFileExtension("txt");
		file.setFileSize("fileContent".getBytes().length);		
		file.setMd5("fileMD5CheckSum");
	}
	
	@Test(expected=Test.None.class)
	public void testHashCode() throws IOException {
		Notice notice_2 = new Notice();
		this.initializeNotice(notice_2);
		
		NoticeFile file_2 = new NoticeFile();
		this.initializeFile(notice_2, file_2);
		assertEquals(file.hashCode(), file_2.hashCode());
		assertEquals(file_2.hashCode(), file.hashCode());
		
		file_2.setFileExtension("pdf");
		assertNotEquals(file.hashCode(), file_2.hashCode());
		assertNotEquals(file_2.hashCode(), file.hashCode());
	}

	@Test
	public void testGetNoticeName() {
		assertEquals("AB-1000 Supplement 01", file.getNoticeName());
	}

	@Test
	public void testGetNextNoticeName() {
		assertEquals("AB-1000 Supplement 02", file.getNextNoticeName());
	}

	@Test
	public void testGetStatus() {
		assertEquals(NOTICE_STATUS.PUBLISHED.statusName, file.getStatus());
	}

	@Test
	public void testGetNoticeNumber() {
		assertEquals(Integer.valueOf(1000), file.getNoticeNumber());
	}

	@Test
	public void testGetFileType() {
		assertTrue(file.isValid());
		assertEquals(ATTACHMENT_TYPE.TXT, file.getFileType());
		file.setFileExtension("jpg");
		assertEquals(ATTACHMENT_TYPE.JPG, file.getFileType());
		file.setFileExtension("");
		assertEquals(ATTACHMENT_TYPE.UNKNOWN, file.getFileType());
		file.setFileExtension("fly");
		assertEquals(ATTACHMENT_TYPE.UNKNOWN, file.getFileType());
	}

	@Test
	public void testIsSameMD5Checksum() {
		NoticeFile temp = new NoticeFile(file);
		assertTrue(file.isSameMD5Checksum(temp));
		assertTrue(temp.isSameMD5Checksum(file));
		assertFalse(file.isSameMD5Checksum(null));
		temp.setMd5("SOME LONG STRING");
		assertFalse(temp.isSameMD5Checksum(null));
		file.setMd5("SOME OTHER VALUE");
		assertFalse(file.isSameMD5Checksum(temp));
		assertFalse(temp.isSameMD5Checksum(file));
	}

	@Test
	public void testIsEquivalent() {
		NoticeFile temp = new NoticeFile(file);
		assertTrue(file.isEquivalent(temp));
		assertTrue(temp.isEquivalent(file));
		temp.setFileName("");
		assertFalse(file.isEquivalent(temp));
		assertFalse(temp.isEquivalent(file));
		temp.setFileName(file.getFileName());
		temp.setMd5("LONG STRING");
		assertFalse(file.isEquivalent(temp));
		assertFalse(temp.isEquivalent(file));		
		temp.setMd5(file.getMd5());
		assertTrue(file.isEquivalent(temp));
		assertTrue(temp.isEquivalent(file));		
	}

	@Test(expected=Test.None.class)
	public void testIsValid() throws IOException {
		assertTrue(file.isValid());
		file.setFileName(null);
		assertFalse(file.isValid());
		file.setFileName("");
		assertFalse(file.isValid());
		file.setFileName("fileName");
		file.setFileContent(new byte[] {});
		assertFalse(file.isValid());
		file.setFileContent(null);
		assertFalse(file.isValid());
		file.setFileContent("fileContents".getBytes());
		file.setFileExtension("fly");
		assertFalse(file.isValid());
		file.setFileExtension("");
		assertFalse(file.isValid());
		file.setFileExtension(null);
		assertFalse(file.isValid());
		file.setFileExtension("txt");
		file.setFileSize(120);		
		assertFalse(file.isValid());
		file.setFileSize(0);		
		assertFalse(file.isValid());
		file.setFileSize(-5);		
		assertFalse(file.isValid());
		file.setFileSize("fileContents".getBytes().length);		
		assertTrue(file.isValid());
		file.setFileExtension("log");
		assertFalse(file.isValid());
		file.setFileExtension("pdf");
		file.setFileContent(VALID_PDF);
		file.setFileSize(VALID_PDF.length);
		assertTrue(file.isValid());
	}

	@Test(expected=Test.None.class)
	public void testIsValidPDF() {
		assertFalse(PDFValidator.isValidPDF(file.getFileContent()));
		notice.setNumber(1000);
		notice.setId(3L);
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		notice.setSupplementNumber(1);
		file = new NoticeFile();
		file.setNotice(notice);
		file.setFileName("fileName");
		file.setFileContent(VALID_PDF);
		file.setFileExtension("pdf");
		file.setFileSize(VALID_PDF.length);		
		file.setMd5("fileMD5CheckSum");
		assertTrue(PDFValidator.isValidPDF(file.getFileContent()));
		file.setFileContent(INVALID_PDF);
		file.setFileExtension("pdf");
		file.setFileSize(INVALID_PDF.length);		
		file.setMd5("fileMD5CheckSum");
		assertFalse(PDFValidator.isValidPDF(file.getFileContent()));	
	}

	@Test(expected=Test.None.class)
	public void testIsSupportedFileType() {
		assertFalse(PDFValidator.isValidPDF(file.getFileContent()));
		notice.setNumber(1000);
		notice.setId(3L);
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		notice.setSupplementNumber(1);
		file = new NoticeFile();
		file.setNotice(notice);
		file.setFileName("fileName");
		file.setFileContent(VALID_PDF);
		file.setFileExtension("pdf");
		file.setFileSize(VALID_PDF.length);		
		file.setMd5("fileMD5CheckSum");
		assertTrue(PDFValidator.isValidPDF(file.getFileContent()));
		assertTrue(file.isSupportedFileType());
		file.setFileExtension("fly");
		assertFalse(file.isSupportedFileType());
	}
}
