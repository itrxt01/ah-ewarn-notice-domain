package com.railinc.assethealth.eqadv.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.EWDomainAbstract;
public class EWDomainAbstractTest {
	
	public class TestClass extends EWDomainAbstract {
		private static final long serialVersionUID = 1L;

		// Just a concrete class for testing abstract parent
		public TestClass() {}
		public TestClass(EWDomainAbstract e){
			super(e);
		}		
	}
	
	
	@Test
	public void testIsValidDateRange() throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		Date date = simpleDateFormat.parse("1995");
		assertFalse(EWDomainAbstract.isValidDateRange(date));
		
		date = simpleDateFormat.parse("2001");
		assertTrue(EWDomainAbstract.isValidDateRange(date));
		
		assertFalse(EWDomainAbstract.isValidDateRange(null));
	}
	
	@Test
	public void testPadNumberWithLeadingZeros() {
		assertEquals("0001", EWDomainAbstract.padNumberWithLeadingZeros(1, 4));
		assertEquals("0012", EWDomainAbstract.padNumberWithLeadingZeros(12, 4));
		assertEquals("0123", EWDomainAbstract.padNumberWithLeadingZeros(123, 4));
		assertEquals("1234", EWDomainAbstract.padNumberWithLeadingZeros(1234, 4));
		assertEquals("12345", EWDomainAbstract.padNumberWithLeadingZeros(12345, 4));
	}
	
	@Test
	public void testNumberAsString() {
		assertEquals("1", EWDomainAbstract.numberAsString(1));
		assertEquals("12", EWDomainAbstract.numberAsString(12));
		assertEquals("-1", EWDomainAbstract.numberAsString(-1));
	}
	
	@Test
	public void testRightJustifyNumber() {
		assertEquals("   1", EWDomainAbstract.rightJustifyNumber(1, 4));
		assertEquals("  12", EWDomainAbstract.rightJustifyNumber(12, 4));
		assertEquals(" 123", EWDomainAbstract.rightJustifyNumber(123, 4));
		assertEquals("1234", EWDomainAbstract.rightJustifyNumber(1234, 4));
		assertEquals("12345", EWDomainAbstract.rightJustifyNumber(12345, 4));
	}
	
	@Test
	public void testRightJustifyString() {
		assertEquals("   a", EWDomainAbstract.rightJustifyString("a", 4));
		assertEquals("  ab", EWDomainAbstract.rightJustifyString("ab", 4));
		assertEquals(" abc", EWDomainAbstract.rightJustifyString("abc", 4));
		assertEquals("abcd", EWDomainAbstract.rightJustifyString("abcd", 4));
		assertEquals("abcde", EWDomainAbstract.rightJustifyString("abcde", 4));
	}

	@Test
	public void testLeftJustifyString() {
		assertEquals("a   ", EWDomainAbstract.leftJustifyString("a", 4));
		assertEquals("ab  ", EWDomainAbstract.leftJustifyString("ab", 4));
		assertEquals("abc ", EWDomainAbstract.leftJustifyString("abc", 4));
		assertEquals("abcd", EWDomainAbstract.leftJustifyString("abcd", 4));
		assertEquals("abcde", EWDomainAbstract.leftJustifyString("abcde", 4));
	}
	
	@Test
	public void testCopyCtor() {
		TestClass c1 = new TestClass();
		c1.setCreateCompany("RAIL");
		c1.setCreatedTimestamp(new Date());
		c1.setCreateUsername("TESTER");
		c1.setModifiedTimestamp(new Date());
		c1.setModifyCompany("RAIL");
		c1.setModifyUsername("TESTER");
		
		TestClass c2 = new TestClass(c1);
		assertNotNull(c2);
		assertNotNull(c1);
		assertEquals(c1, c2);
		assertEquals(c2, c1);
		assertTrue(c1.toString().contains("createCompany=RAIL"));
		assertTrue(c1.toString().contains("createUsername=TESTER"));
		
	}
	
	
}
