package com.railinc.assethealth.eqadv.domain.equipment;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignation;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;

public class MechanicalDesignationTest {
	
	@Test
	public void validMechanicalDesignations() {
		try {
			MechanicalDesignation desig = new MechanicalDesignation("T");
			MechanicalDesignation desig2 = new MechanicalDesignation("t");
			assertNotNull(desig);
			assertTrue(desig.getValueAsString().equals("T"));
			assertNotNull(desig.getValueAsString());
			assertTrue(StringUtils.isNotBlank(desig.getValueAsString()));
			assertTrue(desig.getValueAsString().equals(desig.getValueAsString()));
			assertTrue(desig.getValueAsJustifiedString().equals("T   "));
			desig = new MechanicalDesignation("gwsr");
			desig2 = new MechanicalDesignation("GWSR");
			assertNotNull(desig);
			assertNotNull(desig.getValueAsString());
			assertNotNull(desig2);
			assertNotNull(desig2.getValueAsString());
			assertTrue(StringUtils.isNotBlank(desig.getValueAsString()));
			assertTrue(desig.getValueAsString().equals("GWSR"));
			assertTrue(desig.getValueAsString().equals(desig.getValueAsString()));
			assertTrue(desig.getValueAsJustifiedString().equals("GWSR"));
		} catch (InvalidMechanicalDesignationException e) {
			fail("Exception creating a valid mark.");
		}
	}
	
	@Test
	public void testCopyConstructor() {
		try {
			MechanicalDesignation desig = new MechanicalDesignation("T");
			MechanicalDesignation desig2 = new MechanicalDesignation(desig);
			assertTrue(desig.equals(desig2) && desig2.equals(desig));
			assertTrue(desig2.getValueAsString().equals("T"));
			assertTrue(desig2.getValueAsJustifiedString().equals("T   "));
			desig = new MechanicalDesignation("gwsr");
			assertNotNull(desig);
			assertFalse(desig.equals(desig2) || desig2.equals(desig));
			desig2 = new MechanicalDesignation(desig);
			assertTrue(desig.equals(desig2) && desig2.equals(desig));
			assertTrue(desig2.getValueAsString().equals("GWSR"));
			assertTrue(desig2.getValueAsJustifiedString().equals("GWSR"));
		} catch (InvalidMechanicalDesignationException e) {
			fail("Exception creating a valid mark.");
		}
	}

	@Test(expected=Test.None.class)
	public void validMechanicalDesignationCtor() throws InvalidMechanicalDesignationException {
		MechanicalDesignation desig = new MechanicalDesignation("T", "Tank Car");
		assertNotNull(desig);
		assertTrue(desig.getValueAsString().equals("T"));
		assertTrue(StringUtils.isNotBlank(desig.getDescription()));
		assertTrue(desig.getValueAsJustifiedString().equals("T   "));
	}
	
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void inValidMarks_1() throws InvalidMechanicalDesignationException {
		new MechanicalDesignation("LOCOMOTIVE");
	}
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void inValidMarks_2() throws InvalidMechanicalDesignationException {
		new MechanicalDesignation("D1");
	}
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void inValidMarks_3() throws InvalidMechanicalDesignationException {
		new MechanicalDesignation("D*D");
	}
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void inValidMarks_4() throws InvalidMechanicalDesignationException {
		new MechanicalDesignation("1D*D");
	}
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void inValidMarks_5() throws InvalidMechanicalDesignationException {
		new MechanicalDesignation("");
	}
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void inValidMarks_6() throws InvalidMechanicalDesignationException {
		new MechanicalDesignation("      ");
	}
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void inValidMarks_7() throws InvalidMechanicalDesignationException {
		new MechanicalDesignation((MechanicalDesignation)null);
	}
	
	@Test(expected=InvalidMechanicalDesignationException.class)
	public void inValidMarks_8() throws InvalidMechanicalDesignationException {
		new MechanicalDesignation((String)null);
	}
	
	@Test
	public void testHashcode() throws InvalidRoadMarkException {
		try {
			MechanicalDesignation desig = new MechanicalDesignation("T");
			MechanicalDesignation desig2 = new MechanicalDesignation(desig);
			assertTrue(desig.hashCode() == desig2.hashCode());
			desig2 = new MechanicalDesignation("TT");
			assertFalse(desig.hashCode() == desig2.hashCode());
		} catch (InvalidMechanicalDesignationException e) {
			fail("Exception testing MechanicalDesignation hashcode.");
		}
	}
	
	@Test
	public void testToString() throws InvalidRoadMarkException {
		try {
			MechanicalDesignation desig = new MechanicalDesignation("GSWR");
			assertTrue(StringUtils.isNotBlank(desig.toString()));
			assertTrue(desig.toString().contains("GSWR"));
		} catch (InvalidMechanicalDesignationException e) {
			fail("Exception testing MechanicalDesignation hashcode.");
		}
	}
	
	@Test(expected=Test.None.class)
	public void testCompareTo() throws InvalidMechanicalDesignationException {
		MechanicalDesignation desig = new MechanicalDesignation("T");
		MechanicalDesignation desig2 = new MechanicalDesignation("t");
		assertTrue(0 == desig.compareTo(desig2));
		desig2 = new MechanicalDesignation("L");
		assertTrue(0 < desig.compareTo(desig2));
		assertTrue(0 > desig2.compareTo(desig));
		desig2 = new MechanicalDesignation("ABC");
		assertTrue(0 < desig.compareTo(desig2));
		assertTrue(0 > desig2.compareTo(desig));
	}
}
