package com.railinc.assethealth.eqadv.domain.escalation;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToString;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.CoreMatchers.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.escalation.EscalationType;
import com.railinc.assethealth.eqadv.domain.exception.InvalidSeverityCodeException;

public class EscalationTypeTest {

	@Test
	public void testBean() {
		assertThat(EscalationType.class,
				allOf(hasValidBeanConstructor(), hasValidGettersAndSetters(), hasValidBeanToString()));
	}

	@Test
	public void testArgsConstructor() throws InvalidSeverityCodeException {
		EscalationType code = new EscalationType("D", "Anything");
		assertEquals("Anything", code.getDescription());
		code = new EscalationType("D", "");
		assertEquals("D", code.getType());
		code = new EscalationType("Q", null);
		assertEquals(null, code.getDescription());
	}

}
