package com.railinc.assethealth.eqadv.domain.notice;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class InspectionNoticeTest {
	private static final String CATEGORY_CODE = "CATEGORY_CODE";
	private static final String INSPECTION_CODE = "INSPECTION_CODE";
	private static final String INSPECTION_REPORTER = "INSPECTION_REPORTER";
	private static final String FINAL_FLAG = "y";
	private static final String ACTIVITY_CODE = "ACTIVITY_CODE";
	private static final Long NOTICE_ID = 123_456_789L;
	private static final Integer NOTICE_NUMBER = 123; 
	
	private static final String TEST_STR = "STRING";
	private static final Long TEST_LONG = 123L;
	private static final Integer TEST_INT = 456;
	
	@Test
	public void testGetNoticeId() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(NOTICE_ID, notice.getNoticeId());
	}

	@Test
	public void testSetNoticeId() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(NOTICE_ID, notice.getNoticeId());
		notice.setNoticeId(TEST_LONG);
		assertEquals(TEST_LONG, notice.getNoticeId());
		notice.setNoticeId(NOTICE_ID);
		assertEquals(NOTICE_ID, notice.getNoticeId());
	}

	@Test
	public void testGetNoticeNumber() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(NOTICE_NUMBER, notice.getNoticeNumber());
	}

	@Test
	public void testSetNoticeNumber() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(NOTICE_NUMBER, notice.getNoticeNumber());
		notice.setNoticeNumber(TEST_INT);
		assertEquals(TEST_INT, notice.getNoticeNumber());
		notice.setNoticeNumber(NOTICE_NUMBER);
		assertEquals(NOTICE_NUMBER, notice.getNoticeNumber());
	}

	@Test
	public void testGetCategoryCode() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(CATEGORY_CODE, notice.getCategoryCode());
	}

	@Test
	public void testSetCategoryCode() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(CATEGORY_CODE, notice.getCategoryCode());
		notice.setCategoryCode(TEST_STR);
		assertEquals(TEST_STR, notice.getCategoryCode());
		notice.setCategoryCode(CATEGORY_CODE);
		assertEquals(CATEGORY_CODE, notice.getCategoryCode());
	}

	@Test
	public void testGetIsFinal() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(FINAL_FLAG, notice.getIsFinal());
	}

	@Test
	public void testSetIsFinal() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(FINAL_FLAG, notice.getIsFinal());
		notice.setIsFinal(TEST_STR);
		assertEquals(TEST_STR, notice.getIsFinal());
		notice.setIsFinal(FINAL_FLAG);
		assertEquals(FINAL_FLAG, notice.getIsFinal());
	}

	@Test
	public void testGetNoticeActivityCode() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(ACTIVITY_CODE, notice.getNoticeActivityCode());
	}

	@Test
	public void testSetNoticeActivityCode() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(ACTIVITY_CODE, notice.getNoticeActivityCode());
		notice.setNoticeActivityCode(TEST_STR);
		assertEquals(TEST_STR, notice.getNoticeActivityCode());
		notice.setNoticeActivityCode(ACTIVITY_CODE);
		assertEquals(ACTIVITY_CODE, notice.getNoticeActivityCode());
	}

	@Test
	public void testGetInspectionCode() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(INSPECTION_CODE, notice.getInspectionCode());
	}

	@Test
	public void testSetInspectionCode() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(INSPECTION_CODE, notice.getInspectionCode());
		notice.setInspectionCode(TEST_STR);
		assertEquals(TEST_STR, notice.getInspectionCode());
		notice.setInspectionCode(INSPECTION_CODE);
		assertEquals(INSPECTION_CODE, notice.getInspectionCode());
	}

	@Test
	public void testGetInspectionReporter() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(INSPECTION_REPORTER, notice.getInspectionReporter());
	}

	@Test
	public void testSetInspectionReporter() {
		InspectionNotice notice = getInspectionNotice();
		assertEquals(INSPECTION_REPORTER, notice.getInspectionReporter());
		notice.setInspectionReporter(TEST_STR);
		assertEquals(TEST_STR, notice.getInspectionReporter());
		notice.setInspectionReporter(INSPECTION_REPORTER);
		assertEquals(INSPECTION_REPORTER, notice.getInspectionReporter());
	}

	private InspectionNotice getInspectionNotice() {
		InspectionNotice notice = new InspectionNotice();
		notice.setCategoryCode(CATEGORY_CODE);
		notice.setInspectionCode(INSPECTION_CODE);
		notice.setInspectionReporter(INSPECTION_REPORTER);
		notice.setIsFinal(FINAL_FLAG);
		notice.setNoticeActivityCode(ACTIVITY_CODE);
		notice.setNoticeId(NOTICE_ID);
		notice.setNoticeNumber(NOTICE_NUMBER);
		return notice;
	}
}