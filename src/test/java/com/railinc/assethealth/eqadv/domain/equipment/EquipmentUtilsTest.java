package com.railinc.assethealth.eqadv.domain.equipment;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.equipment.EquipmentUtils;

public class EquipmentUtilsTest {

    @Test
    public void test_isValidEquipmentId_Valid() {
        boolean isValid = EquipmentUtils.isValidEquipmentId("RAIL100");
        assertThat(isValid, is(true));
    }

    @Test
    public void test_isValidEquipmentId_Invalid() {
        boolean isValid = EquipmentUtils.isValidEquipmentId("RAILa100");
        assertThat(isValid, is(false));
    }

    @Test
    public void test_extractNormalizedEquipmentMark() {
        assertEquals("RAIL", EquipmentUtils.extractNormalizedMark("RAILa100"));
        assertEquals("CN  ", EquipmentUtils.extractNormalizedMark("CN  100"));
    }

    @Test
    public void testIsValidNumber() {
        assertTrue(EquipmentUtils.isValidNumber("100"));
        assertTrue(EquipmentUtils.isValidNumber("1"));
        assertTrue(EquipmentUtils.isValidNumber("0000000001"));
        assertFalse(EquipmentUtils.isValidNumber("00000000001"));
        assertFalse(EquipmentUtils.isValidNumber(""));
    }

    @Test
    public void test_extractNormalizedEquipmentNumber() {
        assertEquals("0000000100", EquipmentUtils.extractNormalizedNumber("RAILa100"));
        assertEquals("0000000032", EquipmentUtils.extractNormalizedNumber("CN  32"));
    }
    
    @Test
    public void test_extractMark_Valid() {
        String mark = EquipmentUtils.extractMark("ABC12345");
        assertThat(mark, is(notNullValue()));
        assertThat(mark, is(equalTo("ABC")));
    }

    @Test
    public void test_extractMark_Invalid() {
        String mark = EquipmentUtils.extractMark("12345");
        assertThat(mark, is(notNullValue()));
        assertThat(mark, is(equalTo("")));
    }

    @Test
    public void test_extractNumber_Valid() {
        String mark = EquipmentUtils.extractNumber("ABC12345");
        assertThat(mark, is(notNullValue()));
        assertThat(mark, is(equalTo("12345")));
    }

    @Test
    public void test_extractNumber_Invalid() {
        String mark = EquipmentUtils.extractNumber("ABC");
        assertThat(mark, is(notNullValue()));
        assertThat(mark, is(equalTo("")));
    }

    @Test
    public void test_normalizeMark_AA() {
        String mark = EquipmentUtils.normalizeMark("AA");
        assertThat(mark, is(equalTo("AA  ")));
    }

    @Test
    public void test_normalizeMark_AAA() {
        String mark = EquipmentUtils.normalizeMark("AAA");
        assertThat(mark, is(equalTo("AAA ")));
    }

    @Test
    public void test_normalizeMark_AAAA() {
        String mark = EquipmentUtils.normalizeMark("AAAA");
        assertThat(mark, is(equalTo("AAAA")));
    }

    @Test
    public void test_normalizeNumber() {
        String number = EquipmentUtils.normalizeNumber("12345");
        assertThat(number, is(equalTo("0000012345")));
    }

    @Test
    public void test_normalizeEquipment_MarkOnly() {
        String equipment = EquipmentUtils.normalizeEquipment("ABC");
        assertThat(equipment, is(equalTo("ABC ")));
    }

    @Test
    public void test_normalizeEquipment_NumberOnly() {
        String equipment = EquipmentUtils.normalizeEquipment("12345");
        assertThat(equipment, is(equalTo("0000012345")));
    }

    @Test
    public void test_normalizeEquipment() {
        String equipment = EquipmentUtils.normalizeEquipment("AB12345");
        assertThat(equipment, is(equalTo("AB  0000012345")));
    }

}
