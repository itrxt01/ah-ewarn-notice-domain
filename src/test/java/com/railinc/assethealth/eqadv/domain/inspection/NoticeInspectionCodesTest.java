package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.exception.InspectionNotPeriodicException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidInspectionCodeException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidSeverityCodeException;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;

public class NoticeInspectionCodesTest {

	@Test
	public void testNoticeInspectionCodesListOfNoticeInspectionCode() {
		InspectionCode inspCode_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(new NoticeInspectionCode(inspCode_1, sevCode));
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);		
		assertNotNull(noticeInpsectionCodes);
		assertEquals(inspectionCodes.size(), noticeInpsectionCodes.getInspectionCodes().size());
	}

	@Test
	public void testNoticeInspectionCodesNoticeInspectionCodes() {
		InspectionCode inspCode_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(new NoticeInspectionCode(inspCode_1, sevCode));
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "MF", "description", true, true);
		inspectionCodes.add(new NoticeInspectionCode(inspCode_2, sevCode));
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);		
		assertNotNull(noticeInpsectionCodes);
		assertEquals(inspectionCodes.size(), noticeInpsectionCodes.getInspectionCodes().size());
		NoticeInspectionCodes noticeInpsectionCodes_2 = new NoticeInspectionCodes(noticeInpsectionCodes);		
		assertNotNull(noticeInpsectionCodes_2);
		assertEquals(inspectionCodes.size(), noticeInpsectionCodes_2.getInspectionCodes().size());
		assertEquals(2, noticeInpsectionCodes_2.getInspectionCodesForSeverity(sevCode).size());		
		SeverityCode invalidSevCode = new SeverityCode("A5");
		assertNull(noticeInpsectionCodes_2.getInspectionCodesForSeverity(invalidSevCode));		
	}

	@Test
	public void testGetInspectionCodesForSeverity() {
		InspectionCode inspCode_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(new NoticeInspectionCode(inspCode_1, sevCode));
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "MF", "description", true, true);
		inspectionCodes.add(new NoticeInspectionCode(inspCode_2, sevCode));
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);		
		assertNotNull(noticeInpsectionCodes);
		assertEquals(inspectionCodes.size(), noticeInpsectionCodes.getInspectionCodes().size());
		assertEquals(2, noticeInpsectionCodes.getInspectionCodesForSeverity(sevCode).size());		
		SeverityCode invalidSevCode = new SeverityCode("A5");
		assertNull(noticeInpsectionCodes.getInspectionCodesForSeverity(invalidSevCode));		
	}

	@Test(expected=InvalidSeverityCodeException.class)
	public void testGetInspectionCodeForSeverity() throws InvalidSeverityCodeException, InvalidInspectionCodeException, InspectionNotPeriodicException {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode code_1 = new NoticeInspectionCode(inspCode, sevCode);
		TimePeriod tp = new TimePeriod(31, PERIODIC_TIME_UNIT.MONTHS);
		code_1.setFrequency(tp);
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(code_1);
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MF", "description", true, true);
		NoticeInspectionCode code_2 = new NoticeInspectionCode(inspCode_2, sevCode);
		inspectionCodes.add(code_2);
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);
		SeverityCode invalidSevCode = new SeverityCode("A5");
		noticeInpsectionCodes.getInspectionFrequencyUnits(inspCode_2, invalidSevCode);
	}
	
	
	@Test
	public void testIsInspectionPermitted() {
		InspectionCode inspCode_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(new NoticeInspectionCode(inspCode_1, sevCode));
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.FINAL, "MF", "description", true, true);
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);		
		assertTrue(noticeInpsectionCodes.isInspectionPermitted(inspCode_1, sevCode));		
		assertFalse(noticeInpsectionCodes.isInspectionPermitted(inspCode_2, sevCode));	
		InspectionCode inspCode_3 = new InspectionCode(INSPECTION_TYPE.FINAL, "AB", "description", true, true);
		SeverityCode sevCode_2 = new SeverityCode("B6");
		assertFalse(noticeInpsectionCodes.isInspectionPermitted(inspCode_3, sevCode_2));	
	}

	@Test
	public void testIsFinalInspectionForSeverity() {
		InspectionCode inspCode_1 = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(new NoticeInspectionCode(inspCode_1, sevCode));
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MF", "description", true, true);
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);		
		assertTrue(noticeInpsectionCodes.isFinalInspectionForSeverity(inspCode_1, sevCode));		
		assertFalse(noticeInpsectionCodes.isFinalInspectionForSeverity(inspCode_2, sevCode));
		SeverityCode invalidSevCode = new SeverityCode("B2");		
		assertFalse(noticeInpsectionCodes.isFinalInspectionForSeverity(inspCode_2, invalidSevCode));
	}

	@Test(expected=Test.None.class)
	public void testGetInspectionFrequency() throws InvalidSeverityCodeException, InvalidInspectionCodeException, InspectionNotPeriodicException {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode code_1 = new NoticeInspectionCode(inspCode, sevCode);
		TimePeriod tp = new TimePeriod(31, PERIODIC_TIME_UNIT.MONTHS);
		code_1.setFrequency(tp);
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(code_1);
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MF", "description", true, true);
		NoticeInspectionCode code_2 = new NoticeInspectionCode(inspCode_2, sevCode);
		inspectionCodes.add(code_2);
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);
		assertEquals(tp.getTimePeriod(), noticeInpsectionCodes.getInspectionFrequency(inspCode, sevCode));
		assertEquals(tp.getTimeUnit().units, noticeInpsectionCodes.getInspectionFrequencyUnits(inspCode, sevCode));
		assertEquals(0, noticeInpsectionCodes.getInspectionFrequency(inspCode_2, sevCode));
		assertNull(noticeInpsectionCodes.getInspectionFrequencyUnits(inspCode_2, sevCode));
	}

	@Test(expected=InspectionNotPeriodicException.class)
	public void testGetInspectionFrequencyException() throws InvalidSeverityCodeException, InvalidInspectionCodeException, InspectionNotPeriodicException {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode code_1 = new NoticeInspectionCode(inspCode, sevCode);
		TimePeriod tp = new TimePeriod(31, PERIODIC_TIME_UNIT.MONTHS);
		code_1.setFrequency(tp);
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(code_1);
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MF", "description", true, false);
		NoticeInspectionCode code_2 = new NoticeInspectionCode(inspCode_2, sevCode);
		inspectionCodes.add(code_2);
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);
		noticeInpsectionCodes.getInspectionFrequency(inspCode_2, sevCode);
	}
	
	@Test(expected=InspectionNotPeriodicException.class)
	public void testGetInspectionUnitsException() throws InvalidSeverityCodeException, InvalidInspectionCodeException, InspectionNotPeriodicException {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode code_1 = new NoticeInspectionCode(inspCode, sevCode);
		TimePeriod tp = new TimePeriod(31, PERIODIC_TIME_UNIT.MONTHS);
		code_1.setFrequency(tp);
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(code_1);
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MF", "description", true, false);
		NoticeInspectionCode code_2 = new NoticeInspectionCode(inspCode_2, sevCode);
		inspectionCodes.add(code_2);
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);
		noticeInpsectionCodes.getInspectionFrequencyUnits(inspCode_2, sevCode);
	}
	
	@Test
	public void testGetPermittedFinalInspectionCodesForSeverity() {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode code_1 = new NoticeInspectionCode(inspCode, sevCode);
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(code_1);
		InspectionCode inspCode_2 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MF", "description", true, true);
		NoticeInspectionCode code_2 = new NoticeInspectionCode(inspCode_2, sevCode);
		inspectionCodes.add(code_2);
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);		
		assertEquals(1, noticeInpsectionCodes.getPermittedFinalInspectionCodesForSeverity(sevCode).size());
		assertTrue(noticeInpsectionCodes.getPermittedFinalInspectionCodesForSeverity(sevCode).contains(code_1));
		assertFalse(noticeInpsectionCodes.getPermittedFinalInspectionCodesForSeverity(sevCode).contains(code_2));
	}

	@Test(expected=Test.None.class)
	public void testSetFrequencyForInspection() throws InvalidSeverityCodeException, InvalidInspectionCodeException, InspectionNotPeriodicException {
		InspectionCode inspCode = new InspectionCode(INSPECTION_TYPE.FINAL, "MN", "description", true, true);
		SeverityCode sevCode = new SeverityCode("B5");
		NoticeInspectionCode code_1 = new NoticeInspectionCode(inspCode, sevCode);
		List<NoticeInspectionCode> inspectionCodes = new ArrayList<>(); 
		inspectionCodes.add(code_1);
		NoticeInspectionCodes noticeInpsectionCodes = new NoticeInspectionCodes(inspectionCodes);
		noticeInpsectionCodes.setFrequencyForInspection(inspCode, sevCode, 31, PERIODIC_TIME_UNIT.DAYS);
		assertEquals(31, noticeInpsectionCodes.getInspectionFrequency(inspCode, sevCode));
		assertEquals(PERIODIC_TIME_UNIT.DAYS.units, noticeInpsectionCodes.getInspectionFrequencyUnits(inspCode, sevCode));
	}
}
