package com.railinc.assethealth.eqadv.domain.inspection;

import static org.junit.Assert.*;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.inspection.INSPECTION_TYPE;
import com.railinc.assethealth.eqadv.domain.inspection.InspectionCode;

public class EWCodeTest {

	@Test
	public void testEWCodeString() {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.FINAL, "AS");
		code.setDescription("DESCRIPTION");
		assertNotNull(code);
		assertEquals("AS", code.getCode());
		assertEquals("DESCRIPTION", code.getDescription());
	}

	@Test
	public void testEWCodeStringString() {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "AS", "DESCRIPTION", true, false);
		code.setDescription("DESCRIPTION");
		assertNotNull(code);
		assertEquals("AS", code.getCode());
		assertEquals("DESCRIPTION", code.getDescription());
	}

	@Test
	public void testSetCode() {
		InspectionCode code = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "AS", "DESCRIPTION", true, false);
		assertNotNull(code);
		assertEquals("AS", code.getCode());
		code.setCode("DC");
		assertEquals("DC", code.getCode());
	}
}
