package com.railinc.assethealth.eqadv.domain.transaction;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.transaction.NoticeInspectionCodeEvent;

import static org.junit.Assert.assertEquals;

public class NoticeInspectionCodeEventTest extends NoticeEventTestHelper<NoticeInspectionCodeEvent> {

    private static final String SEVERITY_CODE = "A1";
    private static final String INSPECTION_CODE = "MC";
    private static final String IS_FINAL_FLAG = "N";
    private static final Long FREQUENCY = 30L;
    private static final String FREQUENCY_UNITS = "Days";

    @Test
    public void testNoticeInspectionCodeEvent() {
        NoticeInspectionCodeEvent noticeInspectionCodeEvent = getNoticeEvent();

        assertEquals(EVENT_ID, noticeInspectionCodeEvent.getEventId());
        assertEquals(NOTICE_ID, noticeInspectionCodeEvent.getNoticeId());
        assertEquals(SEVERITY_CODE, noticeInspectionCodeEvent.getSeverityCode());
        assertEquals(INSPECTION_CODE, noticeInspectionCodeEvent.getInspectionCode());
        assertEquals(IS_FINAL_FLAG, noticeInspectionCodeEvent.getIsFinalFlag());
        assertEquals(FREQUENCY, noticeInspectionCodeEvent.getFrequency());
        assertEquals(FREQUENCY_UNITS, noticeInspectionCodeEvent.getFrequencyUnits());
    }

    @Override
    NoticeInspectionCodeEvent getNoticeEvent() {
        NoticeInspectionCodeEvent noticeInspectionCodeEvent = new NoticeInspectionCodeEvent();
        noticeInspectionCodeEvent.setEventId(EVENT_ID);
        noticeInspectionCodeEvent.setNoticeId(NOTICE_ID);
        noticeInspectionCodeEvent.setSeverityCode(SEVERITY_CODE);
        noticeInspectionCodeEvent.setInspectionCode(INSPECTION_CODE);
        noticeInspectionCodeEvent.setIsFinalFlag(IS_FINAL_FLAG);
        noticeInspectionCodeEvent.setFrequency(FREQUENCY);
        noticeInspectionCodeEvent.setFrequencyUnits(FREQUENCY_UNITS);
        return noticeInspectionCodeEvent;
    }
}
