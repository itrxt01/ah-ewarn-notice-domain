package com.railinc.assethealth.eqadv.domain.mark;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;
import com.railinc.assethealth.eqadv.domain.mark.Mark;
import com.railinc.assethealth.eqadv.domain.mark.RoadMark;

public class RoadMarkTest {
	
	@Test
	public void validRoadMarks() {
		Mark mark = new RoadMark("NS");
		assertNotNull(mark);
		assertTrue(mark.isEquipmentOwningMark());
		assertEquals("NS", mark.getValueAsString());
		assertEquals("NS  ", mark.getValueAsJustifiedString());
		mark = new RoadMark("up  ");
		assertNotNull(mark);
		assertTrue(mark.isEquipmentOwningMark());
		assertEquals("UP", mark.getValueAsString());
		assertEquals("UP  ", mark.getValueAsJustifiedString());
		mark = new RoadMark("bnsf");
		assertNotNull(mark);
		assertTrue(mark.isEquipmentOwningMark());
		assertEquals("BNSF", mark.getValueAsString());
		assertEquals("BNSF", mark.getValueAsJustifiedString());
	}

	@Test
	public void validRoadMarkParent() {
		Mark parent = new RoadMark("BNSF");
		assertNotNull(parent);
		assertTrue(parent.isEquipmentOwningMark());
		assertEquals("BNSF", parent.getValueAsString());
		assertEquals("BNSF", parent.getValueAsJustifiedString());
		Mark mark = new RoadMark("BN", parent);
		assertNotNull(mark);
		assertTrue(mark.isEquipmentOwningMark());
		assertEquals("BN", mark.getValueAsString());
		assertTrue(mark.isParent(parent));
	}

	@Test
	public void testCopyConstructor() {
		Mark mark = new RoadMark("NS");
		Mark mark2 = new RoadMark(mark);
		assertNotNull(mark2);
		assertTrue(mark2.equals(mark) && mark.equals(mark2));
	}
	
	
	@Test(expected=Test.None.class)
	public void inValidMarks_1() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("1234");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_11() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("NS");
		Mark mark_2 = new RoadMark("1234", mark);
		assertFalse(mark_2.isValid());	
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_2() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("A23X");
		assertFalse(mark.isValid());		
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_21() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("NS");
		Mark mark_2 = new RoadMark("A23X", mark);
		assertFalse(mark_2.isValid());		
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_3() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("    ");
		assertFalse(mark.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_31() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("NS");
		Mark mark_2 = new RoadMark("    ", mark);
		assertFalse(mark_2.isValid());		
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_4() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("");
		assertFalse(mark.isValid());		
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_41() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("NS");
		Mark mark_2 = new RoadMark("", mark);
		assertFalse(mark_2.isValid());
	}
	@Test(expected=Test.None.class)
	public void inValidMarks_5() throws InvalidRoadMarkException {
		RoadMark mark = new RoadMark((String)null);
		assertFalse(mark.isValid());
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_51() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("NS");
		RoadMark mark_2 = new RoadMark((String)null, mark);
		assertFalse(mark_2.isValid());
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_6() throws InvalidRoadMarkException {
		RoadMark mark = new RoadMark((Mark)null);
		assertFalse(mark.isValid());
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_7() throws InvalidRoadMarkException {
		RoadMark mark = new RoadMark("T");
		assertFalse(mark.isValid());
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_8() throws InvalidRoadMarkException {
		RoadMark mark = new RoadMark("T1");
		assertFalse(mark.isValid());
	}

	@Test(expected=Test.None.class)
	public void inValidMarks_9() throws InvalidRoadMarkException {
		RoadMark mark = new RoadMark("1");
		assertFalse(mark.isValid());
	}
	
	@Test(expected=Test.None.class)
	public void inValidMarks_10() throws InvalidRoadMarkException {
		RoadMark mark = new RoadMark("12");
		assertFalse(mark.isValid());
	}
	
	@Test
	public void testEquals() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("ns");
		Mark mark2 = new RoadMark("NS");
		assertTrue(mark.equals(mark2));
		assertTrue(mark2.equals(mark));
		mark = new RoadMark("NS");
		assertTrue(mark.equals(mark2));
		assertTrue(mark2.equals(mark));
		mark2 = new RoadMark("NO");
		assertFalse(mark.equals(mark2));
		assertFalse(mark2.equals(mark));
		assertFalse(mark.equals("NS"));
		assertFalse(mark.equals(null));
	}

	@Test
	public void testHashcode() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("ns");
		Mark mark2 = new RoadMark("NS");
		assertTrue(mark.hashCode() == mark2.hashCode());
		mark = new RoadMark("BNSF");
		mark2 = new RoadMark("BNSF");
		assertTrue(mark.hashCode() == mark2.hashCode());
		mark = new RoadMark("BNSF");
		mark2 = new RoadMark("BNSX");
		assertTrue(mark.hashCode() != mark2.hashCode());
	}
	
	@Test
	public void testToString() throws InvalidRoadMarkException {
		Mark mark = new RoadMark("ns");
		assertTrue(StringUtils.isNotBlank(mark.toString()));
		assertTrue(mark.toString().contains("NS"));
	}
}
