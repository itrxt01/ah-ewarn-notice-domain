package com.railinc.assethealth.eqadv.domain.severity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.exception.InvalidRoadMarkException;

public class SeverityLevelDeserializerTest {

	@Test
	public void testSeverityLevelDeserializer() {
		SeverityLevelDeserializer des = new SeverityLevelDeserializer();
		assertNotNull(des);
	}

	@Test
	public void testSeverityLevelDeserializerClassOfSeverityLevel() {
		SeverityLevelDeserializer des = new SeverityLevelDeserializer(SeverityLevel.class);
		assertNotNull(des);

	}

	@Test
	public void testMarshallUnmarshall() throws IOException, InvalidRoadMarkException {
		SeverityLevel severityLevel = new SeverityLevel("z1", "z2", 5, "Days");
		severityLevel.setSeverityLevelDesc("a long description");
		
		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(severityLevel);
		System.out.println(JSON);

		SimpleModule module = new SimpleModule();
		module.addDeserializer(SeverityLevel.class, new SeverityLevelDeserializer());
		objectMapper.registerModule(module);

		SeverityLevel unmarshalled = objectMapper.readValue(JSON, SeverityLevel.class);
		assertEquals("z1", unmarshalled.getSeverityLevelCode());
		assertEquals(PERIODIC_TIME_UNIT.DAYS.units, unmarshalled.getEscalationPeriodUnit());
		assertEquals("a long description", unmarshalled.getSeverityLevelDesc());
		assertEquals("z2", unmarshalled.getEscalationLevelCode());
		assertEquals("Days", unmarshalled.getEscalationPeriodUnit());
		assertEquals(new SeverityLevel("z2"), unmarshalled.getEscalationLevel());
		assertEquals(severityLevel.getSeverityCode(), unmarshalled.getSeverityCode());
	}
}
