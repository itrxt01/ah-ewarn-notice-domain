package com.railinc.assethealth.eqadv.domain.transaction;

import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.transaction.NoticeFinalInspectionReporterEvent;

import static org.junit.Assert.assertEquals;

public class NoticeFinalInspectionReporterEventTest extends NoticeEventTestHelper<NoticeFinalInspectionReporterEvent> {

    private static final String MARK = "MARK";

    @Test
    public void testNoticeFinalInspectionReporterEvent() {
        NoticeFinalInspectionReporterEvent noticeFinalInspectionReporterEvent = getNoticeEvent();

        assertEquals(EVENT_ID, noticeFinalInspectionReporterEvent.getEventId());
        assertEquals(NOTICE_ID, noticeFinalInspectionReporterEvent.getNoticeId());
        assertEquals(MARK, noticeFinalInspectionReporterEvent.getMark());
    }

    @Override
    NoticeFinalInspectionReporterEvent getNoticeEvent() {
        NoticeFinalInspectionReporterEvent noticeFinalInspectionReporterEvent = new NoticeFinalInspectionReporterEvent();
        noticeFinalInspectionReporterEvent.setEventId(EVENT_ID);
        noticeFinalInspectionReporterEvent.setNoticeId(NOTICE_ID);
        noticeFinalInspectionReporterEvent.setMark(MARK);
        return noticeFinalInspectionReporterEvent;
    }
}
