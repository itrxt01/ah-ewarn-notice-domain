package com.railinc.assethealth.eqadv.domain.notice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.category.Category;
import com.railinc.assethealth.eqadv.domain.equipment.Assignment;
import com.railinc.assethealth.eqadv.domain.equipment.MECHANICAL_DESIGNATION_INDICATOR;
import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignation;
import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignations;
import com.railinc.assethealth.eqadv.domain.equipment.MechanicalDesignationsDeserializer;
import com.railinc.assethealth.eqadv.domain.exception.InvalidCategoryException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidMechanicalDesignationException;
import com.railinc.assethealth.eqadv.domain.exception.InvalidStatusException;
import com.railinc.assethealth.eqadv.domain.inspection.FINAL_INSPECTION_INDICATOR;
import com.railinc.assethealth.eqadv.domain.inspection.INSPECTION_TYPE;
import com.railinc.assethealth.eqadv.domain.inspection.InspectionCode;
import com.railinc.assethealth.eqadv.domain.inspection.NoticeInspectionCode;
import com.railinc.assethealth.eqadv.domain.inspection.NoticeInspectionCodes;
import com.railinc.assethealth.eqadv.domain.mark.ASSIGNMENT_REPORTER_INDICATOR;
import com.railinc.assethealth.eqadv.domain.mark.Mark;
import com.railinc.assethealth.eqadv.domain.mark.MarkFactory;
import com.railinc.assethealth.eqadv.domain.mark.Marks;
import com.railinc.assethealth.eqadv.domain.mark.MarksDeserializer;
import com.railinc.assethealth.eqadv.domain.pdf.ATTACHMENT_TYPE;
import com.railinc.assethealth.eqadv.domain.pdf.NoticeContent;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevelDeserializer;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevels;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevelsDeserializer;
import com.railinc.assethealth.eqadv.domain.status.NOTICE_STATUS;

public class NoticeTest {
	@Test
	public void testGetDraftNoticeName() {
		Notice notice = new Notice();
		notice.setCategory(new Category("MS", "desc"));
		notice.setNumber(500_001);
		notice.setSupplementNumber(0);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertEquals("MS-DRAFT", notice.getNoticeName());
	}
	
	@Test
	public void testHasBeenPersisted() {
		Notice notice = new Notice();
		notice.setCategory(new Category("MS", "desc"));
		notice.setNumber(null);
		notice.setSupplementNumber(0);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertFalse(notice.hasBeenPersisted());
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertFalse(notice.hasBeenPersisted());
		notice.setStatus(NOTICE_STATUS.ARCHIVED);
		assertFalse(notice.hasBeenPersisted());
		notice.setNumber(1);
		notice.setSupplementNumber(0);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertTrue(notice.hasBeenPersisted());
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertTrue(notice.hasBeenPersisted());
		notice.setStatus(NOTICE_STATUS.ARCHIVED);
		assertTrue(notice.hasBeenPersisted());
	}
	
	@Test(expected=Test.None.class)
	public void testhasPreviouslyBeenPublished() throws InvalidCategoryException {
		Notice notice = new Notice();
		notice.setNumber(5000);
		notice.setSupplementNumber(0);
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertTrue(notice.hasPreviouslyBeenPublished());
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertTrue(notice.hasPreviouslyBeenPublished());
		notice.setStatus(NOTICE_STATUS.ARCHIVED);
		assertTrue(notice.hasPreviouslyBeenPublished());
		notice.setNumber(1);
		notice.setSupplementNumber(1);
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertTrue(notice.hasPreviouslyBeenPublished());
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertTrue(notice.hasPreviouslyBeenPublished());
		notice.setStatus(NOTICE_STATUS.ARCHIVED);
		assertTrue(notice.hasPreviouslyBeenPublished());
		notice.setNumber(500_001);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertFalse(notice.hasPreviouslyBeenPublished());
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertFalse(notice.hasPreviouslyBeenPublished());
		notice.setStatus(NOTICE_STATUS.ARCHIVED);
		assertFalse(notice.hasPreviouslyBeenPublished());
		notice.setNumber(null);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertFalse(notice.hasPreviouslyBeenPublished());
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertFalse(notice.hasPreviouslyBeenPublished());
		notice.setStatus(NOTICE_STATUS.ARCHIVED);
		assertFalse(notice.hasPreviouslyBeenPublished());
	}
	
	@Test(expected=Test.None.class)
	public void testGetStatus() throws InvalidStatusException {
		Notice notice = new Notice();
		notice.setCategory(new Category("MS", "desc"));
		notice.setNumber(500_001);
		notice.setSupplementNumber(0);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertEquals(NOTICE_STATUS.DRAFT, notice.getStatusIndicator());
		assertEquals("DRAFT", notice.getStatus());
		notice.setStatus("PUBLISHED");
		assertEquals(NOTICE_STATUS.PUBLISHED, notice.getStatusIndicator());
		assertEquals("PUBLISHED", notice.getStatus());
		notice.setStatus("ARCHIVED");
		assertEquals(NOTICE_STATUS.ARCHIVED, notice.getStatusIndicator());
		assertEquals("ARCHIVED", notice.getStatus());
		
	}

	@Test
	public void testGetNoticeName() {
		Notice notice = new Notice();
		notice.setCategory(new Category("IN", "desc"));
		notice.setNumber(501);
		notice.setSupplementNumber(1);
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertEquals("IN-0501 Supplement 01", notice.getNoticeName());
	}
	
	@Test
	public void testGetNextNoticeNamePublished() {
		Notice notice = new Notice();
		notice.setCategory(new Category("IN", "desc"));
		notice.setNumber(501);
		notice.setSupplementNumber(1);
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertEquals("IN-0501 Supplement 02", notice.getNextPublishedName());
	}
	
	@Test
	public void testInvalidCatetory() {
		Notice notice = new Notice();
		notice.setCategory(new Category("code", "desc"));
		notice.setNumber(500_001);
		notice.setSupplementNumber(0);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertEquals("code-DRAFT", notice.getNoticeName());
	}
	
	@Test	
	public void testGetDraftNoticeNamePublished() {
		Notice notice = new Notice();
		notice.setCategory(new Category("code", "desc"));
		notice.setNumber(5171);
		notice.setSupplementNumber(0);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertEquals("code-5171 DRAFT", notice.getNoticeName());
	}

	@Test	
	public void testGetNextNoticeNumber() {
		Notice notice = new Notice();
		notice.setNextNoticeNumber(0123);
		assertEquals(Integer.valueOf(0123), notice.getNextNoticeNumber());
	}
	
	
	@Test
	public void testGetDraftSupplementNoticeName() {
		Notice notice = new Notice();
		notice.setCategory(new Category("code", "desc"));
		notice.setNumber(500);
		notice.setSupplementNumber(3);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertEquals("code-0500 Supplement 03 DRAFT", notice.getNoticeName());
	}
	
	@Test
	public void testGetPublishedNoticeName() {
		Notice notice = new Notice();
		notice.setCategory(new Category("code", "desc"));
		notice.setNumber(500);
		notice.setSupplementNumber(0);
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertEquals("code-0500", notice.getNoticeName());
	}
	
	@Test
	public void testGetPublishedSupplementNoticeName_PUBLISHED() {
		Notice notice = new Notice();
		notice.setCategory(new Category("code", "desc"));
		notice.setNumber(500);
		notice.setSupplementNumber(3);
		notice.setStatus(NOTICE_STATUS.PUBLISHED);
		assertEquals("code-0500 Supplement 03", notice.getNoticeName());
	}
	
	@Test
	public void testGetPublishedSupplementNoticeName_DRAFT() {
		Notice notice = new Notice();
		notice.setCategory(new Category("code", "desc"));
		notice.setNumber(500);
		notice.setSupplementNumber(3);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertEquals("code-0500 Supplement 03 DRAFT", notice.getNoticeName());
	}

	@Test
	public void testGetPublishedSupplementNoticeName_NEW() {
		Notice notice = new Notice();
		notice.setCategory(new Category("code", "desc"));
		notice.setNumber(null);
		notice.setSupplementNumber(null);
		notice.setStatus(NOTICE_STATUS.DRAFT);
		assertEquals("code-DRAFT", notice.getNoticeName());
	}

	
	@Test
	public void testNumberCurrentlyAssignedNullList() {
		Notice notice = new Notice();
		assertEquals(0, notice.getNumberEquipmentCurrentlyAssigned());
	}
	
	@Test
	public void testGetNumberAsString() {
		Notice notice = new Notice();
		notice.setNumber(1);
		assertEquals("0001", notice.getNumberAsString());
		notice.setNumber(12);
		assertEquals("0012", notice.getNumberAsString());
		notice.setNumber(123);
		assertEquals("0123", notice.getNumberAsString());
		notice.setNumber(1234);
		assertEquals("1234", notice.getNumberAsString());
		notice.setNumber(12345);
		assertEquals("12345", notice.getNumberAsString());
	}


	@Test
	public void testGetContent() {
		Notice notice = new Notice();
		NoticeContent content = new NoticeContent();
		content.setFileName("Notice 0005 filename.pdf");
		content.setFileSize(1234);
		content.setFileExtension("pdf");
		content.setMd5("ALONGMD5STRING");
		notice.setContent(content);
		assertEquals("ALONGMD5STRING", notice.getContent().getMd5());
		assertEquals(Integer.valueOf(1234), notice.getContent().getFileSize());
		assertEquals("pdf", notice.getContent().getFileExtension());
		assertEquals(ATTACHMENT_TYPE.PDF, notice.getContent().getFileType());
		assertTrue(notice.getContent().isSupportedFileType());
	}
	
	
	@Test
	public void testGetNumberEquipmentCurrentlyAssigned() {
		Notice notice = new Notice();
		Set<Assignment> setOfEquipment = new TreeSet<>();
		int numberAssigned = notice.setAssignedEquipment(setOfEquipment);		
		assertEquals(0, notice.getNumberEquipmentCurrentlyAssigned());
		assertEquals(0, numberAssigned);
	}
	
	@Test
	public void testGetNumberEquipmentCurrentlyAssignedNull() {
		Notice notice = new Notice();		
		assertEquals(0, notice.getNumberEquipmentCurrentlyAssigned());
	}

	@Test
	public void testGetNoticeInspectionCodes() {
		NoticeInspectionCodes inspectionCodes = getFinalInspectionCodes();
		InspectionCode inspectionCode3 = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MC");
		SeverityCode severityCode3 = new SeverityCode("XX");
		NoticeInspectionCode noticeInspectionCode4 = new NoticeInspectionCode(inspectionCode3, severityCode3);
		List<NoticeInspectionCode> testNoticeInspectionCodes = new ArrayList<>(inspectionCodes.getInspectionCodes());
		testNoticeInspectionCodes.add(noticeInspectionCode4);

		Notice notice = new Notice();
		notice.setFinalInspectionCodes(new NoticeInspectionCodes(testNoticeInspectionCodes));

		List<NoticeInspectionCode> noticeInspectionCodes = notice.getNoticeInspectionCodes();
		assertEquals(testNoticeInspectionCodes.size(), noticeInspectionCodes.size());
		assertEquals(4, noticeInspectionCodes.stream().filter(n -> n.getInspectionCode().isFinal()).count());
		assertEquals(1, noticeInspectionCodes.stream().filter(n -> !n.getInspectionCode().isFinal()).count());
	}

	@Test(expected=Test.None.class)
	public void testIsInspectionCodeMC() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		NoticeInspectionCodes inspectionCodes = getFinalInspectionCodes();
		InspectionCode inspectionCodeMC = new InspectionCode(INSPECTION_TYPE.PRELIMINARY, "MC");
		SeverityCode severityCode_1 = new SeverityCode("XX");
		SeverityCode severityCode_2 = new SeverityCode("XX");
		SeverityCode severityCode_3 = new SeverityCode("XX");
		NoticeInspectionCode noticeInspectionCode = new NoticeInspectionCode(inspectionCodeMC, severityCode_1);
		noticeInspectionCode.setFrequency(new TimePeriod(5, PERIODIC_TIME_UNIT.MONTHS));
		List<NoticeInspectionCode> testNoticeInspectionCodes = new ArrayList<>(inspectionCodes.getInspectionCodes());
		testNoticeInspectionCodes.add(noticeInspectionCode);

		noticeInspectionCode = new NoticeInspectionCode(inspectionCodeMC, severityCode_2);
		noticeInspectionCode.setFrequency(new TimePeriod(5, PERIODIC_TIME_UNIT.MONTHS));
		testNoticeInspectionCodes.add(noticeInspectionCode);
		
		noticeInspectionCode = new NoticeInspectionCode(inspectionCodeMC, severityCode_3);
		noticeInspectionCode.setFrequency(new TimePeriod(5, PERIODIC_TIME_UNIT.MONTHS));
		testNoticeInspectionCodes.add(noticeInspectionCode);
		
		Notice notice = new Notice();
		notice.setFinalInspectionCodes(new NoticeInspectionCodes(testNoticeInspectionCodes));
		assertTrue(notice.isInspectionCodeMC());
		assertEquals(PERIODIC_TIME_UNIT.MONTHS.units, notice.getMCFrequencyUnits());
		assertEquals(Integer.valueOf(5), notice.getMCInspectionInterval());
		assertEquals(noticeInspectionCode, notice.getMCInspectionCode());
		assertEquals(Integer.valueOf(5), Integer.valueOf(notice.getInspectionInterval()));
		assertEquals("Months", notice.getFrequencyUnits());
				
		Method m = notice.getClass().getDeclaredMethod("isInspectionCodeMCEnabled");
		m.setAccessible(true);
		Object o = m.invoke(notice);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.TRUE, b);
		
		notice.setInspectionCodeMC(false);
		o = m.invoke(notice);
		b = (Boolean)o;
		assertEquals(Boolean.FALSE, b);
		
		
	}
	
	@Test(expected=Test.None.class)
	public void testNotInspectionCodeMC() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		NoticeInspectionCodes inspectionCodes = getFinalInspectionCodes();

		Notice notice = new Notice();
		notice.setFinalInspectionCodes(inspectionCodes);
		assertFalse(notice.isInspectionCodeMC());
		
		Method m = notice.getClass().getDeclaredMethod("isInspectionCodeMCEnabled");
		m.setAccessible(true);
		Object o = m.invoke(notice);
		Boolean b = (Boolean)o;
		assertEquals(Boolean.FALSE, b);
	}
	
	@Test(expected=Test.None.class)
	public void testMechanicalDesignations() throws InvalidMechanicalDesignationException {
		Notice notice = new Notice();
		notice.setMechanicalDesignationInd(MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED);
		List<MechanicalDesignation> designations = new ArrayList<>();
		designations.add(new MechanicalDesignation("T"));
		designations.add(new MechanicalDesignation("D"));
		designations.add(new MechanicalDesignation("DESI"));
		designations.add(new MechanicalDesignation("GNAT"));
		designations.add(new MechanicalDesignation("ION"));
		notice.addMechanicalDesignations(designations);
		
		assertNotNull(notice.getMechanicalDesignations());
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED, notice.getMechanicalDesignationInd());
		assertEquals(Integer.valueOf(5), Integer.valueOf(notice.getMechanicalDesignationSet().size()));
		assertTrue(notice.getMechanicalDesignations().containsMechanicalDesignation("T"));
		assertFalse(notice.getMechanicalDesignations().containsMechanicalDesignation("RST"));
		assertTrue(notice.getMechanicalDesignationsString().contains("GNAT"));		
		assertFalse(notice.getMechanicalDesignationsString().contains("RST"));
		assertTrue(notice.isMechanicalDesignationAllowed("DESI"));
		assertFalse(notice.isMechanicalDesignationAllowed(""));
		assertFalse(notice.isMechanicalDesignationAllowed((String)null));
		
		notice.addMechanicalDesignation("RST");
		assertTrue(notice.getMechanicalDesignationsString().contains("RST"));
		assertTrue(notice.getMechanicalDesignations().containsMechanicalDesignation("RST"));
		assertTrue(notice.isMechanicalDesignationAllowed("RST"));	
		
		notice.setMechanicalDesignations(null);
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.OPEN, notice.getMechanicalDesignationInd());
	}

	@Test(expected=Test.None.class)
	public void testFinalInspectionCodes() throws InvalidMechanicalDesignationException {
		Notice notice = new Notice();
		NoticeInspectionCodes codes = this.getFinalInspectionCodes();
		notice.setFinalInspectionCodes(codes);
				
		assertNotNull(notice.getFinalInspectionCodes());
		assertEquals(Integer.valueOf(2), Integer.valueOf(notice.getFinalInspectionCodes().size()));
		assertEquals(Integer.valueOf(0), Integer.valueOf(notice.getFinalInspectionCodes(new SeverityCode("XX")).size()));
		assertEquals(Integer.valueOf(2), Integer.valueOf(notice.getFinalInspectionCodes(new SeverityCode("A2")).size()));
		assertEquals(Integer.valueOf(2), Integer.valueOf(notice.getFinalInspectionCodes(new SeverityCode("A1")).size()));
		assertTrue(notice.isFinalInspectionCode("MH"));
		assertTrue(notice.isFinalInspectionCode("MR"));
		assertFalse(notice.isFinalInspectionCode("MC"));		
	}

	@Test(expected=Test.None.class)
	public void testFinalInspectionReporters() throws InvalidMechanicalDesignationException {
		Notice notice = new Notice();
		notice.setFinalInspectionInd(FINAL_INSPECTION_INDICATOR.OPEN);
		assertEquals(FINAL_INSPECTION_INDICATOR.OPEN, notice.getFinalInspectionInd());
		
		Marks marks = new Marks();
		marks.addMark(MarkFactory.newMark("RAIL"));
		marks.addMark(MarkFactory.newMark("AAR"));
		marks.addMark(MarkFactory.newMark("BNSF"));
		notice.setFinalInspectionInd(FINAL_INSPECTION_INDICATOR.SPECIFIED);
		notice.setFinalInspectionMarks(marks);
		
		assertEquals(FINAL_INSPECTION_INDICATOR.SPECIFIED, notice.getFinalInspectionInd());
		assertEquals(Integer.valueOf(3), Integer.valueOf(notice.getFinalInspectionMarks().size()));
		assertTrue(notice.getFinalInspectionMarks().isMarkListed(MarkFactory.newMark("AAR")));
		assertFalse(notice.getFinalInspectionMarks().isMarkListed(MarkFactory.newMark("")));
		assertFalse(notice.getFinalInspectionMarks().isMarkListed((Mark)null));
		
	}

	private NoticeInspectionCodes getFinalInspectionCodes() {
    	InspectionCode inspectionCode1 = new InspectionCode(INSPECTION_TYPE.FINAL, "MR", "Description", true, false);
    	InspectionCode inspectionCode2 = new InspectionCode(INSPECTION_TYPE.FINAL, "MH", "Description", true, false);
    	SeverityCode severityCode1 = new SeverityCode("A2"); 
    	SeverityCode severityCode2 = new SeverityCode("A1"); 
    	NoticeInspectionCode code1 = new NoticeInspectionCode(inspectionCode1, severityCode1);
    	NoticeInspectionCode code2 = new NoticeInspectionCode(inspectionCode1, severityCode2);
    	NoticeInspectionCode code3 = new NoticeInspectionCode(inspectionCode2, severityCode1);
    	NoticeInspectionCode code4 = new NoticeInspectionCode(inspectionCode2, severityCode2);
    	List<NoticeInspectionCode> codes = new ArrayList<>();
    	codes.add(code1);
    	codes.add(code2);
    	codes.add(code3);
    	codes.add(code4);
        return new NoticeInspectionCodes(codes);
	}

	// Note:  What we marshall is not exactly the same as what is unmarshalled from the UI
	@Test  
	public void testMarshall() throws IOException {
		Date effectiveDate = new Date();

		Notice original = new Notice();
		original.setCategory(new Category("MS", "MS Description"));
		original.setNumber(5);
		original.setEffectiveDate(effectiveDate);
		original.setFileNumber("AC-100");
		original.setTitle("Title");
		original.setSupplementNumber(3);
		original.setInspectionCodeMC(false);
		original.setAssignmentReporterInd(ASSIGNMENT_REPORTER_INDICATOR.AAR_ONLY);
		original.setFinalInspectionInd(FINAL_INSPECTION_INDICATOR.OPEN);
		original.setMechanicalDesignationInd(MECHANICAL_DESIGNATION_INDICATOR.OPEN);
		original.setFinalInspectionCodes(this.getFinalInspectionCodes());
		original.setComponentRegistryNotice(false);
		original.setLifetimeNumberEquipmentAssigned(5L);
		original.setEquipmentNumberCurrentlyAssigned(15L);
		original.setStatus(NOTICE_STATUS.DRAFT);
		
		NoticeContent content = new NoticeContent();
		content.setFileName("Notice 0005 filename.pdf");
		content.setFileSize(1234);
		content.setFileExtension("pdf");
		content.setMd5("ALONGMD5STRING");
		original.setContent(content);

		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(original);
		System.out.println(JSON);
		
		assertTrue(JSON.contains("code\" : \"" + original.getCategory().getCode() +"\""));
		assertTrue(JSON.contains("number\" : " + original.getNumber()));
		assertTrue(JSON.contains("fileNumber\" : \"" + original.getFileNumber() +"\""));
		assertTrue(JSON.contains("title\" : \"" + original.getTitle() +"\""));
		assertTrue(JSON.contains("supplementNumber\" : " + original.getSupplementNumber().toString()));
		assertTrue(JSON.contains("code\" : \"" + original.getAssignmentReporterInd().code +"\""));
		assertTrue(JSON.contains("value\" : \"" + original.getAssignmentReporterInd().value +"\""));
		assertTrue(JSON.contains("description\" : \"" + original.getAssignmentReporterInd().description +"\""));
		assertTrue(JSON.contains("code\" : \"" + original.getFinalInspectionInd().code +"\""));
		assertTrue(JSON.contains("value\" : \"" + original.getFinalInspectionInd().value +"\""));
		assertTrue(JSON.contains("description\" : \"" + original.getFinalInspectionInd().description +"\""));
		assertTrue(JSON.contains("code\" : \"" + original.getMechanicalDesignationInd().code +"\""));
		assertTrue(JSON.contains("value\" : \"" + original.getMechanicalDesignationInd().value +"\""));
		assertTrue(JSON.contains("description\" : \"" + original.getMechanicalDesignationInd().description +"\""));
		assertTrue(JSON.contains("componentRegistryNotice\" : " + (original.isComponentRegistryNotice() ? "true" : "false")));
		assertTrue(JSON.contains("lifeTimeNumberEquipmentAssigned\" : " + Long.toString(original.getLifeTimeNumberEquipmentAssigned())));
		assertTrue(JSON.contains("equipmentNumberCurrentlyAssigned\" : " + Long.toString(original.getEquipmentNumberCurrentlyAssigned())));
		assertTrue(JSON.contains("status\" : \"" + original.getStatus() +"\""));
		assertTrue(JSON.contains("pdfFilename\" : \"" + content.getFileName() +"\""));
		assertTrue(JSON.contains("inspectionCodeMC\" : " + (original.isInspectionCodeMC() ? "true" : "false")));
	}

	@Test
	public void testUnmarshall() throws IOException {
		String JSON = "{\"title\":\"RST: Test Create Notice Severity Levels\"\n" + 
				",\"category\":\"MS\"\n" + 
				",\"componentRegistryNotice\":true\n" + 
				",\"effectiveDate\":\"2019-12-30\"\n" + 
				",\"fileNumber\":\"TST.12345\"\n" + 
				",\"assignmentReporterInd\":\"S\"\n" + 
				", \"assignmentReporterMarks\":\"AAR RAIL\"\n" + 
				",\"inspectionCodeMC\":true\n" + 
				",\"inspectionInterval\":3\n" + 
				",\"frequencyUnits\":\"months\"\n" + 
				",\"finalInspectionInd\":\"S\"\n" + 
				", \"finalInspectionMarks\":\"RAIL AAR\"\n" + 
				",\"severityLevels\":[\n" + 
				"{\"severityLevel\":\"XX\"},\n" + 
				"{\"severityLevel\":\"A1\"\n" + 
				", \"escalationLevel\":\"XX\"\n" + 
				", \"escalationPeriod\" : 6\n" + 
				", \"escalationPeriodUnit\": \"months\"},\n" + 
				"{\"severityLevel\":\"A2\"\n" + 
				", \"escalationLevel\":\"A1\"\n" + 
				", \"escalationPeriod\": 1\n" + 
				", \"escalationPeriodUnit\":\"years\"}\n" + 
				"]\n" + 
				",\"finalInspectionCodes\":[\"MR\",\"MH\"]\n" + 
				",\"mechanicalDesignations\":\"D T\"\n" + 
				"} \n";
		
		System.out.println(JSON);
		ObjectMapper objectMapper = new ObjectMapper();	
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Marks.class, new MarksDeserializer());
		module.addDeserializer(MechanicalDesignations.class, new MechanicalDesignationsDeserializer());
		module.addDeserializer(SeverityLevel.class, new SeverityLevelDeserializer());
		module.addDeserializer(SeverityLevels.class, new SeverityLevelsDeserializer());
		objectMapper.registerModule(module);
		Notice result = objectMapper.readValue(JSON, Notice.class);
		
		assertEquals(MECHANICAL_DESIGNATION_INDICATOR.SPECIFIED, result.getMechanicalDesignationInd());
		assertEquals("D, T", result.getMechanicalDesignationsString());		
	}
	
	@Test(expected=Test.None.class)
	public void testHashCode() throws IOException {
		Notice notice = new Notice();
		Notice notice_2 = new Notice();
		assertEquals(notice.hashCode(), notice_2.hashCode());
		assertEquals(notice_2.hashCode(), notice.hashCode());
		
		// Test ID transitive
		notice.setId(1L);
		notice_2.setId(1L);
		assertEquals(notice.hashCode(), notice_2.hashCode());
		assertEquals(notice_2.hashCode(), notice.hashCode());		
		notice_2.setId(2L);
		assertNotEquals(notice.hashCode(), notice_2.hashCode());
		assertNotEquals(notice_2.hashCode(), notice.hashCode());		
		notice_2.setId(1L);
		
		// Test number transitive
		notice.setNumber(1000);
		notice_2.setNumber(1000);
		assertEquals(notice.hashCode(), notice_2.hashCode());
		assertEquals(notice_2.hashCode(), notice.hashCode());		
		notice_2.setNumber(2000);
		assertNotEquals(notice.hashCode(), notice_2.hashCode());
		assertNotEquals(notice_2.hashCode(), notice.hashCode());
		notice_2.setNumber(1000);
		
		// Test supplement number transitive
		notice.setSupplementNumber(5);
		notice_2.setSupplementNumber(5);
		assertEquals(notice.hashCode(), notice_2.hashCode());
		assertEquals(notice_2.hashCode(), notice.hashCode());		
		notice_2.setSupplementNumber(3);
		assertNotEquals(notice.hashCode(), notice_2.hashCode());
		assertNotEquals(notice_2.hashCode(), notice.hashCode());
		notice_2.setSupplementNumber(5);
		
		// Test status transitive
		notice.setStatus(NOTICE_STATUS.DRAFT);
		notice_2.setStatus(NOTICE_STATUS.DRAFT);
		assertEquals(notice.hashCode(), notice_2.hashCode());
		assertEquals(notice_2.hashCode(), notice.hashCode());		
		notice_2.setStatus(NOTICE_STATUS.PUBLISHED);
		assertNotEquals(notice.hashCode(), notice_2.hashCode());
		assertNotEquals(notice_2.hashCode(), notice.hashCode());
		notice_2.setStatus(NOTICE_STATUS.ARCHIVED);
		assertNotEquals(notice.hashCode(), notice_2.hashCode());
		assertNotEquals(notice_2.hashCode(), notice.hashCode());
	}
	
}
