package com.railinc.assethealth.eqadv.domain.pdf;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class PDFValidatorTest {
	
	private static final byte[] PDF_VALID_HEADER = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x33};  // "%PDF-1.3"
	private static final byte[] PDF_INVALID_HEADER = {0X25, 0X50, 0X44, 0X46, 0X2d, 0x31, 0x2E, 0x1E};  // "Invalid"
	private static final byte[] PDF_VALID_TERMINATOR = {0x25, 0x25, 0x45, 0x4f, 0x46};    // "%%EOF" 
	private static final byte[] PDF_INVALID_TERMINATOR = {0x25, 0x25, 0x45, 0x4f, 0x16};    // "Invalid" 
	private byte[] input;
	
	@Before
	public void setup() throws IOException {
		input = java.nio.file.Files.readAllBytes(Paths.get("src/test/resources/EW_Template.pdf"));
	}
	
	@Test(expected=Test.None.class)
	public void getVersion() {
		String version = PDFValidator.getPDFVersion(input);
		assertNotNull(version);
	}

	@Test(expected=Test.None.class)
	public void testInvalidVersion() throws IOException {
		byte[] file = java.nio.file.Files.readAllBytes(Paths.get("src/test/resources/InvalidPDFFileVersion.pdf"));
		boolean result = PDFValidator.isValidPDF(file);
		assertFalse(result);
	}
	
	@Test(expected=Test.None.class)
	public void testInvalidTerminator() throws IOException {
		byte[] file = java.nio.file.Files.readAllBytes(Paths.get("src/test/resources/InvalidPDFFileTerminator.pdf"));
		boolean result = PDFValidator.isValidPDF(file);
		assertFalse(result);
	}
	
	@Test(expected=Test.None.class)
	public void testInvalidPDF() throws IOException {
		byte[] file = java.nio.file.Files.readAllBytes(Paths.get("src/test/resources/InvalidPDF.pdf"));
		boolean result = PDFValidator.isValidPDF(file);
		assertFalse(result);
	}
	
	
	@Test(expected=Test.None.class)
	public void isValidPDF() {
		boolean result = PDFValidator.isValidPDF(input);
		assertTrue(result);
	}	
	
	@Test(expected=Test.None.class)
	public void testinvalidPDFTerminator() {
		byte[] invalidTerminator = new byte[PDF_VALID_HEADER.length + PDF_INVALID_TERMINATOR.length];
		System.arraycopy(PDF_VALID_HEADER, 0, input, 0, PDF_VALID_HEADER.length);
		System.arraycopy(PDF_INVALID_TERMINATOR, 0, input, PDF_VALID_HEADER.length, PDF_INVALID_TERMINATOR.length);
		boolean result = PDFValidator.isValidPDF(invalidTerminator);
		assertFalse(result);
	}	
	
	@Test(expected=Test.None.class)
	public void testInvalidPDFHeader() {
		byte[] invalidHeader = new byte[PDF_INVALID_HEADER.length + PDF_VALID_TERMINATOR.length];
		System.arraycopy(PDF_INVALID_HEADER, 0, input, 0, PDF_INVALID_HEADER.length);
		System.arraycopy(PDF_VALID_TERMINATOR, 0, input, PDF_INVALID_HEADER.length, PDF_VALID_TERMINATOR.length);
		boolean result = PDFValidator.isValidPDF(invalidHeader);
		assertFalse(result);
	}	

	@Test(expected=Test.None.class)
	public void testInvalidPDFHeaderAndTerminator() {
		byte[] invalid = new byte[PDF_INVALID_HEADER.length + PDF_INVALID_TERMINATOR.length];
		System.arraycopy(PDF_INVALID_HEADER, 0, input, 0, PDF_INVALID_HEADER.length);
		System.arraycopy(PDF_INVALID_TERMINATOR, 0, input, PDF_INVALID_HEADER.length, PDF_INVALID_TERMINATOR.length);
		boolean result = PDFValidator.isValidPDF(invalid);
		assertFalse(result);
	}	
	
	
	@Test(expected=UnsupportedOperationException.class)
	public void testInvalidOperation() {
		new PDFValidator();
	}	
}
