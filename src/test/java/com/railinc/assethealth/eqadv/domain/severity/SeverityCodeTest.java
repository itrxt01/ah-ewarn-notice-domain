package com.railinc.assethealth.eqadv.domain.severity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.railinc.assethealth.eqadv.domain.exception.InvalidSeverityCodeException;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;

public class SeverityCodeTest {
	
	private static final List<SeverityCode> severityCodes = new ArrayList<>();
	
	@Before
	public void init() {
		severityCodes.add(new SeverityCode("XX", "Prohibited in interchange"));
		severityCodes.add(new SeverityCode("A1", "Condemnable at anytime"));
		severityCodes.add(new SeverityCode("A2", "Condemnable when on repair track or in shop"));
	}
	
	private SeverityCode getCode(String code) {
		return severityCodes.stream().filter(c -> c.getSeverityCode().equals(code)).findFirst().orElse(null);
	}
	
	@Test
	public void testHappyPath() throws InvalidSeverityCodeException {
		assertEquals(new SeverityCode("XX", "Anything"), getCode("XX"));
		assertEquals(new SeverityCode("A1", "Anything"), getCode("A1"));
		assertEquals(new SeverityCode("A2", "Anything"), getCode("A2"));
		assertEquals(new SeverityCode("XX"), getCode("XX"));
		assertEquals(new SeverityCode("A1"), getCode("A1"));
		assertEquals(new SeverityCode("A2"), getCode("A2"));
	}

	public void testHashCode() {
		SeverityCode code = new SeverityCode("XX", "Anything");
		SeverityCode code2 = new SeverityCode("XX", "Anything");
		assertTrue(code.hashCode() == code2.hashCode());
		code = new SeverityCode("XX", "Anything");
		code2 = new SeverityCode("A2", "Anything");
		assertTrue(code.hashCode() != code2.hashCode());
	}
	
	
	@Test
	public void testDescription() throws InvalidSeverityCodeException {
		SeverityCode code = new SeverityCode("XX", "Anything");
		assertEquals("Anything", code.getDescription());
		code = new SeverityCode("XX", "");
		assertEquals("", code.getDescription());
		code = new SeverityCode("XX", null);
		assertEquals(null, code.getDescription());
	}
	
	@Test
	public void testToString() {
		SeverityCode code = new SeverityCode("XX", "Anything");
		String s = code.toString();
		assertTrue(s.contains("Anything"));
		assertTrue(s.contains("XX"));
	}

	@Test
	public void testInvalidCodes() throws InvalidSeverityCodeException {
		assertNotEquals(new SeverityCode("A1", "Anything"), getCode("XX"));
		assertNotEquals(new SeverityCode("A1", "Anything"), getCode("A2"));
		assertNotEquals(new SeverityCode("A2", "Anything"), getCode("XX"));
		assertNotEquals(new SeverityCode("A2", "Anything"), getCode("A1"));
		assertNotEquals(new SeverityCode("XX", "Anything"), getCode("A1"));
		assertNotEquals(new SeverityCode("XX", "Anything"), getCode("A2"));
	}
}
