package com.railinc.assethealth.eqadv.domain.severity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.railinc.assethealth.eqadv.domain.PERIODIC_TIME_UNIT;
import com.railinc.assethealth.eqadv.domain.TimePeriod;
import com.railinc.assethealth.eqadv.domain.exception.InvalidPeriodicTimeUnitException;
import com.railinc.assethealth.eqadv.domain.exception.SeverityLevelEscalationException;
import com.railinc.assethealth.eqadv.domain.severity.SeverityCode;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevel;
import com.railinc.assethealth.eqadv.domain.severity.SeverityLevelDeserializer;

public class SeverityLevelTest {

	@Test
	public void testZeroArgCtor() throws InvalidPeriodicTimeUnitException {
		SeverityLevel level = new SeverityLevel();
		level.setSeverityLevel(new SeverityCode("AA"));
		level.setEscalationLevel(new SeverityLevel("BB"));
		level.setEscalationPeriodValue(2);
		level.setEscalationPeriodUnit("MONTHS");
		level.setEscalationType("D");		
		level.setQty(102);
		assertTrue(level.isEscalates());
		assertEquals("AA", level.getSeverityLevelCode());
		assertEquals("BB", level.getEscalationLevelCode());
		assertEquals(2, level.getEscalationPeriodValue());
		assertEquals("Months", level.getEscalationPeriodUnit());
		assertEquals(102, level.getQty());
	}
	
	@Test
	public void testSevCodeCtor() throws InvalidPeriodicTimeUnitException {
		SeverityLevel level = new SeverityLevel(new SeverityCode("BB", "Description of some kind"));
		level.setQty(111222);
		assertFalse(level.isEscalates());
		assertEquals("BB", level.getSeverityLevelCode());
		assertEquals("Description of some kind", level.getSeverityLevelDesc());
		assertEquals(111222, level.getQty());
	}
	
	@Test
	public void testSevCodeDescCtor() throws InvalidPeriodicTimeUnitException {
		SeverityLevel level = new SeverityLevel("BB", "Description of some kind");
		level.setQty(111222);
		assertFalse(level.isEscalates());
		assertEquals("BB", level.getSeverityLevelCode());
		assertEquals("Description of some kind", level.getSeverityLevelDesc());
	}
	
	@Test
	public void testGetEscalationDate() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		Date specificDate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01");
		Date testDate = DateUtils.addDays(specificDate, 180);
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		assertEquals("A1", level.getEscalationLevelCode());
		assertEquals(testDate, level.getEscalationDate(specificDate));
	}

	@Test(expected=Test.None.class)
	public void testGetEscalationDateLEVEL() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		Date specificDate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01");
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationLevel(null);
		assertNull(level.getEscalationDate(specificDate));
	}
	
	@Test
	public void testGetEscalationDateBLANK_LEVEL() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		SeverityLevel level = new SeverityLevel("A2", "  ", 180, "days");
		assertNull(level.getEscalationLevelCode());
	}
	
	@Test(expected=Test.None.class)
	public void testSetEscalationDatePERIOD() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		Date specificDate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01");
		Date testDate = DateUtils.addDays(specificDate, 5);
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationPeriodValue(5);
		assertEquals(testDate, level.getEscalationDate(specificDate));
	}
	
	@Test(expected=Test.None.class)
	public void testGetEscalationDatePERIOD() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		Date specificDate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01");
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationPeriod(null);
		assertNull(level.getEscalationDate(specificDate));
	}
	
	@Test
	public void testIsTimeToEscalate() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		Date specificDate = new SimpleDateFormat("yyyy-MM-dd").parse("2017-01-01");
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		assertEquals("A1", level.getEscalationLevelCode());
		assertTrue(level.isTimeToEscalate(specificDate));
	}

	@Test(expected=Test.None.class)
	public void testIsTimeToEscalateLEVEL() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		Date specificDate = new SimpleDateFormat("yyyy-MM-dd").parse("2017-01-01");
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationLevel(null);
		assertFalse(level.isTimeToEscalate(specificDate));
	}
	
	@Test(expected=Test.None.class)
	public void testIsTimeToEscalatePERIOD() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		Date specificDate = new SimpleDateFormat("yyyy-MM-dd").parse("2017-01-01");
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationPeriod(null);
		assertFalse(level.isTimeToEscalate(specificDate));
	}

	@Test
	public void testEscalationPeriodValueINVALID() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationPeriod(null);
		assertEquals(0, level.getEscalationPeriodValue());
	}
	
	@Test
	public void testEscalationLevelINVALID() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationLevel(null);
		assertEquals(0, level.getEscalationPeriodValue());
	}

	@Test
	public void testEscalationPeriodValueUNIT_INVALID() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationPeriod(null);
		assertNull(level.getEscalationPeriodUnit());
	}
	
	@Test
	public void testEscalationLevelUNIT_INVALID() throws InvalidPeriodicTimeUnitException, ParseException, SeverityLevelEscalationException {
		SeverityLevel level = new SeverityLevel("A2", "A1", 180, "days");
		level.setEscalationLevel(null);
		assertNull(level.getEscalationPeriodUnit());
	}
	
	
	@Test
	public void testGetEscalationLevel() throws InvalidPeriodicTimeUnitException {
		SeverityCode code = new SeverityCode("A2");
		SeverityLevel escLevel = new SeverityLevel("A1");
		TimePeriod escPeriod = new TimePeriod(180, "days");
		SeverityLevel level = new SeverityLevel(code, escLevel, escPeriod);
	
		assertEquals(code, level.getSeverityCode());
		assertEquals(code.getSeverityCode(), level.getSeverityLevelCode());
		assertEquals(escLevel, level.getEscalationLevel());
		assertEquals(escLevel.getSeverityCode().getSeverityCode(), level.getEscalationLevelCode());		
		assertEquals(escPeriod, level.getEscalationPeriod());
		assertEquals(escPeriod.getTimeUnit().units, level.getEscalationPeriodUnit());
		assertEquals(escPeriod.getTimePeriod(), level.getEscalationPeriodValue());
	}

	@Test
	public void testSetEscalationPeriod() throws InvalidPeriodicTimeUnitException {
		SeverityCode code = new SeverityCode("A2");
		SeverityLevel level = new SeverityLevel(code);
		level.setEscalationPeriodUnit("days");
		level.setEscalationLevel(new SeverityLevel(new SeverityCode("A3")));
		assertEquals(PERIODIC_TIME_UNIT.DAYS.units, level.getEscalationPeriodUnit());
	}

	@Test
	public void testCompareTo() throws InvalidPeriodicTimeUnitException {
		SeverityLevel level1 = new SeverityLevel("XX");
		SeverityLevel level2 = new SeverityLevel("A1", "XX", 1, "days");
		level2.setEscalationType("D");
		SeverityLevel level3 = new SeverityLevel("A2", "A1", 1, "days");
		level3.setEscalationType("D");
		SeverityLevel level4 = new SeverityLevel("XY");
		SeverityLevel level5 = new SeverityLevel("A1", "XX", 1, "days");
		level5.setEscalationType("D");
		SeverityLevel level6 = new SeverityLevel("A4", "XX", 1, "days");
		level6.setEscalationType("D");
		
		assertTrue(1 == level1.compareTo(level2) && -1 == level2.compareTo(level1));
		assertTrue(1 == level2.compareTo(level3) && -1 == level3.compareTo(level2));
		
		assertTrue(-1 == level2.compareTo(level4));
		assertTrue(-1 == level3.compareTo(level1) && 1 == level1.compareTo(level3));
		assertTrue(-1 == level3.compareTo(level2) && 1 == level2.compareTo(level3));
		assertTrue(-1 == level3.compareTo(level4) && 1 == level4.compareTo(level3));
		
		assertTrue(0 == level1.compareTo(level4) && 0 == level4.compareTo(level1)); 
		assertTrue(0 == level5.compareTo(level2) && 0 == level2.compareTo(level5)); 
		assertTrue(0 == level2.compareTo(level6) && 0 == level6.compareTo(level2));  // Escalate to same level
		
	}

	@Test
	public void testToString() throws InvalidPeriodicTimeUnitException {
		SeverityLevel level1 = new SeverityLevel("XX");
		assertTrue(StringUtils.isNoneBlank(level1.toString()));
		assertTrue(level1.toString().contains("XX"));
		
		SeverityLevel level2 = new SeverityLevel("A1", "XX", 1, "days");
		assertTrue(StringUtils.isNoneBlank(level2.toString()));
		assertTrue(level2.toString().contains("A1"));
		assertTrue(level2.toString().contains("XX"));
	}
	
	@Test
	public void testHashCode() throws InvalidPeriodicTimeUnitException {
		SeverityLevel level1 = new SeverityLevel("XX");
		SeverityLevel level2 = new SeverityLevel("A1", "XX", 1, "days");
		SeverityLevel level3 = new SeverityLevel("A2", "A1", 1, "days");
		SeverityLevel level4 = new SeverityLevel("XX");
		SeverityLevel level5 = new SeverityLevel("A1", "XX", 1, "days");
		
		assertTrue(level1.hashCode() == level4.hashCode());
		assertTrue(level2.hashCode() == level5.hashCode());
		assertTrue(level3.hashCode() != level1.hashCode());
	}
	
	@Test
	public void testMarshallUnmarshall() throws IOException {
		SeverityLevel original = new SeverityLevel("A1", "XX", 6, "months");
		original.setEscalationType("D");
		original.setQty(102);

		ObjectMapper objectMapper = new ObjectMapper();

		String JSON = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(original);
		System.out.println(JSON);
		
		SimpleModule module = new SimpleModule();
		module.addDeserializer(SeverityLevel.class, new SeverityLevelDeserializer());		
		SeverityLevel result = objectMapper.readValue(JSON, SeverityLevel.class);
		assertTrue(result.isEscalates());
		assertEquals("A1", result.getSeverityLevelCode());
		assertEquals("XX", result.getEscalationLevelCode());
		assertEquals(6, result.getEscalationPeriodValue());
		assertEquals("Months", result.getEscalationPeriodUnit());
		assertEquals(102, result.getQty());
	}
		
	@Test
	public void testUnmarshall() throws IOException {
		String JSON = "{\n" + 
				"  \"severityLevel\" : \"A1\",\n" + 
				"  \"qty\" : 102,\n" + 
				"  \"description\" : null,\n" + 
				"  \"escalationLevel\" : \"XX\",\n" + 
				"  \"escalationPeriod\" : 6,\n" + 
				"  \"escalationPeriodUnit\" : \"Months\",\n" + 
				"  \"escalationType\" : \"D\"\n" + 				
				"} \n";
		
		System.out.println(JSON);
		ObjectMapper objectMapper = new ObjectMapper();	
		SimpleModule module = new SimpleModule();
		module.addDeserializer(SeverityLevel.class, new SeverityLevelDeserializer());
		
		SeverityLevel result = objectMapper.readValue(JSON, SeverityLevel.class);
		
		assertTrue(result.isEscalates());
		assertEquals("A1", result.getSeverityLevelCode());
		assertEquals("XX", result.getEscalationLevelCode());
		assertEquals(6, result.getEscalationPeriodValue());
		assertEquals("Months", result.getEscalationPeriodUnit());
		assertEquals(102, result.getQty());
	}

	@Test
	public void testUnmarshall_DOES_NOT_ESCALATE() throws IOException {
		String JSON = "{\n" + 
				"  \"severityLevel\" : \"XX\",\n" + 
				"  \"qty\" : 10284,\n" + 
				"  \"description\" : \"Do NOT Interchange\"\n" + 
				"} \n";
		
		System.out.println(JSON);
		ObjectMapper objectMapper = new ObjectMapper();	
		SimpleModule module = new SimpleModule();
		module.addDeserializer(SeverityLevel.class, new SeverityLevelDeserializer());
		
		SeverityLevel result = objectMapper.readValue(JSON, SeverityLevel.class);
		
		assertEquals("XX", result.getSeverityLevelCode());
		assertFalse(result.isEscalates());
		assertEquals(10284, result.getQty());
	}
}
